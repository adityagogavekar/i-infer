<?php 
include('header.php');
 ?>
 <style type="text/css">
     btn:focus {
    border: 4px solid red;
    background-color: black;
}
* {box-sizing: border-box;}

.img-zoom-container {
  position: relative;
}

.img-zoom-lens {
  position: absolute;
  border: 1px solid #d4d4d4;
  /*set the size of the lens:*/
  width: 40px;
  height: 40px;
}

.img-zoom-result {
  border: 1px solid #d4d4d4;
  /*set the size of the result div:*/
  width: 300px;
  height: 300px;
}
tbody tr:nth-child(odd){
  background-color: #f7f7f7 !important;
  color:#564747;

}
.switch {
  position: relative;
  display: inline-block;
  width: 51px;
  height: 22px;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 17px;
  width: 17px;
  left: 5px;
  bottom: 3px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #377581;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

.fixedHeader-floating {
    overflow: hidden;
}


 </style>
  <div id="page_content">
        <div id="page_content_inner">

         
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                           
                            <tr>
                                 
                                <th>StoryName</th>
                                 <th>Reported By</th>
                                <th>UnPublish</th>
                                <th>Reported On</th>
                               
                               
                            </tr>
                        </thead>
                       
                        <tbody>
                        
                        </tbody>
                    </table>
                   
                     
              
                </div>
            </div>
            </div>
             
</div>
      <p class="uk-text-large">Header/Footer</p>
                            <button class="md-btn" id="reportedUsers" data-uk-modal="{target:'#modal_header_footer'}">Open</button>
                            <div class="uk-modal" id="modal_header_footer">
                                <div class="uk-modal-dialog">
                                    <div class="uk-modal-header">
                                        <h3 class="uk-modal-title">Reported By <i class="material-icons" data-uk-tooltip="{pos:'top'}" title="headline tooltip">&#xE8FD;</i></h3>
                                    </div>
                                   <div id="userDetails"></div>
                                    <div class="uk-modal-footer uk-text-right">
                                        <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                    </div>
                                </div>
                            </div>
                       
<script type="text/javascript">      
$(document).ready(function() {
     $('#Reported').addClass("current_section");
       var a = $(window).height(); // screen height
        var b = 350;
        var pageHeight =a-b;
        if(pageHeight<200){
          pageHeight=600;
        }
  
    //datatables
    table = $('#dt_default').DataTable({ 
    
        "scrollY": pageHeight,
        "scrollCollapse": true,
        "scrollX":true,
        "bDestroy": true,
        "processing": true,
        "serverSide": true,
        "iDisplayLength": 100,
        "lengthMenu": [[10, 25, 50, 100, 200, 500], [10, 25, 50, 100, 200, 500]],
        //"fixedHeader": true,
        "ajax": {

                "url": "<?php echo base_url(); ?>Home/viewReportedStories",
                "type": "POST",
                
                
               },
   "columnDefs": [
                            { 
                                "targets": [ -2 ], //last column*/
                                "orderable": false, //set not orderable
                            },
                          ],
               
   });
   

        
});
function reportStory(storyID)
{
  var isActive = 0;
  
  if($('#isReported').is(":checked"))
  {
    isActive = 1; 
  }
  $('#notification').click();
   $.ajax({
        url: '<?php echo base_url('Home/reportStory');?>',
        type: 'POST',
        data: 'storyID='+storyID+'&isActive='+isActive,
        success: function(result){
          var obj = JSON.parse(result);
          var status = obj.status;
           if(status==1 && isActive==1){
        setTimeout(function () {
            $("#notificationSuccess").attr("data-message", "Story Published successfully.");
            $("#notificationSuccess").click();
               }, 1000);
        
         //window.location.reload();
      }
      else if(status==1 && isActive==0){
        setTimeout(function () {
         
            $("#notificationFailure").attr("data-message", "Story UnPublished successfully.");
            $("#notificationFailure").click();
               }, 1000);
        
      }
     else{
        setTimeout(function () {
            $("#notificationFailure").attr("data-message", "Error while featuring Story");
            $("#notificationFailure").click();
               }, 1000);
     }
        }
        });
  
  
}
 function readUsers(storyID)
{
  //alert(storyID);
  $.ajax({
        url: '<?php echo base_url('Home/getReportedUsers');?>',
        type: 'POST',
        data: 'storyID='+storyID,
        success: function(result){
          var obj = JSON.parse(result);
        var data = obj.users;
        console.log(data);
        var length = data.length;
        $('#userDetails').html('');
        for(i=0;i<length;i++)
        {
          var userName = data[i].userName;
          $('#userDetails').append('<p>'+userName+'</p>');
        }
        
          $('#reportedUsers').click();
        }
        });
  }
     
</script>
<?php 
    include('footer.php');
    ?>