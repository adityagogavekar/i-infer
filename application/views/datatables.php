<?php
include('header.php');
 ?>
 <style type="text/css">
 <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
     btn:focus {
    border: 4px solid red;
    background-color: black;
}
* {box-sizing: border-box;}

.img-zoom-container {
  position: relative;
}

.img-zoom-lens {
  position: absolute;
  border: 1px solid #d4d4d4;
  /*set the size of the lens:*/
  width: 40px;
  height: 40px;
}

.img-zoom-result {
  border: 1px solid #d4d4d4;
  /*set the size of the result div:*/
  width: 300px;
  height: 300px;
}
tbody tr:nth-child(odd){
  background-color: #f7f7f7 !important;
  color:#564747;

}
.switch {
  position: relative;
  display: inline-block;
  width: 51px;
  height: 22px;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 17px;
  width: 17px;
  left: 5px;
  bottom: 3px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #377581;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

.fixedHeader-floating {
    overflow: hidden;
}
thead
{
  background-color: black;
}
th
{
  color:#FFF;
}
::-webkit-scrollbar {
  width: 8px;
  height: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  box-shadow: inset 0 0 1px grey;
  background: #fbfbfb;
}

/* Handle */
::-webkit-scrollbar-thumb {
  background: #A9A9A9;
  border-radius: 4px;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #808080;
}
 </style>
 <div id="page_content">
        <div id="page_content_inner">


            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                <div class="uk-grid">
                  <div class="uk-width-5-10">
                    <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                        <thead>

                            <tr>

                                <th style="width: 10%;">Thumbnail</th>
                                 <th>UserName</th>
                                <th>Story Title</th>
                                <th>Featured</th>
                                <th>Created</th>


                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>
                      <div class="uk-width-5-10" style="padding: 0px; " >

                        <div id="mainDiv" class="md-card md-card-hover md-card-overlay">
                          <div class="uk-grid">

                        <div class="uk-width-5-10">
                          <div class="uk-container" style="padding-top:5%;">
                          <h3 id="captions" style="margin:0px;"></h3>
                        </div>
                      </div>
                      <div class="uk-width-5-10">
                      <div id="main" data-uk-button-radio style="float:right;margin-right:10%;"></div>
                    </div>
                      </div>
                        <div class="uk-grid">
                        <div class="uk-width-5-10" style="text-align:center;">
                           <img id = "display" class=""  style="object-fit: contain;display: none;max-width:60%;" src="">
                      </div>




                      <div class="uk-width-5-10">
                        <video id="video" style="display: none;object-fit:contain;height:300px;" controls controlsList="nodownload">
                        <source id="display1" src="" type="video/mp4">
                        </video>
                      </div>
                    </div>

              </div>
                      <div id="script" style="display:none;text-align:center;overflow-y:auto;height:250px;">

                      </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
             <div class="uk-width-medium-1-3">
                      <a type="hidden" data-uk-modal="{target:'#modal_venueImages'}" id="venueImageModal"></a>
                            <div class="uk-modal" id="modal_venueImages">
                                   <div class="uk-modal-dialog  uk-margin-auto-vertical">
                                <div class="uk-text-center">
                                  <img  id="img01" src="" >
                                </div>
                                   </div>


                            </div>

                        </div>
</div>

<!-- modal -->
<div class="uk-width-medium-1-3">
        <button class="md-btn" data-uk-modal="{target:'#modal_default'}" id="openButton" style="display: none;">Open</button>
         <div class="uk-modal" id="modal_default">
         <div class="uk-modal-dialog">
                            <div class="uk-slidenav-position" data-uk-slideshow="{animation:'fade'}">
                                <ul class="uk-slideshow" id="modalUl">

                                </ul>
                                <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
                                <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
                            </div>
        </div>
        </div>
</div>
<script type="text/javascript">
$(document).ready(function() {

     $('#Stories').addClass("current_section");
       var a = $(window).height(); // screen height
        var b = 220;
        var pageHeight =a-b;
        if(pageHeight<200){
          pageHeight=600;
        }
        var string = ' <?php if(isset($string)){ echo json_encode($string);} else{echo "";} ?>'
        var heightNew = pageHeight/2 + 100;
        var heightScript = pageHeight/2;
        $('#mainDiv').css({"height":heightNew});
        $('#script').css({"height":heightScript});

    //datatables
    table = $('#dt_default').DataTable({

        "scrollY": pageHeight,
        "scrollCollapse": true,
        "scrollX":true,
        "bDestroy": true,
        "processing": true,
        "serverSide": true,
        "iDisplayLength": 100,
        "lengthMenu": [[10, 25, 50, 100, 200, 500], [10, 25, 50, 100, 200, 500]],
        "ajax": {

                "url": "<?php echo base_url(); ?>Home/viewStory",
                "type": "POST",
                 "data": function ( d ) {
                  d.string=string;
                 }

               },
       "columnDefs": [
                            {
                                "targets": [ 0 ], //last column*/
                                "orderable": false, //set not orderable
                            },
                          ],

   });
    $('#display').click(function () {
            var src = ($(this).attr('src'));
             var id = ($(this).attr('class'));
            modalImages(src,id);
        });


});
function getImage(src,id)
{
    $('#display').show();
    $('#display').attr("src",src);
    $('#display').attr("class",id);
  }
function getVideo(src)
{
  if(src!="")
  {
  $('#video').show();
  $('#display').show();
  $('#display1').attr('src', src);
  $('#video').get(0).load();
  $('#video').get(0).play();
}

}
function getCaptions(captions)
{
  $('#captions').text(captions);
}
function getScript(id)
{

  $('#script'+id).show();


}
function getPages(storyID)
{
    $('#video').hide();
    $('#script').hide();
    $('#display').hide();
    $.ajax({
        url: '<?php echo base_url('Home/getPages');?>',
        type: 'POST',
        data: 'storyID='+storyID,
        success: function(result){

            var obj = JSON.parse(result);
            console.log(obj);
            var data = obj.data;
            var length = data.length;
            var video0 = data[0].video;
            var script0 = data[0].script;
            var captions0 = data[0].captions;
            getCaptions(captions0);
            getImage(data[0].url,(storyID));
            if(video0!=""){
                getVideo(video0);
            }
            if(script0!="")
            {
              $('#script').show();
              $('#script').html(script);
            }
            $('#main').html('');

            for(i=0;i<length;i++)
            {
                var url = data[i].url;
                var video = data[i].video;
                var captions = data[i].captions;
                var script = data[i].script;
                $('#script').show();
                $('#script').append('<div id="script'+i+'" style="text-align: center;"></div>');

                $('#script'+i).html(script);
                if(i==0)
                {
                $('#main').append(' <a  href="\'' + url + '\'" id='+(i+1)+' data-caption="hey"></a><button class="uk-button uk-button-default uk-active" id='+(i+1)+' style="margin-top:10px;margin-left:5px;" onclick="getImage(\'' + url +  '\','+storyID+'),getVideo(\'' + video +  '\','+storyID+'),getCaptions(\'' + captions +  '\'),getScript(\'' + i +  '\')">'+(i+1)+'</button>');
                }
                else {
                  $('#main').append(' <a  href="\'' + url + '\'" id='+(i+1)+' data-caption="hey"></a><button class="uk-button uk-button-default" id='+(i+1)+' style="margin-top:10px;margin-left:5px;" onclick="getImage(\'' + url +  '\','+storyID+'),getVideo(\'' + video +  '\','+storyID+'),getCaptions(\'' + captions +  '\'),getScript(\'' + i +  '\')">'+(i+1)+'</button>');
                }
            }
        }
    });
}
function modalImages(src,id)
{
  $.ajax({
    url: '<?php echo base_url('Home/getPages');?>',
    type: 'POST',
    data: 'storyID='+id,
    beforeSend: function () {
    $('#loader').show();

        },
    complete: function(){
    $('#loader').hide();
    },
    success: function(result){
    var obj =JSON.parse(result);
    var data = obj.data;
    $('#modalUl').empty();


    for(i=0;i<data.length;i++){

      if(src==data[i].url)
      {
          $('#modalUl').append('<li id="imageActive"><img src="'+data[i].url+'" alt=""></li>');
      }
      else {
        $('#modalUl').append('<li id="img'+i+'"><img src="'+data[i].url+'" alt=""></li>');

      }


    }
    $('#openButton').click();
    },
       error: function () {
       alert("error");
      }
});
}
jQuery(document).ready(function($) {
  $('#imageActive').addClass('uk-active');

});
function spotlightSwitch(storyID)
{
  //alert(storyID);
   $.ajax({
        url: '<?php echo base_url('Home/setFeaturedStory');?>',
        type: 'POST',
        data: 'storyID='+storyID,
        success: function(result){
          var obj = JSON.parse(result);
          var status = obj.status;
          var isFeaturedKey = obj.isFeatured;
          //alert(status);
        if(status==1 && isFeaturedKey==1){
        setTimeout(function () {
            $("#notificationSuccess").attr("data-message", "Story Featured successfully.");
            $("#notificationSuccess").click();
               }, 1000);

         //window.location.reload();
      }
      else if(status==1 && isFeaturedKey==0){
        setTimeout(function () {

            $("#notificationFailure").attr("data-message", "Story UnFeatured successfully.");
            $("#notificationFailure").click();
               }, 1000);

      }
     else{
        setTimeout(function () {
            $("#notificationFailure").attr("data-message", "Error while featuring Story");
            $("#notificationFailure").click();
               }, 1000);
     }

        }
        });


}
function imageZoom(imgID, resultID) {

  var img, lens, result, cx, cy;
  img = document.getElementById(imgID);
  result = document.getElementById(resultID);
  /*create lens:*/
  lens = document.createElement("DIV");
  lens.setAttribute("class", "img-zoom-lens");
  /*insert lens:*/
  img.parentElement.insertBefore(lens, img);
  /*calculate the ratio between result DIV and lens:*/
  cx = result.offsetWidth / lens.offsetWidth;
  cy = result.offsetHeight / lens.offsetHeight;
  /*set background properties for the result DIV*/
  result.style.backgroundImage = "url('" + img.src + "')";
  result.style.backgroundSize = (img.width * cx) + "px " + (img.height * cy) + "px";
  /*execute a function when someone moves the cursor over the image, or the lens:*/
  lens.addEventListener("mousemove", moveLens);
  img.addEventListener("mousemove", moveLens);
  /*and also for touch screens:*/
  lens.addEventListener("touchmove", moveLens);
  img.addEventListener("touchmove", moveLens);
  function moveLens(e) {
    var pos, x, y;
    /*prevent any other actions that may occur when moving over the image*/
    e.preventDefault();
    /*get the cursor's x and y positions:*/
    pos = getCursorPos(e);
    /*calculate the position of the lens:*/
    x = pos.x - (lens.offsetWidth / 2);
    y = pos.y - (lens.offsetHeight / 2);
    /*prevent the lens from being positioned outside the image:*/
    if (x > img.width - lens.offsetWidth) {x = img.width - lens.offsetWidth;}
    if (x < 0) {x = 0;}
    if (y > img.height - lens.offsetHeight) {y = img.height - lens.offsetHeight;}
    if (y < 0) {y = 0;}
    /*set the position of the lens:*/
    lens.style.left = x + "px";
    lens.style.top = y + "px";
    /*display what the lens "sees":*/
    result.style.backgroundPosition = "-" + (x * cx) + "px -" + (y * cy) + "px";
  }
  function getCursorPos(e) {
    var a, x = 0, y = 0;
    e = e || window.event;
    /*get the x and y positions of the image:*/
    a = img.getBoundingClientRect();
    /*calculate the cursor's x and y coordinates, relative to the image:*/
    x = e.pageX - a.left;
    y = e.pageY - a.top;
    /*consider any page scrolling:*/
    x = x - window.pageXOffset;
    y = y - window.pageYOffset;
    return {x : x, y : y};
  }
}


</script>
<?php
    include('footer.php');
    ?>
