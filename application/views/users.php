<?php
include('header.php');
 ?>
 <style type="text/css">
     btn:focus {
    border: 4px solid red;
    background-color: black;
}
* {box-sizing: border-box;}

.img-zoom-container {
  position: relative;
}

.img-zoom-lens {
  position: absolute;
  border: 1px solid #d4d4d4;
  /*set the size of the lens:*/
  width: 40px;
  height: 40px;
}

.img-zoom-result {
  border: 1px solid #d4d4d4;
  /*set the size of the result div:*/
  width: 300px;
  height: 300px;
}
tbody tr:nth-child(odd){
  background-color: #f7f7f7 !important;
  color:#564747;

}
.switch {
  position: relative;
  display: inline-block;
  width: 51px;
  height: 22px;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 17px;
  width: 17px;
  left: 5px;
  bottom: 3px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #7cb342;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

.fixedHeader-floating {
    overflow: hidden;
}


 </style>
 <div id="page_content">
        <div id="page_content_inner">

            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                        <thead>

                            <tr>

                                <th>ProfilePic</th>
                                 <th>UserName</th>
                                <th>Email ID</th>
                                <th>Contact No</th>
                                <th>Created</th>


                            </tr>
                        </thead>


                        <tbody>

                        </tbody>
                    </table>



                </div>
            </div>
            </div>

</div>
<script type="text/javascript">
$(document).ready(function() {
     $('#Users').addClass("current_section");
       var a = $(window).height(); // screen height
        var b = 220;
        var pageHeight =a-b;
        if(pageHeight<200){
          pageHeight=600;
        }

    //datatables
    table = $('#dt_default').DataTable({

          "scrollY": pageHeight,
        "scrollCollapse": true,
        "scrollX":true,
        "bDestroy": true,
        "processing": true,
        "serverSide": true,
        "iDisplayLength": 100,
        "lengthMenu": [[10, 25, 50, 100, 200, 500], [10, 25, 50, 100, 200, 500]],
        //"fixedHeader": true,
        "ajax": {

                "url": "<?php echo base_url(); ?>Home/viewUsers",
                "type": "POST",


               },
   "columnDefs": [
                            {
                                "targets": [ 0 ], //last column*/
                                "orderable": false, //set not orderable
                            },
                          ],

   });



});


</script>
<?php
    include('footer.php');
    ?>
