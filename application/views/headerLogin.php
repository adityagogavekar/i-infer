<!DOCTYPE html>
<html>
<head>
<title>Linkcards</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/png"  href="<?php echo base_url(); ?>assets/images/favicon.ico">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/login.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/link.js"></script>
<script src="https://code.jquery.com/jquery-1.12.3.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/linkstyle.css?<?php echo time();?>" />
</head>