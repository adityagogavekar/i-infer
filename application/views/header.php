<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/img/favicon-32x32.png" sizes="32x32">

    <title>Intelligence Informatics</title>

    <!-- additional styles for plugins -->
        <!-- weather icons -->
    <link rel="stylesheet" href="<?php echo base_url();?>bower_components/weather-icons/css/weather-icons.min.css" media="all">
    <!-- metrics graphics (charts) -->
    <link rel="stylesheet" href="<?php echo base_url();?>bower_components/metrics-graphics/dist/metricsgraphics.css">
    <!-- chartist -->
     <link rel="stylesheet" href="<?php echo base_url();?>bower_components/chartist/dist/chartist.min.css">

    <!-- uikit -->
    <link rel="stylesheet" href="<?php echo base_url();?>bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/icons/flags/flags.min.css" media="all">

    <!-- style switcher -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style_switcher.min.css" media="all">

    <!-- altair admin -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.min.css" media="all">

    <!-- themes -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/themes/themes_combined.min.css" media="all">
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" media="all">

    <!-- matchMedia polyfill for testing media queries in JS -->
    <!--[if lte IE 9]>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.js"></script>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.addListener.js"></script>
        <link rel="stylesheet" href="assets/css/ie.css" media="all">
    <![endif]-->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<style type="text/css">
    table.dataTable thead .sorting
    {
        background: none;
    }
    table.dataTable thead .sorting_desc
    {
        background: none;
    }
    table.dataTable thead .sorting_asc
    {
        background: none;
    }
    thead
{
  background-color: black;
}
th
{
  color:#FFF;
}
.uk-button-default.uk-active, .uk-button-default:active {
    background-color: black;
    color: #fff;
}
.uk-pagination>li.uk-active>a, .uk-pagination>li.uk-active>span
{
    background-color: black;
}
#page_content_inner
{
    margin-top: 0px !important;
}

</style>
</head>
<body class="disable_transitions sidebar_slim sidebar_main_swipe">
 <input type="hidden" class="md-btn" data-message="Sucess" data-pos="top-right"  data-status="success" id="notificationSuccess"></input>
    <!-- Notification -->
    <input type="hidden" class="md-btn" data-message="Failure" data-pos="top-right"  data-status="danger" id="notificationFailure"></input>

    <!-- main header -->
    <header id="header_main">
        <div class="header_main_content">
            <nav class="uk-navbar">

                <!-- main sidebar switch -->
                <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                    <span class="sSwitchIcon"></span>
                </a>

                <!-- secondary sidebar switch -->
                <a href="#" id="sidebar_secondary_toggle" class="sSwitch sSwitch_right sidebar_secondary_check">
                    <span class="sSwitchIcon"></span>
                </a>



                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav user_actions">
                        <li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">&#xE5D0;</i></a></li>


                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            <a href="#" class="user_action_image"><img class="md-user-image" src="<?php echo base_url();?>assets/img/avatars/avatar_11_tn.png" alt=""/></a>
                            <div class="uk-dropdown uk-dropdown-small">
                                <ul class="uk-nav js-uk-prevent">
                                    <li><a href="page_user_profile.html">My profile</a></li>
                                    <li><a href="page_settings.html">Settings</a></li>
                                    <li><a href="<?php echo base_url('Home/logout');?>">Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="header_main_search_form">
            <i class="md-icon header_main_search_close material-icons">&#xE5CD;</i>
            <form class="uk-form uk-autocomplete" data-uk-autocomplete="{source:'data/search_data.json'}">
                <input type="text" class="header_main_search_input" />
                <button class="header_main_search_btn uk-button-link"><i class="md-icon material-icons">&#xE8B6;</i></button>
                <script type="text/autocomplete">
                    <ul class="uk-nav uk-nav-autocomplete uk-autocomplete-results">
                        {{~items}}
                        <li data-value="{{ $item.value }}">
                            <a href="{{ $item.url }}" class="needsclick">
                                {{ $item.value }}<br>
                                <span class="uk-text-muted uk-text-small">{{{ $item.text }}}</span>
                            </a>
                        </li>
                        {{/items}}
                    </ul>
                </script>
            </form>
        </div>
    </header><!-- main header end -->
    <!-- main sidebar -->
    <aside id="sidebar_main">

        <div class="sidebar_main_header" style="height:48px !important;background: none;
    background-color: #00AB84;
}">

        </div>

        <div class="menu_section">
            <ul>
                       <li id="Stories">
                       <a href="<?php echo base_url('Home/data');?>">
                       <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
                       <span class="menu_title">Stories</span></a></li>
                       <li id="Users">
                       <a href="<?php echo base_url('Home/users');?>">
                       <span class="menu_icon"><i class="material-icons"></i></span>
                       <span class="menu_title">Users</span></a></li>
                       <li id="Featured">
                       <a href="<?php echo base_url('Home/featured');?>">
                       <span class="menu_icon"><i class="material-icons">featured_play_list</i></span>
                       <span class="menu_title">Featured Stories</span></a></li>
                       <li id="Reported">
                       <a href="<?php echo base_url('Home/reported');?>">
                       <span class="menu_icon"><i class="material-icons">report</i></span>
                       <span class="menu_title">Reported Stories</span></a></li>
                       <li id="ReportedStoryByUser">
                       <a href="<?php echo base_url('Home/reportedStoryByUser');?>">
                       <span class="menu_icon"><i class="material-icons">report</i></span>
                       <span class="menu_title">Reported Story By User </span></a></li>
                       <!--  <li>
                        <a href="#">
                        <span class="menu_icon"><i class="material-icons">&#xE241;</i></span>
                        <span class="menu_title">Multi level</span>
                        </a>
                    <ul>
                        <li>
                            <a href="#">First level</a>
                            <ul>
                                <li>
                                    <a href="#">Second Level</a>
                                    <ul>
                                        <li>
                                            <a href="#">Third level</a>
                                        </li>
                                        <li>
                                            <a href="#">Third level</a>
                                        </li>
                                        <li>
                                            <a href="#">Third level</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">Long title to test</a>
                                    <ul>
                                        <li>
                                            <a href="#">Third level</a>
                                        </li>
                                        <li>
                                            <a href="#">Third level</a>
                                        </li>
                                        <li>
                                            <a href="#">Third level</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">Even longer title multi line</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li> -->
            </ul>
        </div>
    </aside><!-- main sidebar end -->
    <!-- image modal -->
