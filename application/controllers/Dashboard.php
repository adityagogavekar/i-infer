<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');
  class Dashboard extends Ci_Controller
  {
    function __construct()
    {
      parent::__construct();
       
        $adminID=$this->session->userdata('name');
      //echo $adminID;
      if($adminID=='')
     {
            //echo "faild";
            return redirect('Admin');
     }
      else{

      }
    }
    public function index()
    {
      $this->load->model('SelectVideo','select');
      $videoList = $this->select->viewList();
      $this->load->view('header');
      $this->load->view('sidebar');
      $this->load->view('viewVideoList',array('videoList'=>$videoList));
      // $this->load->view('viewVideoList');
      $this->load->view('footer');
    }
    public function splode()
    {
   $this->load->model('SelectVideo','select');
 $videoList = $this->select->viewListSplode();
   // print_r($videoList);
    //  $this->load->view('header');
  //    $this->load->view('sidebar');
      $this->load->view('splodeVideo',array('videoList'=>$videoList));
      // $this->load->view('viewVideoList');
    //  $this->load->view('footer');
    }
    public function showUserList()
    {
      $this->load->model('SelectVideo','selectUser');
      $userList = $this->selectUser->viewUserList();
      $this->load->view('header');
      $this->load->view('sidebar');
      $this->load->view('user',array('userList'=>$userList));
      // $this->load->view('viewVideoList');
      $this->load->view('footer');
    }
     public function splodeVedioDeactive1($videoID)//video Active
    {
      $this->load->model('SelectVideo','deactive');
      if($this->deactive->splodeActive($videoID))
      {
        redirect('Dashboard/trashSplodeVideo');
      }
    }
    public function splodeVedioActive($videoID)//video Active
    {
      $this->load->model('SelectVideo','active');
      if($this->active->splodedeActive($videoID))
      {
        redirect('Dashboard/splode');
      }
    }

    public function userVedioDeactive1($videoID)//video Active user
    {
      $this->load->model('SelectVideo','deactive');
      if($this->deactive->userVideoActive($videoID))
      {
        redirect('Dashboard/trashUserVideo');
      }
    }

      public function userVedioDeactiveReport($videoID)//video Active user
    {
      $this->load->model('SelectVideo','deactive');
      if($this->deactive->userVideoActive($videoID))
      {
        redirect('Dashboard/report');
      }

    }

       public function userRemove($userID)//video Active user
    {
      //print_r($userID);
      $this->load->model('SelectVideo','userdeactive');
      if($this->userdeactive->userdelete($userID))
      {
        redirect('Dashboard/trashUser');
      }

    }


    public function userVideoActive($videoID)//video Active user
    {
      $this->load->model('SelectVideo','active');
      if($this->active->userVideoDEActive($videoID))
      {
        redirect('Dashboard/index');
      }
    }

    public function logo()
    {
      $this->load->model('SelectVideo','logo');
      $splodeLogo=$this->logo->logoList();
      $this->load->view('header');
      $this->load->view('sidebar');
      $this->load->view('splodeData',array('splodeLogo'=>$splodeLogo));
      $this->load->view('footer');
    }
    public function userPoints()
    {
      $this->load->model('SelectVideo','userPoints');
      $splodeuserPoints=$this->userPoints->userPointsList();
      $this->load->view('header');
      $this->load->view('sidebar');
      $this->load->view('splodeuserPoints',array('splodeuserPoints'=>$splodeuserPoints));
      $this->load->view('footer');
    }
    public function userPointsDelete($userPointID)
    {
      //print_r($userPointID); 
      $this->load->model('SelectVideo','userPointsDelete');
      if($this->userPointsDelete->userPointsdelete($userPointID))
      {
        redirect('Dashboard/userPoints');
      }
    }
    public function EditUserPoints()
    {
      error_reporting(0);
      $pid = $this->input->post('id');
      $data_update = array(
      'userPointID' => $this->input->post('id'),
      'category' => $this->input->post('category'),
      'points' => $this->input->post('points'));
      $this->form_validation->set_rules('category', 'category', 'trim|required|alpha_numeric_spaces');
      $this->form_validation->set_rules('points', 'points', 'required|numeric');
      if ($this->form_validation->run() == FALSE)
      {
        $this->userPoints();
      }
      else
      {
        $this->load->model('SelectVideo','userPointsUpdate');
        if($this->userPointsUpdate->userPointsupdate($pid,$data_update))
        {
          redirect('Dashboard/userPoints');
        }
      }
    }
    public function userPointsAdd()
    {
      $this->load->view('header');
      $this->load->view('sidebar');
      $this->load->view('addcategory');
      // $this->load->view('viewVideoList');
      $this->load->view('footer');
    }
    public function category()
    {
      $post=$this->input->post();
      unset($post['submit']);
      $this->form_validation->set_rules('category', 'category', 'trim|required|alpha_numeric_spaces');
      $this->form_validation->set_rules('points', 'points', 'required|numeric');
      if ($this->form_validation->run() == FALSE)
      {
        $this->userPointsAdd();
      }
      else
      {
        $this->load->model('SelectVideo','insertCategory1');
        if($this->insertCategory1->insertCategory($post))
        {
          //validation
          redirect('Dashboard/userPoints');
        }
        else
        {
          //validtions
          redirect('Dashboard/category');
        }
      }
    }
    public function points()
    {
      $this->load->model('SelectVideo','point');
      $splodePoints=$this->point->pointsList();
      $this->load->view('header');
      $this->load->view('sidebar');
      $this->load->view('points',array('splodePoints'=>$splodePoints));
      $this->load->view('footer'); 
    }
    public function EditPoints()
    {
      error_reporting(0);
      $pid = $this->input->post('id');
      $data_update = array(
      'pointsID' => $this->input->post('id'),
      'rosePoints' => $this->input->post('rosePoints'),
      'salutePoints' => $this->input->post('salutePoints'),
      'tomatoPoints' => $this->input->post('tomatoPoints'),
      'viewPoints' => $this->input->post('viewPoints'));
      $this->form_validation->set_rules('rosePoints', 'rosePoints', 'required|numeric');
      $this->form_validation->set_rules('salutePoints', 'salutePoints', 'required|numeric');
      $this->form_validation->set_rules('tomatoPoints', 'tomatoPoints', 'required|numeric');
      $this->form_validation->set_rules('viewPoints', 'viewPoints', 'required|numeric');
      if ($this->form_validation->run() == FALSE)
      {
                       


        $this->points();
      }
      else
      {
        $this->load->model('SelectVideo','pointsUpdate');
        if($this->pointsUpdate->Pointsupdate($pid,$data_update))
        {
          redirect('Dashboard/points');
        }
      } 
    }
    public function level()
    {
      $this->load->model('SelectVideo','level');
      $splodeLevel=$this->level->levelList();
      $this->load->view('header');
      $this->load->view('sidebar');
      $this->load->view('level',array('splodeLevel'=>$splodeLevel));
      $this->load->view('footer'); 
    }
    public function EditLevel()
    {
      error_reporting(0);
      $pid = $this->input->post('id');
      $data_update = array(
      'levelID' => $this->input->post('id'),
      'levelName' => $this->input->post('levelName'),
      'points' => $this->input->post('points'));
      $this->form_validation->set_rules('points', 'points', 'required|numeric');
      if ($this->form_validation->run() == FALSE)
      {
        $this->level();
      }
      else
      {
        $this->load->model('SelectVideo','levelUpdate');
        if($this->levelUpdate->levelupdate($pid,$data_update))
        {
          redirect('Dashboard/level');
        }
      } 
    }
    public function genere()
    {
      $this->load->model('SelectVideo','genere');
      $splodeGenere=$this->genere->genereList();
      $this->load->view('header');
      $this->load->view('sidebar');
      $this->load->view('genere',array('splodeGenere'=>$splodeGenere));
      $this->load->view('footer');
    }
    public function EditGenere()
    {
      error_reporting(0);
      echo $pid = $this->input->post('id');
      $data_update = array(
      'uploadGenereID' => $this->input->post('id'),
      'genere' => $this->input->post('genere'));
      //print_r($data_update); 
      $this->form_validation->set_rules('genere', 'genere', 'trim|required|alpha_numeric_spaces');
      if ($this->form_validation->run() == FALSE)
      {
        $this->genere();
      }
      else
      {
        $this->load->model('SelectVideo','levelGenere');
        if($this->levelGenere->genereupdate($pid,$data_update))
        {
          redirect('Dashboard/genere');
        }
      }
    }
    public function genereDelete($uploadGenereID)
    {
      //print_r($userPointID); 
      $this->load->model('SelectVideo','uploadgenereDelete');
      if($this->uploadgenereDelete->usergenereDelete($uploadGenereID))
      {
        redirect('Dashboard/genere');
      }
    }
    public function genereAdd()
    {
      $this->load->view('header');
      $this->load->view('sidebar');
      $this->load->view('addGenere');
      // $this->load->view('viewVideoList');
      $this->load->view('footer');
    }
    public function genereStore()
    {
      $post=$this->input->post();
      unset($post['submit']);
      $this->form_validation->set_rules('genere', 'genere', 'trim|required|alpha_numeric_spaces');
      if ($this->form_validation->run() == FALSE)
      {
        $this->genereAdd();
      }
      else
      {
        $this->load->model('SelectVideo','insertGenere1');
        if($this->insertGenere1->insertGenere($post))
        {
         redirect('Dashboard/genere');
        }
        else
        {
          redirect('Dashboard/genereAdd');
        }
      }
    }
    public function help()
    {
      $this->load->model('SelectVideo','help');
      $splodeHelp=$this->help->helpList();
      $this->load->view('header');
      $this->load->view('sidebar');
      $this->load->view('helpList',array('splodeHelp'=>$splodeHelp));
      $this->load->view('footer');
    }
    public function report()
    {
      $this->load->model('SelectVideo','report');
      $splodeReport=$this->report->reportList();
      $this->load->view('header');
      $this->load->view('sidebar');
      $this->load->view('reportList',array('splodeReport'=>$splodeReport));
      $this->load->view('footer');
    }
    public function reportFlag($videoID)
    {
      $this->load->model('SelectVideo','reportFlagclr');
      if($this->reportFlagclr->reportFlagclere($videoID))
      {
        redirect('Dashboard/report');
      }
    }
    
    public function trashUserVideo()
    {
      $this->load->model('SelectVideo','select');
      $videoList = $this->select->deactivUserVedioList();
      $this->load->view('header');
      $this->load->view('sidebar');
      $this->load->view('trashVideoList',array('videoList'=>$videoList));
      // $this->load->view('viewVideoList');
      $this->load->view('footer');
    }



     public function trashSplodeVideo()
    {
      $this->load->model('SelectVideo','select');
      $videoList = $this->select->deactiveSplodeVedioList();
      $this->load->view('header');
      $this->load->view('sidebar');
      $this->load->view('trashSplodeList',array('videoList'=>$videoList));
      // $this->load->view('viewVideoList');
      $this->load->view('footer');
    }

     public function trashUser()
    {
      $this->load->model('SelectVideo','select');
      $trashUserList = $this->select->trashUserList();
      $this->load->view('header');
      $this->load->view('sidebar');
      $this->load->view('trashUser',array('trashUserList'=>$trashUserList));
      // $this->load->view('viewVideoList');
      $this->load->view('footer');
    }

public function userActive($userID)//video Active
    {
      $this->load->model('SelectVideo','active');
      if($this->active->activeUser($userID))
      {
        redirect('Dashboard/showUserList');
      }
    }



    public function addVideo()
    {
      $data = array();
      $this->load->model('SelectVideo','cat');
      $query = $this->cat->getAllUploadGenere();
      if ($query)
      {
        $data['records'] = $query;
      }
      $this->load->view('header');
      $this->load->view('sidebar');
      $this->load->view('addVideo',$data);
      $this->load->view('footer');
    }
    public function uploadVideo()
     {
        $this->load->library('upload');
        
        function getGUID(){
					if (function_exists('com_create_guid')){
						return com_create_guid();
					}else{
						mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
						$charid = strtoupper(md5(uniqid(rand(), true)));
						$hyphen = chr(45);// "-"
						$uuid = chr(123)// "{"
							.substr($charid, 0, 8).$hyphen
							.substr($charid, 8, 4).$hyphen
							.substr($charid,12, 4).$hyphen
							.substr($charid,16, 4).$hyphen
							.substr($charid,20,12)
							.chr(125);// "}"
						return $uuid;
					}
				}
				$GUID = getGUID();
				  $g_uid = substr($GUID, 1, -1);

             $videoName = "$g_uid".".mp4";
             
             
    
   //$uploaddir = 'http://45.55.187.99/splode/admin/video/';
  
  //$ImageName1 = $uploaddir .$videoName; 
  
        $ImageName1 ='http://45.55.187.99/splode/admin/video/'.$videoName;
        //$fileElementName = 'file';
        $path = 'video/'; 
        $location = $path . $videoName; 
        move_uploaded_file($_FILES['videoLink']['tmp_name'], $location); 
        $ImageName = 'http://45.55.187.99/splode/admin/thumbnail/'.$_FILES['videoThumbnail']['name'];
        //$fileElementName = 'file';
        $path = 'thumbnail/'; 
        $location = $path . $_FILES['videoThumbnail']['name']; 
        move_uploaded_file($_FILES['videoThumbnail']['tmp_name'], $location); 
        
						


        $data = array(                
       'videoTitle' => $this->input->post('videoTitle'),
       'duration' => $this->input->post('duration'),
       'description' => $this->input->post('description'),
       'keyWords' => $this->input->post('keyWords'),
       'genere' => $this->input->post('genere'),
       'isSplode' => $this->input->post('isSplode'),
       'videoLink' => $ImageName1,
       'videoThumbnail' => $ImageName);

        $this->form_validation->set_rules('videoTitle', 'videoTitle', 'trim|required|alpha_numeric_spaces');
     
      $this->form_validation->set_rules('description', 'description', 'trim|required|alpha_numeric_spaces');
    
      $this->form_validation->set_rules('keyWords', 'keyWords', 'trim|required|alpha_numeric_spaces');
      $this->form_validation->set_rules('genere', 'genere', 'required');
      //$this->form_validation->set_rules('videoLink', 'videoLink', 'required');
      //$this->form_validation->set_rules('videoThumbnail', 'videoThumbnail', 'required');
        if ($this->form_validation->run() == FALSE)
      {
        $this->addVideo();
      }

      else
      {
        $this->load->model('SelectVideo','uploadVideo1');
        if($this->uploadVideo1->insertVideo($data))
        {
          redirect('Dashboard/splode');
        }
        else
        {
          redirect('Dashboard/addVideo');
        }
    }
}
    public function add1(){

        $this->load->view('header');
      $this->load->view('sidebar');
      $this->load->view('config');
      $this->load->view('uploadVideo');
      $this->load->view('footer');

    }
    
    public function add2(){

       
      $this->load->view('config');
      $this->load->view('up');
     

    }
}
