<?php
class Home extends CI_Controller {

function __construct() {
		parent::__construct();
		 $this->load->model('HomeModel');
			}
	public function index()
	{
		$getData=$this->checkLogin();
     //print_r($getData);
         //$adminId = $getData['adminId'];
    if(!empty($getData)){
      $this->load->view('datatables');
	}
  else
  {
    $this->load->view('login');
  }
}
   private function checkLogin(){
        if($this->session->userdata('I2'))
            {
                $sessionData = $this->session->userdata('I2');

            }else{
                $sessionData=$this->session->userdata('I2');
            }

            return $sessionData;
    }
  public function view_login()
  {
    $emailID = $this->input->post('loginEmailID');
    $password = $this->input->post('loginPassword');
    $emailID=strtolower($emailID);

          if(!empty($emailID)&&!empty($password)){

            $result = $this->HomeModel->validateEmp($emailID,$password);


            if($result){

            $getEmptData=$this->checkLogin();
            //print_r($getEmptData);

        $this->load->view('datatables');

            }else{
                $this->data['error']="Wrong email or password !";

              $this->load->view('login',$this->data);
            }

          }else{

            $this->data['error']="Required fields not found !";
            $this->load->view('login',$this->data);
          }
    //$this->load->view('datatables');
  }
   function logout(){
      $getData=$this->checkLogin();

    $clearData = array(
            // 'name' => $row->name,
            'validated' => false
          );
    $this->session->sess_destroy('I2',$clearData);

    $this->load->view('login');
  }
/********************************************** View Stories ***********************************************/
	public function data()
	{
    $getData = $this->checkLogin();
		if(!empty($getData))
		{
		 $this->load->view('datatables');
	 }
	 else {
	 $this->load->view('login');
	 }
		//$this->load->view('datatables');
	}
  public function datatables()
  {
		$getData = $this->checkLogin();
		if(!empty($getData))
		{
    if (isset($_POST['string'])) {
         $string=$this->input->post('string');
          $this->session->set_userdata('string',$string);
            }else{
            $string=$this->session->userdata('string');
    }
           $this->data['string'] = $string;
           $this->load->view('datatables',$this->data);
    //$this->load->view('datatables');
	}
	else
	{
	$this->load->view('login');
	}

  }
	public function viewStory()
	{
		$getData = $this->checkLogin();

		if(!empty($getData))
		{
		$data = array();
       // $no = $_POST['start'];
    $string = "";

    if(isset($_POST['string']))
    {
       $string = $this->input->post('string');
    $string = json_decode($string);
    }
      //print_r($string);
        $stories=$this->HomeModel->getStory($string);
        foreach ($stories as $readStories) {

            //$no++;
            $row = array();

           // $row[]=$readEvents->url;
            $page = $this->HomeModel->getPage($readStories->storyID);
            if($page){
            if($page[0]->type=="image"){

             $row[]= '<img src="'.$page[0]->url.'" style="height:40px; width:40px;cursor:pointer;" onclick="getPages(\''.$readStories->storyID.'\')">';
            }
            else
            {
            	$row[] = '<img src="'.$page[0]->thumbnail.'" style="height:40px; width:40px;cursor:pointer;" onclick="getPages(\''.$readStories->storyID.'\')">';
            }
          }
          else
          {
            $row[]="-";
          }
           if($readStories->userName==""){
              $row[]="-";
            }
            else{
            $row[]=$readStories->userName;
            }
            if($readStories->storyTitle==""){
            $row[]="-";
            }
            elseif($readStories->storyTitle) {
                $row[]=$readStories->storyTitle;
            }
            if ($readStories->isFeatured == 1){
                $spotlightSwitch='<label class="switch"><input type="checkbox" class="slider"  data-switchery data-switchery-color="#d32f2f" data-switchery-size="small" id="isFeatured" value="" name="spot" checked="yes" onchange="spotlightSwitch('.$readStories->storyID.');" /><span class="slider round"></span></label>';

            }else{
                 $spotlightSwitch='<label  class="switch"><input type="checkbox" class="slider" data-switcherydata-switchery-color="#d32f2f" data-switchery-size="small" id="isFeatured" value="" class="topp" name="spot" onchange="spotlightSwitch('.$readStories->storyID.');" /><span class="slider round"></span></label>';

            }
            $row[]=$spotlightSwitch;

            $row[]=date('d M Y',strtotime($readStories->created)).'</br>'.date('h:i A',strtotime($readStories->created));







            $data[] = $row;
         }

		 $output = array(
                       "draw" => $_POST['draw'],
                        "recordsTotal" => $this->HomeModel->count_all_events($string),
                        "recordsFiltered" => $this->HomeModel->count_filtered_events($string),
                        "data" => $data,
                );

         //print_r($output);
		 echo json_encode($output);
	 }
	else
 	{
 	$this->load->view('login');
 	}
	}
/************************************* End of View Stories ***********************************************/

/*************************************  Users **********************************************************/
  public function users()
  {
		$getData = $this->checkLogin();
		if(!empty($getData))
		{
    $this->load->view('users');
	}
	else
	{
	$this->load->view('login');
	}
  }
  public function viewUsers()
  {
		$getData = $this->checkLogin();
		if(!empty($getData))
		{
    $data = array();
       // $no = $_POST['start'];
        $users=$this->HomeModel->getUsers();

        foreach ($users as $readUsers) {

            //$no++;
            $row = array();

           // $row[]=$readEvents->url;
            if($readUsers->profilePic=="")
            {
              $profilePic = base_url()."default.png";
              $row[]= '<img src="'.$profilePic.'" style="height:40px; width:40px">';
            }
            else
              {
             $row[]= '<img src="'.$readUsers->profilePic.'" style="height:40px; width:40px">';
              }



           if($readUsers->userName==""){
              $row[]="-";
            }
            else{
            $row[]=$readUsers->userName;
            }
            if($readUsers->emailID==""){
            $row[]="-";
            }
            elseif($readUsers->emailID) {
                $row[]=$readUsers->emailID;
            }
            if($readUsers->mobileNo==""){
            $row[]="-";
            }
            elseif($readUsers->mobileNo) {
                $row[]=$readUsers->mobileNo;
            }

            $row[]=$readUsers->created;
            $data[] = $row;
         }

     $output = array(
                       "draw" => $_POST['draw'],
                        "recordsTotal" => $this->HomeModel->count_all_users(),
                        "recordsFiltered" => $this->HomeModel->count_filtered_users(),
                        "data" => $data,
                );

         //print_r($output);
     echo json_encode($output);
	 }
	 else
 	{
 	$this->load->view('login');
 	}
  }
 /************************************* End of View Users ***********************************************/
 /************************************* Get Pages of Story ***********************************************/
	public function getPages()
	{
		$getData = $this->checkLogin();
		if(!empty($getData))
		{
		$storyID = $this->input->post('storyID');
		 $page = $this->HomeModel->getPage($storyID);
		 $arrResponse = array();
		 $arrResponse = array('status' => 1,'data'=>$page);
		    echo json_encode($arrResponse);
			}
			else
			{
			$this->load->view('login');
			}
	}
/************************************* End of Get Pages ***************************************************/

/************************************* Set Story Featured ***********************************************/
  public function setFeaturedStory()
  {
		$getData = $this->checkLogin();
		if(!empty($getData))
		{
    $storyID = $this->input->post('storyID');
    // $isFeatured = $this->input->post('isFeatured');

    $update = $this->HomeModel->updateStory($storyID);
		$isFeatured = $this->HomeModel->getisFeatured($storyID);

     $arrResponse = array();
     if($update)
     {
     $arrResponse = array('status' => 1,'isFeatured'=>$isFeatured->isFeatured);
   }
   else
   {
    $arrResponse = array('status' => 0);
   }
        echo json_encode($arrResponse);
			}
			else
			{
			$this->load->view('login');
			}
  }
/************************************* End of Setting Featured Story ****************************************/

/************************************* View Featured Stories ***********************************************/
  public function featured()
  {
		$getData = $this->checkLogin();
		if(!empty($getData))
		{
    $this->load->view('featured');
	}
	else
	{
	$this->load->view('login');
	}
  }
  public function viewFeaturedStories()
  {
		$getData = $this->checkLogin();
		if(!empty($getData))
		{
     $data = array();
    $stories=$this->HomeModel->getFeaturedStories();
        foreach ($stories as $readStories) {

            //$no++;
            $row = array();

           // $row[]=$readEvents->url;
            $page = $this->HomeModel->getPage($readStories->storyID);
            if($page){
            if($page[0]->type=="image"){

             $row[]= '<img src="'.$page[0]->url.'" style="height:40px; width:40px" onclick="getPages(\''.$readStories->storyID.'\')">';
            }
            else
            {
              $row[] = '<img src="'.$page[0]->thumbnail.'" style="height:40px; width:40px" onclick="getPages(\''.$readStories->storyID.'\')">';
            }
          }
          else
          {
            $row[]="-";
          }
           if($readStories->userName==""){
              $row[]="-";
            }
            else{
            $row[]=$readStories->userName;
            }
            if($readStories->storyTitle==""){
            $row[]="-";
            }
            elseif($readStories->storyTitle) {
                $row[]=$readStories->storyTitle;
            }
            if ($readStories->isFeatured == 1){
                $spotlightSwitch='<label class="switch"><input type="checkbox" class="slider"  data-switchery data-switchery-color="#d32f2f" data-switchery-size="small" id="isFeatured" value="" name="spot" checked="yes" onchange="spotlightSwitch('.$readStories->storyID.');" /><span class="slider round"></span></label>';

            }else{
                 $spotlightSwitch='<label  class="switch"><input type="checkbox" class="slider" data-switcherydata-switchery-color="#d32f2f" data-switchery-size="small" id="isFeatured" value="" class="topp" name="spot" onchange="spotlightSwitch('.$readStories->storyID.');" /><span class="slider round"></span></label>';

            }
            if($readStories)
            {
            $row[]=$spotlightSwitch;
            }
            else
            {
              $row[]="-";
            }

            $row[]=$readStories->created;







            $data[] = $row;
         }

     $output = array(
                       "draw" => $_POST['draw'],
                        "recordsTotal" => $this->HomeModel->count_all_featured(),
                        "recordsFiltered" => $this->HomeModel->count_filtered_featured(),
                        "data" => $data,
                );

         //print_r($output);
     echo json_encode($output);
	 }
	 else
 	{
 	$this->load->view('login');
 	}
  }
/************************************* End of View Featured Stories *****************************************/

/************************************* View Reported Stories ***********************************************/
   public function reported()
  {
		$getData = $this->checkLogin();
		if(!empty($getData))
		{
    $this->load->view('reported');
	}
	else
	{
	$this->load->view('login');
	}
  }
  public function viewReportedStories()
  {
		$getData = $this->checkLogin();
		if(!empty($getData))
		{
     $data = array();
    $reported=$this->HomeModel->getReportedStories();
        foreach ($reported as $readStories) {

            //$no++;
            $row = array();

           // $row[]=$readEvents->url;


           if($readStories->storyTitle==""){
              $row[]="-";
            }
            else{
            $row[]=$readStories->storyTitle;
            }
            $noOfUsers = $this->HomeModel->getNoofUsers($readStories->storyID);

            $total = count($noOfUsers);
                $row[]='<a title="Reported Users"class="btn-table" onClick="readUsers('.$readStories->storyID.');"style="border-color: #0000;background-color: #0000;">'.$total.' Users</a>';

              if ($readStories->isActive == 1){
                $spotlightSwitch='<label class="switch"><input type="checkbox" class="slider"  data-switchery data-switchery-color="#d32f2f" data-switchery-size="small" id="isReported" value="" name="spot" checked="yes" onchange="reportStory('.$readStories->storyID.');" /><span class="slider round"></span></label>';

            }else{
                 $spotlightSwitch='<label  class="switch"><input type="checkbox" class="slider" data-switcherydata-switchery-color="#d32f2f" data-switchery-size="small" id="isReported" value="" class="topp" name="spot" onchange="reportStory('.$readStories->storyID.');" /><span class="slider round"></span></label>';

            }


            $row[]=$spotlightSwitch;

           $row[]=$readStories->reportedOn;







            $data[] = $row;
         }

     $output = array(
                       "draw" => $_POST['draw'],
                        "recordsTotal" => $this->HomeModel->count_all_reported(),
                        "recordsFiltered" => $this->HomeModel->count_filtered_reported(),
                        "data" => $data,
                );

         //print_r($output);
     echo json_encode($output);
	 }
	 else
 	{
 	$this->load->view('login');
 	}
  }
/************************************* End of View Reported Stories *****************************************/

/*********************************** Get Users Who have Reported Stories  **********************************/
  public function getReportedUsers()
  {
		$getData = $this->checkLogin();
		if(!empty($getData))
		{
    $storyID = $this->input->post('storyID');
   $noOfUsers = $this->HomeModel->getReportedUsers($storyID);
     $arrResponse = array();
     $arrResponse = array('status' => 1,'users'=>$noOfUsers);
        echo json_encode($arrResponse);
			}
			else
			{
			$this->load->view('login');
			}
  }
/*********************************** End Of Get Users Who have Reported Stories  ****************************/

/*********************************** Unpublish Story From Report Stories  **********************************/
   public function reportStory()
  {
		$getData = $this->checkLogin();
		if(!empty($getData))
		{
    $storyID = $this->input->post('storyID');
     $isActive = $this->input->post('isActive');

    $data = array("isActive"=>$isActive);
    $update = $this->HomeModel->updateStory(array("storyID"=>$storyID),$data);
     $arrResponse = array();
     $arrResponse = array('status' => 1);
        echo json_encode($arrResponse);
			}
			else
			{
			$this->load->view('login');
			}
  }
/*********************************** End of Unpublish Story From Report Stories  ****************************/
/*********************************** No of Story Reported By A User  ***************************************/
 public function reportedStoryByUser()
  {
		$getData = $this->checkLogin();
		if(!empty($getData))
		{
    $this->load->view('reportedStoryByUser');
	}
	else
	{
	$this->load->view('login');
	}
  }
  public function viewReportedStoryByUser()
  {
		$getData = $this->checkLogin();
		if(!empty($getData))
		{
     $data = array();
     $type = $this->input->post('type');
    $reported=$this->HomeModel->getReportedStoryByUser($type);
        foreach ($reported as $readStories) {

            //$no++;
            $row = array();

           // $row[]=$readEvents->url;


           if($readStories->profilePic=="")
            {
              $profilePic = base_url()."default.png";
              $row[]= '<img src="'.$profilePic.'" style="height:40px; width:40px">';
            }
            else
              {
             $row[]= '<img src="'.$readStories->profilePic.'" style="height:40px; width:40px">';
              }



           if($readStories->userName==""){
              $row[]="-";
            }
            else{
            $row[]=$readStories->userName;
            }
            if($type==1)
             {
            $noOfStories = $this->HomeModel->getNoofStoriesReportedByUsers($readStories->userID);

          }
          else
          {
            $noOfStories = $this->HomeModel->getReportedStoriesOfUser($readStories->userID);

          }
          $total = count($noOfStories);
             $row[]='<a title="Reported Users"class="btn-table" onClick="readStories('.$readStories->userID.',);"style="border-color: #0000;background-color: #0000;">'.$total.' Stories</a>';






           $row[]=$readStories->reportedOn;







            $data[] = $row;
         }

     $output = array(
                       "draw" => $_POST['draw'],
                        "recordsTotal" => $this->HomeModel->count_all_reportedStoryByUser($type),
                        "recordsFiltered" => $this->HomeModel->count_filtered_reportedStoryByUser($type),
                        "data" => $data,
                );

         //print_r($output);
     echo json_encode($output);
	 }
	 else
 	{
 	$this->load->view('login');
 	}
  }
/*********************************** End of No of Story Reported By A User **********************************/
/********************************** Get Story Name of Reported Stories of User ******************************/
public function getStoriesReportedByUser()
{
	$getData = $this->checkLogin();
	if(!empty($getData))
	{
  $userID = $this->input->post('userID');
  $noOfStories = $this->HomeModel->getReportedStory($userID);
     $arrResponse = array();
     $arrResponse = array('status' => 1,'stories'=>$noOfStories);
        echo json_encode($arrResponse);
			}
			else
			{
			$this->load->view('login');
			}

}
/******************************* End of Get Story Name of Reported Stories  User **************************/
/********************************** Get Story Name of Reported Stories of User ******************************/
public function getReportedStoriesOfUser()
{
	$getData = $this->checkLogin();
	if(!empty($getData))
	{
  $userID = $this->input->post('userID');
  $noOfStories = $this->HomeModel->getReportedStoriesOfUser($userID);
     $arrResponse = array();
     $arrResponse = array('status' => 1,'stories'=>$noOfStories);
        echo json_encode($arrResponse);
			}
			else
			{
			$this->load->view('login');
			}

}
/******************************* End of Get Story Name of Reported Stories of User **************************/
}

?>
