<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicev4 extends CI_Controller {

  function __construct() {
    parent::__construct();
    date_default_timezone_set("Asia/Kolkata");
    $this->load->model('serviceModelv4','serviceModelv2');
    $this->load->helper('string');
    $this->load->helper('cookie');
    $this->load->library('session');
      //$this->load->library('aws_sdk');
    $this->load->library('mastercontroller');
    //$this->load->library ( 'getID3-master/getid3', '', 'getid3' );
    $this->dbcharesc = $this->db->conn_id;

  }
  /*****************FETCH Headers ************************************/
  #getHeaders
  private function getHeaders() {
    $accessToken = "";
    //featch all http requets headers from current request
    $headers = apache_request_headers();

    foreach ($headers as $key => $value) {
      switch ($key) {
      case 'Authorization':
        $accessToken = $value;
        break;

      case 'authorization':
        $accessToken = $value;
        break;

      }
    }
    return $accessToken;
  }

  #generate Token
  private function getTokens() {
    do {
      $accessToken = $this->mastercontroller->generateRandomString(50);
      $flagAccess = $this->serviceModelv2->checkAccessToken($accessToken);

    } while ($flagAccess > 0);
    return $accessToken;
  }
  public function index()
  {
    echo "hello";
  }
  /********************************* Strip Values of Anything Harmful **************************************/
   public function stripTags($db,$value){
   // $db=$this->db->conn_id;
  $value = mysqli_real_escape_string($db,$value);
  $value1=str_replace(array( "'" ), '', $value);
  $value1= strip_tags($value);
  $remove[] = "'";
  $remove[] = '"';
  //$remove[] = "-";
  $remove[] = ";";
  $remove[] = "(";
  $remove[] = ")";
  $remove[] = "--";
  $remove[] = "";


  $value1=str_replace($remove, '', $value1);
  if($value1==$value){
      return $value;
    }
    else{
      $value="";
    }
  }
  #generate Otp
    private function generateOTP(){

        return mt_rand(100000, 999999);
    }

    function generateRandomString($length = 16) {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
function validateMobile($mobile)
    {
        if(preg_match('/^[0-9]{10}+$/', $mobile)){

            return true;
        }

        return false;
    }
  /********************************************* Send OTP ***************************************************/
  public function sendOTP() {

    if ($_SERVER['REQUEST_METHOD'] == "POST") {

      $data = json_decode(file_get_contents('php://input'), true);

      $mobileNo = isset($data['mobileNo']) ? mysqli_real_escape_string($this->dbcharesc, $data['mobileNo']) : '';
       $countryCode = isset($data['countryCode']) ? mysqli_real_escape_string($this->dbcharesc, $data['countryCode']) : '';
      if (!empty($mobileNo)) {
      //  $emailID = strtolower($emailID);
        if($this->validateMobile($mobileNo)){
        $mobileNo = $countryCode.$mobileNo;

            //check email already exists or not
            //$userData = $this->serviceModelv2->getUserByEmail($emailID);
            //check Mobile No already exists
             $userData = $this->serviceModelv2->getUserByMobileNo($mobileNo);
            if ($userData) {
              $arrResponse = array("status" => 409, "message" => "User already exists with this Mobile No.");
              $statusCode = 409;
            } else {

              //$password = password_hash($password, PASSWORD_DEFAULT);


              if(empty($androidKey)){
                $androidKey = "";
              }
              if(empty($iosKey)){
                $iosKey = "";
              }
               $otp=$this->generateOTP();
              //$otp = 123456;
               // Update all previous OTP's isActive to 0 for verifying OTP later
               $updateVerification = $this->serviceModelv2->updateVerification(array("isActive"=>0),array("mobileNo"=>$mobileNo));

                $insertVerification =$this->serviceModelv2->insertVerification(array("mobileNo"=>$mobileNo,"otp"=>$otp,"isActive"=>1,"created"=>date('Y-m-d H:i:s')));
                if($insertVerification)
                {
                 $message = 'Your unique verification code for Intelligence Informatics is '.$otp.'. Thank you.';
              $sendOtp = $this->sendSms2($mobileNo, $message);
              if($sendOtp)
              {
                $arrResponse = array("status" => 200, "message" => "OTP Sent Successfully !","mobileNo"=>$mobileNo);
                $statusCode = 200;
              }
              else
              {
               $arrResponse = array("status" => 400, "message" => "OTP Not Sent  !");
                $statusCode = 400;
              }
              } else {
                $arrResponse = array("status" => 500, "message" => "Something went wrong !");
                $statusCode = 500;
              }

            }

          }else{
                     $arrResponse = array("status" => 400, "message" => "Enter valid mobile no !");
                     $statusCode=400;
                }

      } else {
        $arrResponse = array("status" => 400, "message" => "Required fields not found !");
        $statusCode = 400;
      }

    } else {
      $arrResponse = array("status" => 405, "message" => "Request method not accepted !");

      $statusCode = 405;
    }
    http_response_code($statusCode);
    echo json_encode($arrResponse);

  }

  /************************************End Of Send OTP*****************************************************/
  /************************************Verify OTP**********************************************************/
  public function verifyOTP()
  {
   if($_SERVER['REQUEST_METHOD'] == "POST"){

            $data = json_decode(file_get_contents('php://input'), true);
            $mobileNo = isset($data['mobileNo']) ? mysqli_real_escape_string($this->dbcharesc,$data['mobileNo']) : '';
            $OTP = isset($data['OTP']) ? mysqli_real_escape_string($this->dbcharesc,$data['OTP']) : '';
            $androidKey = isset($data['androidKey']) ? mysqli_real_escape_string($this->dbcharesc,$data['androidKey']) : '';
            $iosKey = isset($data['iosKey']) ? mysqli_real_escape_string($this->dbcharesc,$data['iosKey']) : '';
             $countryCode = isset($data['countryCode']) ? mysqli_real_escape_string($this->dbcharesc, $data['countryCode']) : '';
            if(!empty($mobileNo)&&!empty($OTP)){

                /*if($this->validateMobile($mobileNo)){*/

                    $verifyCode =$this->serviceModelv2->verifyCode($mobileNo);


                    if($verifyCode->otp==$OTP){
                      $accessToken = $this->getTokens();
                         $data = array(
                "userName" => "",
                "emailID" => "",
                "password" => "",
                "mobileNo" => $mobileNo,
                "accessToken" => $accessToken,
                "androidKey" => $androidKey,
                "iosKey" => $iosKey,
                "isActive" => 1,
                "created" => date('Y-m-d H:i:s'));
              $insertUser = $this->serviceModelv2->insertUser($data);
              $userID = $this->db->insert_id();
              if ($insertUser && !empty($userID)) {
                $userData = $this->serviceModelv2->getUserById($userID);
                if ($userData)
                {

                      //inserting Sessions

                      $updateSession['accessToken']=$accessToken;
                          $updateSession['androidKey']=$androidKey;
                           $updateSession['iosKey']=$iosKey;
                          $updateSession['userID']=$userID;
                          $updateSession['isActive']=1;

                              if(!empty($updateSession)){
                                $success = $this->serviceModelv2->insertSession($updateSession);
                              }

                  header("Authorization: $accessToken");

                }

                }


                            $arrResponse = array("status" => 200, "message" => "OTP verified successfully!","userID"=>$userData->userID);
                            $statusCode=200;

                        }else{
                            $arrResponse = array("status" => 400, "message" => "Entered opt is wrong !");
                            $statusCode=400;
                        }
               /* }else{
                     $arrResponse = array("status" => 400, "message" => "Enter valid mobile no !");
                     $statusCode=400;
                }   */

            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                $statusCode=400;
            }

        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
            $statusCode=405;
        }
        http_response_code($statusCode);
      echo json_encode($arrResponse);

    }
   /************************************End of Verify OTP***************************************************/
   /*********************************** Register/Update User *********************************************/
    public function registerUser()
    {
      if($_SERVER['REQUEST_METHOD'] == "POST")
      {
          $db=$this->db->conn_id;
         // $data = json_decode(file_get_contents('php://input'), true);
      $emailID = isset($_POST['emailID']) ? mysqli_real_escape_string($this->dbcharesc, $_POST['emailID']) : '';
      $userName = isset($_POST['userName']) ? mysqli_real_escape_string($this->dbcharesc, $_POST['userName']) : '';
      $password = isset($_POST['password']) ? mysqli_real_escape_string($this->dbcharesc, $_POST['password']) : '';
      $androidKey = isset($_POST['androidKey']) ? mysqli_real_escape_string($this->dbcharesc, $_POST['androidKey']) : '';
      $iosKey = isset($_POST['iosKey']) ? mysqli_real_escape_string($this->dbcharesc, $_POST['iosKey']) : '';
      $userID = isset($_POST['userID']) ? mysqli_real_escape_string($this->dbcharesc, $_POST['userID']) : '';
      $flag = isset($_POST['flag']) ? mysqli_real_escape_string($this->dbcharesc, $_POST['flag']) : '';
      if ($userID != NULL)
            {
               $isUserExist = $this->serviceModelv2->isUserExist($userID);

          if($isUserExist)
          {

          $headers = apache_request_headers();
          foreach ($headers as $key => $value)
          {
           switch ($key)
           {
            case 'Authorization':
            $accessToken = $value;
            break;
          }
        }
        $getAccessToken = $this->serviceModelv2->getAccessToken($userID);
        $getAccessToken = $getAccessToken['accessToken'];


              $compAccessToken= $this->serviceModelv2->checkSessionAceessToken($accessToken);
                if($compAccessToken)
                {

                  $password = password_hash($password, PASSWORD_DEFAULT);
                  $updateData = array();
                  $isVerified = $this->serviceModelv2->isUserVerified($userID);
                  $categories = $this->serviceModelv2->getAllCategories();
                  if(!empty($androidKey)){
                  $updateData['androidKey']=$androidKey;
                   }
                   elseif($isVerified)
                   {
                     $updateData['androidKey']=$isVerified->androidKey;
                   }
                   else
                   {
                     $updateData['androidKey']="";
                   }
                   if(!empty($iosKey)){
                  $updateData['iosKey']=$iosKey;
                   }
                   elseif($isVerified)
                   {
                     $updateData['iosKey']=$isVerified->iosKey;
                   }
                   else
                   {
                     $updateData['iosKey']="";
                   }
                    if(!empty($password)){
                  $updateData['password']=$password;
                   }
                   elseif($isVerified)
                   {
                     $updateData['password']=$isVerified->password;
                   }
                   else
                   {
                     $updateData['password']="";
                   }
                    if(!empty($userName)){
                  $updateData['userName']=$userName;
                   }
                   elseif($isVerified)
                   {
                     $updateData['userName']=$isVerified->userName;
                   }
                   else
                   {
                     $updateData['userName']="";
                   }
                    if(!empty($emailID)){
                  $updateData['emailID']=$emailID;
                   }
                   elseif($isVerified)
                   {
                     $updateData['emailID']=$isVerified->emailID;
                   }
                   else
                   {
                     $updateData['emailID']="";
                   }

                  $updateData['isVerified']=1;


                if (isset($_FILES['profilePic']['name']) && !empty($_FILES['profilePic']['name'])) {

                            $images = array();
                            $files=$_FILES['profilePic'];
                            $count = count($files["name"]);

                           for($i=0;$i<$count;$i++){

                            $file =array(
                                "name"=>$files["name"][$i],
                                "type"=>$files["type"][$i],
                                "tmp_name"=>$files["tmp_name"][$i],
                                "size"=>$files["size"][$i],
                            );

                             $result = $this->uploadDocuemtImage($file);

                              $url[] = $result['bucketURL'];

                              $updateData['profilePic']=$result['bucketURL'];;
                            }
                          }
                          elseif ($isVerified->profilePic!="")
                          {
                             $updateData['profilePic']=$isVerified->profilePic;
                          }
                          else
                          {
                            $updateData['profilePic']="";
                          }
                $update = $this->serviceModelv2->registerUser(array("userID"=>$userID),$updateData);
                 $userData=$this->serviceModelv2->getRegisterData($userID);
                if($update)
                {
                  $arrResponse = array('status'=>200, 'message'=>'Registration Successfull','userData'=>$userData,'category'=>$categories);
                  $statusCode=200;
                }
                else
                {
                   $arrResponse = array('status'=>400, 'message'=>'Something Went wrong');
                   $statusCode=400;
                }
                }
                else
                {
                $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
                $statusCode=400;
                }
              }
              else
              {
                 $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
                 $statusCode=400;
              }
            }
            else
            {
               $arrResponse = array('status'=>400, 'message'=>'Required Fields Missing');
               $statusCode=400;
            }
    }
   else
      {
        $arrResponse = array("status" => 405, "message" => "Request method not accepted");
        $statusCode=405;
      }
      http_response_code($statusCode);
     echo json_encode($arrResponse);
  }
   /*********************************** End Of Register/Update User ****************************************/
   /***************************************   Login ********************************************************/

    public function login() {
      if($_SERVER['REQUEST_METHOD'] == "POST"){
        $db=$this->db->conn_id;

        $arrResponse = array();
          $data = json_decode(file_get_contents('php://input'), true);
      $emailID = isset($data['emailID']) ? mysqli_real_escape_string($this->dbcharesc, $data['emailID']) : '';
      $password = isset($data['password']) ? mysqli_real_escape_string($this->dbcharesc, $data['password']) : '';
      $androidKey = isset($data['androidKey']) ? mysqli_real_escape_string($this->dbcharesc, $data['androidKey']) : '';
      $iosKey = isset($data['iosKey']) ? mysqli_real_escape_string($this->dbcharesc, $data['iosKey']) : '';
      $mobileNo = isset($data['mobileNo']) ? mysqli_real_escape_string($this->dbcharesc, $data['mobileNo']) : '';
       $countryCode = isset($data['countryCode']) ? mysqli_real_escape_string($this->dbcharesc, $data['countryCode']) : '';

        if ($mobileNo != NULL && $password != NULL) {
          $mobileNo = $countryCode.$mobileNo;
          $response = $this->serviceModelv2->doLogin($mobileNo, $password);
          //print_r($response);
          $userID = $response['userID'];
          if($userID)
          {

            $userData=$this->serviceModelv2->getUserById($userID);

            $updateSession = array();
            if(!empty($androidKey)){
            $updateSession['androidKey']=$androidKey;
          }
           if(!empty($iosKey)){
            $updateSession['iosKey']=$iosKey;
          }
           do{
               $accessToken = random_string('unique');
               $flag=$this->serviceModelv2->checkSessionAccessToken($accessToken);

           }while($flag > 0);
          $updateSession['accessToken']=$accessToken;
          $accessToken=$updateSession['accessToken'];



          $updateSession['userID']=$userID;
          $updateSession['isActive']=1;
          /* $updateSessionLogout=$this->serviceModelv2->updateSessionLogout(array("userID"=>$userID,"isActive"=>1),array("isActive"=>0,"accessToken"=>""));*/
          header("Authorization: $accessToken");
                   //changes
              if(!empty($updateSession)){
                $success = $this->serviceModelv2->insertSession($updateSession);
              }


            if($userData){
              $arrResponse = array('status' => 200, 'message' => 'User Login Successfull!','userData'=>$userData);
              $statusCode=200;
            }else{
              $arrResponse = array('status' => 400, 'message' => 'You are not verified user!');
              $statusCode=400;
            }

          } else {
            $arrResponse = array('status' => 400, 'message' => 'Invalid email or password.','mobileNo'=>$mobileNo);
            $statusCode=400;
          }
        } else {
          $arrResponse = array('status' => 400, 'message' => 'email and Password are mandatory.');
          $statusCode=400;
        }

      } else {
        $arrResponse = array("status" => 405, "message" => "Request method not accepted");
        $statusCode=405;
      }

      echo json_encode($arrResponse);
       http_response_code($statusCode);
    }



/*********************************** End of Login **********************************************************/
/************************************* Logout **************************************************************/
  public function logout()
    {
      if($_SERVER['REQUEST_METHOD'] == "POST"){

            $db=$this->db->conn_id;

           $arrResponse = array();
           $headers = apache_request_headers();
          foreach ($headers as $key => $value)
          {
           switch ($key)
           {
            case 'Authorization':
            $getHeaders = $value;
            break;
          }
        }
            $data = json_decode(file_get_contents('php://input'), true);


          if(!empty($getHeaders)){
                #validating customer
                $checkuser =$this->serviceModelv2->getUserByToken($getHeaders);

            $userID = $checkuser['userID'];

            $userProfileData=$this->serviceModelv2->getUserSessionData($userID);
           // $accessToken = $userProfileData->accessToken;
            $updateData = array();


                            $updateSessionLogout=$this->serviceModelv2->updateSessionLogout(array("userID"=>$userID,"isActive"=>1),array("isActive"=>0,"accessToken"=>""));


                  //  $userProfileData=$this->serviceModel->getUserData($userID);

              $arrResponse = array('status' => 1, 'message' => 'User Logout Successfull!','userData'=>$userProfileData);

          } else {
            $arrResponse = array('status' => 0, 'message' => 'Invalid email or password.');
          }


      } else {
        $arrResponse = array("status" => 0, "message" => "Request method not accepted");
      }
      echo json_encode($arrResponse);
    }
/************************************* End of Logout *******************************************************/

/************************************* Upload Stories *****************************************************/
    public function upload(){

      if($_SERVER['REQUEST_METHOD'] == "POST")
      {
          $db=$this->db->conn_id;

          $arrResponse = array();
           $userID = isset($_POST['userID']) ?  $_POST['userID'] : '';
            $captions = isset($_POST['captions']) ?  $_POST['captions'] : '';
            $flag = isset($_POST['flag']) ?  $_POST['flag'] : '';
            $keywords = isset($_POST['keywords']) ?  $_POST['keywords'] : '';
            $categories = isset($_POST['categories']) ?  $_POST['categories'] : '';
            $script = isset($_POST['script']) ?  $_POST['script'] : '';
            if ($userID != NULL)
            {
               $isUserExist = $this->serviceModelv2->isUserExist($userID);

          if($isUserExist)
          {

          $headers = apache_request_headers();
          foreach ($headers as $key => $value)
          {
           switch ($key)
           {
            case 'Authorization':
            $accessToken = $value;
            break;
          }
        }
        $getAccessToken = $this->serviceModelv2->getAccessToken($userID);
        $getAccessToken = $getAccessToken['accessToken'];


              $compAccessToken= $this->serviceModelv2->checkSessionAceessToken($accessToken);
                if($compAccessToken)
                {

                  $captions=explode(",",$captions);

                  $data = array(
                    "userID"=>$userID,
                    "keywords"=>$keywords,
                    "categories"=>$categories,
                    "storyTitle"=>($captions)?$captions[0]:"",
                    "flag"=>$flag,
                    "created"=>date('Y-m-d H:i:s'),
                    "isActive"=>1
                    );
                  $storyID = $this->serviceModelv2->insertStory($data);

                if (isset($_FILES['story']['name']) && !empty($_FILES['story']['name'])) {

                            $images = array();
                            $files=$_FILES['story'];
                            $count = count($files["name"]);

                           for($i=0;$i<$count;$i++){
                            $ext = pathinfo($files["name"][$i], PATHINFO_EXTENSION);
                            if($ext=="mp4"){
                              $type = "video";
                            }
                            else{
                              $type = "image";
                            }


                            $file =array(
                                "name"=>$files["name"][$i],
                                "type"=>$files["type"][$i],
                                "tmp_name"=>$files["tmp_name"][$i],
                                "size"=>$files["size"][$i],


                            );

                             $result = $this->uploadDocuemtImage($file);

                              $url[] = $result['bucketURL'];
                             // print_r($type);

                             if($type=="image")
                        {
                        list($width, $height) = getimagesize($result['bucketURL']);

              $message = "Image Uploaded Successfully";
              $thumbnail = "";
              $secs=10;
            }
          //   if($type=="video")
          //   {
          //
          //   $ffmpeg = '/usr/local/bin/ffmpeg/ffmpeg-4.0.2-64bit-static/ffmpeg';
          //
          //
          //   $video = $result['bucketURL'];
          //   // Get Duration of Video
          //   $time = exec("$ffmpeg -i ".$video." 2>&1 | grep Duration | cut -d ' ' -f 4 | sed s/,//");
          //
          //   $hours = (int) substr($time, 0, 2);
          //   $mins = (int) substr($time, 3, 2) + $hours * 60;
          //   $secs = (int) substr($time, 6, 2) + $mins * 60;
          //   $secs += ((int) substr($time, 9, 2)) / 100;
          //   //
          //   if($secs<30)
          //   {
          //     $img_name = random_string('alnum').random_string('unique');
          //     $thumbnail = base_url().'uploads/userImages/'.$img_name.'.jpg';
          //   //where to save the image
          //   $image = '/var/www/html/I2/uploads/userImages/'.$img_name.'.jpg';
          //
          //   //time to take screenshot at
          //   $interval = $secs/2;
          //
          //   //screenshot size
          //   $size = '320x240';
          //
          //   //ffmpeg command
          //   $cmd = "$ffmpeg -i $video -deinterlace -an -ss $interval -f mjpeg -t 1 -r 1 -y -s $size $image 2>&1";
          //   $return = `$cmd`;
          //
          //
          //   preg_match('/(\d{2,4})x(\d{2,4})/', $return, $matches);
          //
          //   $width = $matches[1];
          //   $height = $matches[2];
          //
          //
          //
          //   $message = "Video Uploaded Successfully";
          // }
          // else
          // {
          //   $video = "";
          //   $height = "";
          //   $width = "";
          //   $thumbnail = "";
          //   $message = "Cannot Upload Video Of length more than 30 Secs";
          // }
          //   }
            if($captions!="")
            {
              if($captions[$i])
              {
              $captions123 = $captions[$i];
            }
            else
            {
              $captions123 = "";
            }
            }
            else
            {
              $captions123 = "";
            }

            if (isset($_FILES['video']['name'][$i]) && !empty($_FILES['video']['name'][$i])) {
              $images = array();
              $files=$_FILES['video'];
              $count = count($files["name"]);

             for($j=0;$j<$count;$j++){
              $ext = pathinfo($files["name"][$j], PATHINFO_EXTENSION);
              $file =array(
                  "name"=>$files["name"][$j],
                  "type"=>$files["type"][$j],
                  "tmp_name"=>$files["tmp_name"][$j],
                  "size"=>$files["size"][$j],


              );

               $resultVideo = $this->uploadDocuemtImage($file);

                $url123[] = $resultVideo['bucketURL'];

                $videoA = $resultVideo['bucketURL'];
                 $ffmpeg = '/usr/local/bin/ffmpeg/ffmpeg-4.0.2-64bit-static/ffmpeg';
                $heightVideo = exec("$ffmpeg -i $videoA 2>&1 | grep Video: | grep -Po '\d{3,5}x\d{3,5}' | cut -d'x' -f1");
                $widthVideo = exec("$ffmpeg -i $videoA 2>&1 | grep Video: | grep -Po '\d{3,5}x\d{3,5}' | cut -d'x' -f2");

              }

            }
            else {
              $resultVideo['bucketURL'] = "";
            }
            if($script)
            {
              if($script[$i])
              {
              $uploadscript = $script[$i];
            }
            else
            {
              $uploadscript = "";
            }
          }
          else {
          $uploadscript = "";
          }
                      $data = array(
                      "storyID"=>$storyID,
                      "userID"=>$userID,
                      "url"=>$result['bucketURL'],
                      "type"=>$type,
                      "height"=>$height,
                      "width"=>$width,
                      "duration"=>$secs,
                      "thumbnail"=>$thumbnail,
                      "captions"=>($captions123),
                      "script"=>$uploadscript,
                      "video"=>$resultVideo['bucketURL'],
                      );
                    $insert = $this->serviceModelv2->insertPages($data);

                           }
                              $arrResponse = array("status" => 200, "message" => $message);
                               $statusCode=200;

                        }

                        }
                else
                {
                $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
                 $statusCode=400;
                }
              }
              else
              {
                 $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
                  $statusCode=400;
              }
            }
            else
            {
               $arrResponse = array('status'=>400, 'message'=>'Required Fields Missing');
                $statusCode=400;
            }

                    }
                    else
                    {
                      $arrResponse = array("status" => 405, "message" => "Request method not accepted");
                       $statusCode=405;
                    }
                    echo json_encode($arrResponse);
                    // http_response_code($statusCode);

    }

/************************************* End Of Upload Stories ***********************************************/
/*********************************** Category Stories ******************************************************/
public function storiesByCategory()
{
  if($_SERVER['REQUEST_METHOD'] == "POST")
  {
      $db=$this->db->conn_id;

      $arrResponse = array();
      $categories = $this->serviceModelv2->getAllCategories();
      foreach ($categories as $readCategories) {
        $allStories = $this->serviceModelv2->getStoryByCategories($readCategories->categoryID);

        if($allStories)
        {
          foreach ($allStories as $stories) {

          $storyID = $stories->storyID;
          $userId = $stories->userID;

          $userName = $stories->userName;
          if($stories->profilePic!="")
          {
          $profilePic = $stories->profilePic;
          }
          else {
              $profilePic = "";
          }

          $total = "";
          $getPages = $this->serviceModelv2->getPagesByStoryID($storyID);
          $title = $stories->storyTitle;
          $Stories=array();
          if($getPages)
          {
            if($getPages[0]->thumbnail!=""){
              $thumbnail = $getPages[0]->thumbnail;
            }
            else{
            $thumbnail = $getPages[0]->url;
          }
            $pageCount = count($getPages);
              foreach ($getPages as $stories) {
                $captions = "";
                if($stories->captions!="")
                {
                  $captions = $stories->captions;
                }
                $video='';
                if($stories->video!='')
                {
                $video = $stories->video;
                }
                $script='';
                if($stories->script!='')
                {
                $script = $stories->script;
                }

              $Stories[] = array(
                "pageID"=>$stories->pageID,
                "captions"=>$captions,
                "url"=>$stories->url,
                "userID"=>$stories->userID,
                "type"=>$stories->type,
                "height"=>$stories->height,
                "duration"=>$stories->duration,
                "width"=>$stories->width,
                "video"=>$video,
                "script"=>$script,
                );
            }

            $getLikedReaction = $this->serviceModelv2->getLikesByStoryID($storyID);
            $LikeCount = count($getLikedReaction);
            $userID = 0;
            $getLikedReactionOfUser = $this->serviceModelv2->getLikesOfUserByStoryID($storyID,$userID);
            $isLikedByUser = 0;
            if($getLikedReactionOfUser)
            {
              $isLikedByUser=1;
            }

          $userStories[] = array(
                "storyID"=>$storyID,
                "title"=>$title,
                "userID"=>$userId,
                "total"=>$total,
                "userName"=>$userName,
                "profilePic"=>$profilePic,
                "thumbnail"=>$thumbnail,
                "height"=>$getPages[0]->height,
                "width"=>$getPages[0]->width,
                "pageCount"=>$pageCount,
                "currentViewIndex"=>1,
                "stories"=>$Stories,
                "isLiked"=>$isLikedByUser,
                "LikeCount"=>$LikeCount,


                );



          }
          }
        }
        else
        {
          $userStories = array();
        }

        $categoryArr[] = array("categoryID"=>$readCategories->categoryID,"categoryName"=>$readCategories->categoryName,'stories'=>$userStories);


      }
      $arrResponse = array('status'=>200, 'message'=>'success','data'=>$categoryArr,"categories"=>$categories);
       $statusCode=200;
  }
  else
  {
    $arrResponse = array("status" => 405, "message" => "Request method not accepted");
     $statusCode=405;
  }
 echo json_encode($arrResponse);
  http_response_code($statusCode);
}
/*********************************** End of Category Stories ***********************************************/
/*********************************** Get Stories **********************************************************/
     public function getStories(){
      if($_SERVER['REQUEST_METHOD'] == "POST")
      {
          $db=$this->db->conn_id;

          $arrResponse = array();
          $data = json_decode(file_get_contents('php://input'), true);
            $userID = isset($data['userID']) ? mysqli_real_escape_string($this->dbcharesc, $data['userID']) : '';
            $flag = isset($data['flag']) ? mysqli_real_escape_string($this->dbcharesc, $data['flag']) : '';

            if ($userID != NULL)
            {
               $isUserExist = $this->serviceModelv2->isUserExist($userID);

          if($isUserExist)
          {

          $headers = apache_request_headers();
          foreach ($headers as $key => $value)
          {
           switch ($key)
           {
            case 'Authorization':
            $accessToken = $value;
            break;
          }
        }
        $getAccessToken = $this->serviceModelv2->getAccessToken($userID);
        $getAccessToken = $getAccessToken['accessToken'];


              $compAccessToken= $this->serviceModelv2->checkSessionAceessToken($accessToken);
                if($compAccessToken)
                {
                  $id=0;
                  $allStories = $this->serviceModelv2->getAllStories($flag,$id);
                  $categories = $this->serviceModelv2->getAllCategories();
                  if($allStories){
                  foreach ($allStories as $stories) {

                  $storyID = $stories->storyID;
                  $userId = $stories->userID;

                  $userName = $stories->userName;
                  if($stories->profilePic!="")
                  {
                  $profilePic = $stories->profilePic;
                  }
                  else {
                      $profilePic = "";
                  }

                  $total = "";
                  if($flag==1)
                  {
                  $tableName = "likes";
                      $getLikes = $this->serviceModelv2->getTrendingStories($tableName,$stories->storyID);
                      $count=0;
                      if($getLikes)
                      {
                      $count = count($getLikes);
                      }
                      $tableName = "comments";
                      $getComments = $this->serviceModelv2->getTrendingStories($tableName,$stories->storyID);
                      $count1=0;
                      if($getComments)
                      {
                      $count1 = count($getComments);
                      }
                      $total = ($count*3) + $count1;
                  }

                  $getPages = $this->serviceModelv2->getPagesByStoryID($storyID);
                  $title = $stories->storyTitle;
                  $Stories=array();
                  if($getPages)
                  {
                    if($getPages[0]->thumbnail!=""){
                      $thumbnail = $getPages[0]->thumbnail;
                    }
                    else{
                    $thumbnail = $getPages[0]->url;
                  }
                    $pageCount = count($getPages);
                      foreach ($getPages as $stories) {
                        $captions = "";
                        if($stories->captions!="")
                        {
                          $captions = $stories->captions;
                        }
                        $video='';
                        if($stories->video!='')
                        {
                        $video = $stories->video;
                        }
                        $script='';
                        if($stories->script!='')
                        {
                        $script = $stories->script;
                        }

                      $Stories[] = array(
                        "pageID"=>$stories->pageID,
                        "captions"=>$captions,
                        "url"=>$stories->url,
                        "userID"=>$stories->userID,
                        "type"=>$stories->type,
                        "height"=>$stories->height,
                        "duration"=>$stories->duration,
                        "width"=>$stories->width,
                        "video"=>$video,
                        "script"=>$script,
                        );
                    }

                    $getLikedReaction = $this->serviceModelv2->getLikesByStoryID($storyID);
                    $LikeCount = count($getLikedReaction);
                    $getLikedReactionOfUser = $this->serviceModelv2->getLikesOfUserByStoryID($storyID,$userID);
                    $isLikedByUser = 0;
                    if($getLikedReactionOfUser)
                    {
                      $isLikedByUser=1;
                    }

                  $userStories[] = array(
                        "storyID"=>$storyID,
                        "title"=>$title,
                        "userID"=>$userId,
                        "total"=>$total,
                        "userName"=>$userName,
                        "profilePic"=>$profilePic,
                        "thumbnail"=>$thumbnail,
                        "height"=>$getPages[0]->height,
                        "width"=>$getPages[0]->width,
                        "pageCount"=>$pageCount,
                        "currentViewIndex"=>1,
                        "stories"=>$Stories,
                        "isLiked"=>$isLikedByUser,
                        "LikeCount"=>$LikeCount,


                        );
                  if($flag==1)
                  {
                     usort($userStories, function ($a, $b) {

              if($a['total']==$b['total']) return 0;
              return $a['total'] < $b['total']?1:-1;
              });
                  }


                  }
                  }
                }
                else
                {
                  $userStories = array();
                }

                  $arrResponse = array('status'=>200, 'message'=>'success','data'=>$userStories,"categories"=>$categories);
                   $statusCode=200;

                }
                else
                {
                $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
                 $statusCode=400;
                }
              }
              else
              {
                 $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
                  $statusCode=400;
              }
            }
            else
            {
               $arrResponse = array('status'=>400, 'message'=>'Required Fields Missing');
                $statusCode=400;
            }

      }
      else
      {
        $arrResponse = array("status" => 405, "message" => "Request method not accepted");
         $statusCode=405;
      }
     echo json_encode($arrResponse);
      http_response_code($statusCode);
    }
/************************************* End Of Get Stories *************************************************/
/************************************* Report Stories *****************************************************/
public function reportedStories()
{
  if($_SERVER['REQUEST_METHOD'] == "POST")
  {
          $db=$this->db->conn_id;

          $arrResponse = array();

          $data = json_decode(file_get_contents('php://input'), true);
          $userID = isset($data['userID']) ? mysqli_real_escape_string($this->dbcharesc, $data['userID']) : '';
          $storyID = isset($data['storyID']) ?   mysqli_real_escape_string($this->dbcharesc, $data['storyID']) : '';
    if ($userID != NULL)
    {
        $isUserExist = $this->serviceModelv2->isUserExist($userID);

      if($isUserExist)
      {

          $headers = apache_request_headers();
          foreach ($headers as $key => $value)
          {
           switch ($key)
           {
            case 'Authorization':
            $accessToken = $value;
            break;
          }
        }
        $getAccessToken = $this->serviceModelv2->getAccessToken($userID);
        $getAccessToken = $getAccessToken['accessToken'];


        $compAccessToken= $this->serviceModelv2->checkSessionAceessToken($accessToken);
        if($compAccessToken)
        {
          $tableName = "stories";
          $getStoryData = $this->serviceModelv2->isExist($tableName,array("storyID"=>$storyID));
          $data = array(
            "userID"=>$userID,
            "storyID"=>$storyID,
            "reportedOn"=>date('Y-m-d H:i:s'),
            );
          $insert = $this->serviceModelv2->insertReportedStories($data);
          $count = $getStoryData->reportedCount;
          $count = $count+1;
          $update = $this->serviceModelv2->update($tableName,array("storyID"=>$storyID),array("reportedCount"=>$count));
          if($insert)
          {
              $arrResponse = array("status" => 200, "message" => "Story Reported Successfully");
               $statusCode=200;
          }
          else
          {
             $arrResponse = array("status" => 400, "message" => "Something Went wrong");
              $statusCode=400;
          }
        }
        else
        {
        $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
         $statusCode=400;
        }
      }
      else
      {
      $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
       $statusCode=400;
      }
    }
    else
    {
    $arrResponse = array('status'=>400, 'message'=>'Required Fields Missing');
     $statusCode=400;
    }
  }
  else
  {
  $arrResponse = array("status" => 405, "message" => "Request method not accepted");
   $statusCode=405;
  }
          echo json_encode($arrResponse);
           http_response_code($statusCode);
}
/************************************* End of Reported Stories ********************************************/
/************************************* User Profile  *******************************************************/
public function userProfile()
{
   if($_SERVER['REQUEST_METHOD'] == "POST")
  {
          $db=$this->db->conn_id;

          $arrResponse = array();

          $data = json_decode(file_get_contents('php://input'), true);
          $userID = isset($data['userID']) ? mysqli_real_escape_string($this->dbcharesc, $data['userID']) : '';
          $storyID = isset($data['storyID']) ?   mysqli_real_escape_string($this->dbcharesc, $data['storyID']) : '';
           $flag = isset($data['flag']) ? mysqli_real_escape_string($this->dbcharesc, $data['flag']) : '';
    if ($userID != NULL)
    {
        $isUserExist = $this->serviceModelv2->isUserExist($userID);

      if($isUserExist)
      {

          $headers = apache_request_headers();
          foreach ($headers as $key => $value)
          {
           switch ($key)
           {
            case 'Authorization':
            $accessToken = $value;
            break;
          }
        }
        $getAccessToken = $this->serviceModelv2->getAccessToken($userID);
        $getAccessToken = $getAccessToken['accessToken'];


        $compAccessToken= $this->serviceModelv2->checkSessionAceessToken($accessToken);
        if($compAccessToken)
        {
          $userData=$this->serviceModelv2->getRegisterData($userID);
          $accounts=$this->serviceModelv2->getAccountsData($userID);
          $accountsData=array();
          if($accounts){
          foreach ($accounts as $readAccounts) {
            $accountsData[] = array(
              "userID"=>$readAccounts->userID,
              "userName"=>$readAccounts->userName,
              "emailID"=>$readAccounts->emailID,
              "mobileNo"=>$readAccounts->mobileNo,
              "profilePic"=>$readAccounts->profilePic,
              );
          }
        }
           $allStories = $this->serviceModelv2->getAllStories($flag,$userID);
           $categories = $this->serviceModelv2->getAllCategories();
                   $categories = $this->serviceModelv2->getAllCategories();
                  if($allStories){
                  foreach ($allStories as $stories) {
                  $storyCategory = array();
                  $storyID = $stories->storyID;
                  $storyCategories = $stories->categories;
                  $storyKeyword = $stories->keywords;
                  $dataFlag = $stories->flag;
                  $userId = $stories->userID;
                  $user = $this->serviceModelv2->getUserById($userId);
                  $userName = $user->userName;
                  $profilePic = $user->profilePic;
                  $total = "";
                  if($flag==1)
                  {
                  $tableName = "likes";
                      $getLikes = $this->serviceModelv2->getTrendingStories($tableName,$stories->storyID);
                      $count = count($getLikes);
                      $tableName = "comments";
                      $getComments = $this->serviceModelv2->getTrendingStories($tableName,$stories->storyID);
                      $count1 = count($getComments);
                      $total = ($count*3) + $count1;
                  }
                  $getPages = $this->serviceModelv2->getPagesByStoryID($storyID);
                  $title = $stories->storyTitle;
                  $Stories=array();
                  if($getPages)
                  {
                    if($getPages[0]->thumbnail!=""){
                      $thumbnail = $getPages[0]->thumbnail;
                    }
                    else{
                    $thumbnail = $getPages[0]->url;
                  }
                    $pageCount = count($getPages);
                      foreach ($getPages as $stories) {
                        $captions = "";
                        if($stories->captions!="")
                        {
                          $captions = $stories->captions;
                        }
                        $video='';
                        if($stories->video!='')
                        {
                        $video = $stories->video;
                        }
                        $script='';
                        if($stories->script!='')
                        {
                        $script = $stories->script;
                        }

                      $Stories[] = array(
                        "pageID"=>$stories->pageID,
                        "captions"=>$captions,
                        "url"=>$stories->url,
                        "userID"=>$stories->userID,
                        "type"=>$stories->type,
                        "height"=>$stories->height,
                        "duration"=>$stories->duration,
                        "width"=>$stories->width,
                        "video"=>$video,
                        "script"=>$script,
                        );
                    }

                    $getLikedReaction = $this->serviceModelv2->getLikesByStoryID($storyID);
                    $LikeCount = count($getLikedReaction);
                    $getLikedReactionOfUser = $this->serviceModelv2->getLikesOfUserByStoryID($storyID,$userID);
                    $isLikedByUser = 0;
                    if($getLikedReactionOfUser)
                    {
                      $isLikedByUser=1;
                    }

                  $userStories[] = array(
                        "storyID"=>$storyID,
                        "storyCategory"=>$storyCategories,
                        'storyKeyword'=>$storyKeyword,
                        "flag"=>$dataFlag,
                        "title"=>$title,
                        "userID"=>$userID,
                        "total"=>$total,
                        "userName"=>$userName,
                        "profilePic"=>$profilePic,
                        "thumbnail"=>$thumbnail,
                        "height"=>$getPages[0]->height,
                        "width"=>$getPages[0]->width,
                        "pageCount"=>$pageCount,
                        "currentViewIndex"=>1,
                        "stories"=>$Stories,
                        "categories"=>$categories,
                        "isLiked"=>$isLikedByUser,
                        "LikeCount"=>$LikeCount,
                        );
                  if($flag==1)
                  {
                     usort($userStories, function ($a, $b) {

              if($a['total']==$b['total']) return 0;
              return $a['total'] < $b['total']?1:-1;
              });
                  }

                      }
                  }
                }
                else
                {
                  $userStories = array();
                }
                $data = array(
                  "userData"=>$userData,
                  "stories"=>$userStories,
                  "Accounts"=>$accountsData
                  );

                  $arrResponse = array('status'=>200, 'message'=>'success','data'=>$data,'categories'=>$categories);
                   $statusCode=200;
        }
        else
        {
        $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
         $statusCode=400;
        }
      }
      else
      {
      $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
       $statusCode=400;
      }
    }
    else
    {
    $arrResponse = array('status'=>400, 'message'=>'Required Fields Missing');
     $statusCode=400;
    }
  }
  else
  {
  $arrResponse = array("status" => 405, "message" => "Request method not accepted");
   $statusCode=405;
  }
          echo json_encode($arrResponse);
           http_response_code($statusCode);
}
/************************************* End Of User Profile *************************************************/
/************************************* Get User Profile  ****************************************************/

public function getUserProfile()
{
   if($_SERVER['REQUEST_METHOD'] == "POST")
  {
          $db=$this->db->conn_id;

          $arrResponse = array();

          $data = json_decode(file_get_contents('php://input'), true);
          $userID = isset($data['userID']) ? mysqli_real_escape_string($this->dbcharesc, $data['userID']) : '';
          $viewUserID = isset($data['viewUserID']) ? mysqli_real_escape_string($this->dbcharesc, $data['viewUserID']) : '';
          $storyID = isset($data['storyID']) ?   mysqli_real_escape_string($this->dbcharesc, $data['storyID']) : '';
           $flag = isset($data['flag']) ? mysqli_real_escape_string($this->dbcharesc, $data['flag']) : '';
    if ($userID != NULL)
    {
        $isUserExist = $this->serviceModelv2->isUserExist($userID);

      if($isUserExist)
      {

          $headers = apache_request_headers();
          foreach ($headers as $key => $value)
          {
           switch ($key)
           {
            case 'Authorization':
            $accessToken = $value;
            break;
          }
        }
        $getAccessToken = $this->serviceModelv2->getAccessToken($userID);
        $getAccessToken = $getAccessToken['accessToken'];


        $compAccessToken= $this->serviceModelv2->checkSessionAceessToken($accessToken);
        if($compAccessToken)
        {
          $userData=$this->serviceModelv2->getRegisterData($viewUserID);


           $allStories = $this->serviceModelv2->getAllStories($flag,$viewUserID);

                  if($allStories){
                  foreach ($allStories as $stories) {

                  $storyID = $stories->storyID;
                  $dataFlag = $stories->flag;
                  $userId = $stories->userID;
                  $user = $this->serviceModelv2->getUserById($userId);
                  $userName = $user->userName;
                  $profilePic = $user->profilePic;
                  $total = "";
                  if($flag==1)
                  {
                  $tableName = "likes";
                      $getLikes = $this->serviceModelv2->getTrendingStories($tableName,$stories->storyID);
                      $count = count($getLikes);
                      $tableName = "comments";
                      $getComments = $this->serviceModelv2->getTrendingStories($tableName,$stories->storyID);
                      $count1 = count($getComments);
                      $total = ($count*3) + $count1;
                  }

                  $getPages = $this->serviceModelv2->getPagesByStoryID($storyID);
                  $title = $stories->storyTitle;
                  $Stories=array();
                  if($getPages)
                  {
                    if($getPages[0]->thumbnail!=""){
                      $thumbnail = $getPages[0]->thumbnail;
                    }
                    else{
                    $thumbnail = $getPages[0]->url;
                  }
                    $pageCount = count($getPages);
                      foreach ($getPages as $stories) {
                        $captions = "";
                        if($stories->captions!="")
                        {
                          $captions = $stories->captions;
                        }

                      $Stories[] = array(
                        "pageID"=>$stories->pageID,
                        "captions"=>$captions,
                        "url"=>$stories->url,
                        "userID"=>$stories->userID,
                        "type"=>$stories->type,
                        "height"=>$stories->height,
                        "duration"=>$stories->duration,
                        "width"=>$stories->width,
                        );
                    }


                  $userStories[] = array(
                        "storyID"=>$storyID,
                        "flag"=>$dataFlag,
                        "title"=>$title,
                        "userID"=>$userID,
                        "total"=>$total,
                        "userName"=>$userName,
                        "profilePic"=>$profilePic,
                        "thumbnail"=>$thumbnail,
                        "height"=>$getPages[0]->height,
                        "width"=>$getPages[0]->width,
                        "pageCount"=>$pageCount,
                        "currentViewIndex"=>1,
                        "stories"=>$Stories,
                        );
                  if($flag==1)
                  {
                     usort($userStories, function ($a, $b) {

              if($a['total']==$b['total']) return 0;
              return $a['total'] < $b['total']?1:-1;
              });
                  }


                  }
                  }
                }
                else
                {
                  $userStories = array();
                }
                $data = array(
                  "userData"=>$userData,
                  "stories"=>$userStories

                  );

                  $arrResponse = array('status'=>200, 'message'=>'success','data'=>$data);
                   $statusCode=200;
        }
        else
        {
        $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
         $statusCode=400;
        }
      }
      else
      {
      $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
       $statusCode=400;
      }
    }
    else
    {
    $arrResponse = array('status'=>400, 'message'=>'Required Fields Missing');
     $statusCode=400;
    }
  }
  else
  {
  $arrResponse = array("status" => 405, "message" => "Request method not accepted");
   $statusCode=405;
  }
          echo json_encode($arrResponse);
           http_response_code($statusCode);
}
/************************************* End Of User Profile *************************************************/
/************************************* Insert Search History ***********************************************/
public function insertSearchHistory()
{
  if($_SERVER['REQUEST_METHOD'] == "POST")
  {
          $db=$this->db->conn_id;

          $arrResponse = array();
          $userID = isset($_POST['userID']) ? mysqli_real_escape_string($this->dbcharesc, $_POST['userID']) : '';
          $keywords = isset($_POST['keywords']) ?   mysqli_real_escape_string($this->dbcharesc, $_POST['keywords']) : '';
          $type = isset($_POST['type']) ?   mysqli_real_escape_string($this->dbcharesc, $_POST['type']) : '';
    if ($userID != NULL)
    {
        $isUserExist = $this->serviceModelv2->isUserExist($userID);

      if($isUserExist)
      {

          $headers = apache_request_headers();
          foreach ($headers as $key => $value)
          {
           switch ($key)
           {
            case 'Authorization':
            $accessToken = $value;
            break;
          }
        }
        $getAccessToken = $this->serviceModelv2->getAccessToken($userID);
        $getAccessToken = $getAccessToken['accessToken'];


        $compAccessToken= $this->serviceModelv2->checkSessionAceessToken($accessToken);
        if($compAccessToken)
        {
          $data = array(
            "userID"=>$userID,
            "keywords"=>$keywords,
            "type"=>$type,
            "date"=>date('Y-m-d H:i:s'),
            );
          $insertid = $this->serviceModelv2->insertSearchHistory($data);
          if($insertid)
          {
            $keywordsData = $this->serviceModelv2->getKeywordFromSearchHistory($insertid);
            $keywords = $keywordsData->keywords;
            $user = array();
            if($type==1)
            {
            $story =  $this->serviceModelv2->searchKeywords($keywordsData->keywords);
            $user= $this->getSearchStories($story,$userID);
            }
            else if($type==2)
            {
              $story =  $this->serviceModelv2->searchCaptions($keywordsData->keywords);
              $user = $this->getSearchStories($story,$userID);
            }
              $arrResponse = array("status" => 200, "message" => "Search History Entered Successfully","data"=>$user);
               $statusCode=200;
          }
          else
          {
             $arrResponse = array("status" => 400, "message" => "Something Went wrong");
              $statusCode=400;
          }
        }
        else
        {
        $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
         $statusCode=400;
        }
      }
      else
      {
      $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
       $statusCode=400;
      }
    }
    else
    {
    $arrResponse = array('status'=>400, 'message'=>'Required Fields Missing');
     $statusCode=400;
    }


  }
  else
  {
  $arrResponse = array("status" => 405, "message" => "Request method not accepted");
   $statusCode=405;
  }
          echo json_encode($arrResponse);
           http_response_code($statusCode);

}
/************************************* End Of insert Search History ****************************************/
public function getSearchStories($story,$userID)
{

                   $id=0;
                   $flag = 0;
                  if($story){
                  foreach ($story as $stories) {

                  $storyID = $stories->storyID;
                  $userId = $stories->userID;
                  $user = $this->serviceModelv2->getUserById($userId);
                  if($user)
                  {
                  $userName = $user->userName;
                  $profilePic = $user->profilePic;
                  }
                  $total = "";
                  if($flag==1)
                  {
                  $tableName = "likes";
                      $getLikes = $this->serviceModelv2->getTrendingStories($tableName,$stories->storyID);
                      $count = count($getLikes);
                      $tableName = "comments";
                      $getComments = $this->serviceModelv2->getTrendingStories($tableName,$stories->storyID);
                      $count1 = count($getComments);
                      $total = ($count*3) + $count1;
                  }

                  $getPages = $this->serviceModelv2->getPagesByStoryID($storyID);
                  $title = $stories->storyTitle;
                  $Stories=array();
                  if($getPages)
                  {
                    if($getPages[0]->thumbnail!=""){
                      $thumbnail = $getPages[0]->thumbnail;
                    }
                    else{
                    $thumbnail = $getPages[0]->url;
                  }
                    $pageCount = count($getPages);
                      foreach ($getPages as $stories) {
                        $captions = "";
                        if($stories->captions!="")
                        {
                          $captions = $stories->captions;
                        }
                        $video='';
                        if($stories->video!='')
                        {
                        $video = $stories->video;
                        }
                        $script='';
                        if($stories->script!='')
                        {
                        $script = $stories->script;
                        }

                      $Stories[] = array(
                        "pageID"=>$stories->pageID,
                        "captions"=>$captions,
                        "url"=>$stories->url,
                        "userID"=>$stories->userID,
                        "type"=>$stories->type,
                        "height"=>$stories->height,
                        "duration"=>$stories->duration,
                        "width"=>$stories->width,
                        "video"=>$video,
                        "script"=>$script,
                        );
                    }
                    $getLikedReaction = $this->serviceModelv2->getLikesByStoryID($storyID);
                    $LikeCount = count($getLikedReaction);
                    $getLikedReactionOfUser = $this->serviceModelv2->getLikesOfUserByStoryID($storyID,$userID);
                    $isLikedByUser = 0;
                    if($getLikedReactionOfUser)
                    {
                      $isLikedByUser=1;
                    }

                  $userStories[] = array(
                        "storyID"=>$storyID,
                        "title"=>$title,
                        "userID"=>$userID,
                        "total"=>$total,
                        "userName"=>$userName,
                        "profilePic"=>$profilePic,
                        "thumbnail"=>$thumbnail,
                        "height"=>$getPages[0]->height,
                        "width"=>$getPages[0]->width,
                        "pageCount"=>$pageCount,
                        "currentViewIndex"=>1,
                        "stories"=>$Stories,
                        "LikeCount"=>$LikeCount,
                        "isLiked"=>$isLikedByUser,

                        );
                  if($flag==1)
                  {
                     usort($userStories, function ($a, $b) {

              if($a['total']==$b['total']) return 0;
              return $a['total'] < $b['total']?1:-1;
              });
                  }


                  }
                  }
                }
                else
                {
                  $userStories = array();
                }
                return($userStories);

}
/************************************* Trending Stories ****************************************************/
public function getSearchPage()
{
  if($_SERVER['REQUEST_METHOD'] == "POST")
  {
          $db=$this->db->conn_id;

          $arrResponse = array();
          $data = json_decode(file_get_contents('php://input'), true);
          $userID = isset($data['userID']) ? mysqli_real_escape_string($this->dbcharesc, $data['userID']) : '';
    if ($userID != NULL)
    {
        $isUserExist = $this->serviceModelv2->isUserExist($userID);

      if($isUserExist)
      {

          $headers = apache_request_headers();
          foreach ($headers as $key => $value)
          {
           switch ($key)
           {
            case 'Authorization':
            $accessToken = $value;
            break;
          }
        }
        $getAccessToken = $this->serviceModelv2->getAccessToken($userID);
        $getAccessToken = $getAccessToken['accessToken'];


        $compAccessToken= $this->serviceModelv2->checkSessionAceessToken($accessToken);
        if($compAccessToken)
        {
          $trending = $this->serviceModelv2->getTrendingKeyWords();
          $userSearchHistory = $this->serviceModelv2->userSearchHistory($userID);
          if(!$userSearchHistory)
          {
            $userSearchHistory = array();
          }
          $data = array("Trending"=>$trending,
                        "userSearchHistory"=>$userSearchHistory);

              $arrResponse = array("status" => 200, "data"=>$data);
               $statusCode=200;

        }
        else
        {
        $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
         $statusCode=400;
        }
      }
      else
      {
      $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
       $statusCode=400;
      }
    }
    else
    {
    $arrResponse = array('status'=>400, 'message'=>'Required Fields Missing');
     $statusCode=400;
    }


  }
  else
  {
  $arrResponse = array("status" => 405, "message" => "Request method not accepted");
   $statusCode=405;
  }
          echo json_encode($arrResponse);
           http_response_code($statusCode);

}
/************************************* End Of Trending Stories *********************************************/
/************************************* Search Stories ******************************************************/
public function searchStory()
{
  if($_SERVER['REQUEST_METHOD'] == "POST")
  {
          $db=$this->db->conn_id;

          $arrResponse = array();
         $data = json_decode(file_get_contents('php://input'), true);
          $userID = isset($data['userID']) ? mysqli_real_escape_string($this->dbcharesc, $data['userID']) : '';
          $storyID = isset($data['storyID']) ?   mysqli_real_escape_string($this->dbcharesc, $data['storyID']) : '';
           $searchKey = isset($data['searchKey']) ? mysqli_real_escape_string($this->dbcharesc, $data['searchKey']) : '';
    if ($userID != NULL)
    {
       $isUserExist = $this->serviceModelv2->isUserExist($userID);

      if($isUserExist)
      {

          $headers = apache_request_headers();
          foreach ($headers as $key => $value)
          {
           switch ($key)
           {
            case 'Authorization':
            $accessToken = $value;
            break;
          }
        }
        $getAccessToken = $this->serviceModelv2->getAccessToken($userID);
        $getAccessToken = $getAccessToken['accessToken'];


        $compAccessToken= $this->serviceModelv2->checkSessionAceessToken($accessToken);
        if($compAccessToken)
        {
          $searchKeywords = $this->serviceModelv2->searchKeywords(strtolower($searchKey));
          $search = array();

          if($searchKeywords)
          {
          foreach ($searchKeywords as $keywords) {
            $foundKeyWords = explode('#',$keywords->keywords);

            $key =  $this->array_search_partial($foundKeyWords,strtolower($searchKey));

            if($key!=0)
             {
              $checkifAlreadyExists = $this->searchArray($search,'suggestions',$foundKeyWords[$key]);
              if(!$checkifAlreadyExists)
              {
                 $search[] = array("suggestions"=>$foundKeyWords[$key],"type"=>1);
               }

          }
          $key1 =  $this->array_search_partial($foundKeyWords,$searchKey);
          if($key1!=0)
           {
            $checkifAlreadyExists = $this->searchArray($search,'suggestions',$foundKeyWords[$key1]);
            if(!$checkifAlreadyExists)
            {
               $search[] = array("suggestions"=>$foundKeyWords[$key1],"type"=>1);
             }

        }
          }
        }
        $searchCaptions = $this->serviceModelv2->searchCaptions($searchKey);
        $search1 = array();
        if($searchCaptions)
        {
          foreach ($searchCaptions as $captions) {
          $search1[] = array("suggestions"=>$captions->captions,"type"=>2);
          }
        }
        $search = array_merge($search1, $search);
         $arrResponse = array("status" => 200, "data" => $search);
               $statusCode=200;

        }
        else
        {
        $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
         $statusCode=400;
        }
      }
      else
      {
      $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
       $statusCode=400;
      }
    }
    else
    {
    $arrResponse = array('status'=>400, 'message'=>'Required Fields Missing');
     $statusCode=400;
    }


  }
  else
  {
  $arrResponse = array("status" => 405, "message" => "Request method not accepted");
   $statusCode=405;
  }
          echo json_encode($arrResponse);
           http_response_code($statusCode);

}

/************************************** End of Search Stories **********************************************/
/***************************************Search if Value Exists for key in array**************************************************/
function searchArray($array, $key, $val) {
    foreach ($array as $item)
        if (isset($item[$key]) && $item[$key] == $val)
        {
            return true;
        }
        else {
          return false;
        }

}
/***************************************************************************************************************/
/***************************** Return Index if Exists in array *************************************************/
function array_search_partial($arr, $keyword) {
    foreach($arr as $index => $string) {
        if (strpos($string, $keyword) !== FALSE)
            return $index;
    }
}
/***************************************************************************************************************/
/************************************* Like Story **********************************************************/
public function insertLikes()
{
  if($_SERVER['REQUEST_METHOD'] == "POST")
  {
          $db=$this->db->conn_id;

          $arrResponse = array();
         $data = json_decode(file_get_contents('php://input'), true);
          $userID = isset($data['userID']) ? mysqli_real_escape_string($this->dbcharesc, $data['userID']) : '';
          $storyID = isset($data['storyID']) ?   mysqli_real_escape_string($this->dbcharesc, $data['storyID']) : '';
           $type = isset($data['type']) ? mysqli_real_escape_string($this->dbcharesc, $data['type']) : '';
    if ($userID != NULL)
    {
       $isUserExist = $this->serviceModelv2->isUserExist($userID);

      if($isUserExist)
      {

          $headers = apache_request_headers();
          foreach ($headers as $key => $value)
          {
           switch ($key)
           {
            case 'Authorization':
            $accessToken = $value;
            break;
          }
        }
        $getAccessToken = $this->serviceModelv2->getAccessToken($userID);
        $getAccessToken = $getAccessToken['accessToken'];


        $compAccessToken= $this->serviceModelv2->checkSessionAceessToken($accessToken);
        if($compAccessToken)
        {
            $tableName = "likes";
          $likeData =  $this->serviceModelv2->isExist($tableName,array("userID"=>$userID,"storyID"=>$storyID));
          $data = array(
            "userID"=>$userID,
            "type"=>$type,
            "storyID"=>$storyID,
            "likedOn"=>date('Y-m-d H:i:s'),
            );

          if($likeData)
          {
            $update = $this->serviceModelv2->update($tableName,array("userID"=>$userID,"storyID"=>$storyID),$data);
          }
          else
          {
          $update = $this->serviceModelv2->insert($tableName,$data);
        }
          if($update)
          {
            $tableName = "stories";
          $storyData =  $this->serviceModelv2->isExist($tableName,array("storyID"=>$storyID));
          if($storyData)
          {
            $notification = array(
              "senderID"=>$userID,
              "notifications"=>"reacted on your story",
              "type"=>1,
              "dataID"=>$storyID,
              "receiverID"=>$storyData->userID,
              "created"=>date('Y-m-d H:i:s'),
              );
            $tableName = "notifications";
             $Insert = $this->serviceModelv2->insert($tableName,$notification);
           }
              $arrResponse = array("status" => 200, "message" => "Story Liked Successfully");
               $statusCode=200;
          }
          else
          {
             $arrResponse = array("status" => 400, "message" => "Something Went wrong");
              $statusCode=400;
          }
        }
        else
        {
        $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
         $statusCode=400;
        }
      }
      else
      {
      $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
       $statusCode=400;
      }
    }
    else
    {
    $arrResponse = array('status'=>400, 'message'=>'Required Fields Missing');
     $statusCode=400;
    }


  }
  else
  {
  $arrResponse = array("status" => 405, "message" => "Request method not accepted");
   $statusCode=405;
  }
          echo json_encode($arrResponse);
           http_response_code($statusCode);

}
/************************************* End of Like Story ***************************************************/
/************************************* Insert Comments *****************************************************/
public function insertComments()
{
  if($_SERVER['REQUEST_METHOD'] == "POST")
  {
          $db=$this->db->conn_id;

          $arrResponse = array();

          $userID = isset($_POST['userID']) ? mysqli_real_escape_string($this->dbcharesc, $_POST['userID']) : '';
          $storyID = isset($_POST['storyID']) ?   mysqli_real_escape_string($this->dbcharesc, $_POST['storyID']) : '';
           $comment = isset($_POST['comment']) ? mysqli_real_escape_string($this->dbcharesc, $_POST['comment']) : '';
    if ($userID != NULL)
    {
        $isUserExist = $this->serviceModelv2->isUserExist($userID);

      if($isUserExist)
      {

          $headers = apache_request_headers();
          foreach ($headers as $key => $value)
          {
           switch ($key)
           {
            case 'Authorization':
            $accessToken = $value;
            break;
          }
        }
        $getAccessToken = $this->serviceModelv2->getAccessToken($userID);
        $getAccessToken = $getAccessToken['accessToken'];


         $compAccessToken= $this->serviceModelv2->checkSessionAceessToken($accessToken);
        if($compAccessToken)
        {
          $tableName = "comments";

          $data = array(
            "userID"=>$userID,
            "comment"=>$comment,
            "storyID"=>$storyID,
            "commentedOn"=>date('Y-m-d H:i:s'),
            );

          $lastID = $this->serviceModelv2->insert($tableName,$data);

          if($lastID)
          {
            $commentedData = $this->serviceModelv2->getRecentComment($lastID);
             $tableName = "stories";
          $storyData =  $this->serviceModelv2->isExist($tableName,array("storyID"=>$storyID));
          if($storyData)
          {
            $notification = array(
              "senderID"=>$userID,
              "notifications"=>"commented on your story",
              "type"=>2,
              "dataID"=>$storyID,
              "receiverID"=>$storyData->userID,
              "created"=>date('Y-m-d H:i:s'),
              );
            $tableName = "notifications";
             $Insert = $this->serviceModelv2->insert($tableName,$notification);
           }
              $arrResponse = array("status" => 200, "message" => "Commented On Story Successfully","comment"=>$commentedData);
               $statusCode=200;
          }
          else
          {
             $arrResponse = array("status" => 400, "message" => "Something Went wrong");
              $statusCode=400;
          }
        }
        else
        {
        $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
         $statusCode=400;
        }
      }
      else
      {
      $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
       $statusCode=400;
      }
    }
    else
    {
    $arrResponse = array('status'=>400, 'message'=>'Required Fields Missing');
     $statusCode=400;
    }


  }
  else
  {
  $arrResponse = array("status" => 405, "message" => "Request method not accepted");
   $statusCode=405;
  }
          echo json_encode($arrResponse);
           http_response_code($statusCode);

}
/************************************* End of Insert Comments **********************************************/
/************************************* Get Comments ********************************************************/
public function getComments()
{
  if($_SERVER['REQUEST_METHOD'] == "POST")
  {
          $db=$this->db->conn_id;

          $arrResponse = array();
         $data = json_decode(file_get_contents('php://input'), true);
          $userID = isset($data['userID']) ? mysqli_real_escape_string($this->dbcharesc, $data['userID']) : '';
          $storyID = isset($data['storyID']) ?   mysqli_real_escape_string($this->dbcharesc, $data['storyID']) : '';

    if ($userID != NULL)
    {
        $isUserExist = $this->serviceModelv2->isUserExist($userID);

      if($isUserExist)
      {

          $headers = apache_request_headers();
          foreach ($headers as $key => $value)
          {
           switch ($key)
           {
            case 'Authorization':
            $accessToken = $value;
            break;
          }
        }
        $getAccessToken = $this->serviceModelv2->getAccessToken($userID);
        $getAccessToken = $getAccessToken['accessToken'];


         $compAccessToken= $this->serviceModelv2->checkSessionAceessToken($accessToken);
        if($compAccessToken)
        {
          $getComments = $this->serviceModelv2->getComments($storyID,$userID);
          if($getComments)
           {
          $count = count($getComments);

          foreach ($getComments as $readComments) {
            $comments[] = array(
              "userID"=>$readComments->userID,
              "userName"=> $readComments->userName,
              "profilePic"=> $readComments->profilePic,
              "comment" => $readComments->comment,
              "commentedOn" => $readComments->commentedOn,

              );
          }
        }
        else
        {
          $comments = array();
        }



              $arrResponse = array("status" => 200, "Comments" => $comments);
               $statusCode=200;

        }
        else
        {
        $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
         $statusCode=400;
        }
      }
      else
      {
      $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
       $statusCode=400;
      }
    }
    else
    {
    $arrResponse = array('status'=>400, 'message'=>'Required Fields Missing');
     $statusCode=400;
    }


  }
  else
  {
  $arrResponse = array("status" => 405, "message" => "Request method not accepted");
   $statusCode=405;
  }
          echo json_encode($arrResponse);
           http_response_code($statusCode);

}
/***************************************End of Get Comments ************************************************/
/*************************************** Get Notifications *************************************************/
public function getNotifications()
{
  if($_SERVER['REQUEST_METHOD'] == "POST")
  {
          $db=$this->db->conn_id;

          $arrResponse = array();
         $data = json_decode(file_get_contents('php://input'), true);
          $userID = isset($data['userID']) ? mysqli_real_escape_string($this->dbcharesc, $data['userID']) : '';

    if ($userID != NULL)
    {
        $isUserExist = $this->serviceModelv2->isUserExist($userID);

      if($isUserExist)
      {

          $headers = apache_request_headers();
          foreach ($headers as $key => $value)
          {
           switch ($key)
           {
            case 'Authorization':
            $accessToken = $value;
            break;
          }
        }
        $getAccessToken = $this->serviceModelv2->getAccessToken($userID);
        $getAccessToken = $getAccessToken['accessToken'];


         $compAccessToken= $this->serviceModelv2->checkSessionAceessToken($accessToken);
        if($compAccessToken)
        {

          $getLikedNotifications = $this->serviceModelv2->getNotifcations($userID,1);
          $notification = array();
          if($getLikedNotifications)
           {
            foreach ($getLikedNotifications as $readNotifications) {
          $getNotifcationsByDataID = $this->serviceModelv2->getNotifiyData($readNotifications->dataID,$readNotifications->receiverID,$readNotifications->type);
          $lastLikedNotfication = reset($getNotifcationsByDataID);
          $count = $this->serviceModelv2->getCount($readNotifications->dataID,$readNotifications->receiverID,$readNotifications->type);
          $countLiked = count($count);
          $story = $this->serviceModelv2->getNotifiedStories($lastLikedNotfication->dataID);

          $userLiked = $this->getSearchStories($story,$userID);
          $user = $this->serviceModelv2->getUserById($lastLikedNotfication->senderID);
          $userName = $user->userName;
          $lastLikedNotfication->userName = $userName;
          $lastLikedNotfication->count = $countLiked;
          $lastLikedNotfication->stories = $userLiked;
          $message = " ";
          if($countLiked>1)
           {
          $message = " and ".($countLiked-1)." others ";
        }
          $lastLikedNotfication->message = $userName.$message.$lastLikedNotfication->notifications;
          array_push($notification,$lastLikedNotfication);
        }

        }

          $getCommentedNotification = $this->serviceModelv2->getNotifcations($userID,2);
          //Get Last Commented Notfications
          if($getCommentedNotification)
           {
           foreach ($getCommentedNotification as $readNotifications) {
            $getNotifcationsByDataID = $this->serviceModelv2->getNotifiyData($readNotifications->dataID,$readNotifications->receiverID,$readNotifications->type);
            $lastCommentedNotfication = reset($getNotifcationsByDataID);
           //Get Count OF Commented Notifications
           $count = $this->serviceModelv2->getCount($readNotifications->dataID,$readNotifications->receiverID,$readNotifications->type);
          $countCommented = count($count);
          $story = $this->serviceModelv2->getNotifiedStories($lastCommentedNotfication->dataID);
           $userCommented = $this->getSearchStories($story,$userID);

           $user = $this->serviceModelv2->getUserById($lastCommentedNotfication->senderID);
           $userName = $user->userName;
           $lastCommentedNotfication->userName = $userName;

           $lastCommentedNotfication->count = $countCommented;

           $lastCommentedNotfication->stories = $userCommented;
           $message = " ";
          if($countCommented>1)
           {
           $message = " and ".($countCommented-1)." others ";
         }
           $lastCommentedNotfication->message = $userName.$message.$lastCommentedNotfication->notifications;
            array_push($notification,$lastCommentedNotfication);
          }
        }





           $arrResponse = array("status" => 200, "Notifications" => $notification);
           $statusCode=200;

        }
        else
        {
        $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
         $statusCode=400;
        }
      }
      else
      {
      $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
       $statusCode=400;
      }
    }
    else
    {
    $arrResponse = array('status'=>400, 'message'=>'Required Fields Missing');
     $statusCode=400;
    }


  }
  else
  {
  $arrResponse = array("status" => 405, "message" => "Request method not accepted");
   $statusCode=405;
  }
          echo json_encode($arrResponse);
           http_response_code($statusCode);

}
/*************************************** End Of Get Notifications ******************************************/
/*************************************** List of Users Liking the Post  ************************************/
public function likedNotificationList()
{
   if($_SERVER['REQUEST_METHOD'] == "POST")
  {
          $db=$this->db->conn_id;

          $arrResponse = array();
         $data = json_decode(file_get_contents('php://input'), true);
          $userID = isset($data['userID']) ? mysqli_real_escape_string($this->dbcharesc, $data['userID']) : '';
          $storyID = isset($data['storyID']) ?   mysqli_real_escape_string($this->dbcharesc, $data['storyID']) : '';

    if ($userID != NULL)
    {
        $isUserExist = $this->serviceModelv2->isUserExist($userID);

      if($isUserExist)
      {

          $headers = apache_request_headers();
          foreach ($headers as $key => $value)
          {
           switch ($key)
           {
            case 'Authorization':
            $accessToken = $value;
            break;
          }
        }
        $getAccessToken = $this->serviceModelv2->getAccessToken($userID);
        $getAccessToken = $getAccessToken['accessToken'];


         $compAccessToken= $this->serviceModelv2->checkSessionAceessToken($accessToken);
        if($compAccessToken)
        {
          $likedNotificationList = $this->serviceModelv2->likedNotificationList($storyID,$userID);

          $count = count($likedNotificationList);
          if($likedNotificationList)
           {
          foreach ($likedNotificationList as $readLikes) {
            $likes[] = array(
              "userID"=>$readLikes->userID,
              "userName"=> $readLikes->userName,
              "profilePic"=> $readLikes->profilePic,
              "type" => $readLikes->type,
              "likedOn" => $readLikes->likedOn,

              );
          }
        }
        else
        {
          $likes = array();
        }



              $arrResponse = array("status" => 200, "likedByData" => $likes);
               $statusCode=200;

        }
        else
        {
        $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
         $statusCode=400;
        }
      }
      else
      {
      $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
       $statusCode=400;
      }
    }
    else
    {
    $arrResponse = array('status'=>400, 'message'=>'Required Fields Missing');
     $statusCode=400;
    }


  }
  else
  {
  $arrResponse = array("status" => 405, "message" => "Request method not accepted");
   $statusCode=405;
  }
          echo json_encode($arrResponse);
           http_response_code($statusCode);
}
/*************************************** Add Account *******************************************************/

public function addAccount()
{
   if($_SERVER['REQUEST_METHOD'] == "POST")
      {
          $db=$this->db->conn_id;
         // $data = json_decode(file_get_contents('php://input'), true);
      $emailID = isset($_POST['emailID']) ? mysqli_real_escape_string($this->dbcharesc, $_POST['emailID']) : '';
      $userName = isset($_POST['userName']) ? mysqli_real_escape_string($this->dbcharesc, $_POST['userName']) : '';
      $password = isset($_POST['password']) ? mysqli_real_escape_string($this->dbcharesc, $_POST['password']) : '';
      $androidKey = isset($_POST['androidKey']) ? mysqli_real_escape_string($this->dbcharesc, $_POST['androidKey']) : '';
      $iosKey = isset($_POST['iosKey']) ? mysqli_real_escape_string($this->dbcharesc, $_POST['iosKey']) : '';
      $userID = isset($_POST['userID']) ? mysqli_real_escape_string($this->dbcharesc, $_POST['userID']) : '';
      $mobileNo = isset($_POST['mobileNo']) ? mysqli_real_escape_string($this->dbcharesc, $_POST['mobileNo']) : '';
      $flag = isset($_POST['flag']) ? mysqli_real_escape_string($this->dbcharesc, $_POST['flag']) : '';
      if ($userID != NULL)
            {
               $isUserExist = $this->serviceModelv2->isUserExist($userID);

          if($isUserExist)
          {

          $headers = apache_request_headers();
          foreach ($headers as $key => $value)
          {
           switch ($key)
           {
            case 'Authorization':
            $accessToken = $value;
            break;
          }
        }
        $getAccessToken = $this->serviceModelv2->getAccessToken($userID);
        $getAccessToken = $getAccessToken['accessToken'];


              $compAccessToken= $this->serviceModelv2->checkSessionAceessToken($accessToken);
                if($compAccessToken)
                {

                  $password = password_hash($password, PASSWORD_DEFAULT);
                  $updateData = array();
                  $isVerified = $this->serviceModelv2->isUserVerified($userID);



                if (isset($_FILES['profilePic']['name']) && !empty($_FILES['profilePic']['name'])) {

                            $images = array();
                            $files=$_FILES['profilePic'];
                            $count = count($files["name"]);

                           for($i=0;$i<$count;$i++){

                            $file =array(
                                "name"=>$files["name"][$i],
                                "type"=>$files["type"][$i],
                                "tmp_name"=>$files["tmp_name"][$i],
                                "size"=>$files["size"][$i],
                            );

                             $result = $this->uploadDocuemtImage($file);

                              $url[] = $result['bucketURL'];


                            }
                          }

                          else
                          {
                            $url="";
                          }
                $data = array(
                  "userName"=>$userName,
                  "password"=>$password,
                  "emailID"=>$emailID,
                  "profilePic"=>$result['bucketURL'],
                  "parentID"=>$userID,
                  "mobileNo"=>$mobileNo,
                  "isVerified"=>1,
                  "isActive"=>1,
                  "created"=>date('Y-m-d H:i:s'),

                  );
                $insertUser = $this->serviceModelv2->insertUser($data);
              $userId = $this->db->insert_id();
              if ($insertUser && !empty($userID)) {
                $userData = $this->serviceModelv2->getUserById($userId);
                if ($userData)
                {

                      //inserting Sessions

                      $updateSession['accessToken']=$accessToken;
                          $updateSession['androidKey']=$androidKey;
                           $updateSession['iosKey']=$iosKey;
                          $updateSession['userID']=$userId;
                          $updateSession['isActive']=1;

                              if(!empty($updateSession)){
                                $success = $this->serviceModelv2->insertSession($updateSession);
                              }



                }
                   $arrResponse = array("status" => 200, "message" => "Child Account Added  successfully!");
                            $statusCode=200;
                }

                else
                {
                   $arrResponse = array('status'=>400, 'message'=>'Something Went wrong');
                   $statusCode=400;
                }
                }
                else
                {
                $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
                $statusCode=400;
                }
              }
              else
              {
                 $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
                 $statusCode=400;
              }
            }
            else
            {
               $arrResponse = array('status'=>400, 'message'=>'Required Fields Missing');
               $statusCode=400;
            }
    }
   else
      {
        $arrResponse = array("status" => 405, "message" => "Request method not accepted");
        $statusCode=405;
      }
      http_response_code($statusCode);
     echo json_encode($arrResponse);
}
/************************************* End of Add Account *************************************************/
/************************************* Edit/Delete Story ***********************************************/
public function editStory()
{
  if($_SERVER['REQUEST_METHOD'] == "POST")
  {
          $db=$this->db->conn_id;

          $arrResponse = array();
          $data = json_decode(file_get_contents('php://input'), true);
          $userID = isset($data['userID']) ? mysqli_real_escape_string($this->dbcharesc, $data['userID']) : '';
          $keywords = isset($data['keywords']) ?   mysqli_real_escape_string($this->dbcharesc, $data['keywords']) : '';
          $categories = isset($data['categories']) ?   mysqli_real_escape_string($this->dbcharesc, $data['categories']) : '';
          $storyID = isset($data['storyID']) ?   mysqli_real_escape_string($this->dbcharesc, $data['storyID']) : '';
          $flag = isset($data['flag']) ?   mysqli_real_escape_string($this->dbcharesc, $data['flag']) : '';
    if ($userID != NULL)
    {
        $isUserExist = $this->serviceModelv2->isUserExist($userID);

      if($isUserExist)
      {

          $headers = apache_request_headers();
          foreach ($headers as $key => $value)
          {
           switch ($key)
           {
            case 'Authorization':
            $accessToken = $value;
            break;
          }
        }
        $getAccessToken = $this->serviceModelv2->getAccessToken($userID);
        $getAccessToken = $getAccessToken['accessToken'];


        $compAccessToken= $this->serviceModelv2->checkSessionAceessToken($accessToken);
        if($compAccessToken)
        {
        	$tableName = "stories";
        	// Delete Story
        	if($flag==1)
        	{
        		$updateData = array("isActive"=>0);
        		$message = "Story Deleted Successfully";

        	}
        	// Edit Story
        	else if($flag==2)
        	{
        		$storyData = $this->serviceModelv2->getStoryData($storyID);

        		$updateData = array();
        		if($keywords!="")
        		{
        			$updateData['keywords'] = $keywords;
        		}
        		else
        		{
        			$updateData['keywords'] = $storyData->keywords;
        		}
        		if($categories!="")
        		{
        			$updateData['categories'] = $categories;
        		}
        		else
        		{
        			$updateData['categories'] = $storyData->categories;
        		}
        		$message = "Story Updated Successfully";
        	}
        	//UnPublish Story Save It As A Draft
        	else if($flag==3)
        	{
        		$updateData = array("flag"=>3);
        		$message = "Story UnPublished Successfully";
        	}
        	//Publish Story Make it Public
        	else if($flag==4)
        	{
        		$updateData = array("flag"=>1);
        		$message = "Story Published Successfully";
        	}

         	$update = $this->serviceModelv2->update($tableName,array("storyID"=>$storyID),$updateData);
              $arrResponse = array("status" => 200, "message" => $message);
               $statusCode=200;

        }
        else
        {
        $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
         $statusCode=400;
        }
      }
      else
      {
      $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
       $statusCode=400;
      }
    }
    else
    {
    $arrResponse = array('status'=>400, 'message'=>'Required Fields Missing');
     $statusCode=400;
    }


  }
  else
  {
  $arrResponse = array("status" => 405, "message" => "Request method not accepted");
   $statusCode=405;
  }
          echo json_encode($arrResponse);
           http_response_code($statusCode);

}
/************************************* End Of Edit Story **************************************************/
/************************************* Remove Account ***********************************************/
public function removeAccount()
{
  if($_SERVER['REQUEST_METHOD'] == "POST")
  {
          $db=$this->db->conn_id;

          $arrResponse = array();
          $data = json_decode(file_get_contents('php://input'), true);
          $userID = isset($data['userID']) ? mysqli_real_escape_string($this->dbcharesc, $data['userID']) : '';
           $accountID = isset($data['accountID']) ? mysqli_real_escape_string($this->dbcharesc, $data['accountID']) : '';

    if ($userID != NULL)
    {
        $isUserExist = $this->serviceModelv2->isUserExist($userID);

      if($isUserExist)
      {

          $headers = apache_request_headers();
          foreach ($headers as $key => $value)
          {
           switch ($key)
           {
            case 'Authorization':
            $accessToken = $value;
            break;
          }
        }
        $getAccessToken = $this->serviceModelv2->getAccessToken($userID);
        $getAccessToken = $getAccessToken['accessToken'];


        $compAccessToken= $this->serviceModelv2->checkSessionAceessToken($accessToken);
        if($compAccessToken)
        {
        	$tableName = "users";
        	// Remove User

        		$updateData = array("isActive"=>0);




         	$update = $this->serviceModelv2->update($tableName,array("userID"=>$accountID),$updateData);
              $arrResponse = array("status" => 200, "message" => "Account Removed Successfully");
               $statusCode=200;

        }
        else
        {
        $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
         $statusCode=400;
        }
      }
      else
      {
      $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
       $statusCode=400;
      }
    }
    else
    {
    $arrResponse = array('status'=>400, 'message'=>'Required Fields Missing');
     $statusCode=400;
    }


  }
  else
  {
  $arrResponse = array("status" => 405, "message" => "Request method not accepted");
   $statusCode=405;
  }
          echo json_encode($arrResponse);
           http_response_code($statusCode);

}
/************************************* End Of Edit Story **************************************************/
/************************************* Remove Account ***********************************************/
public function userInterest()
{
  if($_SERVER['REQUEST_METHOD'] == "POST")
  {
          $db=$this->db->conn_id;

          $arrResponse = array();
          $data = json_decode(file_get_contents('php://input'), true);
          $userID = isset($data['userID']) ? mysqli_real_escape_string($this->dbcharesc, $data['userID']) : '';
           $categories = isset($data['categories']) ? mysqli_real_escape_string($this->dbcharesc, $data['categories']) : '';

    if ($userID != NULL)
    {
        $isUserExist = $this->serviceModelv2->isUserExist($userID);

      if($isUserExist)
      {

          $headers = apache_request_headers();
          foreach ($headers as $key => $value)
          {
           switch ($key)
           {
            case 'Authorization':
            $accessToken = $value;
            break;
          }
        }
        $getAccessToken = $this->serviceModelv2->getAccessToken($userID);
        $getAccessToken = $getAccessToken['accessToken'];


        $compAccessToken= $this->serviceModelv2->checkSessionAceessToken($accessToken);
        if($compAccessToken)
        {
          $tableName = "userInterest";
          // Remove User
          $category = explode(',', $categories);
          for($i=0;$i<count($category);$i++)
           {
            $data = array("userID"=>$userID,"categoryID"=>$category[$i]);
            $insert = $this->serviceModelv2->insert($tableName,$data);
           }




              $arrResponse = array("status" => 200, "message" => "Categoires Added Successfully");
               $statusCode=200;

        }
        else
        {
        $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
         $statusCode=400;
        }
      }
      else
      {
      $arrResponse = array('status'=>400, 'message'=>'You are Not Valid User');
       $statusCode=400;
      }
    }
    else
    {
    $arrResponse = array('status'=>400, 'message'=>'Required Fields Missing');
     $statusCode=400;
    }


  }
  else
  {
  $arrResponse = array("status" => 405, "message" => "Request method not accepted");
   $statusCode=405;
  }
          echo json_encode($arrResponse);
           http_response_code($statusCode);

}
/************************************* End Of Edit Story **************************************************/
/************************************* Extension of Image *************************************************/
     private function getExtension($str)
    {
    $i = strrpos($str,".");
    if (!$i) { return ""; }
    $l = strlen($str) - $i;
    $ext = substr($str,$i+1,$l);
    return $ext;
    }
/************************************* Upload Bucket Image *************************************************/
    private function uploadDocuemtImage($FILES)
    {

         $s3file="";
         $message ="0";

        //Here you can add valid file extensions.

        $valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP","mp4");

        $name = $FILES["name"];
        $size = $FILES["size"];
        $tmp = $FILES["tmp_name"];
        $ext = $this->getExtension($FILES["name"]);



        if(strlen($name) > 0)
        {


        if(in_array($ext,$valid_formats))
        {

         $this->load->library('aws_sdk');

        $dateCurrent=date('Y-m-d-H-i A');
        $bucket="codetreasure-ct";
        $img_name = $dateCurrent.random_string('alnum').random_string('unique');
        //Rename image name.
        $image_name_actual = 'images/'.md5($img_name).'.'.$ext;
        if($ext=="mp4")
        {
            $image_name_actual = 'videos/'.md5($img_name).'.'.$ext;
            $ContentType = 'video/mp4';
        }
        else
        {
          $ContentType = 'image/jpeg';
        }

        try{
            $aws_object=$this->aws_sdk->saveObject(array(
                'Bucket'      => $bucket,
                'Key'         =>  $image_name_actual,
                'ACL'         => 'public-read',
                'SourceFile'  => $tmp,
                'ContentType' =>$ContentType,


            ))->toArray();
             $s3file='https://'.$bucket.'.s3.amazonaws.com/'.$image_name_actual;

            $message = "1";
        }catch (Exception $e){
            $message= $e->getMessage();
        }
        }else{
        $message = "0";

        }
        }else{
        $message = "0";

        }
        return array("status"=>$message,"bucketURL"=>$s3file);

}
/************************************* End Of Uploading Bucket Image ***************************************/
/************************************* SMS Send Service ****************************************************/
private function sendSms2($number,$message){

        require_once APPPATH .'src/awsSDK/autoloader.php'; error_reporting(E_ALL); ini_set("display_errors", 1);

        $params = array( 'credentials' => array( 'key' => 'AKIAI4BYB47RN5RTSZRA', 'secret' => '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', ), 'region' => 'ap-southeast-1','version' => 'latest' );
        $sns = new \Aws\Sns\SnsClient($params);
        $args = array( "SenderID" => "sms-user", "DefaultSMSType" => "Transactional", "Message" => $message , "PhoneNumber" => $number );

        $result = $sns->publish($args);
                //print_r($result);exit;
                //var_dump(array_key_exists('200', $result));exit;
        if($result){

            return true;

        }else{
            return false;
        }
    }
/************************************* End of SMS Send Service *********************************************/
       public function testInv()
      {
      $result = $this->sendSms2('+919004041287', "test");
      }
/***********************************************************************************************************/

}
?>
