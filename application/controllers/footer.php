 <!-- common functions -->
    <script src="<?php echo base_url();?>assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="<?php echo base_url();?>assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="<?php echo base_url();?>assets/js/altair_admin_common.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <!-- page specific plugins -->
        <!-- d3 -->
        <script src="<?php echo base_url();?>bower_components/d3/d3.min.js"></script>
        <!-- metrics graphics (charts) -->
        <script src="<?php echo base_url();?>bower_components/metrics-graphics/dist/metricsgraphics.min.js"></script>
        <!-- chartist (charts) -->
        <script src="<?php echo base_url();?>bower_components/chartist/dist/chartist.min.js"></script>
        <!-- maplace (google maps) -->
        <script src="http://maps.google.com/maps/api/js"></script>
        <script src="<?php echo base_url();?>bower_components/maplace-js/dist/maplace.min.js"></script>
        <!-- peity (small charts) -->
        <script src="<?php echo base_url();?>bower_components/peity/jquery.peity.min.js"></script>
        <!-- easy-pie-chart (circular statistics) -->
        <script src="<?php echo base_url();?>bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        <!-- countUp -->
        <script src="<?php echo base_url();?>bower_components/countUp.js/dist/countUp.min.js"></script>
        <!-- handlebars.js -->
        <script src="<?php echo base_url();?>bower_components/handlebars/handlebars.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/custom/handlebars_helpers.min.js"></script>
        <!-- CLNDR -->
        <script src="<?php echo base_url();?>bower_components/clndr/clndr.min.js"></script>

        <!--  dashbord functions -->
        <script src="<?php echo base_url();?>assets/js/pages/dashboard.min.js"></script>
              <!-- page specific plugins -->
    <!-- datatables -->
    <script src="<?php echo base_url();?>bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables buttons-->
    <script src="<?php echo base_url();?>bower_components/datatables-buttons/js/dataTables.buttons.js"></script>
    <script src="<?php echo base_url();?>assets/js/custom/datatables/buttons.uikit.js"></script>
    <script src="<?php echo base_url();?>bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>bower_components/datatables-buttons/js/buttons.colVis.js"></script>
    <script src="<?php echo base_url();?>bower_components/datatables-buttons/js/buttons.html5.js"></script>
    <script src="<?php echo base_url();?>bower_components/datatables-buttons/js/buttons.print.js"></script>
    
    <!-- datatables custom integration -->
    <script src="<?php echo base_url();?>assets/js/custom/datatables/datatables.uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="<?php echo base_url();?>assets/js/pages/plugins_datatables.min.js"></script>
     <script src="<?php echo base_url();?>assets/js/pages/components_notifications.min.js"></script>
<script type="text/javascript">
    
</script>
 
         </body>
    </html>