


<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

/* Author: Aditya Gogavekar
 * Description: Login model class
 */
class ServiceModelv2 extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->database();
		$this->db->simple_query('SET NAMES \'utf8mb4\'');
	}

	#getCustomerByEmail
	public function getUserByEmail($emailID) {
		$this->db->select('userID,userName,mobileNo,profilePic');
		$this->db->where('emailID', $emailID);
		$this->db->where('isActive', 1);
		$query = $this->db->get('users');
		if ($query->num_rows() >= 1) {
			return $query->row();
		} else {
			return false;
		}
	}

    #getUserByMobileNo
    public function getUserByMobileNo($mobileNo) {
        $this->db->select('userID,userName,mobileNo,profilePic');
        $this->db->where('mobileNo', $mobileNo);
        $this->db->where('isActive', 1);
         $this->db->where('isVerified', 1);
        $query = $this->db->get('users');
        if ($query->num_rows() >= 1) {
            return $query->row();
        } else {
            return false;
        }
    }


	#checkAccessToken
	public function checkAccessToken($accessToken) {

        $sql = "SELECT userID FROM sessions WHERE accessToken = ? ";
        $query = $this->db->query($sql,array($accessToken));
        $res = $query->row();
        return $res;

	}
     public function getUserByToken($getHeaders){
        $sql = "SELECT userID FROM sessions WHERE accessToken = ? ";
        $query = $this->db->query($sql,array($getHeaders));
        $res = $query->row_array();
        return $res;
    }
	public function checkSessionAceessToken($accessToken)
  {
    $sql = "SELECT *
        FROM sessions WHERE accessToken = ? AND isActive = 1";
        $query = $this->db->query($sql,array($accessToken));
        if ($query->num_rows() >= 1) {

            return true;
        } else {
            return false;
        }
    }
	 //getAccessToken by UserID
    public function getAccessToken($userID){
        $sql = "SELECT accessToken FROM sessions WHERE userID = ? AND isActive = 1 ";
        $query = $this->db->query($sql,array($userID));
        $res = $query->row_array();
        return $res;
    }

	public function getUserSessionData($userID){

        $sql = "SELECT * FROM sessions where userID = ?";
        $query = $this->db->query($sql,array($userID));
        $res = $query->row();

        return $res;
     }
	public function updateSessionLogout($where,$updateData){

        $this->db->update('sessions', $updateData, $where);
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE)
        {
            return false;
        }else{

         return true;
        }

    }
    #insertOTP
    public function insertVerification($data) {
        $this->db->insert('verifyNo', $data);
        return ($this->db->affected_rows() > 0) ? true : false;
    }
    #verifyCode
    public function verifyCode($mobileNo){

        $this->db->where('mobileNo', $mobileNo);
        $this->db->where('isActive', 1);
        $query = $this->db->get('verifyNo');

        if($query->num_rows() >= 1){
            return $query->row();
        }else{
            return 0;
        }

    }
     public function updateVerification($updateData,$where) {
      $this->db->update('verifyNo', $updateData, $where);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            return false;
        } else {

            return true;
        }
    }
///////////////////////////////////////////
	#insertUser
	public function insertUser($data) {
		$this->db->insert('users', $data);
		return ($this->db->affected_rows() > 0) ? true : false;
	}
    public function registerUser($where,$updateData){

        $this->db->update('users', $updateData, $where);
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE)
        {
            return false;
        }else{

         return true;
        }

    }

	#updateCustomer
	public function updateUser($where, $updateData) {

		$this->db->update('users', $updateData, $where);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			return false;
		} else {

			return true;
		}

	}
	//isUserExist
    public function isUserExist($userID){
        //checks if the userID is pesent in userProfile table AND if it is active
        $sql = "SELECT userID FROM `users` WHERE userID = ? AND isActive='1'";
        $query = $this->db->query($sql,array($userID));

        if ($query->num_rows() >= 1) {
            return true;
        } else {
            return false;
        }
    }
    //isUserExist
    public function isUserVerified($userID){
        //checks if the userID is pesent in userProfile table AND if it is active
        $sql = "SELECT * FROM `users` WHERE userID = ? AND isVerified=1";
        $query = $this->db->query($sql,array($userID));

        if ($query->num_rows() >= 1) {
            return $query->row();
        } else {
            return false;
        }
    }
		public function isUserEmailVerified($userID){
        //checks if the userID is pesent in userProfile table AND if it is active
        $sql = "SELECT * FROM `users` WHERE userID = ? AND isVerified=1 AND emailIsVerified=1";
        $query = $this->db->query($sql,array($userID));

        if ($query->num_rows() >= 1) {
            return $query->row();
        } else {
            return false;
        }
    }
		public function emailExist($emailID)
		{
			$sql = "SELECT * FROM `users` WHERE emailID = ? AND isVerified=1 AND isActive=1";
			$query = $this->db->query($sql,array($emailID));

			if ($query->num_rows() >= 1) {
					return $query->row();
			} else {
					return false;
			}
		}




	#getUserById
	public function getUserById($userID) {
		$this->db->select('*');
		$this->db->where('userID', $userID);
		$this->db->where('isActive', 1);
		$query = $this->db->get('users');
		if ($query->num_rows() >= 1) {
			$res = $query->row();
			return $res;
		} else {
			return false;
		}
	}
    #getUserById
    public function getRegisterData($userID) {
        $this->db->select('*');
        $this->db->where('userID', $userID);
        $this->db->where('isActive', 1);
        $query = $this->db->get('users');
        if ($query->num_rows() >= 1) {
            $res = $query->row();
            return $res;
        } else {
            return false;
        }
    }
    public function getAccountsData($userID) {
        $this->db->select('userID,userName,emailID,mobileNo,profilePic,androidKey,iosKey');
        $this->db->where('parentID', $userID);
        $this->db->where('isActive', 1);
         $this->db->where('isVerified', 1);
        $query = $this->db->get('users');
        if ($query->num_rows() >= 1) {
            $res = $query->result();
            return $res;
        } else {
            return false;
        }
    }


	#doLogin
	public function doLogin($mobileNo, $hash) {
		//$mobileNo = strtolower($mobileNo);
		$sql = "SELECT userID,password FROM users WHERE mobileNo = ? AND isActive=1 AND isVerified=1";

		$query = $this->db->query($sql, array($mobileNo));
		if ($query->num_rows() == 1) {
			$res = $query->row_array();
			$hashPass = $res['password'];

			if (password_verify($hash, $hashPass)) {

				$response['userID'] = $res['userID'];
			} else {
				$response['userID'] = 0;
			}

		} else {
			$response['userID'] = 0;
		}

		return $response;
	}
	#doLogin
	public function doLoginByEmail($emailID, $hash) {
		//$mobileNo = strtolower($mobileNo);
		$sql = "SELECT userID,password FROM users WHERE emailID = ? AND isActive=1 AND emailIsVerified=1";

		$query = $this->db->query($sql, array($emailID));
		if ($query->num_rows() == 1) {
			$res = $query->row_array();
			$hashPass = $res['password'];

			if (password_verify($hash, $hashPass)) {

				$response['userID'] = $res['userID'];
			} else {
				$response['userID'] = 0;
			}

		} else {
			$response['userID'] = 0;
		}

		return $response;
	}
	#checkSessionAccessToken
public function checkSessionAccessToken($token)
{
		$this->db->select('userID');
        $this->db->where('accessToken', $token);
        $query = $this->db->get('sessions');

        if ($query->num_rows() >= 1) {
            return 1;
        } else {
            return 0;
        }
}
	 //insert session
    public function insertSession($updateData){

       $this->db->insert('sessions', $updateData);
        return ($this->db->affected_rows() > 0) ? true : false;

    }
     //insert pages
    public function insertPages($data){

       $this->db->insert('pages', $data);
        return ($this->db->affected_rows() > 0) ? true : false;

    }
     //insert story
    public function insertStory($data){

       $this->db->insert('stories', $data);
        return ($this->db->affected_rows() > 0) ?  $this->db->insert_id() : false;

    }
    //get Stories By User ID
    public function getPagesByStoryID($storyID){
    	$this->db->select('*');
    	$this->db->where('storyID', $storyID);
    	$query = $this->db->get('pages');

        if ($query->num_rows() >= 1) {
           $res = $query->result();
			return $res;
        } else {
            return 0;
        }
    }
     //get Stories By User ID
    public function getAllUsers(){
    	$this->db->select('*');

    	$query = $this->db->get('users');

        if ($query->num_rows() >= 1) {
           $res = $query->result();
			return $res;
        } else {
            return 0;
        }
    }
    //get All Stories
    public function getAllStories($flag,$userID,$offset){
        $this->db->select('*');
				$this->db->from('stories AS s');
				$this->db->join('users As u ','s.userID=u.userID','LEFT');
        if($flag==2)
        {
            $this->db->where('s.isFeatured',1);
        }

        if($userID!=0)
        {
            $this->db->where('s.userID',$userID);
        }
        else
        {
            $this->db->where('s.flag',1);
        }

         $this->db->where('s.isActive',1);
				 $this->db->where('u.isActive',1);
				 $this->db->where('u.isVerified',1);
         $this->db->order_by('s.storyID','DESC');
				 $this->db->limit(30,$offset);
        $query = $this->db->get();
				if ($query->num_rows() >= 1) {
           $res = $query->result();
            return $res;
        } else {
            return 0;
        }
    }

		public function getStoryByCategories($categoryID,$offset)
		{
			$this->db->select('*');
			$this->db->from('stories AS s');
			$this->db->join('users As u ','s.userID=u.userID','LEFT');
			 $this->db->where('s.isActive',1);
			 $this->db->where('u.isActive',1);
			 $this->db->where('u.isVerified',1);
			 $this->db->where("FIND_IN_SET($categoryID, s.categories) !=", 0);
			 $this->db->order_by('s.storyID','DESC');
			// $this->db->limit(100,$offset);
			$query = $this->db->get();
			//print_r($this->db->last_query());
			if ($query->num_rows() >= 1) {
				 $res = $query->result();
					return $res;
			} else {
					return 0;
			}
		}
		public function seeAllStoriesByCategory($categoryID,$offset)
		{
			$this->db->select('*');
			$this->db->from('stories AS s');
			$this->db->join('users As u ','s.userID=u.userID','LEFT');
			 $this->db->where('s.isActive',1);
			 $this->db->where('u.isActive',1);
			 $this->db->where('u.isVerified',1);
			 $this->db->where("FIND_IN_SET($categoryID, s.categories) !=", 0);
			 $this->db->order_by('s.storyID','DESC');
			 $this->db->limit(100,$offset);
			$query = $this->db->get();
			//print_r($this->db->last_query());
			if ($query->num_rows() >= 1) {
				 $res = $query->result();
					return $res;
			} else {
					return 0;
			}
		}
    //insert Search History
    public function insertSearchHistory($data){

       $this->db->insert('searchHistory', $data);
        return ($this->db->affected_rows() > 0) ?  $this->db->insert_id(): false;

    }
    //insert Reported Stories
    public function insertReportedStories($data){

       $this->db->insert('reportedStories', $data);
        return ($this->db->affected_rows() > 0) ?  true: false;

    }
     public function insert($tableName,$data)
    {
        $this->db->insert($tableName, $data);
        return ($this->db->affected_rows() > 0) ?  $this->db->insert_id(): false;
    }
    public function update($tableName,$where, $updateData)
    {
        $this->db->update($tableName, $updateData, $where);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
        return false;
        }
        else
        {
        return true;
        }
    }
    public function isExist($tableName,$where)
    {
        $this->db->select('*');
        $this->db->where($where);
        $query = $this->db->get($tableName);
        if ($query->num_rows() >= 1)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }
     //get Stories By User ID
    public function getReactionByStoryID($storyID){
        $this->db->select('*');
        $this->db->where('storyID', $storyID);
        $query = $this->db->get('likes');

        if ($query->num_rows() >= 1) {
           $res = $query->result();
            return $res;
        } else {
            return 0;
        }
    }
		public function getLikesByStoryID($storyID){
				$this->db->select('*');
				$this->db->where('storyID', $storyID);
				$this->db->where('type',1);
				$query = $this->db->get('likes');
		 		$res = $query->result();
						return $res;

		}
		public function getLikesOfUserByStoryID($storyID,$userID){
				$this->db->select('*');
				$this->db->where('storyID', $storyID);
				$this->db->where('userID', $userID);
				$this->db->where('type',1);
				$query = $this->db->get('likes');

				if ($query->num_rows() >= 1) {
					 $res = $query->result();
						return $res;
				} else {
						return 0;
				}
		}
    public function getStoryData($storyID){
        $this->db->select('*');
        $this->db->where('storyID', $storyID);
        $query = $this->db->get('stories');

        if ($query->num_rows() >= 1) {
           $res = $query->row();
            return $res;
        } else {
            return 0;
        }
    }
     public function getCountOfReactionByType($storyID,$type){
        $this->db->select('*');
        $this->db->where('storyID', $storyID);
         $this->db->where('type', $type);
        $query = $this->db->get('likes');

        if ($query->num_rows() >= 1) {
           $res = $query->result();
            return $res;
        } else {
            return 0;
        }
    }
     public function ifUserLikedStory($storyID,$userID){
        $this->db->select('*');
        $this->db->where('storyID', $storyID);
         $this->db->where('userID', $userID);
        $query = $this->db->get('likes');

        if ($query->num_rows() >= 1) {
           $res = $query->row();
            return $res;
        } else {
            return 0;
        }
    }
     public function getComments($storyID){
         $sql = "SELECT * FROM `comments` LEFT JOIN users ON comments.userID=users.userID WHERE storyID=? ORDER BY commentID DESC";
        $query = $this->db->query($sql,array($storyID));

        if ($query->num_rows() >= 1) {
            return $query->result();
        } else {
            return false;
        }
    }
     public function getRecentComment($commentID){
        $sql = "SELECT u.userID,u.userName,u.profilePic,c.comment,c.commentedOn FROM `comments` AS c LEFT JOIN users AS u ON c.userID=u.userID WHERE c.commentID=? ORDER BY c.commentID DESC";
        $query = $this->db->query($sql,array($commentID));

        if ($query->num_rows() >= 1) {
           $res = $query->row();
            return $res;
        } else {
            return 0;
        }
    }
    public function getTrendingStories($tableName,$storyID){
        $this->db->select('*');
        $this->db->where('storyID', $storyID);
        $query = $this->db->get($tableName);

        if ($query->num_rows() >= 1) {
           $res = $query->result();
            return $res;
        } else {
            return 0;
        }
    }
    public function searchKeywords($searchKey,$offset)
    {

        $sql = "SELECT * FROM `stories` where keywords LIKE '%$searchKey%'  AND isActive=1 GROUP BY keywords LIMIT $offset,30  ";
        $query = $this->db->query($sql);

        if ($query->num_rows() >= 1) {
           $res = $query->result();
            return $res;
        } else {
            return 0;
        }
    }
    public function searchCaptions($searchKey,$offset)
    {

        $sql = "SELECT * FROM `pages` LEFT JOIN stories ON pages.storyID = stories.storyID where captions LIKE '%$searchKey%' AND stories.isActive=1 GROUP BY captions LIMIT $offset,30";
        $query = $this->db->query($sql);

        if ($query->num_rows() >= 1) {
           $res = $query->result();
            return $res;
        } else {
            return 0;
        }
    }
 public function getKeywordFromSearchHistory($searchID)
    {

        $sql = "SELECT * FROM `searchHistory` where id =?";
        $query = $this->db->query($sql,array($searchID));

        if ($query->num_rows() >= 1) {
           $res = $query->row();
            return $res;
        } else {
            return 0;
        }
    }
    public function getTrendingKeyWords()
    {

        $sql = "SELECT COUNT(*) AS total,keywords,type FROM `searchHistory` WHERE keywords LIKE  '%#%' GROUP BY keywords ORDER BY total DESC";
        $query = $this->db->query($sql);

        if ($query->num_rows() >= 1) {
           $res = $query->result();
            return $res;
        } else {
            return 0;
        }
    }
    public function userSearchHistory($userID)
    {

        $sql = "SELECT DISTINCT(keywords),type FROM `searchHistory` where userID = ? ORDER BY id DESC LIMIT 5";
        $query = $this->db->query($sql,array($userID));

        if ($query->num_rows() >= 1) {
           $res = $query->result();
            return $res;
        } else {
            return 0;
        }
    }
    public function getAllCategories()
    {

        $sql = "SELECT * FROM `categories` where isActive = 1 ";
        $query = $this->db->query($sql);

        if ($query->num_rows() >= 1) {
           $res = $query->result();
            return $res;
        } else {
            return 0;
        }
    }
    public function getCategoryData($categoryID)
    {

        $sql = "SELECT * FROM `categories` where isActive = 1 AND categoryID = ? ";
        $query = $this->db->query($sql,array($categoryID));

        if ($query->num_rows() >= 1) {
           $res = $query->row();
            return $res;
        } else {
            return 0;
        }
    }
    public function getNotifcations($userID,$type){
         $sql = "SELECT *  FROM `notifications` AS n LEFT JOIN users AS u ON n.senderID=u.userID  WHERE n.type = ? AND n.receiverID = ? AND n.senderID != $userID AND u.isActive=1 AND u.isVerified=1 GROUP BY n.dataID
        ORDER BY n.notificationID  DESC";
        $query = $this->db->query($sql,array($type,$userID));

        if ($query->num_rows() >= 1) {
            return $query->result();
        } else {
            return false;
        }
    }
   public function getNotifiedStories($storyID){
        $this->db->select('*');
         $this->db->where('isActive',1);
         $this->db->where('storyID',$storyID);
         $this->db->order_by('storyID','DESC');
        $query = $this->db->get('stories');

        if ($query->num_rows() >= 1) {
           $res = $query->result();
            return $res;
        } else {
            return 0;
        }
    }
    public function getNotifiyData($dataID,$userID,$type){
        $this->db->select('*');

         $this->db->where('dataID',$dataID);
         $this->db->where('senderID!=',$userID);
         $this->db->where('receiverID',$userID);
         $this->db->where('type',$type);

        $query = $this->db->get('notifications');

        if ($query->num_rows() >= 1) {
           $res = $query->result();
            return $res;
        } else {
            return 0;
        }
    }
     public function getCount($dataID,$userID,$type){
        $this->db->select('*');

         $this->db->where('dataID',$dataID);
         $this->db->where('senderID!=',$userID);
         $this->db->where('receiverID',$userID);
         $this->db->where('type',$type);
         $this->db->group_by('senderID');
        $query = $this->db->get('notifications');

        if ($query->num_rows() >= 1) {
           $res = $query->result();
            return $res;
        } else {
            return 0;
        }
    }

    public function likedNotificationList($storyID,$userID){
        $this->db->select('*');
				$this->db->from('likes AS l');
				$this->db->join('users AS u','l.userID=u.userID','LEFT');
        $this->db->where('l.storyID', $storyID);
				$this->db->where('u.isActive', '1');
				$this->db->where('u.isVerified', '1');
				$query = $this->db->get();

        if ($query->num_rows() >= 1) {
           $res = $query->result();
            return $res;
        } else {
            return 0;
        }
    }
		public function getVerificationCode($emailID)
	     {   $this->db->select('userName,verificationCode,emailIsVerified');
	         $this->db->from('users');
	         $this->db->where('emailId',$emailID);
	       //  $this->db->where('password != ""');
	         $query = $this->db->get();

	         return $query->row();
	     }
	     public function update123($where, $data)
	     { //  $this->db->where('password != ""');
	         $this->db->update('users', $data, $where);
	         return $this->db->affected_rows();
	     }

			 public function getChildAccount($userID){
					$this->db->select('*');
					$this->db->from('users');
					$this->db->where('parentID',$userID);
					$query = $this->db->get();
					    $res = $query->result();
	            return $res;

				}
				public function getUserCategories($userID){
 					$this->db->select('categoryID');
 					$this->db->from('userInterest');
 					$this->db->where('userID',$userID);
 					$query = $this->db->get();
					if ($query->num_rows() >= 1) {
						 $res = $query->result();
							return $res;
					} else {
							return 0;
					}

 				}
				public function updateCategory($where, $data)
 	     { //  $this->db->where('password != ""');
 	         $this->db->update('userInterest', $data, $where);
 	         return $this->db->affected_rows();
 	     }
			 public function deleteCategory($userID,$categoryID)
	     {

	         $sql = "DELETE FROM `userInterest` where userID=? AND categoryID = ? ";
	         $query = $this->db->query($sql,array($userID,$categoryID));
					 return ($this->db->affected_rows() > 0) ? true : false;
	     }
			 public function isCategorySelectedByUser($categoryID,$userID){
				 $this->db->select('*');
				 $this->db->from('userInterest');
				 $this->db->where('userID',$userID);
				 $this->db->where('categoryID',$categoryID);
				 $query = $this->db->get();
				 if ($query->num_rows() >= 1) {
						$res = $query->row();
						 return $res;
				 } else {
						 return 0;
				 }

			 }


}


?>
