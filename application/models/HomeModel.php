<?php
class HomeModel extends CI_Model{

public function validateEmp($emailID,$password){

         // Prep the query
         $this->db->where('emailID', $emailID);
          //$this->db->where('password', $password);
         $this->db->where('isActive', 1);


         // Run the query
         $query = $this->db->get('employee');
         // Let's check if there are any results
         if($query->num_rows() == 1)
         {
             $res = $query->row_array();


              $hashPass = $res['password'];

              if (password_verify($password, $hashPass)) {

             // If there is a user, then create session data
             $row = $query->row();
             $data = array(

                     'employeeID'=>$row->id,
                     'accountID'=> $row->accountID,
                     'validated' => true,

                     );
//
             $this->session->set_userdata('I2',$data);

           return true;
                    }

         return false;
     }
 }
	private function _get_datatables_query($term='',$string){ //term is value of $_REQUEST['search']['value']

        $column = array('','u.userName','s.storyTitle','s.isFeatured','s.created');

        $this->db->select('s.storyTitle,u.userName,s.storyID,s.userID,s.created,s.isFeatured');
        $this->db->from('stories AS s');
        $this->db->join('users AS u','s.userID=u.userID','LEFT');
      if($string!="")
      {

         $this->db->where_in('storyID',$string);
        }

      else
      {
        $this->db->where('s.isActive', 1);
      }




        if($term !=''){
        $this->db->where("(s.userID LIKE '%$term%' OR s.storyTitle LIKE '%$term%' OR s.storyID LIKE '%$term%' OR u.userName LIKE '%$term%')");
            }

        //$this->db->where('s.isActive', 1);




        if(isset($_REQUEST['order'])) // here order processing
        {
           $this->db->order_by($column[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
           $order = $this->order;
           $this->db->order_by(key($order), $order[key($order)]);
        }else{
            $this->db->order_by('s.storyID', 'desc');
        }
    }

	public function getStory($string){
		  $term = $_REQUEST['search']['value'];
       $this->_get_datatables_query($term,$string);

      if($_REQUEST['length'] != -1)

      $this->db->limit($_REQUEST['length'], $_REQUEST['start']);

		$query = $this->db->get();

      	$response = $query->result();
      		// return json_encode($response);
      	return $response;


	}
	function count_filtered_events($string){
      $term = $_REQUEST['search']['value'];
      $this->_get_datatables_query($term,$string);

      $query = $this->db->get();
      return $query->num_rows();
    }

    public function count_all_events($string){
      $this->db->from('stories');

      return $this->db->count_all_results();
    }
    function getPage($storyID)
    {
    	$sql = "SELECT * FROM pages WHERE storyID = ? ";
        $query = $this->db->query($sql,array($storyID));
        $res = $query->result();
        return $res;
    }
    #setFeaturedStory
  public function updateStory($storyID) {
          $this->db->from('stories');
          $this->db->where('storyID',$storyID);
           $query= $this->db->get();
           $response = $query->row();
           //print_r($response);

           if($response->isFeatured==0){
             $this->db->set('isFeatured',1);
           }
           elseif ($response->isFeatured==1) {
             $this->db->set('isFeatured',0);
           }
    $this->db->where('storyID',$storyID);
    $this->db->update('stories');
    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE) {
      return false;
    } else {

      return true;
    }

  }
  function getisFeatured($storyID)
  {
    $sql = "SELECT * FROM stories WHERE storyID = ? ";
      $query = $this->db->query($sql,array($storyID));
      $res = $query->row();
      return $res;
  }
private function _get_datatables_users($term=''){ //term is value of $_REQUEST['search']['value']

        $column = array('u.profilePic','u.userName','u.emailID','u.mobileNo','u.created');

        $this->db->select('*');
        $this->db->from('users AS u');

        if($term !=''){
        $this->db->where("(u.userName LIKE '%$term%' OR u.emailID LIKE '%$term%' OR u.mobileNo LIKE '%$term%' OR u.created LIKE '%$term%')");
            }
        $this->db->where('u.isActive', 1);
        $this->db->where('u.isVerified', 1);





        if(isset($_REQUEST['order'])) // here order processing
        {
           $this->db->order_by($column[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
           $order = $this->order;
           $this->db->order_by(key($order), $order[key($order)]);
        }else{
            $this->db->order_by('u.userName', 'asc');
        }
    }

  public function getUsers()
  {
    $term = $_REQUEST['search']['value'];
       $this->_get_datatables_users($term);

      if($_REQUEST['length'] != -1)

      $this->db->limit($_REQUEST['length'], $_REQUEST['start']);

    $query = $this->db->get();

        $response = $query->result();
          // return json_encode($response);
        return $response;

  }
  function count_filtered_users(){
      $term = $_REQUEST['search']['value'];
      $this->_get_datatables_users($term,$term);

      $query = $this->db->get();
      return $query->num_rows();
    }

    public function count_all_users(){
      $this->db->from('users');
      $this->db->where('isVerified',1);
      return $this->db->count_all_results();
    }
    private function _get_datatables_featured($term=''){ //term is value of $_REQUEST['search']['value']

        $column = array('','u.userName','s.storyTitle','s.isFeatured','s.created');

        $this->db->select('s.storyTitle,u.userName,s.storyID,s.userID,s.created,s.isFeatured');
        $this->db->from('stories AS s');
        $this->db->join('users AS u','s.userID=u.userID','LEFT');

        if($term !=''){
        $this->db->where("(s.userID LIKE '%$term%' OR s.storyTitle LIKE '%$term%' OR s.storyID LIKE '%$term%' OR u.userName LIKE '%$term%')");
            }
        $this->db->where('s.isFeatured', 1);
         $this->db->where('s.isActive', 1);




        if(isset($_REQUEST['order'])) // here order processing
        {
           $this->db->order_by($column[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
           $order = $this->order;
           $this->db->order_by(key($order), $order[key($order)]);
        }else{
            $this->db->order_by('s.storyID', 'desc');
        }
    }

  public function getFeaturedStories(){
      $term = $_REQUEST['search']['value'];
       $this->_get_datatables_featured($term);

      if($_REQUEST['length'] != -1)

      $this->db->limit($_REQUEST['length'], $_REQUEST['start']);

    $query = $this->db->get();

        $response = $query->result();
          // return json_encode($response);
        return $response;


  }
  function count_filtered_featured(){
      $term = $_REQUEST['search']['value'];
      $this->_get_datatables_featured($term,$term);

      $query = $this->db->get();
      return $query->num_rows();
    }

    public function count_all_featured(){
      $this->db->from('stories');
      $this->db->where('isFeatured',1);
      return $this->db->count_all_results();
    }
    private function _get_datatables_reported($term=''){ //term is value of $_REQUEST['search']['value']

        $column = array('s.storyTitle','COUNT(r.storyID)','r.reportedOn','','');

        $this->db->select('s.storyTitle,COUNT(r.storyID) AS total,r.reportedOn,r.id,r.storyID,s.isActive');
        $this->db->from('reportedStories AS r');
        $this->db->join('stories AS s','r.storyID=s.storyID','LEFT');
        $this->db->group_by('r.storyID');





        if($term !=''){
        $this->db->where("(s.storyTitle LIKE '%$term%' OR r.reportedOn LIKE '%$term%' )");
            }
        //$this->db->where('s.isFeatured', 1);




        if(isset($_REQUEST['order'])) // here order processing
        {
           $this->db->order_by($column[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
           $order = $this->order;
           $this->db->order_by(key($order), $order[key($order)]);
        }else{
            $this->db->order_by('r.storyID', 'desc');
        }
    }

  public function getReportedStories(){
      $term = $_REQUEST['search']['value'];
       $this->_get_datatables_reported($term);

      if($_REQUEST['length'] != -1)

      $this->db->limit($_REQUEST['length'], $_REQUEST['start']);

    $query = $this->db->get();

        $response = $query->result();
          // return json_encode($response);
        return $response;


  }
  function count_filtered_reported(){
      $term = $_REQUEST['search']['value'];
      $this->_get_datatables_reported($term,$term);

      $query = $this->db->get();
      return $query->num_rows();
    }

    public function count_all_reported(){
      $this->db->from('reportedStories');
    //  $this->db->where('isFeatured',1);
      return $this->db->count_all_results();
    }
    public function getReportedUsers($storyID)
    {
      $sql = "SELECT * FROM reportedStories AS r LEFT JOIN users AS u  ON r.userID=u.userID  WHERE storyID = ? GROUP BY r.userID ";
        $query = $this->db->query($sql,array($storyID));
        $res = $query->result();
        return $res;
    }
    public function getNoofUsers($storyID)
    {
      $sql = "SELECT DISTINCT (userID) FROM reportedStories  WHERE storyID = ?";
        $query = $this->db->query($sql,array($storyID));
        $res = $query->result();
        return $res;
    }
    private function _get_datatables_reportedStoryByUser($term='',$type){ //term is value of $_REQUEST['search']['value']

        $column = array('u.profilePic','u.userName','r.storyID','r.reportedOn');
      if($type==1)
      {
        $this->db->select('u.profilePic,COUNT(r.storyID) AS total,r.reportedOn,r.id,r.storyID,u.userName,u.userID,r.userID');
        $this->db->from('reportedStories AS r');
        $this->db->join('users AS u','r.userID=u.userID','LEFT');
      $this->db->group_by('r.userID');
    }
    else if($type==2)
    {
       $this->db->select('u.profilePic,COUNT(s.reportedCount) AS total,r.reportedOn,r.id,r.storyID,u.userName,u.userID');
        $this->db->from('reportedStories AS r');
         $this->db->join('stories AS s','r.storyID=s.storyID','LEFT');
        $this->db->join('users AS u','s.userID=u.userID','LEFT');
      $this->db->group_by('u.userID');
    }




        if($term !=''){
        $this->db->where("(u.userName LIKE '%$term%' OR r.reportedOn LIKE '%$term%' )");
            }
        //$this->db->where('s.isFeatured', 1);




        if(isset($_REQUEST['order'])) // here order processing
        {
           $this->db->order_by($column[$_REQUEST['order']['0']['column']], $_REQUEST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
           $order = $this->order;
           $this->db->order_by(key($order), $order[key($order)]);
        }else{
            $this->db->order_by('u.userName', 'desc');
        }
    }

  public function getReportedStoryByUser($type){
      $term = $_REQUEST['search']['value'];
       $this->_get_datatables_reportedStoryByUser($term,$type);

      if($_REQUEST['length'] != -1)

      $this->db->limit($_REQUEST['length'], $_REQUEST['start']);

    $query = $this->db->get();

        $response = $query->result();
          // return json_encode($response);
        return $response;


  }
  function count_filtered_reportedStoryByUser($type){
      $term = $_REQUEST['search']['value'];
      $this->_get_datatables_reportedStoryByUser($term,$type);

      $query = $this->db->get();
      return $query->num_rows();
    }

    public function count_all_reportedStoryByUser($type){
      $this->db->from('reportedStories');
    //  $this->db->where('isFeatured',1);
      return $this->db->count_all_results();
    }
     public function getNoofStoriesReportedByUsers($userID)
    {
      $sql = "SELECT DISTINCT(storyID) FROM reportedStories  WHERE userID = ?";
        $query = $this->db->query($sql,array($userID));
        $res = $query->result();
        return $res;
    }

     public function getReportedStory($userID)
    {
      $sql = "SELECT * FROM reportedStories AS r LEFT JOIN stories AS s  ON r.storyID=s.storyID  WHERE r.userID = ? GROUP BY r.storyID ";
        $query = $this->db->query($sql,array($userID));
        $res = $query->result();
        return $res;
    }
    public function getReportedStoriesOfUser($userID)
    {
      $sql = "SELECT * FROM reportedStories AS r LEFT JOIN stories AS s  ON r.storyID=s.storyID  WHERE s.userID = ? GROUP BY r.storyID ";
        $query = $this->db->query($sql,array($userID));
        $res = $query->result();
        return $res;
    }
}
?>
