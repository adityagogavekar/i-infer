<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apicustomer extends CI_Controller {

    /**
     * Codetreasure 07-02-2018 - Mandar
     *  
     */
    

    function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Kolkata");
        $this->load->model('Apimodelcustomer','apimodelcustomer');
        $this->load->helper('string');
        $this->load->helper('cookie');
        $this->load->library('session');
         require(APPPATH . "/third_party/pepocampaign/PepoCampaigns.class.php");

    // key and secret to be passed while initiation
    $this->pepo_campaigns = new PepoCampaigns(array(
      'key' => '2178b97ebd6b76971d6b061748b0d435',
        'secret' => '5f9a1e6498bb878a35fbe0eac4584474'
    ));
        $this->dbcharesc=$this->db->conn_id;

    }

    private function arrangetMyAddress($address){
          $array=explode(",",$address);     
        $array = array_filter($array, 'strlen');
        $array=array_values(array_filter(array_diff($array,array(" ","","``",",","  ","     "))));      
        $serviceAddress=implode(", ",$array);

    return $serviceAddress;   
   }

   private function base64url_encode($plainText)
   {
    return strtr(base64_encode($plainText), '+/=', '-_&');
   }

    private function base64url_decode($b64Text)
    {
    return base64_decode(strtr($b64Text, '-_&', '+/='));
    }



    function digitToWord($number)
    {
    list($integer, $fraction) = explode(".", (string) $number);

    $output = "";

    if ($integer{0} == "-")
    {
        $output = "negative ";
        $integer    = ltrim($integer, "-");
    }
    else if ($integer{0} == "+")
    {
        $output = "positive ";
        $integer    = ltrim($integer, "+");
    }

    if ($integer{0} == "0")
    {
        $output .= "zero";
    }
    else
    {
        $integer = str_pad($integer, 36, "0", STR_PAD_LEFT);
        $group   = rtrim(chunk_split($integer, 3, " "), " ");
        $groups  = explode(" ", $group);

        $groups2 = array();
        foreach ($groups as $g)
        {
            $groups2[] = $this->convertThreeDigit($g{0}, $g{1}, $g{2});
        }

        for ($z = 0; $z < count($groups2); $z++)
        {
            if ($groups2[$z] != "")
            {
                $output .= $groups2[$z] . $this->convertGroup(11 - $z) . (
                        $z < 11
                        && !array_search('', array_slice($groups2, $z + 1, -1))
                        && $groups2[11] != ''
                        && $groups[11]{0} == '0'
                            ? " and "
                            : ", "
                    );
            }
        }

        $output = rtrim($output, ", ");
    }

    if ($fraction > 0)
    {
        $output .= " cents";
        /*for ($i = 0; $i < strlen($fraction); $i++)
        {*/
            $output .= " " . $this->convertTwoDigit($fraction{0},$fraction{1});
       // }
    }

        return $output;
    }

function convertGroup($index)
{
    switch ($index)
    {
        case 11:
            return " Decillion";
        case 10:
            return " Nonillion";
        case 9:
            return " Octillion";
        case 8:
            return " Septillion";
        case 7:
            return " Sextillion";
        case 6:
            return " Quintrillion";
        case 5:
            return " Quadrillion";
        case 4:
            return " Trillion";
        case 3:
            return " Billion";
        case 2:
            return " Million";
        case 1:
            return " Thousand";
        case 0:
            return "";
    }
}

function convertThreeDigit($digit1, $digit2, $digit3)
{
    $buffer = "";

    if ($digit1 == "0" && $digit2 == "0" && $digit3 == "0")
    {
        return "";
    }

    if ($digit1 != "0")
    {
        $buffer .= $this->convertDigit($digit1) . " Hundred";
        if ($digit2 != "0" || $digit3 != "0")
        {
            $buffer .= " and ";
        }
    }

    if ($digit2 != "0")
    {
        $buffer .= $this->convertTwoDigit($digit2, $digit3);
    }
    else if ($digit3 != "0")
    {
        $buffer .= $this->convertDigit($digit3);
    }

    return $buffer;
}

function convertTwoDigit($digit1, $digit2)
{
    if ($digit2 == "0")
    {
        switch ($digit1)
        {
            case "1":
                return "Ten";
            case "2":
                return "Twenty";
            case "3":
                return "Thirty";
            case "4":
                return "Forty";
            case "5":
                return "Fifty";
            case "6":
                return "Sixty";
            case "7":
                return "Seventy";
            case "8":
                return "Eighty";
            case "9":
                return "Ninety";
        }
    } else if ($digit1 == "1")
    {
        switch ($digit2)
        {
            case "1":
                return "Eleven";
            case "2":
                return "Twelve";
            case "3":
                return "Thirteen";
            case "4":
                return "Fourteen";
            case "5":
                return "Fifteen";
            case "6":
                return "Sixteen";
            case "7":
                return "Seventeen";
            case "8":
                return "Eighteen";
            case "9":
                return "Nineteen";
        }
    } else
    {
        $temp = $this->convertDigit($digit2);
        switch ($digit1)
        {
            case "2":
                return "Twenty $temp";
            case "3":
                return "Thirty $temp";
            case "4":
                return "Forty $temp";
            case "5":
                return "Fifty $temp";
            case "6":
                return "Sixty $temp";
            case "7":
                return "Seventy $temp";
            case "8":
                return "Eighty $temp";
            case "9":
                return "Ninety $temp";
        }
    }
}

function convertDigit($digit)
{
    switch ($digit)
    {
        case "0":
            return "Zero";
        case "1":
            return "One";
        case "2":
            return "Two";
        case "3":
            return "Three";
        case "4":
            return "Four";
        case "5":
            return "Five";
        case "6":
            return "Six";
        case "7":
            return "Seven";
        case "8":
            return "Eight";
        case "9":
            return "Nine";
    }
}


    function validateMobile($mobile)
    {
        if(preg_match('/^[0-9]{10}+$/', $mobile)){
            
            return true;
        }

        return false;
    }

    function validateEmail($email)
    {   
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){

            return true;
        }

        return false;
    }

    #generate Otp
    private function generateOTP(){

        return mt_rand(100000, 999999);
    }

    function generateRandomString($length = 16) {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

    #generate Token
    private function getTokens(){

         do{
              $accessToken = random_string('unique');
               $flagAccess=$this->apimodelcustomer->checkAccessToken($accessToken);

        }while($flagAccess > 0);


        return $accessToken ;
    }

   #generate transactionID for paytm
    private function generateTransactionID(){

         do{
              $transactionID = $this->generateRandomString('22');
               $flagTxnID=$this->apimodelcustomer->checkTransactionID($transactionID);

        }while($flagTxnID > 0);


        return $transactionID ;
    }

    #generate refundID for paytm
    private function generateRefundID(){

         do{
              $refundID = "ref_".$this->generateRandomString('18');
              $flagRefID=$this->apimodelcustomer->checkRefundID($refundID);

        }while($flagRefID > 0);


        return $refundID ;
    }

    #sendSMSNEw
    private function sendSms2($number,$message){

            require_once APPPATH .'src/awsSDK/autoloader.php'; error_reporting(E_ALL); ini_set("display_errors", 1);
            
            $params = array( 'credentials' => array( 'key' => 'AKIAIOJ2KLAAQKXJUXOA', 'secret' => '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', ), 'region' => 'ap-southeast-1','version' => 'latest' ); 
            $sns = new \Aws\Sns\SnsClient($params);//"SenderID" => "Refreshed", ap-southeast-1
            $args = array( 'MessageAttributes' => [
                                                      'AWS.SNS.SMS.SenderID' => [
                                                           'DataType' => 'String',
                                                           'StringValue' => 'REF'
                                                    ]
                                                 ], 
                            "SMSType" => "Transactional", "Message" => $message , "PhoneNumber" => '+91'.$number );

            $result = $sns->publish($args); 
          //  print_r($result);exit;
            //var_dump(array_key_exists('200', $result));exit;
            if($result){
                
                return true;

            }else{
                return false;
            }
      }

      public function testInv()
{          $result = $this->sendSms2('8149921024', "test");
               
}

    #get Extension of file
    private function getExtension($str)
    {
    $i = strrpos($str,".");
    if (!$i) { return ""; }
    $l = strlen($str) - $i;
    $ext = substr($str,$i+1,$l);
    return $ext;
    }

/*****************Send Booking pepo campaign**************************/

public function bookingPepoCampaign($email,$custName,$jobIdentifier,$date,$time,$serviceAddress,$custMobNo,$pkgName,$pkgAmount,$jobID){

        $templateName="Booking Confirmation";
        
        $data=array('name' => $custName,
                    'fullName' => $custName,
                    'refID' =>  $jobIdentifier, 
                    'date' => date('j / m / y',strtotime($date)),            
                    'time' => date('g:i A',strtotime($time)),
                    'serviceAddress'=>$serviceAddress,
                    'mobileNo'=>$custMobNo,
                    'serviceName'=>$pkgName,
                    'typeOfService'=>$pkgName,
                    'price'=>"₹ ".$pkgAmount,
                    'subject'=>"Your Refreshed service is confirmed: Booking ID ".$jobIdentifier);
        #make email sent entry    
        $emailData=$this->apimodelcustomer->getJobsEmailByJobID($jobID);
        if($emailData){

            if($emailData->isBookingConfirmSent!=1){
                $response=$this->pepo_campaigns->send_transactional_email($email, $templateName, $data);
                $data= json_decode($response);

                if($data->error==null){

                  $data=array('isBookingConfirmSent' =>1);
                  $updateData=$this->apimodelcustomer->updateJobsEmail(array("id"=>$emailData->id),$data);
                 } 
            }

        }else{
           $response=$this->pepo_campaigns->send_transactional_email($email, $templateName, $data);
          $data= json_decode($response);
           if($data->error==null){

              $data=array('jobID'=>$jobID,
                          'isBookingConfirmSent' =>1,
                          'isActive'=>1,
                          'created'=>date('Y-m-d H:i:s'));
              $insertData=$this->apimodelcustomer->insertJobsEmail($data);
             } 
        }
}


/************* Generate REF ID *************************************/
public function generateRefId($jobType){

    #fetching last job Identifier                                                
    $lastID=$this->apimodelcustomer->getLastJobRefID($jobType);    
    $jobIdentifier=$lastID->jobIdentifier;

    #for special hub  & destination                   
    if($jobType==1){
        do{

            $checkJob=$this->apimodelcustomer->getJobByJobRefID($jobIdentifier);
            if($checkJob){

                $pos=3;
                $str=$jobIdentifier;
                $str1 = substr($str, 0, $pos);
                $str2 = substr($str, $pos);
                $jobIdentifier='RPN'.($str2+1);
                $flag=0;
            }else{
                $flag=1;
            }
        }while($flag<=0);
    }else{
        #for on Demand
        do{
          
          $checkJob=$this->apimodelcustomer->getJobByJobRefID($jobIdentifier);
          if($checkJob){
           
            $refNo=explode('REF',$jobIdentifier);
            $jobIdentifier="REF".str_pad(($refNo[1]+1),5,"0",STR_PAD_LEFT);
            $flag=0;
          
          }else{
            $flag=1;
          }
        }while($flag<=0); 
    }

    return $jobIdentifier; 

}


/*****************Upload Image**************************************/
#upload Image S3
    private function uploadImage($FILES){
         $s3file="";
         $message ="0";
        //Here you can add valid file extensions. 
        $valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
       
        if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name']) && $_FILES['file']['error'] == 0)
        {
        $name = $_FILES['file']['name'];
        $size = $_FILES['file']['size'];
        $tmp = $_FILES['file']['tmp_name'];
        }else if(isset($_FILES['profilePic']['name']) && !empty($_FILES['profilePic']['name']) && $_FILES['profilePic']['error'] == 0){
        $name = $_FILES['profilePic']['name'];
        $size = $_FILES['profilePic']['size'];
        $tmp = $_FILES['profilePic']['tmp_name'];    
        }
        $ext = $this->getExtension($name);
         
        if(strlen($name) > 0)
        {
        // File format validation
        if(in_array($ext,$valid_formats))
        {
         $this->load->library('aws_sdk');
        $dateCurrent=date('Y-m-d-H-i-s');   
        $bucket="refreshed-images";
        $img_name = $dateCurrent.random_string('alnum').random_string('unique');    
        //Rename image name.
        $image_name_actual = 'images/'.md5($img_name).'.'.$ext;
         $path='images/'.$image_name_actual;        

        try{
            $aws_object=$this->aws_sdk->saveObject(array(
                'Bucket'      => $bucket,  
                'Key'         =>  $image_name_actual,
                'ACL'         => 'public-read',
                'SourceFile'  => $tmp,
              //  'ContentType' => 'image/jpeg'
            ))->toArray();
             $s3file='http://'.$bucket.'.s3.amazonaws.com/'.$image_name_actual;
            $message = "1";
        }catch (Exception $e){
            //$error = "Something went wrong saving your file.\n".$e;
            $message="0"; 
        }       
        }else{
        $message = "0";
         
        }
        }else{
        $message = "0";
         
        }
        return array($message,$s3file);
    }
/******************Arrange Address********************************/
private function arrangeAddress($address){
          $array=explode(",",$address);     
        $array = array_filter($array, 'strlen');
        $array=array_values(array_filter(array_diff($array,array(" ","","``",",","  ","     "))));      
        $serviceAddress=implode(", ",$array);

    return $serviceAddress;   
   }    

/*****************FETCH Headers ************************************/
    #getHeaders
    private function getHeaders(){
        $accessToken ="";
        $headers = apache_request_headers();
            foreach ($headers as $key => $value) {
                switch ($key) {
                   case 'Authorization':
                   $accessToken = $value;
                   break;
        	   case 'authorization':
                   $accessToken = $value;
                   break;	
                }
            }
        return $accessToken;
    }

    /**************Get Job By Job ID*************************/

    private function getJobByID($jobID){
                $jobDetails=$this->apimodelcustomer->getJobByID($jobID);
                if($jobDetails){
                            #checking engineCAL reading is taken or not 
                            if($jobDetails->batteryHealth!=""||$jobDetails->batteryComment!=""||$jobDetails->dtcNum!=0){
                                $isEngineCAL=1;
                            }else{
                                $isEngineCAL=0;
                            }
                
                            #Job status
                $serviceStatus=$jobDetails->jobStatus;
                            switch ($jobDetails->jobStatus) {
                              case '0':
                              $jobStatus="Assigned";
                              break;
                              case '1':
                              $jobStatus="On The Way";
                              break;  
                              case '2':
                              $jobStatus="Reached";
                              break;
                              case '3':
                              $jobStatus="Job Started";
                              break;  
                              case '4':
                              $jobStatus="Completed";
                  $serviceStatus=5;
                              break;  
                              case '5':
                              $jobStatus="Completed";                               
                              break;          
                              case '6':
                              $jobStatus="Scheduled"; 
                              break;  
                              case '7':
                              $jobStatus="Canceled";   
                              break;  
                              default:
                              $jobStatus="-";
                              break;
                            }
                           
                            #For Petrol Pump seting service address of destination
                            if($jobDetails->type==0){$serviceAddress=$jobDetails->serviceAddress;}else{$serviceAddress=$jobDetails->destAddress;}

                            #fetching pre wash Images
                            $imageData =$this->apimodelcustomer->getJobImagesByJobID($jobDetails->jobID);

                            $preWashImages=isset($imageData["preWash"]) ? $imageData["preWash"] : (object)array();
                            $postWashImages=isset($imageData["postWash"]) ? $imageData["postWash"] : (object)array();

                            if(!empty($jobDetails->numberPlateAw)){
                            $imgData=(object)array("imageUrl"=>$jobDetails->numberPlateAw,
                                                   "imageType"=>"After Wash : License Plate No",
                                                   "title"=>"License Plate No");
                            array_push($preWashImages, $imgData);
                            }                            
                            if(!empty($jobDetails->frontViewAW)){
                            $imgData=(object)array("imageUrl"=>$jobDetails->frontViewAW,
                                                   "imageType"=>"After Wash : One Interior View",
                                                   "title"=>"One Interior View");
                            array_push($preWashImages, $imgData);
                            }
                            if(!empty($jobDetails->leftSideViewAW)){
                            $imgData=(object)array("imageUrl"=>$jobDetails->leftSideViewAW,
                                                   "imageType"=>"After Wash : One Side View",
                                                   "title"=>"One Side View");
                            array_push($preWashImages, $imgData);
                            }
                            if(!empty($jobDetails->rightSideViewAW)){
                            $imgData=(object)array("imageUrl"=>$jobDetails->rightSideViewAW,
                                                   "imageType"=>"After Wash : One Single Tyre View",
                                                   "title"=>"One Single Tyre View");
                            array_push($preWashImages, $imgData);
                            }
                            if(!empty($jobDetails->odometerReadingAW)){
                            $imgData=(object)array("imageUrl"=>$jobDetails->odometerReadingAW,
                                                   "imageType"=>"After Wash : Odometer Reading",
                                                   "title"=>"Odometer Reading");
                            array_push($preWashImages, $imgData);
                            }
                            if(!empty($jobDetails->backViewAW)){
                            $imgData=(object)array("imageUrl"=>$jobDetails->backViewAW,
                                                   "imageType"=>"After Wash",
                                                   "title"=>"");
                            array_push($preWashImages, $imgData);
                            }
                            #merging wash Images
                            $washImages=array_merge($preWashImages,$postWashImages);

                            #check for invoice
                            $invoiceData =$this->apimodelcustomer->getInvoiceByJobID($jobDetails->jobID);
                            if($invoiceData){
                                $encodedStr= $this->base64url_encode($invoiceData->invoiceID);
                                $invoiceLink='http://refreshednow.com/cms/invoice/'.$encodedStr;
                                
                            }else{
                                $invoiceLink="";
                            }
                            $addressData=$this->apimodelcustomer->getCustomerAddressByID($jobDetails->addressID);
                            if($addressData){
                                $pincode=$addressData->pincode;
                                 if(!empty($pincode)){
                                    $pincodeData=$this->apimodelcustomer->getPincodeDetails($pincode);
                                    if($pincodeData){
                                      switch ($jobDetails->pkgID) {
                                        case '1':
                                            $addressData->hubID=$pincodeData->parentHub2;
                                            break;
                                        case '2':
                                          $addressData->hubID=$pincodeData->parentHub;
                                            break;
                                        default:
                                            $addressData->hubID=0;
                                            break;
                                    }  
                                        
                                    }

                                }
                            }else{
                                $pincode="";
                                $addressData=(object)array();
                            }

                            #can cancel order
                           // $today=date('Y-m-d'); // H:i:s

                          //  $jobDate=date('Y-m-d',strtotime($jobDetails->date)); // H:i:s

                          //  if(strtotime($today)<strtotime($jobDate)&&($jobDetails->jobStatus!=7)){
                $today=date('Y-m-d  H:i:s'); //

                            $jobDate=date('Y-m-d',strtotime($jobDetails->date.' '.$jobDetails->serviceTime)); // H:i:s

                            $timediff = strtotime($jobDate) - strtotime($today);

                            if(($timediff > 86400)&&($jobDetails->jobStatus!=7)){
                            $canCancel="1";
                            }else{
                            $canCancel="0";    
                            }

                            #can book again
                            $today=date('Y-m-d H:i:s'); // H:i:s

                            $jobDate=date('Y-m-d H:i:s',strtotime($jobDetails->date.' '.$jobDetails->serviceTime)); // H:i:s

                            if((strtotime($today)>strtotime($jobDate))||$jobDetails->jobStatus==7){
                            $bookAgain="1";
                            }else{
                            $bookAgain="0";    
                            }

                            #for special hub
                            if($jobDetails->addressID==0){
                               $serviceAddressID= $jobDetails->billingAddressID;
                            }else{
                                $serviceAddressID=$jobDetails->addressID;
                            }

                            #cheking hub type
                            $hubData=$this->apimodelcustomer->getHubByID($jobDetails->hubDestID); 
                            if($hubData&&($hubData->type==1||$hubData->specialHub==1)){
                               $hubType="1"; 
				 $bookAgain="0";
                              /* if($hubData->type==1||$hubData->specialHub==0){
                                $bookAgain="0";
                               }*/
                            }else{
                                $hubType="0";
                            }
                            $custData =$this->apimodelcustomer->getCustByID($jobDetails->customerID);
                            $jobArr=array("jobName"=>$jobDetails->jobIdentifier, 
                                              "jobID"=>$jobDetails->jobID,
                                              "pkgID"=>$jobDetails->pkgID,
                                              "pkgDetails"=>$this->apimodelcustomer->getPackageDetails($jobDetails->pkgID),
                                              "serviceName"=>$jobDetails->pkgName,
                                              "serviceAmount"=>($jobDetails->pkgAmount!=0)?$jobDetails->pkgAmount-$jobDetails->discountAmount:0,
                                              "billingName"=>$jobDetails->billingName,
                                              "custName"=>$jobDetails->custName,
                                              "custEmailID"=>$jobDetails->custEmailID,
                                              "serviceAddressID"=>$serviceAddressID,
                                              "serviceAddress"=>$serviceAddress,
                                              "serviceAddressDetails"=>$addressData,
                                              "billingAddress"=>$jobDetails->billingAddress,
                                              "serviceDate"=>$jobDetails->date,
                                              "serviceTime"=>$jobDetails->reportingTime,
                                                "formatedTime"=>date('h:i A',strtotime($jobDetails->reportingTime)), 
                                              "reportingTime"=>$jobDetails->reportingTime,
                                              "onTheWay"=>$jobDetails->onTheWay,
                                              "reachTime"=>$jobDetails->reachTime,
                                              "startTime"=>$jobDetails->startTime,
                                              "endTime"=>$jobDetails->endTime,
                                              "jobStatus"=>$jobStatus,
                                              "serviceStatus"=>$serviceStatus,
                                              "paymentMethod"=>$jobDetails->paymentMethod,
                                              "paymentStatus"=>$jobDetails->paymentStatus,
                                              "paymentReceipt"=>$jobDetails->paymentReceipt,
                                              "chequeNo"=>$jobDetails->chequeNo,
                                              "amountPaid"=>$jobDetails->amountPaid,
                                              "vehicleID"=>$jobDetails->vehicleID,
                                              "vehicleMake"=>$jobDetails->vehicleMake,
                                              "vehicleModel"=>$jobDetails->vehicleModel,
                                              "vehicleNoPlate"=>$jobDetails->vehicleNoPlate,
                                              "vehicleDetails"=>array("carID"=>$jobDetails->vehicleID,
                                                                      "carMake"=>$jobDetails->vehicleMake,
                                                                      "carModel"=>$jobDetails->vehicleModel,
                                                                      "carNoPlate"=>$jobDetails->vehicleNoPlate),
                                              "oilReading"=>$jobDetails->oilReading,
                                              "carDetails"=>array(array("title"=>"Tyre Pressure",
                                                                      "parameters"=>array(array("key1"=>"Front Left",
                                                                                                "value1"=>($jobDetails->tyrepressureFL) ? $jobDetails->tyrepressureFL."/36" :"NA",
                                                                                                "unit1"=>"PSI",
                                                                                                "icon1"=>"http://refreshednow.com/cms/assets/icons/others/tyrePressure.png",
                                                                                                "key2"=>"Front Right",
                                                                                                "value2"=>($jobDetails->tyrepressureFR) ? $jobDetails->tyrepressureFR."/36" :"NA",
                                                                                                "unit2"=>"PSI",
                                                                                                "icon2"=>"http://refreshednow.com/cms/assets/icons/others/tyrePressure.png"),
                                                                                           array("key1"=>"Rear Left",
                                                                                                "value1"=>($jobDetails->tyrepressureRL) ? $jobDetails->tyrepressureRL."/36" :"NA",
                                                                                                "unit1"=>"PSI",
                                                                                                "icon1"=>"http://refreshednow.com/cms/assets/icons/others/tyrePressure.png",
                                                                                                "key2"=>"Rear Right",
                                                                                                "value2"=>($jobDetails->tyrepressureRR) ? $jobDetails->tyrepressureRR."/36" :"NA",
                                                                                                "unit2"=>"PSI",
                                                                                                "icon2"=>"http://refreshednow.com/cms/assets/icons/others/tyrePressure.png")
                                                                                           ),
                                                                           "flag"=>0),
                                                                 array("title"=>"Battery Details",
                                                                  "parameters"=>array(array("key1"=>"Battery Health",
                                                                                            "value1"=>($jobDetails->batteryHealth) ? $jobDetails->batteryHealth."/5" :"NA",
                                                                                            "unit1"=>"",
                                                                                            "icon1"=>"http://refreshednow.com/cms/assets/icons/others/CarBattery.png",
                                                                                            "key2"=>"Battery Comment",
                                                                                            "value2"=>$jobDetails->batteryComment,
                                                                                            "unit2"=>"",
                                                                                            "icon2"=>""),
                                                                                      ),
                                                                  "flag"=>1),
                                                                array("title"=>"Tyre Tread Depth",
                                                                  "parameters"=>array(array("key1"=>"Front Left",
                                                                                            "value1"=>($jobDetails->tyreTreadDepthFL) ? $jobDetails->tyreTreadDepthFL:"NA",
                                                                                            "unit1"=>"mm",
                                                                                            "icon1"=>"http://refreshednow.com/cms/assets/icons/others/TyreTradeDepth.png",
                                                                                            "key2"=>"Front Right",
                                                                                            "value2"=>($jobDetails->tyreTreadDepthFR) ? $jobDetails->tyreTreadDepthFR :"NA",
                                                                                            "unit2"=>"mm",
                                                                                            "icon2"=>"http://refreshednow.com/cms/assets/icons/others/TyreTradeDepth.png"),
                                                                                       array("key1"=>"Rear Left",
                                                                                            "value1"=>($jobDetails->tyreTreadDepthRL) ? $jobDetails->tyreTreadDepthRL :"NA",
                                                                                            "unit1"=>"mm",
                                                                                            "icon1"=>"http://refreshednow.com/cms/assets/icons/others/TyreTradeDepth.png",
                                                                                            "key2"=>"Rear Right",
                                                                                            "value2"=>($jobDetails->tyreTreadDepthRR) ? $jobDetails->tyreTreadDepthRR :"NA",
                                                                                            "unit2"=>"mm",
                                                                                            "icon2"=>"http://refreshednow.com/cms/assets/icons/others/TyreTradeDepth.png")),
                                                                  "flag"=>0),
                                                                array("title"=>"Other Details",
                                                                  "parameters"=>array(array("key1"=>"Oil Reading",
                                                                                            "value1"=>"NA",
                                                                                            "unit1"=>"",
                                                                                            "icon1"=>"http://refreshednow.com/cms/assets/icons/others/oilReading",
                                                                                            "key2"=>"Odometer Reading",
                                                                                            "value2"=>($jobDetails->odometerReading) ? $jobDetails->odometerReading :"NA",
                                                                                            "unit2"=>"Kms",
                                                                                            "icon2"=>"http://refreshednow.com/cms/assets/icons/others/odometerReading.png"),
                                                                                     ),
                                                                  "flag"=>0),
                                                                ),
                                              /*"tyrepressureFL"=>$jobDetails->tyrepressureFL,
                                              "tyrepressureFR"=>$jobDetails->tyrepressureFR,
                                              "tyrepressureRL"=>$jobDetails->tyrepressureRL,
                                              "tyrepressureRR"=>$jobDetails->tyrepressureRR,
                                              "tyreTreadDepthFL"=>$jobDetails->tyreTreadDepthFL,
                                              "tyreTreadDepthFR"=>$jobDetails->tyreTreadDepthFR,
                                              "tyreTreadDepthRL"=>$jobDetails->tyreTreadDepthRL,
                                              "tyreTreadDepthRR"=>$jobDetails->tyreTreadDepthRR,   
                                              "engDiagnoError"=>$jobDetails->engDiagnoError,
                                              "batteryHealth"=>$jobDetails->batteryHealth,
                                              "batteryComment"=>$jobDetails->batteryComment,*/
                                              "hubID"=>$jobDetails->hubDestID,
                                              "hubName"=>$jobDetails->destName,
                                              "hubType"=>$hubType,
                                              "type"=>$jobDetails->type,
                                              "isEngineCAL"=>$isEngineCAL,
                                              "washImages"=>$washImages,                                            
                                              "rating"=>$jobDetails->rating,
                                              "invoiceLink"=>$invoiceLink,
                                              "carHealthReport"=>"", //please change this with proper link
                                              "pincode"=>$pincode,
                                              "canCancel"=>$canCancel,
                                              "canBookAgain"=>$bookAgain,
                                              "isNotifyMe"=>$jobDetails->isNotifyMe,
                                              "contactNo"=>$jobDetails->contactNo  
                                            );  
                    }else{
                        $jobArr=false;
                    }                             


        return $jobArr;
    }



    public function index()
    {
        $this->load->view('welcome_message');
    }


    /************************************ sendOTP ************************************/

    #sendOTP
    public function sendOTP(){

        if($_SERVER['REQUEST_METHOD'] == "POST"){           
      
            $data = json_decode(file_get_contents('php://input'), true);  
            $mobileNo = isset($data['mobileNo']) ? mysqli_real_escape_string($this->dbcharesc,$data['mobileNo']) : '';

            if(!empty($mobileNo)){   

                if($this->validateMobile($mobileNo)){            
            
            if($mobileNo!="9665006351"){    
                    $otp=$this->generateOTP();
                     }else{
            $otp=123456;
            }
                    $updateContactNo =$this->apimodelcustomer->updateVerification(array("mobileNo"=>$mobileNo,"isActive"=>1),array("isActive"=>0));

                    $insertVerification =$this->apimodelcustomer->insertVerification(array("mobileNo"=>$mobileNo,"otp"=>$otp,"isActive"=>1,"created"=>date('Y-m-d H:i:s')));

                    if($insertVerification){  

                            /*******Send OTP*******/
                            $message = 'Your unique verification code for Refreshed Car Care is '.$otp.'. Thank you.';   
             //  if($mobileNo!="9665006351"){                       
                            $sendOTP=$this->sendSms2($mobileNo,$message);
                     //       }else{
         //   $sendOTP=true;
          //  }
                            if($sendOTP){
                                $arrResponse = array("status" => 200, "message" => "OTP sent successfully!");
                                $statusCode=200;
                            }else{
                                $arrResponse = array("status" => 500, "message" => "Unable to send otp !");
                                $statusCode=500;
                            }
                            
                        }else{
                            $arrResponse = array("status" => 500, "message" => "Something went wrong !");
                            $statusCode=500;
                        }                    
                }else{
                     $arrResponse = array("status" => 400, "message" => "Enter valid mobile no !");
                     $statusCode=400;
                }                       
                
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                $statusCode=400;
            }

        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
            $statusCode=405;
        }
        http_response_code($statusCode);

      echo json_encode($arrResponse);
    
    }

    /************************************ verifyOTP ************************************/
    #verifyOTP
    public function verifyOTP(){

        if($_SERVER['REQUEST_METHOD'] == "POST"){           
      
            $data = json_decode(file_get_contents('php://input'), true);  
            $mobileNo = isset($data['mobileNo']) ? mysqli_real_escape_string($this->dbcharesc,$data['mobileNo']) : '';
            $OTP = isset($data['OTP']) ? mysqli_real_escape_string($this->dbcharesc,$data['OTP']) : '';
            $androidKey = isset($data['androidKey']) ? mysqli_real_escape_string($this->dbcharesc,$data['androidKey']) : '';
            $iosKey = isset($data['iosKey']) ? mysqli_real_escape_string($this->dbcharesc,$data['iosKey']) : '';
            if(!empty($mobileNo)&&!empty($OTP)){   

                if($this->validateMobile($mobileNo)){            

                                     
                    $verifyCode =$this->apimodelcustomer->verifyCode($mobileNo);
                  
                
                    if($verifyCode->otp==$OTP){  
                            $custData =$this->apimodelcustomer->getCustomerByMobNo($mobileNo);
                            if($custData){

                                if(empty($custData->custToken)){
                                    $accesstoken=$this->getTokens();
                                    $updateCustomer=$this->apimodelcustomer->updateCustomer(array("custID"=>$custData->custID),array("custToken"=>$accesstoken));
                                }

                                /**************** Inserting Device Tokens *************/
                                #inserting / updating android device token
                                if(!empty($androidKey)){
                                    $tokenData =$this->apimodelcustomer->checkDeviceToken($custData->custID,$androidKey,0);

                                    if(!$tokenData){
                                        $data=array("custID"=>$custData->custID,
                                                    "deviceToken"=>$androidKey,
                                                    "deviceType"=>0,
                                                    "isActive"=>1,
                                                    "created"=>date('Y-m-d H:i:s'));
                                        $tokenData =$this->apimodelcustomer->insertDeviceToken($data);
                                    }else{
                                        $updateDeviceToken=$this->apimodelcustomer->updateDeviceToken(array("deviceToken"=>$androidKey,"deviceType"=>0,"custID"=>$custData->custID),array("isActive"=>1));
                                    }
                                    
                                }

                                #inserting / updating ios device token
                                if(!empty($iosKey)){
                                    $tokenData =$this->apimodelcustomer->checkDeviceToken($custData->custID,$iosKey,1);

                                    if(!$tokenData){
                                        $data=array("custID"=>$custData->custID,
                                                    "deviceToken"=>$iosKey,
                                                    "deviceType"=>1,
                                                    "isActive"=>1,
                                                    "created"=>date('Y-m-d H:i:s'));
                                        $tokenData =$this->apimodelcustomer->insertDeviceToken($data);
                                    }else{
                                        $updateDeviceToken=$this->apimodelcustomer->updateDeviceToken(array("deviceToken"=>$iosKey,"deviceType"=>1,"custID"=>$custData->custID),array("isActive"=>1));
                                    }
                                    
                                }
                               
                                #pass access token to header
                                if(!empty($custData->custToken)){
                                header("accesstoken: $custData->custToken");
                                }
                                $isCustomerExist="1";
                                unset($custData->custToken);

                                #Login record
                                $lastLoginData=array("custID"=>$custData->custID,
                                                    "mobileNo"=>$mobileNo,
                                                    "ipaddress"=>$this->input->ip_address(),
                                                    "loginType"=>1,
                                                    "isActive"=>1,
                                                    "created"=>date('Y-m-d H:i:s'));
                                $insertLastLogin =$this->apimodelcustomer->insertLastLogin($lastLoginData);

                            }else{
                                $isCustomerExist="0";
                                $custData=(object)array();
                            }   
                            $arrResponse = array("status" => 200, "message" => "OTP verified successfully!","isCustomerExist"=>$isCustomerExist,"data"=>$custData);
                            $statusCode=200;
                           
                        }else{
                            $arrResponse = array("status" => 400, "message" => "Entered opt is wrong !");
                            $statusCode=400;
                        }                    
                }else{
                     $arrResponse = array("status" => 400, "message" => "Enter valid mobile no !");
                     $statusCode=400;
                }                       
                
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                $statusCode=400;
            }

        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
            $statusCode=405;
        }
        http_response_code($statusCode); 
      echo json_encode($arrResponse);
    
    }


    /************************************ registerCustomer ************************************/
    #registerCustomer
    public function registerCustomer(){

        if($_SERVER['REQUEST_METHOD'] == "POST"){           
      
            /*$data = json_decode(file_get_contents('php://input'), true);  
            $mobileNo = isset($data['mobileNo']) ? mysqli_real_escape_string($this->dbcharesc,$data['mobileNo']) : '';
            $custName=isset($data['custName']) ? mysqli_real_escape_string($this->dbcharesc,$data['custName']) : '';
            $custEmailID=isset($data['custEmailID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custEmailID']) : '';
            $androidKey = isset($data['androidKey']) ? mysqli_real_escape_string($this->dbcharesc,$data['androidKey']) : '';
            $iosKey = isset($data['iosKey']) ? mysqli_real_escape_string($this->dbcharesc,$data['iosKey']) : '';*/

            $mobileNo = isset($_POST['mobileNo']) ? mysqli_real_escape_string($this->dbcharesc,$_POST['mobileNo']) : '';
            $custName=isset($_POST['custName']) ? mysqli_real_escape_string($this->dbcharesc,$_POST['custName']) : '';
            $custEmailID=isset($_POST['custEmailID']) ? mysqli_real_escape_string($this->dbcharesc,$_POST['custEmailID']) : '';
            $androidKey = isset($_POST['androidKey']) ? mysqli_real_escape_string($this->dbcharesc,$_POST['androidKey']) : '';
            $iosKey = isset($_POST['iosKey']) ? mysqli_real_escape_string($this->dbcharesc,$_POST['iosKey']) : '';
            if(!empty($custName)&&!empty($mobileNo)){   

                if($this->validateMobile($mobileNo)){            

                    if(!empty($custEmailID)){
                    if($this->validateEmail($custEmailID)){ 
                        $validEmail=true;
                    }else{
                        $validEmail=false; 
                    }
                    }else{
                        $validEmail=true; 
                    }
                     
                    if($validEmail){                 
                    $verifyCode =$this->apimodelcustomer->verifyCode($mobileNo);

                
                    if($verifyCode&&$verifyCode->isActive==1){  
                            $custData =$this->apimodelcustomer->getCustomerByMobNo($mobileNo);
                            if($custData){
                                $arrResponse = array("status" => 409, "message" => "Customer already exists with this mobile number.");
                                $statusCode=409;
                            }else{
                               $accesstoken=$this->getTokens();

                                #uplod profile Pic
                               $profilePic="";
                               if(isset($_FILES['profilePic']['name']) && $_FILES["profilePic"]["error"] == 0 && !empty($_FILES['profilePic']['name'])){
                                $uploadImage= $this->uploadImage($_FILES["profilePic"]["tmp_name"]);
                                if($uploadImage[0]==1){
                                    $profilePic=$uploadImage[1];
                                }    
                                }

                                $data=array("custName"=>$custName,
                                           "custEmailID"=>$custEmailID,
                                           "custMobNo"=>$mobileNo,
                                           "custToken"=>$accesstoken,
                                           "custProfilePic"=>$profilePic,
                                           "createdFrom"=>2,
                                           "custIsActive"=>1,
                                           "created"=>date('Y-m-d H:i:s'));
                               $insertCustomer=$this->apimodelcustomer->insertCustomer($data);
                               if($insertCustomer){
                                    $custData =$this->apimodelcustomer->getCustomerByMobNo($mobileNo);
                                    if($custData){

                                         /**************** Inserting Device Tokens *************/

                                            if(!empty($androidKey)){
                                                $tokenData =$this->apimodelcustomer->checkDeviceToken($custData->custID,$androidKey,0);

                                                if(!$tokenData){
                                                    $data=array("custID"=>$custData->custID,
                                                                "deviceToken"=>$androidKey,
                                                                "deviceType"=>0,
                                                                "isActive"=>1,
                                                                "created"=>date('Y-m-d H:i:s'));
                                                    $tokenData =$this->apimodelcustomer->insertDeviceToken($data);
                                                }else{
                                                    $updateDeviceToken=$this->apimodelcustomer->updateDeviceToken(array("deviceToken"=>$androidKey,"deviceType"=>0,"custID"=>$checkCustomer->custID),array("isActive"=>1));
                                                }
                                                
                                            }

                                            if(!empty($iosKey)){
                                                $tokenData =$this->apimodelcustomer->checkDeviceToken($custData->custID,$iosKey,1);

                                                if(!$tokenData){
                                                    $data=array("custID"=>$custData->custID,
                                                                "deviceToken"=>$iosKey,
                                                                "deviceType"=>1,
                                                                "isActive"=>1,
                                                                "created"=>date('Y-m-d H:i:s'));
                                                    $tokenData =$this->apimodelcustomer->insertDeviceToken($data);
                                                }else{
                                                    $updateDeviceToken=$this->apimodelcustomer->updateDeviceToken(array("deviceToken"=>$iosKey,"deviceType"=>1,"custID"=>$checkCustomer->custID),array("isActive"=>1));
                                                }
                                                
                                            }
                                                if(!empty($custData->custToken)){
                                                header("accesstoken: $custData->custToken");
                                                }
                                                $isCustomerExist="1";
                                                unset($custData->custToken);

                                                #Login record
                                                $lastLoginData=array("custID"=>$custData->custID,
                                                                    "mobileNo"=>$mobileNo,
                                                                    "ipaddress"=>$this->input->ip_address(),
                                                                    "loginType"=>1,
                                                                    "isActive"=>1,
                                                                    "created"=>date('Y-m-d H:i:s'));
                                                $insertLastLogin =$this->apimodelcustomer->insertLastLogin($lastLoginData);
                                        }else{
                                                $isCustomerExist="0";
                                        }   
                                    $arrResponse = array("status" => 200, "message" => "Registration successfull!","isCustomerExist"=>$isCustomerExist,"data"=>$custData);
                               
                                $statusCode=200;
                                }else{
                                     $arrResponse = array("status" => 500, "message" => "Something went wrong !");
                                     $statusCode=500;
                                }
                            }   
                           
                           
                        }else{
                            $arrResponse = array("status" => 403, "message" => "You are not valid user. Please verify your mobile number !");
                            $statusCode=403;
                        }                   
                    }else{
                     $arrResponse = array("status" => 400, "message" => "Enter valid email !");
                     $statusCode=400;
                }     
                }else{
                     $arrResponse = array("status" => 400, "message" => "Enter valid mobile no !");
                     $statusCode=400;
                }                       
                
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                $statusCode=400;
            }

        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");

         $statusCode=405;
        }
       http_response_code($statusCode); 
      echo json_encode($arrResponse);
    
    }

    /************************************ Update Customer ************************************/
    #updateCustomer
    public function updateCustomer(){

        if($_SERVER['REQUEST_METHOD'] == "POST"){           
      
            /*$data = json_decode(file_get_contents('php://input'), true);  
            $mobileNo = isset($data['mobileNo']) ? mysqli_real_escape_string($this->dbcharesc,$data['mobileNo']) : '';
            $custName=isset($data['custName']) ? mysqli_real_escape_string($this->dbcharesc,$data['custName']) : '';
            $custEmailID=isset($data['custEmailID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custEmailID']) : '';
            $androidKey = isset($data['androidKey']) ? mysqli_real_escape_string($this->dbcharesc,$data['androidKey']) : '';
            $iosKey = isset($data['iosKey']) ? mysqli_real_escape_string($this->dbcharesc,$data['iosKey']) : '';*/

           // $mobileNo = isset($_POST['mobileNo']) ? mysqli_real_escape_string($this->dbcharesc,$_POST['mobileNo']) : '';
            $custID=isset($_POST['custID']) ? mysqli_real_escape_string($this->dbcharesc,$_POST['custID']) : '';
            $custName=isset($_POST['custName']) ? mysqli_real_escape_string($this->dbcharesc,$_POST['custName']) : '';
            $custEmailID=isset($_POST['custEmailID']) ? mysqli_real_escape_string($this->dbcharesc,$_POST['custEmailID']) : '';
            $androidKey = isset($_POST['androidKey']) ? mysqli_real_escape_string($this->dbcharesc,$_POST['androidKey']) : '';
            $iosKey = isset($_POST['iosKey']) ? mysqli_real_escape_string($this->dbcharesc,$_POST['iosKey']) : '';

            $getHeaders=$this->getHeaders();
                   
           
            if(!empty($getHeaders)&&!empty($custName)){
                $dataArr=array();
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
            
                if($checkCustomer&&$custID==$checkCustomer->custID){  

                                #uplod profile Pic
                               
                               $profilePic="";
                               $error="";
                               if(isset($_FILES['profilePic']['name']) && $_FILES["profilePic"]["error"] == 0 && !empty($_FILES['profilePic']['name'])){
                                $uploadImage= $this->uploadImage($_FILES["profilePic"]["tmp_name"]);
                                if($uploadImage[0]==1){
                                    $profilePic=$uploadImage[1];
                                }  
                                }else{
                                    $error= "Image not found";
                                }

                                if(!empty($custName)){
                                $data["custName"]=$custName;
                                }
                              //  if(!empty($custEmailID)){
                                $data["custEmailID"]=strtolower($custEmailID);
                                //}
                                if(!empty($profilePic)){
                                $data["custProfilePic"]=$profilePic;
                                }
                                
                               $updateCustomer=$this->apimodelcustomer->updateCustomer(array('custID' =>$checkCustomer->custID),$data);
                               
                                if($updateCustomer){
                                    $custData =$this->apimodelcustomer->getCustByID($checkCustomer->custID);
                                    $checkMembership=$this->apimodelcustomer->checkCustomerMembership($custData->custID);    
                                    if($checkMembership){
                                        $custData->ismember=1;
                                    }else{
                                        $custData->ismember=0;
                                    }
                                           
                                    $arrResponse = array("status" => 200, "message" => "Customer updated successfully!","data"=>$custData,"error"=>$error);
                               
                                    $statusCode=200;
                               
                                }else{
                                    $arrResponse = array("status" => 500, "message" => "Something went wrong !");
                                    $statusCode=500;
                                }   
                           
                           
                        }else{
                            $arrResponse = array("status" => 403, "message" => "You are not valid user!");
                            $statusCode=403;
                        }         
                
                }else{
                    $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                    $statusCode=400;
                }

            }else{

            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");

         $statusCode=405;
        }
       http_response_code($statusCode); 
      echo json_encode($arrResponse);
    
    }

    /************************************ getCarMake ************************************/
    public function getCarMake(){
        

        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $getHeaders=$this->getHeaders();
            $data = json_decode(file_get_contents('php://input'), true); 
            
            $searchKey = isset($data['searchKey']) ? mysqli_real_escape_string($this->dbcharesc,$data['searchKey']) : '';
            $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            if(!empty($getHeaders)){
                $dataArr=array();
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
            
                if($checkCustomer&&$custID==$checkCustomer->custID){
                                    
                        $carMake=$this->apimodelcustomer->getCarMake($searchKey);
                                                       
 
                    $arrResponse = array("status" => 200, "data" =>$carMake); 
                    $statusCode=200;

                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
            $statusCode=405;
        }
       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }

    /************************************ getCarModel ************************************/
    public function getCarModel(){        

        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $dataArr=array();
            $getHeaders=$this->getHeaders();
            $data = json_decode(file_get_contents('php://input'), true); 
            $make = isset($data['make']) ? mysqli_real_escape_string($this->dbcharesc,$data['make']) : '';
            $searchKey = isset($data['searchKey']) ? mysqli_real_escape_string($this->dbcharesc,$data['searchKey']) : '';
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            if(!empty($getHeaders)&&!empty($make)){
                $dataArr=array();
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
            
                if($checkCustomer&&$custID==$checkCustomer->custID){
                                    
                        $carModel=$this->apimodelcustomer->getCarModels($make,$searchKey);
                                                      
 
                    $arrResponse = array("status" => 200, "data" =>$carModel); 
                    $statusCode=200;

                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
            $statusCode=405;
        }
       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }

    /************************************ addCar ************************************/
    #addCar
    public function addCar(){

        if($_SERVER['REQUEST_METHOD'] == "POST"){
           
            $data = json_decode(file_get_contents('php://input'), true);  
           
            $carMake = isset($data['carMake']) ? mysqli_real_escape_string($this->dbcharesc,$data['carMake']) : '';
            $carModel = isset($data['carModel']) ? mysqli_real_escape_string($this->dbcharesc,$data['carModel']) : '';
            $carNoPlate = isset($data['carNoPlate']) ? mysqli_real_escape_string($this->dbcharesc,$data['carNoPlate']) : '';
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            $getHeaders=$this->getHeaders();
            if(!empty($carMake)&&!empty($carModel)&&!empty($getHeaders)){

                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);

                if($checkCustomer&&$custID==$checkCustomer->custID){  
                        
                
                        $insertData=array("custID"=>$checkCustomer->custID,
                                    "carMake"=>$carMake,
                                    "carModel"=>$carModel,
                                    "carNoPlate"=>$carNoPlate,
                                    "carIsActive"=>1,
                                    "created"=>date('Y-m-d H:i:s'));

                        $insertCar=$this->apimodelcustomer->insertCar($insertData);

                        $carID= $this->db->insert_id();

                        if($insertCar){

                            $carData=$this->apimodelcustomer->getCustomerCarsByID($carID);

                            $arrResponse = array("status" => 200, "message" => "Car registered successfully !", "carData"=>$carData);
                            $statusCode=200;
                        }else{
                            $arrResponse = array("status" => 500, "message" => "Error while registering  Car!");
                            $statusCode=500;
                        }   
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }    
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
            $statusCode=405;
        }
       http_response_code($statusCode); 
      echo json_encode($arrResponse);    
    }

    /************************************ updateCar ************************************/
    #updateCar
    public function updateCar(){

        if($_SERVER['REQUEST_METHOD'] == "PUT"){
           
            $data = json_decode(file_get_contents('php://input'), true);  
          
            $carID = isset($data['carID']) ? mysqli_real_escape_string($this->dbcharesc,$data['carID']) : '';
            $carMake = isset($data['carMake']) ? mysqli_real_escape_string($this->dbcharesc,$data['carMake']) : '';
            $carModel = isset($data['carModel']) ? mysqli_real_escape_string($this->dbcharesc,$data['carModel']) : '';
            $carNoPlate = isset($data['carNoPlate']) ? mysqli_real_escape_string($this->dbcharesc,$data['carNoPlate']) : '';
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            $getHeaders=$this->getHeaders();
            if(!empty($carMake)&&!empty($carModel)&&!empty($carID)&&!empty($getHeaders)){

                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);

                if($checkCustomer&&$custID==$checkCustomer->custID){  
                    
                    $carData=$this->apimodelcustomer->getCustomerCarsByID($carID);
                    if($carData&&($carData->custID==$checkCustomer->custID)){
                        $updateData=array(
                                    "carMake"=>$carMake,
                                    "carModel"=>$carModel,
                                    "carNoPlate"=>$carNoPlate
                                   );

                        $updateCar=$this->apimodelcustomer->updateCar(array("carID"=>$carID),$updateData);

                
                            if($updateCar){

                                $carData=$this->apimodelcustomer->getCustomerCarsByID($carID);

                                $arrResponse = array("status" => 200, "message" => "Car Updated successfully !", "carData"=>$carData);
                                $statusCode=200;
                            }else{
                                $arrResponse = array("status" => 500, "message" => "Error while updating  Car!");
                                $statusCode=500;
                            } 
                        }else{
                            $arrResponse = array("status" => 403, "message" => "Oops, please try again !"); 
                            $statusCode=403;  
                        }  
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }    
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
            $statusCode=405;
        }
       http_response_code($statusCode); 
      echo json_encode($arrResponse);    
    }


     /************************************ deleteCar ************************************/
    #deleteCar
    public function deleteCar(){

        if($_SERVER['REQUEST_METHOD'] == "DELETE"){
           
            $data = json_decode(file_get_contents('php://input'), true);  
          
            $carID = isset($data['carID']) ? mysqli_real_escape_string($this->dbcharesc,$data['carID']) : '';
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            
            $getHeaders=$this->getHeaders();
            if(!empty($carID)&&!empty($getHeaders)){

                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);

                if($checkCustomer&&$custID==$checkCustomer->custID){  
                    
                    $carData=$this->apimodelcustomer->getCustomerCarsByID($carID);
                    if($carData&&($carData->custID==$checkCustomer->custID)){
                        $updateData=array(
                                    "carIsActive"=>0
                                   );

                        $updateCar=$this->apimodelcustomer->updateCar(array("carID"=>$carID),$updateData);
                
                            if($updateCar){                               
                                $arrResponse = array("status" => 200, "message" => "Car deleted successfully !");
                                $statusCode=200;
                            }else{
                                $arrResponse = array("status" => 500, "message" => "Error while deleting  Car!");
                                $statusCode=500;
                            } 
                        }else{
                            $arrResponse = array("status" => 403, "message" => "Oops, please try again !");   
                            $statusCode=403;
                        }  
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }    
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
            $statusCode=405;
        }
       http_response_code($statusCode); 
      echo json_encode($arrResponse);    
    }

    /************************************ getCars ************************************/
    public function getCars(){        

        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $dataArr=array();
            $getHeaders=$this->getHeaders();
            
            $data = json_decode(file_get_contents('php://input'), true);  
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            $pincode = isset($data['pincode']) ? mysqli_real_escape_string($this->dbcharesc,$data['pincode']) : '';
            if(!empty($getHeaders)){
                $dataArr=array();
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
            
                if($checkCustomer&&$custID==$checkCustomer->custID){
                       $carData=$this->apimodelcustomer->getCustomerCars($checkCustomer->custID);              
                            $arrResponse = array("status" => 200, "message"=>"success", "data" =>$carData); 
                            $statusCode=200;
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                 $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
             $statusCode=405;
        }

       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }

    /************************************ validatePincode ************************************/
    public function validatePincode(){        

        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $dataArr=array();
            $getHeaders=$this->getHeaders();
          
            $data = json_decode(file_get_contents('php://input'), true);  
           
            $pincode = isset($data['pincode']) ? mysqli_real_escape_string($this->dbcharesc,$data['pincode']) : '';
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
             $pkgID=isset($data['pkgID']) ? mysqli_real_escape_string($this->dbcharesc,$data['pkgID']) : '';
            if(!empty($getHeaders)&&!empty($pincode)&&!empty($pkgID)){
                $dataArr=array();
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
            
                if($checkCustomer&&$custID==$checkCustomer->custID){
                 $data=array("pincode"=>$pincode,"custID"=>$custID,"created"=>date('Y-m-d H:i:s'));
                 $insertPincodeReq=$this->apimodelcustomer->insertPincodeReq($data);                   
                $pincodeData=$this->apimodelcustomer->getPincodeDetails($pincode);

                if($pincodeData){                             
                    switch ($pkgID) {
                        case '1':
                            $pincodeData->parentHub=$pincodeData->parentHub2;
                            break;
                        case '2':
                          $continue=1;
                            break;
                        default:
                            $pincodeData->parentHub=0;
                            break;
                    }
                    $arrResponse = array("status" => 200, "message" => "success", "data" =>$pincodeData); 
                       $statusCode=200;
                }else{
                    $arrResponse = array("status" => 203, "message" => "Currently we are not providing service to this area !"); 
                       $statusCode=203;
                }   

                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                       $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                   $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
               $statusCode=405;
        }
        http_response_code($statusCode); 
      echo json_encode($arrResponse);   

    }

     /************************************ getPincodes ************************************/
    public function getPincodes(){        

        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $dataArr=array();
            $data = json_decode(file_get_contents('php://input'), true);
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            $getHeaders=$this->getHeaders();
            
            if(!empty($getHeaders)){
                $dataArr=array();
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
            
                if($checkCustomer&&$custID==$checkCustomer->custID){
                                    
                $pincodeData=$this->apimodelcustomer->getPincodes();                                       
 
                $arrResponse = array("status" => 200, "message" => "success", "data" =>$pincodeData); 
                $statusCode=200;
               
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                       $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                   $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
               $statusCode=405;
        }
        http_response_code($statusCode); 
      echo json_encode($arrResponse);   

    }

    /************************************ addAddress ************************************/
    #addAddress
    public function addAddress(){

        if($_SERVER['REQUEST_METHOD'] == "POST"){
           
            $data = json_decode(file_get_contents('php://input'), true);  
           
            $addressTitle = isset($data['addressTitle']) ? mysqli_real_escape_string($this->dbcharesc,$data['addressTitle']) : '';
            $addressLine1 = isset($data['addressLine1']) ? $data['addressLine1'] : '';
            $addressLine2 = isset($data['addressLine2']) ? mysqli_real_escape_string($this->dbcharesc,$data['addressLine2']) : '';
            $landmark = isset($data['landmark']) ? mysqli_real_escape_string($this->dbcharesc,$data['landmark']) : '';
            $city = isset($data['city']) ? mysqli_real_escape_string($this->dbcharesc,$data['city']) : '';
            $state = isset($data['state']) ? mysqli_real_escape_string($this->dbcharesc,$data['state']) : '';
            $pincode = isset($data['pincode']) ? mysqli_real_escape_string($this->dbcharesc,$data['pincode']) : '';
            $GSTNo = isset($data['GSTNo']) ? mysqli_real_escape_string($this->dbcharesc,$data['GSTNo']) : '';
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            $getHeaders=$this->getHeaders();
            if(!empty($getHeaders)){

                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);

                if($checkCustomer&&$custID==$checkCustomer->custID){  
                        
                        if(empty($GSTNo)){
                            $GSTNo="NA";
                        }
                        $insertData=array("custID"=>$checkCustomer->custID,
                                    "addressTitle"=>$addressTitle,
                                    "buildingDetails"=>$addressLine1,
                                    "areaDetails"=>$addressLine2,
                                    "landmark"=>$landmark,
                                    "city"=>$city,
                                    "state"=>$state,
                                    "pincode"=>$pincode,
                                    "GSTNo"=>$GSTNo,
                                    "isActive"=>1,
                                    "created"=>date('Y-m-d H:i:s'));

                        $insertAddress=$this->apimodelcustomer->insertAddress($insertData);

                        $addressID= $this->db->insert_id();

                        if($insertAddress){

                            $addressData=$this->apimodelcustomer->getCustomerAddressByID($addressID);

                            $arrResponse = array("status" => 200, "message" => "Address created successfully !", "data"=>$addressData);
                            $statusCode=200;
                        }else{
                            $arrResponse = array("status" => 500, "message" => "Error while creating  address!");
                            $statusCode=500;
                        }   
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }    
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
            $statusCode=405;
        }
       http_response_code($statusCode); 
      echo json_encode($arrResponse);    
    }

    /************************************ updateAddress ************************************/
    #updateAddress
    public function updateAddress(){

        if($_SERVER['REQUEST_METHOD'] == "PUT"){
           
            $data = json_decode(file_get_contents('php://input'), true);  
            $addressID = isset($data['addressID']) ? mysqli_real_escape_string($this->dbcharesc,$data['addressID']) : '';
            $addressTitle = isset($data['addressTitle']) ? mysqli_real_escape_string($this->dbcharesc,$data['addressTitle']) : '';
            $addressLine1 = isset($data['addressLine1']) ? $data['addressLine1'] : '';
            $addressLine2 = isset($data['addressLine2']) ? mysqli_real_escape_string($this->dbcharesc,$data['addressLine2']) : '';
            $landmark = isset($data['landmark']) ? mysqli_real_escape_string($this->dbcharesc,$data['landmark']) : '';
            $city = isset($data['city']) ? mysqli_real_escape_string($this->dbcharesc,$data['city']) : '';
            $state = isset($data['state']) ? mysqli_real_escape_string($this->dbcharesc,$data['state']) : '';
            $pincode = isset($data['pincode']) ? mysqli_real_escape_string($this->dbcharesc,$data['pincode']) : '';
            $GSTNo = isset($data['GSTNo']) ? mysqli_real_escape_string($this->dbcharesc,$data['GSTNo']) : '';
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            $getHeaders=$this->getHeaders();
            if(!empty($addressID)&&!empty($getHeaders)){

                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);

                if($checkCustomer&&$custID==$checkCustomer->custID){  
                        
                        if(empty($GSTNo)){
                            $GSTNo="NA";
                        }
                        $updateData=array(
                                    "addressTitle"=>$addressTitle,
                                    "buildingDetails"=>$addressLine1,
                                    "areaDetails"=>$addressLine2,
                                    "landmark"=>$landmark,
                                    "city"=>$city,
                                    "state"=>$state,
                                    "pincode"=>$pincode,
                                    "GSTNo"=>$GSTNo,
                                    );

                        $updateAddress=$this->apimodelcustomer->updateAddress(array("addressID"=>$addressID),$updateData);


                        if($updateAddress){

                            $addressData=$this->apimodelcustomer->getCustomerAddressByID($addressID);

                            $arrResponse = array("status" => 200, "message" => "Address udated successfully !", "data"=>$addressData);
                            $statusCode=200;
                        }else{
                            $arrResponse = array("status" => 500, "message" => "Error while updating  address!");
                            $statusCode=500;
                        }   
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }    
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
            $statusCode=405;
        }
       http_response_code($statusCode); 
      echo json_encode($arrResponse);    
    }

    
    /************************************ deleteAddress ************************************/
    #deleteAddress
    public function deleteAddress(){

        if($_SERVER['REQUEST_METHOD'] == "DELETE"){
           
            $data = json_decode(file_get_contents('php://input'), true);  
          
            $addressID = isset($data['addressID']) ? mysqli_real_escape_string($this->dbcharesc,$data['addressID']) : '';
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            
            $getHeaders=$this->getHeaders();
            if(!empty($addressID)&&!empty($getHeaders)){

                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);

                if($checkCustomer&&$custID==$checkCustomer->custID){  
                    
                    $addressData=$this->apimodelcustomer->getCustomerAddressByID($addressID);
                    if($addressData&&($addressData->custID==$checkCustomer->custID)){
                        $updateData=array(
                                    "isActive"=>0
                                   );

                        $updateAddress=$this->apimodelcustomer->updateAddress(array("addressID"=>$addressID),$updateData);
                
                            if($updateAddress){                               
                                $arrResponse = array("status" => 200, "message" => "Address deleted successfully !");
                                $statusCode=200;
                            }else{
                                $arrResponse = array("status" => 500, "message" => "Error while deleting  address!");
                                $statusCode=500;
                            } 
                        }else{
                            $arrResponse = array("status" => 403, "message" => "Oops, please try again !");   
                            $statusCode=403;
                        }  
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }    
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
            $statusCode=405;
        }
       http_response_code($statusCode); 
      echo json_encode($arrResponse);    
    }

    /************************************ getAddresses ************************************/
    public function getAddresses(){        

        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $dataArr=array();
            $getHeaders=$this->getHeaders();
          
            $data = json_decode(file_get_contents('php://input'), true);  
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
           
            if(!empty($getHeaders)){
                $dataArr=array();
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
            
                if($checkCustomer&&$custID==$checkCustomer->custID){
                       $addressData=$this->apimodelcustomer->getCustomerAddress($checkCustomer->custID);              
                            $arrResponse = array("status" => 200, "message"=>"success", "data" =>$addressData); 
                            $statusCode=200;
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                 $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
             $statusCode=405;
        }

       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }


    /************************************ getAddrCarBillingNames ************************************/
    public function getAddrCarBillingNames(){        

        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $dataArr=array();
            $getHeaders=$this->getHeaders();
          
            $data = json_decode(file_get_contents('php://input'), true);  
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
           
            if(!empty($getHeaders)){
                $dataArr=array();
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
            
                if($checkCustomer&&$custID==$checkCustomer->custID){
                        $addressData=$this->apimodelcustomer->getCustomerAddress($checkCustomer->custID);   
                        $carData=$this->apimodelcustomer->getCustomerCars($checkCustomer->custID);  
                        $billingNames=$this->apimodelcustomer->getBillingNames($checkCustomer->custID);          
                        $arrResponse = array("status" => 200, "message"=>"success", "addressData" =>$addressData,"carData"=>$carData,"billingNames"=>$billingNames); 
                        $statusCode=200;
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                 $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
             $statusCode=405;
        }

       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }

     /************************************ getProfile ************************************/
    public function getProfile(){        

        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $dataArr=array();

            $data = json_decode(file_get_contents('php://input'), true);  
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            $getHeaders=$this->getHeaders();
                   
           
            if(!empty($getHeaders)){
                $dataArr=array();
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
            
                if($checkCustomer&&$custID==$checkCustomer->custID){
                        $addressData=$this->apimodelcustomer->getCustomerAddress($checkCustomer->custID);  
                        $checkMembership=$this->apimodelcustomer->checkCustomerMembership($checkCustomer->custID);  
                        $carData=$this->apimodelcustomer->getCustomerCars($checkCustomer->custID);
                        
                        if($checkMembership){
                            $checkCustomer->ismember=1;
                        }else{
                            foreach ($carData as $item) {                            
                             
                                $item->isInMembership = '0';
                           
                            }
                            $checkCustomer->ismember=0;
                        }
                        $billingNames=$this->apimodelcustomer->getBillingNames($checkCustomer->custID);     
                       
                        $specialHubs=$this->apimodelcustomer->getSpecialHubs();   
                        
                        $pkgData=array();
                        $onDemand=array();
                        $destinations=array();
                        $pkgDataAll=$this->apimodelcustomer->getPackages();
                        foreach ($pkgDataAll as $readPkg) {
                          // $serviceData=$this->apimodelcustomer->getServicesByPkgID($readPkg->pkgID);
                          /*  switch ($readPkg->pkgID) {
                                case '1':
                                    $serviceData=$this->apimodelcustomer->getServicesByPkgID(1);
                                    break;
                                case '2':
                                    $serviceData=$this->apimodelcustomer->getServicesByPkgID(2);
                                    break;    
                                
                                default:
                                   $serviceData=(object)array();
                                    break;
                            }*/
					$serviceData=array();
                          
				if($readPkg->pkgID==2){
				
				$serviceData[]=array("serviceName"=>"* Requirements *");
				
				$serviceData[]=array("serviceName"=>"Car to be parked in a gated / private");		
				$serviceData[]=array("serviceName"=>"premises or building compound - with");
				$serviceData[]=array("serviceName"=>"access to all 4 doors and trunk.");
				$serviceData[]=array("serviceName"=>"Electrical point to power the vacuum");
				$serviceData[]=array("serviceName"=>"cleaner. In absence of an electrical");
				$serviceData[]=array("serviceName"=>"point, we will use the 12V power socket");
				$serviceData[]=array("serviceName"=>"of your car.");
				$serviceData[]=array("serviceName"=>'As part of Express Wash - scratches, ');
				$serviceData[]=array("serviceName"=>'hard stains, tar marks may not get');
				$serviceData[]=array("serviceName"=>'addressed. We recommend upgrading');
				$serviceData[]=array("serviceName"=>'to our "Deep Clean" package');
				$serviceData[]=array("serviceName"=>'for such requirements.');
				$serviceData[]=array("serviceName"=>"* Service Includes *");
				$serviceData[]=array("serviceName"=>"Refreshed Cleaners x 1");
				$serviceData[]=array("serviceName"=>"Exterior Body Cleaning (Dry Wash)");
				$serviceData[]=array("serviceName"=>"Interior Wipe Cleaning");
				$serviceData[]=array("serviceName"=>"Interior & Trunk Vacuuming");
				$serviceData[]=array("serviceName"=>"Foot Mat and Carpet Cleaning");
				$serviceData[]=array("serviceName"=>"Tyre Dressing & Glass Cleaning");
				$serviceData[]=array("serviceName"=>"Cabin Deodorizing");
				$serviceData[]=array("serviceName"=>"Foot Mats and Wet Wipes");

				}elseif($readPkg->pkgID==1){

				$serviceData[]=array("serviceName"=>"* Requirements *");
				$serviceData[]=array("serviceName"=>"Car to be parked in a gated / private");		
				$serviceData[]=array("serviceName"=>"premises or building compound - with");
				$serviceData[]=array("serviceName"=>"access to all 4 doors and trunk.");
				$serviceData[]=array("serviceName"=>"Kindly arrange for our Van to enter");
				$serviceData[]=array("serviceName"=>'your premises.');
				$serviceData[]=array("serviceName"=>"Our Van will arrive with steam machine");
				$serviceData[]=array("serviceName"=>"and a genset. Expect some noise ");
				$serviceData[]=array("serviceName"=>'through the service. ');
				$serviceData[]=array("serviceName"=>"As part of Deep Clean - scratches may");
				$serviceData[]=array("serviceName"=>"not get addressed. Our polish process");
				$serviceData[]=array("serviceName"=>"helps protect the paint and give a mild");
				$serviceData[]=array("serviceName"=>"gloss finish."); 
				$serviceData[]=array("serviceName"=>"* Service Includes *");				
				$serviceData[]=array("serviceName"=>"Refreshed Cleaners x 3");
				$serviceData[]=array("serviceName"=>"Exterior & Interior Steam Cleaning");
				$serviceData[]=array("serviceName"=>"Upholstery Steam Cleaning");
				$serviceData[]=array("serviceName"=>"Dashboard Polish");
				$serviceData[]=array("serviceName"=>"AC Vents Sanitizing with Steam");
				$serviceData[]=array("serviceName"=>"Interior & Trunk Vacuuming");
				$serviceData[]=array("serviceName"=>"Foot Mat and Carpet Cleaning");
				$serviceData[]=array("serviceName"=>"Tyre Dressing & Glass Cleaning");
				$serviceData[]=array("serviceName"=>"Cabin Sanitizing & Deodorizing");
				$serviceData[]=array("serviceName"=>"Exterior Polishing");
				$serviceData[]=array("serviceName"=>"Engine Bay Cleaning");
				$serviceData[]=array("serviceName"=>"Foot Mats and Wet Wipes");
			}
			elseif($readPkg->pkgID==3)
			{
				$serviceData[]=array("serviceName"=>"* Service Includes *");				
				$serviceData[]=array("serviceName"=>"Professional technician (2)");
				$serviceData[]=array("serviceName"=>"Exterior steam cleaning");
				$serviceData[]=array("serviceName"=>"Glass cleaning with steam");
				$serviceData[]=array("serviceName"=>"Tyre & wheel cleaning with steam");
				$serviceData[]=array("serviceName"=>"Tyre polishing");
				$serviceData[]=array("serviceName"=>"Interior cleaning & vacuuming");
				$serviceData[]=array("serviceName"=>"Dashboard polishing");
				$serviceData[]=array("serviceName"=>"Footmat and floor cleaning");
				$serviceData[]=array("serviceName"=>"Trunk cleaning & vacuuming");
				$serviceData[]=array("serviceName"=>"Cabin deodorising");

			}elseif($readPkg->pkgID==4)
			{
				$serviceData[]=array("serviceName"=>"* Service Includes *");				
				$serviceData[]=array("serviceName"=>"Refreshed Cleaners x 1");
				$serviceData[]=array("serviceName"=>"Exterior Body Cleaning (Dry Wash)");
				$serviceData[]=array("serviceName"=>"Interior Wipe Cleaning");
				$serviceData[]=array("serviceName"=>"Interior & Trunk Vacuuming");
				$serviceData[]=array("serviceName"=>"Foot Mat and Carpet Cleaning");
				$serviceData[]=array("serviceName"=>"Tyre Dressing & Glass Cleaning");
				$serviceData[]=array("serviceName"=>"Cabin Deodorizing");
				$serviceData[]=array("serviceName"=>"Foot Mats and Wet Wipes");

			}else{
			$serviceData=array();
			}
                        
			    if($readPkg->pkgType==0){

                            $onDemand[]=$pkgData[]=array("pkgID"=>$readPkg->pkgID,
                                                        "pkgName"=>$readPkg->appName,
                                                         "pkgSubName"=>$readPkg->pkgSubName,
                                                         "price"=>$readPkg->price,
                                                         "membershipPrice"=>$readPkg->membershipPrice,
                                                         "services"=>$serviceData,
                                                         "pkgType"=>$readPkg->pkgType,
                                                         "isMember"=>$checkCustomer->ismember
                                                         );
                           /* if($readPkg->pkgID==2){
                            $destinations[]=$pkgData[]=array("pkgID"=>$readPkg->pkgID,
                                                            "pkgName"=>$readPkg->pkgName,
                                                             "pkgSubName"=>$readPkg->pkgSubName,
                                                             "price"=>$readPkg->price,
                                                             "membershipPrice"=>$readPkg->membershipPrice,
                                                             "services"=>$serviceData,
                                                             "pkgType"=>$readPkg->pkgType
                                                             );     
                            }*/
                            }else{
                            $destinations[]=$pkgData[]=array("pkgID"=>$readPkg->pkgID,
                                                            "pkgName"=>$readPkg->appName,
                                                             "pkgSubName"=>$readPkg->pkgSubName,
                                                             "price"=>$readPkg->price,
                                                             "membershipPrice"=>$readPkg->membershipPrice,
                                                             "services"=>$serviceData,
                                                             "pkgType"=>$readPkg->pkgType,
                                                              "isMember"=>$checkCustomer->ismember
                                                             );    
                            }
                        }

                        $serviceData=$this->apimodelcustomer->getLastService($custID);
                        $lastServiceAddress=(object)array();
                        if($serviceData){
                        $lastServiceAddress=$this->apimodelcustomer->getAddressByID($custID,$serviceData->addressID);
                        if(!$lastServiceAddress){
                        $lastServiceAddress=(object)array();   
                        }
                        }

                        $arrResponse = array("status" => 200, "message"=>"success","custData"=>$checkCustomer, "addressData" =>$addressData,"carData"=>$carData,"billingNames"=>$billingNames,"pkgData"=>$pkgData,"specialHubs"=>$specialHubs,"onDemand"=>$onDemand,"destinations"=>$destinations,"lastServiceAddress"=>$lastServiceAddress,"badgeCount"=>$this->apimodelcustomer->getBadgeCount($custID),"carLimit"=>5); 

                      
                        $statusCode=200;
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, Please try again !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                 $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
             $statusCode=405;
        }

       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }

    /**********************************get Timeslot availability**************************************/

    public function getTimeslotAvailability(){

    if($_SERVER['REQUEST_METHOD'] == "POST"){
           
            $data = json_decode(file_get_contents('php://input'), true);  
            $pkgID = isset($data['pkgID']) ? mysqli_real_escape_string($this->dbcharesc,$data['pkgID']) : '';
            $hubID = isset($data['hubID']) ? mysqli_real_escape_string($this->dbcharesc,$data['hubID']) : '';
            $addressID = isset($data['addressID']) ? mysqli_real_escape_string($this->dbcharesc,$data['addressID']) : '';
            $carID = isset($data['carID']) ? mysqli_real_escape_string($this->dbcharesc,$data['carID']) : '';
            $date = isset($data['date']) ? mysqli_real_escape_string($this->dbcharesc,$data['date']) : '';           
            $pincode = isset($data['pincode']) ? mysqli_real_escape_string($this->dbcharesc,$data['pincode']) : '';
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            $getHeaders=$this->getHeaders();
            if(!empty($pkgID)&&!empty($hubID)&&!empty($date)&&$date!='0000-00-00'&&!empty($getHeaders)){
                #validating Customer
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
		//print_r($checkCustomer);
                if($checkCustomer&&$custID==$checkCustomer->custID){                      
                    
                    $dateToday=date('Y-m-d');
                    $dateBooking=date('Y-m-d', strtotime($date));

                    if(strtotime($dateToday)<=strtotime($dateBooking)){ 
                       
                        $hubData=$this->apimodelcustomer->getHubByID($hubID); 
                      //  $addressData=$this->apimodelcustomer->getCustomerAddressByID($addressID);
                         if($hubData&&$hubData->specialHub==1){
                        #special Hub condition                           
                       
                        $addressData=(object)array("custID"=>$checkCustomer->custID);
                         
                        }else{ 
                        #feching address details, pincode
                        $addressData=$this->apimodelcustomer->getCustomerAddressByID($addressID);  
                     
                        }  

                       // print_r($addressData);
                        $carData=$this->apimodelcustomer->getCustomerCarsByID($carID);
                     //  print_r($carData);
                        if($addressData&&($addressData->custID==$checkCustomer->custID)){

                            #cheking for spacial Hub
                            if($hubData&&$hubData->specialHub==1){
                                $validateBooking=true;
                                $pkgData=$this->apimodelcustomer->validatePkg($pkgID,1);
                            }else{
                                #checking fo on demand hub                          
                                $pkgData=$this->apimodelcustomer->validatePkg($pkgID,0);
                                if($addressData&&!empty($addressData->pincode)){
                                    $validatePincode=$this->apimodelcustomer->getPincodeDetails($addressData->pincode);
                                    if($validatePincode){
                                     switch ($pkgID) {
                                        case '1':
                                            $validatePincode->parentHub=$validatePincode->parentHub2;
                                            break;
                                        case '2':
                                          $validatePincode->parentHub=$validatePincode->parentHub;
                                            break;
                                        default:
                                            $validatePincode->parentHub=0;
                                            break;
                                        }
                                     }   
                                    if($validatePincode&&$validatePincode->parentHub!=0){
                                    $validateBooking=true;  
                                    $hubID=$validatePincode->parentHub;  
                                    }else{
                                    $validateBooking=false;
                                    $hubID=0;    
                                    }
                                }else{
                                    $validateBooking=false;
                                    $hubID=0;
                                }
                            }

                            if($validateBooking&&$pkgData){


                            # fetching time slots          
                            $timeSlots=$this->apimodelcustomer->getTimeslots($pkgID,$hubID,$hubData->specialHub);
                            $timeSlotsData=array();
                            if($timeSlots){
                                      
                                foreach ($timeSlots as $readTimeslot) {
                                   
                                    $time=$readTimeslot->time;                                                 
                                        #cheking for availability
                                        $dateTime = date('Y-m-d H:i:s', strtotime("$dateBooking $time"));
                                        $timeslotsAvailability=$this->apimodelcustomer->getTimeslotsAvailability($pkgID,$hubID,$dateTime,$hubData->specialHub);
                                        $dateTimeCurr = date('Y-m-d H:i:s');
                                        if (strtotime($dateTime) > strtotime($dateTimeCurr)){ 

                                            if($timeslotsAvailability){
                                                $capacity=$timeslotsAvailability->capacity;

                                                #checking no of bookings done
                                                $noOfBookings=$this->apimodelcustomer->geNoOfBookings($pkgID,$hubID,$dateBooking,$time,$hubData->specialHub);

                                                #cheking no of bookings going on
                                                $noBookingReq=$this->apimodelcustomer->getNoOfBookingRequests($pkgID,$hubID,$date,$time,$hubData->specialHub,0,$checkCustomer->custID);

                                                $available=$capacity-($noOfBookings+$noBookingReq);

                                                #validate time slot available or not
                                                if($available>0){
                                                  $isAvailable="1";  
                                                }else{
                                                  $isAvailable="0";  
                                                }
                                                $timeSlotsData[] = array('date' =>$date,
                                                                         'time'=>$time,  
                                     'formatedTime'=>date('h:i A',strtotime($time)),                                                                  
                                                                         'isAvailable'=>$isAvailable );
                                            }else{
                                                $timeSlotsData[] = array('date' =>$date,
                                                                         'time'=>$time,
                                     'formatedTime'=>date('h:i A',strtotime($time)),   
                                                                         'isAvailable'=>0,
                                                                        );
                                            }
                                        }
                                }
                                if($timeSlotsData){
                               
                               /* #cheking for membership 
                                $membership=$this->apimodelcustomer->getCustomerMembership($checkCustomer->custID);
                                if($membership){
                                $carData->isInMembership=1;
                                }else{
                                $carData->isInMembership=0;    
                                } */
                //$timeSlotsData=null;
                                $arrResponse = array("status" => 200, "message" => "success","timeslots"=>$timeSlotsData);
                                $statusCode=200;
                                }else{
                                $arrResponse = array("status" => 520, "message" => "No timeslots available for selected date, Please try another date.");
                                $statusCode=520;   
                                }
                            }else{
                                $arrResponse = array("status" => 520, "message" => "No timeslots available for selected date, Please try another date.");
                                $statusCode=520; 
                            }
                  
                            
                            }else{
                                $arrResponse = array("status" => 203, "message" => "Currently we are not providing service for this area!");
                                $statusCode=203; 
                            }
                        }else{
                            $arrResponse = array("status" => 400, "message" => "Please enter valid data!");
                            $statusCode=400; 
                        }
                    }else{
                      $arrResponse = array("status" => 400, "message" => "Please use correct date!");
                    $statusCode=400;  
                    }
                }else{
                    $arrResponse = array("status" => 403, "message" => "Please try again !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Oops, Please try again !");
                 $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Oops, Please try again !");
             $statusCode=405;
        }

       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }




	
    /**********************************get Timeslot availability**************************************/

    public function getTimeslotAvailabilityForUpdate(){
            $getHeaders=$this->getHeaders();
            // print_r($getHeaders);
            $custID = $this->input->post('custID');
            $pkgID = $this->input->post('pkgID');
            $hubID = $this->input->post('hubID');
            // $getHeaders = $this->input->post('Authorization');
            $datess = $this->input->post('serviceDate');
            $date = date('Y-m-d',strtotime($datess));
        
            if(!empty($pkgID)&&!empty($hubID)&&!empty($date)&&$date!='0000-00-00'&&!empty($getHeaders)){
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
                if($checkCustomer&&$custID==$checkCustomer->custID){                      
                    
                    $dateToday=date('Y-m-d');
                    $dateBooking=date('Y-m-d', strtotime($date));
                       
                        $hubData=$this->apimodelcustomer->getHubByID($hubID); 
                            # fetching time slots          
                            $timeSlots=$this->apimodelcustomer->getTimeslots($pkgID,$hubID,$hubData->specialHub);
                            $timeSlotsData=array();
                            if($timeSlots){
                                      
                                foreach ($timeSlots as $readTimeslot) {
                                   
                                    $time=$readTimeslot->time;                                                 
                                        #cheking for availability
                                        $dateTime = date('Y-m-d H:i:s', strtotime("$dateBooking $time"));
                                        $timeslotsAvailability=$this->apimodelcustomer->getTimeslotsAvailability($pkgID,$hubID,$dateTime,$hubData->specialHub);
                                        $dateTimeCurr = date('Y-m-d H:i:s');
                                        if (strtotime($dateTime) > strtotime($dateTimeCurr)){ 

                                            if($timeslotsAvailability){
                                                $capacity=$timeslotsAvailability->capacity;

                                                #checking no of bookings done
                                                $noOfBookings=$this->apimodelcustomer->geNoOfBookings($pkgID,$hubID,$dateBooking,$time,$hubData->specialHub);

                                                #cheking no of bookings going on
                                                $noBookingReq=$this->apimodelcustomer->getNoOfBookingRequests($pkgID,$hubID,$date,$time,$hubData->specialHub,0,$checkCustomer->custID);

                                                $available=$capacity-($noOfBookings+$noBookingReq);

                                                #validate time slot available or not
                                                if($available>0){
                                                  // $isAvailable="1";  
                                                /*else{
                                                  $isAvailable="0";  
                                                }*/
                                                $timeSlotsData[] = array('date' =>$date,
                                                                         'time'=>$time,  
                                     'formatedTime'=>date('h:i A',strtotime($time)),                                                                  
                                                                         'isAvailable'=>1 );
                                            }}
                                        }
                                }
                                if($timeSlotsData){

                                $arrResponse = array("status" => 200, "message" => "success","timeslots"=>$timeSlotsData);
                                $statusCode=200;
                                }else{
                                $arrResponse = array("status" => 520, "message" => "No timeslots available!");
                                $statusCode=520;   
                                }
                            }else{
                                $arrResponse = array("status" => 520, "message" => "No timeslots available At All!");
                                $statusCode=520; 
                            }
                  
                }else{
                    $arrResponse = array("status" => 403, "message" => "You are not a valid user !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                 $statusCode=400;
            }
       
       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }

   




    /**********************************Booking Request*************************************/

    public function bookingRequest(){

    if($_SERVER['REQUEST_METHOD'] == "POST"){
           
            $data = json_decode(file_get_contents('php://input'), true);  
            $pkgID = isset($data['pkgID']) ? mysqli_real_escape_string($this->dbcharesc,$data['pkgID']) : '';
            $hubID = isset($data['hubID']) ? mysqli_real_escape_string($this->dbcharesc,$data['hubID']) : '';
            $addressID = isset($data['addressID']) ? mysqli_real_escape_string($this->dbcharesc,$data['addressID']) : '';
            $carID = isset($data['carID']) ? mysqli_real_escape_string($this->dbcharesc,$data['carID']) : '';
            $date = isset($data['date']) ? mysqli_real_escape_string($this->dbcharesc,$data['date']) : '';   
            $time = isset($data['time']) ? mysqli_real_escape_string($this->dbcharesc,$data['time']) : '';         
            $officeNo = isset($data['officeNo']) ? mysqli_real_escape_string($this->dbcharesc,$data['officeNo']) : '';
            $floorNo = isset($data['floorNo']) ? mysqli_real_escape_string($this->dbcharesc,$data['floorNo']) : '';
            $jobID = isset($data['jobID']) ? mysqli_real_escape_string($this->dbcharesc,$data['jobID']) : '';
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            $getHeaders=$this->getHeaders();
            if(!empty($pkgID)&&!empty($hubID)&&!empty($date)&&$date!='0000-00-00'&&!empty($time)&&$time!='00:00:00'&&!empty($getHeaders)){
                #validating Customer
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);

                if($checkCustomer&&$custID==$checkCustomer->custID){    
                       
                    $hubData=$this->apimodelcustomer->getHubByID($hubID); 
                    
                    $carData=$this->apimodelcustomer->getCustomerCarsByID($carID);

                    if($hubData&&$hubData->specialHub==1){
                        #special Hub condition                           
                     //  if(!empty($officeNo)||!empty($jobID)){

                        $addressData=(object)array("custID"=>$checkCustomer->custID);
                       /* }else{

                         $addressData=(object)array("custID"=>0);   
                        }*/
                         
                        }else{ 
                        #feching address details, pincode
                        $addressData=$this->apimodelcustomer->getCustomerAddressByID($addressID);  
                     
                        }  
                       

                    if($addressData&&($addressData->custID==$checkCustomer->custID)&&$carData&&($carData->custID==$checkCustomer->custID)){

                            #cheking for spacial Hub
                            if($hubData&&$hubData->specialHub==1){
                                $validateBooking=true;
                                $pkgData=$this->apimodelcustomer->validatePkg($pkgID,1);
                            }else{
                                #checking fo on demand hub                          
                                $pkgData=$this->apimodelcustomer->validatePkg($pkgID,0);
                                if($addressData&&!empty($addressData->pincode)){
                                    $validatePincode=$this->apimodelcustomer->getPincodeDetails($addressData->pincode);
                                    if($validatePincode){
                                     switch ($pkgID) {
                                        case '1':
                                            $validatePincode->parentHub=$validatePincode->parentHub2;
                                            break;
                                        case '2':
                                          $validatePincode->parentHub=$validatePincode->parentHub;
                                            break;
                                        default:
                                            $validatePincode->parentHub=0;
                                            break;
                                    }
                                }
                                    if($validatePincode&&$validatePincode->parentHub!=0){
                                    $validateBooking=true;  
                                    $hubID=$validatePincode->parentHub;  
                                    }else{
                                    $validateBooking=false;
                                    $hubID=0;    
                                    }
                                }else{
                                    $validateBooking=false;
                                    $hubID=0;
                                }
                            }

                            if($validateBooking&&$pkgData){
                                                                         
                                        #cheking for availability
                                        $dateTime = date('Y-m-d H:i:s', strtotime("$date $time"));
                                        
                                        $dateTimeCurr = date('Y-m-d H:i:s');
                                        if (strtotime($dateTime) > strtotime($dateTimeCurr)){ 
                                            #checking time slot availability
                                            $timeslotsAvailability=$this->apimodelcustomer->getTimeslotsAvailability($pkgID,$hubID,$dateTime,$hubData->specialHub); 

                                            if($timeslotsAvailability){
                                                $capacity=$timeslotsAvailability->capacity;

                                                #checking no of bookings done
                                                $noOfBookings=$this->apimodelcustomer->geNoOfBookings($pkgID,$hubID,$date,$time,$hubData->specialHub);
                                                #cheking no of bookings going on
                                                $noBookingReq=$this->apimodelcustomer->getNoOfBookingRequests($pkgID,$hubID,$date,$time,$hubData->specialHub,0,$checkCustomer->custID);
                                                #calculating availability
                                                $available=$capacity-($noOfBookings+$noBookingReq);

                                                #validate time slot available or not
                                                if($available>0){

                                                    $data=array("custID"=>$checkCustomer->custID,
                                                              "pkgID"=>$pkgID,
                                                              "addressID"=>$addressID,
                                                              "hubID"=>$hubID,
                                                              "carID"=>$carID,
                                                              "date"=>$date,
                                                              "time"=>$time,
                                                              "bookingFrom"=>2,
                                                              "isActive"=>1,
                                                              "created"=>date('Y-m-d H:i:s'));
                                                    $insertBookingReq=$this->apimodelcustomer->insertBookingReq($data);
                                                    $reqID= $this->db->insert_id();
                                                    if($insertBookingReq){
                                                        #cheking for membership 
                                                        $membership=$this->apimodelcustomer->getCustomerMembership($checkCustomer->custID);
                                                        if($membership){
                                                        $price=$pkgData->membershipPrice;
                                                        }else{
                                                        $price=$pkgData->price;    
                                                        } 

                                                        $lastJob=$this->apimodelcustomer->getLastJobByCustID($checkCustomer->custID);
                                                        if($lastJob&&!empty($lastJob->billingName)){
                                                            $billingName=$lastJob->billingName;
                                                        }else{
                                                            $billingName=$checkCustomer->custName;
                                                        }

                                                       /* $address=$addressData->buildingDetails.','.$addressData->areaDetails.','.$addressData->landmark.',  '.$addressData->city.'  '.$addressData->state.', '.$addressData->pincode ;
                                                        $serviceAddress=$billingAddress=$this->arrangeAddress($address);

                                                        #for special hub service address
                                                        if($hubData->specialHub==1){
                                                        $serviceAddress=$hubData->destName.', '.$hubData->destAddress; 
                                                        }*/

                                                        #for special hub service address
                                                        if($hubData->specialHub==1){

                                                        if(!empty($jobID)){
                                                        $jobData=$this->apimodelcustomer->getJobByID($jobID);    
                                                        $address=$jobData->serviceAddress; 
                                                        }else{    
                                                            
                                                        if(!empty($floorNo)){
                                                            $floorNo=', Floor No- '.$floorNo;
                                                        }
                                                        $address=$officeNo.$floorNo.' / '.$hubData->destAddress; 
                                                        }
                                                        $addressID=0;
                                                        $serviceAddress=$billingAddress=$this->arrangeAddress($address);
                                                        $pincode="";
                                                        $gstNo="";
                                                        }else{
                                                        #for on Demand     
                                                        #formating Address
                                                        
                                                        $address=$addressData->buildingDetails.','.$addressData->areaDetails.','.$addressData->landmark.',  '.$addressData->city.'  '.$addressData->state.', '.$addressData->pincode ;
                                                        $serviceAddress=$billingAddress=$this->arrangeAddress($address);
                                                        $addressID=$addressData->addressID;
                                                        $pincode=$addressData->pincode;
                                                        $gstNo=$addressData->GSTNo;
                                                        }

                                                        $respArr=array("pkgID"=>$pkgID,
                                                                       "pkgName"=>$pkgData->pkgName,
                                                                       "price"=>$price,
                                                                       "addressID"=>$addressID,
                                                                       "serviceAddress"=>$serviceAddress,
                                                                       "billingAddress"=>$billingAddress,
                                                                       "gstNo"=>$gstNo,
                                                                       "hubID"=>$hubID,
                                                                       "carID"=>$carID,
                                                                       "carMake"=>$carData->carMake,
                                                                       "carModel"=>$carData->carModel,
                                                                       "carNoPlate"=>$carData->carNoPlate,
                                                                       "billingName"=>$billingName,
                                                                       "date"=>$date,
                                                                       "time"=>date('h:i A',strtotime($time)),
                                                                       "custEmailID"=>$checkCustomer->custEmailID,
                                                                       "reqID"=>$reqID,
                                                                       "addressPincode"=>$pincode,
                                                                       "contactNo"=>$checkCustomer->custMobNo
                                                                        );
                                                        $arrResponse = array("status" => 200, "message" => "success", "bookingData"=>$respArr);
                                                        $statusCode=200; 
                                                    }else{
                                                        $arrResponse = array("status" => 500, "message" => "Something went wrong !");
                                                        $statusCode=500; 
                                                    }
                                                }else{
                                                   $arrResponse = array("status" => 520, "message" => "No timeslots available for selected date, Please try another date.");
                                                    $statusCode=520;  
                                                }
                                               
                                            }else{
                                                $arrResponse = array("status" => 520, "message" => "No timeslots available for selected date, Please try another date.");
                                                $statusCode=520;
                                            }
                                        }else{
                                            $arrResponse = array("status" => 400, "message" => "Please select valid date & time !");
                                            $statusCode=400;
                                        }                           
                  
                            }else{
                                    $arrResponse = array("status" => 203, "message" => "Currently we are not providing service for this area!");
                                    $statusCode=203; 
                            }
                    }else{
                            $arrResponse = array("status" => 400, "message" => "Please enter valid data!");
                            $statusCode=400; 
                    }                   
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                 $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
             $statusCode=405;
        }

       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }

    /**********************************Book Job**************************************/

    public function bookJobTest(){

    if($_SERVER['REQUEST_METHOD'] == "POST"){
        $getHeaders=$this->getHeaders();
        $data = json_decode(file_get_contents('php://input'), true);  
        $pkgID = isset($data['pkgID']) ? mysqli_real_escape_string($this->dbcharesc,$data['pkgID']) : '';
        $hubID = isset($data['hubID']) ? mysqli_real_escape_string($this->dbcharesc,$data['hubID']) : '';
        $carID = isset($data['carID']) ? mysqli_real_escape_string($this->dbcharesc,$data['carID']) : '';
        $date = isset($data['date']) ? mysqli_real_escape_string($this->dbcharesc,$data['date']) : '';           
        $time = isset($data['time']) ? mysqli_real_escape_string($this->dbcharesc,$data['time']) : '';
        $addressID = isset($data['addressID']) ? mysqli_real_escape_string($this->dbcharesc,$data['addressID']) : '';
        $billingName = isset($data['billingName']) ? mysqli_real_escape_string($this->dbcharesc,$data['billingName']) : '';
        $emailID = isset($data['emailID']) ? mysqli_real_escape_string($this->dbcharesc,$data['emailID']) : '';
        $gstNo = isset($data['gstNo']) ? mysqli_real_escape_string($this->dbcharesc,$data['gstNo']) : '';
        $pincode = isset($data['pincode']) ? mysqli_real_escape_string($this->dbcharesc,$data['pincode']) : '';
        $reqID = isset($data['reqID']) ? mysqli_real_escape_string($this->dbcharesc,$data['reqID']) : '';
        $isNotifyMe = isset($data['isNotifyMe']) ? mysqli_real_escape_string($this->dbcharesc,$data['isNotifyMe']) : '';
        $bookedFrom = isset($data['bookedFrom']) ? mysqli_real_escape_string($this->dbcharesc,$data['bookedFrom']) : '';
        $officeNo = isset($data['officeNo']) ? mysqli_real_escape_string($this->dbcharesc,$data['officeNo']) : '';
        $floorNo = isset($data['floorNo']) ? mysqli_real_escape_string($this->dbcharesc,$data['floorNo']) : '';
        $jobID = isset($data['jobID']) ? mysqli_real_escape_string($this->dbcharesc,$data['jobID']) : '';
        $paymentMethod = isset($data['paymentMethod']) ? mysqli_real_escape_string($this->dbcharesc,$data['paymentMethod']) : '0';
        $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
        $contactNo=isset($data['contactNo']) ? mysqli_real_escape_string($this->dbcharesc,$data['contactNo']) : '';
            

        if(!empty($pkgID)&&!empty($hubID)&&!empty($date)&&$date!='0000-00-00'&&!empty($getHeaders)&&!empty($carID)&&!empty($time)&&$time!='00:00:00'&&!empty($reqID)){

                #validate customer
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
                
                if($checkCustomer&&$custID==$checkCustomer->custID){ 
                    #email validation
                    if(!empty($emailID)){
                    if($this->validateEmail($emailID)){ 
                        $validEmail=true;

                    }else{
                        $validEmail=false; 
                    }
                    }else{
                        $validEmail=true; 
                    }
                     
                    if($validEmail){ 
                        #validating hub
                        $hubData=$this->apimodelcustomer->getHubByID($hubID);

                        if($hubData&&($hubData->specialHub==1)){

                        #special Hub condition                            
                        $pincodeData=(object)array("parentHub"=>$hubData->destID);

                        if(!empty($officeNo)||!empty($jobID)){
                        $addressData=(object)array("custID"=>$checkCustomer->custID);
                        }else{
                         $addressData=(object)array("custID"=>0);   
                        }
                         $jobType=1; 
                        
                        }else{ 

                        #feching address details, pincode
                        $addressData=$this->apimodelcustomer->getCustomerAddressByID($addressID);
                            
                       // if($addressData->pincode==$pincode){    
                        $pincodeData=$this->apimodelcustomer->getPincodeDetails($addressData->pincode); 
                        /*}else{
                        $pincodeData=false;    
                        }*/
                        if($pincodeData){
                         switch ($pkgID) {
                                        case '1':
                                            $pincodeData->parentHub=$pincodeData->parentHub2;
                                            break;
                                        case '2':
                                          $pincodeData->parentHub=$pincodeData->parentHub;
                                            break;
                                        default:
                                            $pincodeData->parentHub=0;
                                            break;
                                    }
                        }

                        $jobType=0;
                        }                   
                        
                        if($pincodeData&&$pincodeData->parentHub!=0){

                            $dateTimeToday=date('Y-m-d H:i:s',strtotime('-15 minutes'));

                            $dateTimeBooking=date('Y-m-d H:i:s', strtotime($date.' '.$time));

                            if(strtotime($dateTimeToday)<=strtotime($dateTimeBooking)){ 

                                #validating other details
                                           
                                $pkgData=$this->apimodelcustomer->validatePkg($pkgID,$jobType);

                                $carData=$this->apimodelcustomer->getCustomerCarsByID($carID);
                                $validateBookingRequest=$this->apimodelcustomer->validateBookingRequest($reqID);
                                if($hubData&&$pkgData&&$pincodeData->parentHub==$hubID&&$hubID!=0&&$hubData&&$carData&&$carData->custID==$checkCustomer->custID&&$addressData->custID==$checkCustomer->custID&&$validateBookingRequest){                                                   
                                  //  if(strtotime($dateTimeBooking) > strtotime($dateTimeToday)){ 

                                        #cheking time slot availablity
                                        $timeslotsAvailability=$this->apimodelcustomer->getTimeslotsAvailability($pkgID,$hubID,$dateTimeBooking,$hubData->specialHub);
                                        if($timeslotsAvailability){
                                            $capacity=$timeslotsAvailability->capacity;

                                            #checking no of bookings done
                                            $noOfBookings=$this->apimodelcustomer->geNoOfBookings($pkgID,$hubID,$date,$time,$hubData->specialHub);

                                            #cheking no of bookings going on
                                            $noBookingReq=$this->apimodelcustomer->getNoOfBookingRequests($pkgID,$hubID,$date,$time,$hubData->specialHub,$reqID,$checkCustomer->custID);
                                            $available=$capacity-($noOfBookings+$noBookingReq);

                                            #validate time slot available or not
                                            if($available>0){
                                               
                                                #for special hub service address
                                                if($hubData->specialHub==1){

                                                $serviceAddressID=0; 
                                                if(!empty($jobID)){
                                                    $jobData=$this->apimodelcustomer->getJobByID($jobID);    
                                                    $address=$jobData->serviceAddress; 
                                                }else{   
                                                    if(!empty($floorNo)){
                                                        $floorNo=', Floor No- '.$floorNo;
                                                    }
                                                    $address=$officeNo.$floorNo.' / '.$hubData->destAddress; 
                                                }
                                                
                                                $serviceAddress=$billingAddress=$this->arrangeAddress($address);
                                                }else{
                                                #for on Demand     
                                                #formating Address
                                                if(!empty($gstNo)){
                                                    $gstNoAddr=$gstNo;
                                                }else if(empty($addressData->GSTNo)){
                                                    $gstNoAddr="NA";
                                                }else{
                                                     $gstNoAddr=$addressData->GSTNo;
                                                }
                                                $address=$addressData->buildingDetails.','.$addressData->areaDetails.','.$addressData->landmark.',  '.$addressData->city.'  '.$addressData->state.', PIN : '.$addressData->pincode .',  GSTIN : '.$gstNoAddr;
                                                $serviceAddress=$billingAddress=$this->arrangeAddress($address);
                                                $serviceAddressID=$addressData->addressID;

                                                }

                                                #cheking for membership 
                                                $membership=$this->apimodelcustomer->getCustomerMembership($checkCustomer->custID);
                                                if($membership&&!empty($carData->isInMembership)){
                                                    $pkgAmount=$pkgData->membershipPrice;

                                                }else{
                                                    $pkgAmount=$pkgData->price;
                                                }

                                                #checking billing Name
                                                if(empty($billingName)){
                                                    $billingName=$checkCustomer->custName;
                                                }

                                                #generate REF ID
                                                $jobIdentifier="";
                                                do{
                                                $jobIdentifier=$this->generateRefId($jobType);  
                                                #check id exists
                                                $checkJob=$this->apimodelcustomer->getJobByJobRefID($jobIdentifier);
                                                if($checkJob){  
                                                $jobIdentifier="";    
                                                }
                                                }while($jobIdentifier=="");
                                                
                                                $data=array('customerID' =>$checkCustomer->custID,
                                                            'jobIdentifier'=>$jobIdentifier,
                                                            'empID' =>0,
                                                            'pkgID' =>$pkgID,
                                                            'pkgName' =>$pkgData->pkgName,
                                                            'pkgAmount' =>$pkgAmount,
                                                            'billingName'=>$billingName,
                                                            'serviceAddress' =>$serviceAddress,
                                                            'billingAddress' =>$billingAddress,
                                                            'addressID'=>$serviceAddressID,
                                                            'billingAddressID'=>$serviceAddressID,
                                                            'billAddrSameAsServAddr'=>1,
                                                            'date'=>date('Y-m-d',strtotime($date)),
                                                            'vehicleID'=>$carID,
                                                            'vehicleMake' =>$carData->carMake,
                                                            'vehicleModel' =>$carData->carModel,
                                                            'vehicleNoPlate' =>$carData->carNoPlate,
                                                            'hubDestID' =>$hubID,
                                                            'serviceTime' =>date('H:i:s',strtotime($time)),
                                                            'reportingTime'=>date('H:i:s',strtotime($time)),
                                                            'type' =>$hubData->type,
                                                            'bookedFrom'=>$bookedFrom,
                                                            'isNotifyMe'=>$isNotifyMe,
                                                            'paymentMethod'=>$paymentMethod,
                                                            'contactNo'=>$contactNo,
                                                            'jobStatus'=>6,
                                                            'isActive'=>0,
                               'bookingReqID'=>$reqID,
                                                            'created' =>date('Y-m-d H:i:s') );
                                                

                                                #insert Booking
                                                $insertJob =$this->apimodelcustomer->insertJob($data);

                                                $jobID= $this->db->insert_id();

                                                if($insertJob){ 
                                                       /* $updateData['jobIdentifier'] = $jobIdentifier;
                                                        $updateJob =$this->apimodelcustomer->updateJob(array('jobID' =>$jobID),$updateData);*/

                                                        #update email ID
                                                        if(!empty($emailID)){
                                                          $updateEmail=$this->apimodelcustomer->updateCustomer(array('custID' =>$checkCustomer->custID),array('custEmailID' =>  $emailID));
                                                        }
                                                       
                                                        #billing data    
                                                        $checkBillingName=$this->apimodelcustomer->checkBillingName($checkCustomer->custID,$billingName);
                                                        if(!$checkBillingName){
                                                        $dataBillingName=array("customerID"=>$checkCustomer->custID,
                                                                                "billingName"=>$billingName,
                                                                                "isActive"=>1,
                                                                                "created"=>date('Y-m-d H:i:s'));
                                                        $checkBillingName=$this->apimodelcustomer->insertBillingName($dataBillingName);
                                                        }

                                                        #update car 
                                                        if($carData->carIsActive==0){
                                                           $updateCar=$this->apimodelcustomer->updateCar(array("carID"=>$carData->carID),array("carIsActive"=>1));
                                                        }

                                                        if($hubData->specialHub!=1){
                                                        #update Address 
                                                        if($addressData->isActive==0){
                                                           $updateAddress=$this->apimodelcustomer->updateAddress(array("addressID"=>$addressData->addressID),array("isActive"=>1));
                                                        }

                                                         #update GST No
                                                        if(!empty($gstNo)){  
                                                          $updateAddress=$this->apimodelcustomer->updateAddress(array("addressID"=>$addressID),array("GSTNo"=>$gstNo));   
                                                        }
                                                        }

                                                        $transactionID="";
                                                        #fetching job details
                                                        $job=$this->getJobByID($jobID);
                                                        if($job){
                                                          $jobData=$job;  
                                                            if($paymentMethod==1||$paymentMethod==2){ //1 for payment method paytm

                                                                do{
                                                                    $refTransactionID=$this->generateTransactionID();//1 for txn type job

                                                                    $txnData=array("refTransactionID"=>$refTransactionID,
                                                                                    "dataID"=>$jobID,
                                                                                    "refType"=>1,
                                                                                    "platform"=>$bookedFrom,
                                                                                    "isActive"=>1,
                                                                                    "created"=>date('Y-m-d H:i:s'));
                                                                    $insertTxn=$this->apimodelcustomer->insertPaytmTxn($txnData);

                                                                   
                                                                }while($insertTxn==0);

                                                            }else{
                                                             $this->apimodelcustomer->updateJob(array("jobID"=>$jobID),array("isActive"=>1));
                                                                #update Booking Req
                                                                $updateBookingReq=$this->apimodelcustomer->updateBookingReq(array("reqID"=>$reqID),array('jobID' =>$jobID,'isComplete'=>1));
                                                                $refTransactionID="";
                                                                
                                                                #send Sms 
                                                                if(!empty($checkCustomer->custMobNo)&&($checkCustomer->isSMSSubscribed==1)){
                                                                    $smsDate=date('d-M',strtotime($date));
                                                                    $smsTime=date('h:i A',strtotime($time));
                                                                    $message = 'Your '.$pkgData->pkgName.' ('.$jobIdentifier.') is confirmed for '.$smsDate.' at '.$smsTime.'. To cancel or reschedule, please call 8080720000. Thanks, Refreshed Car Care';
                                                                  
                                                                  $sendMsg=$this->sendSms2($checkCustomer->custMobNo,$message);
                                                                  if($sendMsg){

                                                                    $dataSms=array('recordID' =>$jobID,
                                                                                    'custID'=>$checkCustomer->custID,
                                                                                    'mobileNo'=>$checkCustomer->custMobNo,
                                                                                    'sms'=>$message,
                                                                                    'smsType'=>1, //For Job SMS
                                                                                    'smsSubType'=>1, //For Confirm Job SMS
                                                                                    'isActive'=>1,
                                                                                    'created'=>date('Y-m-d H:i:s'));
                                                                    $insertSMS=$this->apimodelcustomer->insertSms($dataSms);
                                                                                                
                                                                    $updateJob =$this->apimodelcustomer->updateJob(array('jobID' =>$jobID),array("isSmsSentBooking"=>1));
                                                                  }
                                                                }

                                                                #send Pepo Campaign Email
                                                                if(!empty($checkCustomer->custEmailID)){
                                                                $this->bookingPepoCampaign($checkCustomer->custEmailID,$checkCustomer->custName,$jobIdentifier,$date,$time,$serviceAddress,$checkCustomer->custMobNo,$pkgData->pkgName,$pkgAmount,$jobID);
                                                                   
                                                                }
                                                                
                                                                # send Noti
                                                                if(!empty($isNotifyMe)){
                                                                
                                                                $notification="Your Refreshed service is confirmed: Booking ID ".$jobIdentifier;
                                                                
                                                                $notiData=array("recieverID"=>$checkCustomer->custID,
                                                                              "notification"=>$notification,
                                                                              "type"=>1,
                                                                              "subType"=>1,
                                                                              "dataID"=>$jobID,
                                                                              "isActive"=>1,
                                                                              "created"=>date('Y-m-d H:i:s'));
                                                                $insertNotiCust=$this->apimodelcustomer->insertNotiCust($notiData);
                                                                }

                                                                $deviceTokens=$this->apimodelcustomer->getDeviceTokens($checkCustomer->custID);
                                                                 $badgeCount=$this->apimodelcustomer->getBadgeCount($checkCustomer->customerID);
                                                                if($deviceTokens){
                                                                  foreach ($deviceTokens as $readToken) {
                                                                    if($readToken->deviceType==1){
                                                                    //send ios noti   
                                                                    $this->iOSNotification($readToken->deviceToken,$notification,$jobID,1,1,$badgeCount);
                                                                    }else{
                                                                    //send ios android noti
                                                                        $this->androidNotification($readToken->deviceToken,$notification,$jobID,1,1);
                                                                    }
                                                                  }
                                                                }

                                                        }

                                                        }else{
                                                          $jobData=array();  
                                                        }
                                                        $arrResponse = array("status" => 200, "message" => "success","jobData"=>$jobData,"refTransactionID"=>$refTransactionID);
                                                        $statusCode=200; 
                                                }else{
                                                  $arrResponse = array("status" => 500, "message" => "Something went wrong! Please try again.");
                                                    $statusCode=500;   
                                                }
                                            }else{
                                                $arrResponse = array("status" => 520, "message" => "No time slots available! Please try again.");
                                                $statusCode=520;   
                                            }
                                        
                                        }else{
                                            $arrResponse = array("status" => 520, "message" => "No time slots available! Please try again.");
                                            $statusCode=520; 
                                        }
                                    /*}else{
                                        $arrResponse = array("status" => 400, "message" => "Please enter correct date & time!");
                                            $statusCode=400; 
                                    }  */
                        
                                }else{
                                    $arrResponse = array("status" => 400, "message" => "Please enter valid data!");
                                $statusCode=400;  
                                }          
                    
                            }else{
                              $arrResponse = array("status" => 400, "message" => "Please enter correct date & time!");
                            $statusCode=400;  
                            }
                        }else{
                        $arrResponse = array("status" => 203, "message" => "Currently we are not providing service to this area !");
                        $statusCode=203;
                        }
                    }else{
                        $arrResponse = array("status" => 400, "message" => "Please enter valid email !");
                        $statusCode=403;
                    } 
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                 $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
             $statusCode=405;
        }

       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }

    
    public function bookJob(){

    if($_SERVER['REQUEST_METHOD'] == "POST"){
        $getHeaders=$this->getHeaders();
        $data = json_decode(file_get_contents('php://input'), true);  
        $pkgID = isset($data['pkgID']) ? mysqli_real_escape_string($this->dbcharesc,$data['pkgID']) : '';
        $hubID = isset($data['hubID']) ? mysqli_real_escape_string($this->dbcharesc,$data['hubID']) : '';
        $carID = isset($data['carID']) ? mysqli_real_escape_string($this->dbcharesc,$data['carID']) : '';
        $date = isset($data['date']) ? mysqli_real_escape_string($this->dbcharesc,$data['date']) : '';           
        $time = isset($data['time']) ? mysqli_real_escape_string($this->dbcharesc,$data['time']) : '';
        $addressID = isset($data['addressID']) ? mysqli_real_escape_string($this->dbcharesc,$data['addressID']) : '';
        $billingName = isset($data['billingName']) ? mysqli_real_escape_string($this->dbcharesc,$data['billingName']) : '';
        $emailID = isset($data['emailID']) ? mysqli_real_escape_string($this->dbcharesc,$data['emailID']) : '';
        $gstNo = isset($data['gstNo']) ? mysqli_real_escape_string($this->dbcharesc,$data['gstNo']) : '';
        $pincode = isset($data['pincode']) ? mysqli_real_escape_string($this->dbcharesc,$data['pincode']) : '';
        $reqID = isset($data['reqID']) ? mysqli_real_escape_string($this->dbcharesc,$data['reqID']) : '';
        $isNotifyMe = isset($data['isNotifyMe']) ? mysqli_real_escape_string($this->dbcharesc,$data['isNotifyMe']) : '';
        $bookedFrom = isset($data['bookedFrom']) ? mysqli_real_escape_string($this->dbcharesc,$data['bookedFrom']) : '';
        $officeNo = isset($data['officeNo']) ? mysqli_real_escape_string($this->dbcharesc,$data['officeNo']) : '';
        $floorNo = isset($data['floorNo']) ? mysqli_real_escape_string($this->dbcharesc,$data['floorNo']) : '';
        $jobID = isset($data['jobID']) ? mysqli_real_escape_string($this->dbcharesc,$data['jobID']) : '';
        $paymentMethod = isset($data['paymentMethod']) ? mysqli_real_escape_string($this->dbcharesc,$data['paymentMethod']) : '0';
        $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
        $contactNo=isset($data['contactNo']) ? mysqli_real_escape_string($this->dbcharesc,$data['contactNo']) : '';
        $billingAddress=isset($data['billingAddress']) ? mysqli_real_escape_string($this->dbcharesc,$data['billingAddress']) : '';    
        $promoCode=isset($data['promoCode']) ? mysqli_real_escape_string($this->dbcharesc,$data['promoCode']) : '';
        if(!empty($pkgID)&&!empty($hubID)&&!empty($date)&&$date!='0000-00-00'&&!empty($getHeaders)&&!empty($carID)&&!empty($time)&&$time!='00:00:00'&&!empty($reqID)){

                #validate customer
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
                
                if($checkCustomer&&$custID==$checkCustomer->custID){ 
                    #email validation
                    if(!empty($emailID)){
                    if($this->validateEmail($emailID)){ 
                        $validEmail=true;

                    }else{
                        $validEmail=false; 
                    }
                    }else{
                        $validEmail=true; 
                    }
                     
                    if($validEmail){ 
                        #validating hub
                        $hubData=$this->apimodelcustomer->getHubByID($hubID);

                        if($hubData&&($hubData->specialHub==1)){

                        #special Hub condition                            
                        $pincodeData=(object)array("parentHub"=>$hubData->destID);

                        if(!empty($officeNo)||!empty($jobID)){
                        $addressData=(object)array("custID"=>$checkCustomer->custID);
                        }else{
                         $addressData=(object)array("custID"=>0);   
                        }
                         $jobType=1; 
                        
                        }else{ 

                        #feching address details, pincode
                        $addressData=$this->apimodelcustomer->getCustomerAddressByID($addressID);
                            
                       // if($addressData->pincode==$pincode){    
                        $pincodeData=$this->apimodelcustomer->getPincodeDetails($addressData->pincode); 
                        if($pincodeData){
                         switch ($pkgID) {
                                        case '1':
                                            $pincodeData->parentHub=$pincodeData->parentHub2;
                                            break;
                                        case '2':
                                          $pincodeData->parentHub=$pincodeData->parentHub;
                                            break;
                                        default:
                                            $pincodeData->parentHub=0;
                                            break;
                                    }
                        }
                        /*}else{
                        $pincodeData=false;    
                        }*/
                        $jobType=0;
                        }                   
                        
                        if(!empty($promoCode)){
                            $promoCodeDetails=$this->apimodelcustomer->getPromoCodeDetails($promoCode);
                            if($promoCodeDetails){
                                if($promoCodeDetails->multiTimes==0){
                                    $promoCodeUseDetails=$this->apimodelcustomer->checkForIsUsed($promoCode,0);
                                    if($promoCodeUseDetails){
                                        $message="This promocode has been already used .";
                                        $promoCodeValid=false;
                                    }else{
                                        $promoCodeValid=true;
                                    }
                                }elseif($promoCodeDetails->multiTimes==1){
                                    $promoCodeUseDetails=$this->apimodelcustomer->checkForIsUsed($promoCode,$checkCustomer->custID);
                                      if($promoCodeUseDetails){
                                       $promoCodeValid=false;
                                        $message="You have already used this promocode.";
                                    }else{
                                        $promoCodeValid=true;
                                    }
                                }else{
                                    $promoCodeValid=false;
                                       $message="Please enter valid promocode.";
                                }

                               
                            }else{
                                 $promoCodeValid=false;
                                  $message="Please enter valid promocode.";
                            }
                        }else{
                            $promoCodeValid=true;
                        }
                        if($promoCodeValid){

                        if($pincodeData&&$pincodeData->parentHub!=0){

                            $dateTimeToday=date('Y-m-d H:i:s',strtotime('-15 minutes'));

                            $dateTimeBooking=date('Y-m-d H:i:s', strtotime($date.' '.$time));

                            if(strtotime($dateTimeToday)<=strtotime($dateTimeBooking)){ 

                                #validating other details
                                           
                                $pkgData=$this->apimodelcustomer->validatePkg($pkgID,$jobType);

                                $carData=$this->apimodelcustomer->getCustomerCarsByID($carID);
                                $validateBookingRequest=$this->apimodelcustomer->validateBookingRequest($reqID);
                                if($hubData&&$pkgData&&$pincodeData->parentHub==$hubID&&$hubID!=0&&$hubData&&$carData&&$carData->custID==$checkCustomer->custID&&$addressData->custID==$checkCustomer->custID&&$validateBookingRequest){                                                   
                                  //  if(strtotime($dateTimeBooking) > strtotime($dateTimeToday)){ 

                                        #cheking time slot availablity
                                        $timeslotsAvailability=$this->apimodelcustomer->getTimeslotsAvailability($pkgID,$hubID,$dateTimeBooking,$hubData->specialHub);
                                        if($timeslotsAvailability){
                                            $capacity=$timeslotsAvailability->capacity;

                                            #checking no of bookings done
                                            $noOfBookings=$this->apimodelcustomer->geNoOfBookings($pkgID,$hubID,$date,$time,$hubData->specialHub);

                                            #cheking no of bookings going on
                                            $noBookingReq=$this->apimodelcustomer->getNoOfBookingRequests($pkgID,$hubID,$date,$time,$hubData->specialHub,$reqID,$checkCustomer->custID);
                                            $available=$capacity-($noOfBookings+$noBookingReq);

                                            #validate time slot available or not
                                            if($available>0){
                                               
                                                #for special hub service address
                                                if($hubData->specialHub==1){

                                                $serviceAddressID=0; 
                                                if(!empty($jobID)){
                                                    $jobData=$this->apimodelcustomer->getJobByID($jobID);    
                                                    $address=$jobData->serviceAddress; 
                                                }else{   
                                                    if(!empty($floorNo)){
                                                        $floorNo=', Floor No- '.$floorNo;
                                                    }
                                                    $address=$officeNo.$floorNo.' / '.$hubData->destAddress; 
                                                }
                                                
                                                $serviceAddress=$this->arrangeAddress($address);
                                                }else{
                                                #for on Demand     
                                                #formating Address
                                                if(!empty($gstNo)){
                                                    $gstNoAddr=$gstNo;
                                                }else if(empty($addressData->GSTNo)){
                                                    $gstNoAddr="NA";
                                                }else{
                                                     $gstNoAddr=$addressData->GSTNo;
                                                }
                                                $address=$addressData->buildingDetails.','.$addressData->areaDetails.','.$addressData->landmark.',  '.$addressData->city.'  '.$addressData->state.', PIN : '.$addressData->pincode .',  GSTIN : '.$gstNoAddr;
                                                $serviceAddress=$this->arrangeAddress($address);
                                                $serviceAddressID=$addressData->addressID;

                                                }
                        if(!empty($gstNo)){
                                                    $gstNoAddrBill=$gstNo;
                                                }else{
                        $gstNoAddrBill="NA";
                        }
                                                if(empty($billingAddress)){
                                                    $billingAddress=$serviceAddress;
                                                }else{
                            $billingAddress=$billingAddress.',  GSTIN : '.$gstNoAddrBill;
                        }
						$isMembershipService=0;
                                                 $isMember="0";
                                                #cheking for membership 
                                             //   $membership=$this->apimodelcustomer->getCustomerMembership($checkCustomer->custID);
                          $membership=$this->apimodelcustomer->getCustomerMembershipByDate($checkCustomer->custID,date('Y-m-d',strtotime($date)));
                                                if($membership&&!empty($carData->isInMembership)){
                                                    $pkgAmount=$pkgData->membershipPrice;
						    $isMembershipService="1";
                                                    $isMember="1";
                                                }else{
                                                    $pkgAmount=$pkgData->price;
                                                }

                                                #checking billing Name
                                                if(empty($billingName)){
                                                    $billingName=$checkCustomer->custName;
                                                }

                                                #check already requested or not
                                                $jobDataRequest=$this->apimodelcustomer->getJobByJobReqID($reqID,$checkCustomer->custID);
                                                if($jobDataRequest){
						$discountAmount="0";
                                                $bookingType="0";
						$amountTobePaid=$pkgAmount;
                                                    // Check valid promo code
                                                if(!empty($promoCode)){
                                               
                                                
                                                if($promoCodeDetails){
                                                  //   $discountAmount=$promoCodeDetails->value;
                                               //     $amountTobePaid=$pkgAmount-$promoCodeDetails->value;

						if($promoCodeDetails->unit=="%"){
                                                        $discountAmount=round($promoCodeDetails->value*$pkgAmount/100);
                                                    }else{
                                                        $discountAmount=$promoCodeDetails->value;
                                                    }
						  $amountTobePaid=$pkgAmount-$discountAmount;
                                                    $bookingType="3";


                                                }
                                                }
                        /* if($paymentMethod==2){
                                                  $paymentMethodDb=3;  
                                                }else{
                                                   $paymentMethodDb=1; 
                                                }*/
                if($paymentMethod==2){
                                                  $paymentMethodDb=3;  
                                                }else if($paymentMethod==1){
                                                   $paymentMethodDb=1; 
                                                }else{
                                                    $paymentMethodDb=0;
                                                }
                                                   $data=array('billingName'=>$billingName,
                                                            'billingAddress' =>$billingAddress,
                                                            'isNotifyMe'=>$isNotifyMe,
                                                            'paymentMethod'=>$paymentMethodDb,
                                                            'contactNo'=>$contactNo,
							    'discountAmount'=>$discountAmount,
                                                            'bookingType'=>$bookingType
                                                           );
                                                

                                                #insert Booking
                                                $insertJob =$this->apimodelcustomer->updateJob(array("jobID"=>$jobDataRequest->jobID),$data);

                                                $jobID=$jobDataRequest->jobID;  

                                                }else{

                                                #generate REF ID
                                           //     $jobIdentifier="";
                                               /* do{
                                                $jobIdentifier=$this->generateRefId($jobType);  
                                                #check id exists
                                                $checkJob=$this->apimodelcustomer->getJobByJobRefID($jobIdentifier);
                                                if($checkJob){  
                                                $jobIdentifier="";    
                                                }
                                                }while($jobIdentifier=="");*/
                                               /* if($paymentMethod==2){
                                                  $paymentMethodDb=3;  
                                                }else{
                                                   $paymentMethodDb=1; 
                                                }*/
                        if($paymentMethod==2){
                                                  $paymentMethodDb=3;  
                                                }else if($paymentMethod==1){
                                                   $paymentMethodDb=1; 
                                                }else{
                                                /*     do{
                                                        $jobIdentifier=$this->generateRefId($jobType);  
                                                        #check id exists
                                                        $checkJob=$this->apimodelcustomer->getJobByJobRefID($jobIdentifier);
                                                        if($checkJob){  
                                                        $jobIdentifier="";    
                                                        }
                                                        }while($jobIdentifier=="");*/
                                                    $paymentMethodDb=0;

                                                }
						$discountAmount="0";
                                              $bookingType="0";
						$amountTobePaid=$pkgAmount;
                                                // Check valid promo code
                                                if(!empty($promoCode)){
                                               
                                                
                                                if($promoCodeDetails){
                                                  //  $discountAmount=$promoCodeDetails->value;
                                                   // $amountTobePaid=$pkgAmount-$promoCodeDetails->value;
						if($promoCodeDetails->unit=="%"){
                                                        $discountAmount=round($promoCodeDetails->value*$pkgAmount/100);
                                                    }else{
                                                        $discountAmount=$promoCodeDetails->value;
                                                    }
                                                    
                                                    $amountTobePaid=$pkgAmount-$discountAmount;
                                                    $bookingType="3";


                                                }
                                                }

                                                ////////////////Generate Job Reference Number/////////
                                                do
                                                {
                                                        $jobIdentifier=$this->generateRefId($jobType);  
                                                        #check id exists
                                                        $checkJob=$this->apimodelcustomer->getJobByJobRefID($jobIdentifier);
                                                        if($checkJob)
                                                        {  
                                                            $jobIdentifier="";    
                                                        }
                                                }while($jobIdentifier=="");
                                                
                                                ////////////////////////////////
                                                
                                                $data=array('customerID' =>$checkCustomer->custID,
                                                            'jobIdentifier'=>$jobIdentifier,
                                                            'empID' =>0,
                                                            'pkgID' =>$pkgID,
                                                            'pkgName' =>$pkgData->pkgName,
                                                            'pkgAmount' =>$pkgAmount,
                                                            'billingName'=>$billingName,
                                                            'serviceAddress' =>$serviceAddress,
                                                            'billingAddress' =>$billingAddress,
                                                            'addressID'=>$serviceAddressID,
                                                            'billingAddressID'=>$serviceAddressID,
                                                            'billAddrSameAsServAddr'=>1,
                                                            'date'=>date('Y-m-d',strtotime($date)),
                                                            'vehicleID'=>$carID,
                                                            'vehicleMake' =>$carData->carMake,
                                                            'vehicleModel' =>$carData->carModel,
                                                            'vehicleNoPlate' =>$carData->carNoPlate,
                                                            'hubDestID' =>$hubID,
                                                            'serviceTime' =>date('H:i:s',strtotime($time)),
                                                            'reportingTime'=>date('H:i:s',strtotime($time)),
                                                            'type' =>$hubData->type,
                                                            'bookedFrom'=>$bookedFrom,
                                                            'isNotifyMe'=>$isNotifyMe,
                                                            'paymentMethod'=>$paymentMethodDb,
                                                            'contactNo'=>$contactNo,
                                                            'jobStatus'=>6,
                                                            'isActive'=>0,
						 	    'discountAmount'=>$discountAmount,
                                                            'bookingType'=>$bookingType,
                                                            'isMembershipService'=>$isMembershipService,
                                                            'isMember'=>$isMember,
                                                            'bookingReqID'=>$reqID,
                                                            'created' =>date('Y-m-d H:i:s') );
                                                

                                                #insert Booking
                                                $insertJob =$this->apimodelcustomer->insertJob($data);

                                                $jobID= $this->db->insert_id();



                                                }

                                                if($insertJob){ 

                                                         if(!empty($promoCode)){                                               
                                                
                                                        if($promoCodeDetails){
                                                            //$pkgAmount=$pkgAmount-$promoCodeDetails->value;
                                                            $jobPromo=$this->apimodelcustomer->checkForIsUsedJobID($promoCode,$checkCustomer->custID,$jobID);
                                                            if(!$jobPromo){
                                                            $dataJobCode=array("jobId"=>$jobID,
                                                                               "promoCodeId"=>$promoCodeDetails->promoCodeId,
                                                                               "promoCode"=>$promoCode,
                                                                               "value"=>$promoCodeDetails->value,
                                                                               "isActive"=>1,
                                                                               "created"=>date('Y-m-d H:i:s'));

                                                            $insertJobPromo=$this->apimodelcustomer->insertJobPromo($dataJobCode);
                                                            }
                                                        }
                                                        }
                                                               /* $updateData['jobIdentifier'] = $jobIdentifier;
                                                        $updateJob =$this->apimodelcustomer->updateJob(array('jobID' =>$jobID),$updateData);*/

                                                        #update email ID
                                                        if(!empty($emailID)){
                                                          $updateEmail=$this->apimodelcustomer->updateCustomer(array('custID' =>$checkCustomer->custID),array('custEmailID' =>  $emailID));
                                                        }
                                                       
                                                        #billing data    
                                                        $checkBillingName=$this->apimodelcustomer->checkBillingName($checkCustomer->custID,$billingName);
                                                        if(!$checkBillingName){
                                                        $dataBillingName=array("customerID"=>$checkCustomer->custID,
                                                                                "billingName"=>$billingName,
                                                                                "isActive"=>1,
                                                                                "created"=>date('Y-m-d H:i:s'));
                                                        $checkBillingName=$this->apimodelcustomer->insertBillingName($dataBillingName);
                                                        }

                                                        #update car 
                                                        if($carData->carIsActive==0){
                                                           $updateCar=$this->apimodelcustomer->updateCar(array("carID"=>$carData->carID),array("carIsActive"=>1));
                                                        }

                                                        if($hubData->specialHub!=1){
                                                        #update Address 
                                                        if($addressData->isActive==0){
                                                           $updateAddress=$this->apimodelcustomer->updateAddress(array("addressID"=>$addressData->addressID),array("isActive"=>1));
                                                        }

                                                         #update GST No
                                                        if(!empty($gstNo)){  
                                                          $updateAddress=$this->apimodelcustomer->updateAddress(array("addressID"=>$addressID),array("GSTNo"=>$gstNo));   
                                                        }
                                                        }

                                                        $transactionID="";
                                                        #fetching job details
                                                        $job=$this->getJobByID($jobID);
                                                        if($job){
                                                          $jobData=$job;  
                                                            $jobIdentifier=$jobData['jobName'];
                                                            if($paymentMethod==1||$paymentMethod==2){ //1 for payment method paytm

                                                                do{
                                                                    $refTransactionID=$this->generateTransactionID();//1 for txn type job

                                                                    $txnData=array("refTransactionID"=>$refTransactionID,
                                                                                    "dataID"=>$jobID,
                                                                                    "refType"=>1,
                                                                                    "platform"=>$bookedFrom,
                                                                                    "isActive"=>1,
                                                                                    "created"=>date('Y-m-d H:i:s'));
                                                                    $insertTxn=$this->apimodelcustomer->insertPaytmTxn($txnData);

                                                                   
                                                                }while($insertTxn==0);

                                                            }else{
                                                             $this->apimodelcustomer->updateJob(array("jobID"=>$jobID),array("isActive"=>1,"isCODviaApp"=>1));
                                                                #update Booking Req
                                                                $updateBookingReq=$this->apimodelcustomer->updateBookingReq(array("reqID"=>$reqID),array('jobID' =>$jobID,'isComplete'=>1));
                                                                $refTransactionID="";
                                                                
                                                                #send Sms 
                                                                if(!empty($checkCustomer->custMobNo)&&($checkCustomer->isSMSSubscribed==1)){
                                                                    $smsDate=date('d-M',strtotime($date));
                                                                    $smsTime=date('h:i A',strtotime($time));
                                                                    $message = 'Your '.$pkgData->pkgName.' ('.$jobIdentifier.') is confirmed for '.$smsDate.' at '.$smsTime.'. To cancel or reschedule, please call 8080720000. Thanks, Refreshed Car Care';
                                                                  
                                                                  $sendMsg=$this->sendSms2($checkCustomer->custMobNo,$message);
                                                                  if($sendMsg){

                                                                    $dataSms=array('recordID' =>$jobID,
                                                                                    'custID'=>$checkCustomer->custID,
                                                                                    'mobileNo'=>$checkCustomer->custMobNo,
                                                                                    'sms'=>$message,
                                                                                    'smsType'=>1, //For Job SMS
                                                                                    'smsSubType'=>1, //For Confirm Job SMS
                                                                                    'isActive'=>1,
                                                                                    'created'=>date('Y-m-d H:i:s'));
                                                                    $insertSMS=$this->apimodelcustomer->insertSms($dataSms);
                                                                                                
                                                                    $updateJob =$this->apimodelcustomer->updateJob(array('jobID' =>$jobID),array("isSmsSentBooking"=>1));
                                                                  }
                                                                }

                                                                #send Pepo Campaign Email
                                                                if(!empty($checkCustomer->custEmailID)){
                                                                $this->bookingPepoCampaign($checkCustomer->custEmailID,$checkCustomer->custName,$jobIdentifier,$date,$time,$serviceAddress,$checkCustomer->custMobNo,$pkgData->pkgName,$pkgAmount,$jobID);
                                                                   
                                                                }
                                                                
                                                                # send Noti
                                                                if(!empty($isNotifyMe)){
                                                                
                                                                $notification="Your Refreshed service is confirmed: Booking ID ".$jobIdentifier;
                                                                
                                                                $notiData=array("recieverID"=>$checkCustomer->custID,
                                                                              "notification"=>$notification,
                                                                              "type"=>1,
                                                                              "subType"=>1,
                                                                              "dataID"=>$jobID,
                                                                              "isActive"=>1,
                                                                              "created"=>date('Y-m-d H:i:s'));
                                                                $insertNotiCust=$this->apimodelcustomer->insertNotiCust($notiData);
                                                               

                                                                $deviceTokens=$this->apimodelcustomer->getDeviceTokens($checkCustomer->custID);
                                                                  $badgeCount=$this->apimodelcustomer->getBadgeCount($checkCustomer->custID);
                                                                if($deviceTokens){
                                                                  foreach ($deviceTokens as $readToken) {
                                                                    if($readToken->deviceType==1){
                                                                    //send ios noti   
                                                                    $this->iOSNotification($readToken->deviceToken,$notification,$jobID,1,1,$badgeCount);
                                                                    }else{
                                                                    //send ios android noti
                                                                    $this->androidNotification($readToken->deviceToken,$notification,$jobID,1,1);    
                                                                    }
                                                                  }
                                                                }




                                 }

                                                        }

                                                        }else{
                                                          $jobData=array();  
                                                        }
                                                        $arrResponse = array("status" => 200, "message" => "success","jobData"=>$jobData,"refTransactionID"=>$refTransactionID, "jobIdentifier"=>$jobIdentifier,"pkgAmount"=>$amountTobePaid);
                                                        $statusCode=200; 
                                                }else{
                                                  $arrResponse = array("status" => 500, "message" => "Something went wrong! Please try again.");
                                                    $statusCode=500;   
                                                }
                                            }else{
                                                $arrResponse = array("status" => 520, "message" => "No time slots available! Please try again.");
                                                $statusCode=520;   
                                            }
                                        
                                        }else{
                                            $arrResponse = array("status" => 520, "message" => "No time slots available! Please try again.");
                                            $statusCode=520; 
                                        }
                                    /*}else{
                                        $arrResponse = array("status" => 400, "message" => "Please enter correct date & time!");
                                            $statusCode=400; 
                                    }  */
                        
                                }else{
                                    $arrResponse = array("status" => 400, "message" => "Please enter valid data!");
                                $statusCode=400;  
                                }          
                    
                            }else{
                              $arrResponse = array("status" => 400, "message" => "Please enter correct date & time!");
                            $statusCode=400;  
                            }
                        }else{
                        $arrResponse = array("status" => 203, "message" => "Currently we are not providing service to this area !");
                        $statusCode=203;
                        }
                        }else{
                        $arrResponse = array("status" => 400, "message" => $message);
                        $statusCode=403;
                    } 
                    }else{
                        $arrResponse = array("status" => 400, "message" => "Please enter valid email !");
                        $statusCode=403;
                    } 
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                 $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
             $statusCode=405;
        }

       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }

  
     /**********************************get Days**************************************/

    public function getDays(){

    if($_SERVER['REQUEST_METHOD'] == "POST"){
           
            $data = json_decode(file_get_contents('php://input'), true); 
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            $getHeaders=$this->getHeaders();
            if(!empty($getHeaders)){

                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);

                if($checkCustomer&&$custID==$checkCustomer->custID){  
                    
                  
                   $dateArr=array();

                    for($x=0;$x<7;$x++){
                        $date = date('Y-m-d', strtotime('+'.$x.' Day'));    
                        $day=date('D', strtotime($date));
                        if($day !="Wed"){
                        $dateArr[]=array("date"=>$date,
                                         "day"=>$day);
                        }
                    }                   
                    
          
                    $arrResponse = array("status" => 200, "message" => "success","dateData"=>$dateArr);
                    $statusCode=200;
                  
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                 $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
             $statusCode=405;
        }

       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }

     /**********************************get My Orders**************************************/

    public function getMyOrders(){

    if($_SERVER['REQUEST_METHOD'] == "POST"){
            
            $getHeaders=$this->getHeaders();
            $data = json_decode(file_get_contents('php://input'), true); 
            $offset = isset($data['offset']) ? mysqli_real_escape_string($this->dbcharesc,$data['offset']) : '';
            $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            $flag=isset($data['flag']) ? mysqli_real_escape_string($this->dbcharesc,$data['flag']) : '0';
            if(!empty($getHeaders)){
                #validating customer
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);

                if($checkCustomer&&$custID==$checkCustomer->custID){  
                   
                    if(empty($offset)){
                        $offset=0;
                    }
                  
                   $ordersArr=array();
                   #fetching orders data
                   $jobData =$this->apimodelcustomer->getJobsByCustID($checkCustomer->custID,$offset,$flag);

                   if($jobData){

                        foreach ($jobData as $readJob) {   
                            #fetching job details 
                            $job=$this->getJobByID($readJob->jobID);
                            if($job){
                               
                              $ordersArr[]=$job;  
                            }

                        }
                    }

                    $offset=$offset+count($ordersArr);
          
                    $arrResponse = array("status" => 200, "message" => "success","ordersData"=>$ordersArr,"offset"=>$offset);
                    $statusCode=200;
                  
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                 $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
             $statusCode=405;
        }

       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }

     /**********************************Give Rating**************************************/

    public function giveRating(){

    if($_SERVER['REQUEST_METHOD'] == "POST"){
            
            $getHeaders=$this->getHeaders();
            $data = json_decode(file_get_contents('php://input'), true); 
            $jobID = isset($data['jobID']) ? mysqli_real_escape_string($this->dbcharesc,$data['jobID']) : '';
            $rating = isset($data['rating']) ? mysqli_real_escape_string($this->dbcharesc,$data['rating']) : '';
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            if(!empty($getHeaders)&&!empty($rating)&&$rating<=5&&!empty($jobID)){
                #validating customer
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);

                if($checkCustomer&&$custID==$checkCustomer->custID){                     
                  
                   $ordersArr=array();
                   #fetching orders data
                   $jobData =$this->apimodelcustomer->getJobByID($jobID);

                    if($jobData){

                        if($jobData->jobStatus==5||$jobData->jobStatus==4){
                           
                            if($jobData->rating==0){

                                $updateRating=$this->apimodelcustomer->updateJob(array("jobID"=>$jobID),array("rating"=>$rating));
                                
                                if($updateRating){
                                    $arrResponse = array("status" => 200, "message" => "Rating updated successfully.","rating"=>$rating);
                                    $statusCode=200;
                                }else{
                                    $arrResponse = array("status" => 500, "message" => "Something went wrong.");
                                    $statusCode=500;
                                }  
                            }else{
                            $arrResponse = array("status" => 400, "message" => "You have already given rating for this service !");
                            $statusCode=400;    
                            }
                         }else{
                            $arrResponse = array("status" => 400, "message" => "You can give rating to completed service !");
                            $statusCode=400;    
                        }    
                       
                    }else{
                      $arrResponse = array("status" => 403, "message" => "Please enter correct data !");
                      $statusCode=403;  
                    }
                  
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                 $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
             $statusCode=405;
        }

       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }

     /**********************************Cancel Job**************************************/

    public function cancelJob(){

    if($_SERVER['REQUEST_METHOD'] == "POST"){
            
            $getHeaders=$this->getHeaders();
            $data = json_decode(file_get_contents('php://input'), true); 
            $jobID = isset($data['jobID']) ? mysqli_real_escape_string($this->dbcharesc,$data['jobID']) : '';
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            $cancelReason=isset($data['reason']) ? mysqli_real_escape_string($this->dbcharesc,$data['reason']) : ''; 
            if(!empty($getHeaders)&&!empty($jobID)){
                #validating customer
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);

                if($checkCustomer&&$custID==$checkCustomer->custID){                     
                  
                   $ordersArr=array();
                   #fetching orders data
                   $jobData =$this->apimodelcustomer->getJobByID($jobID);

                    if($jobData){

                        if($jobData->customerID==$checkCustomer->custID){

                          //  $today=date('Y-m-d'); // H:i:s

                          //  $jobTime=date('Y-m-d',strtotime($jobData->date)); // H:i:s

                          //  if(strtotime($today)<strtotime($jobTime)){
                #can cancel order
                            $today=date('Y-m-d  H:i:s'); //

                            $jobDate=date('Y-m-d',strtotime($jobData->date.' '.$jobData->serviceTime)); // H:i:s

                            $timediff = strtotime($jobDate) - strtotime($today);
                            
                            if(($timediff > 86400)&&($jobData->jobStatus!=7)){
                                  /** Start Transaction **/
                                   // $this->db->trans_begin();

                                if($jobData->jobStatus==6){      

                                        $updateJob=$this->apimodelcustomer->updateJob(array("jobID"=>$jobID),array("jobStatus"=>7,"cancelReason"=>$cancelReason));
                                        
                                        if($updateJob){

                                            if($jobData->paymentStatus==1&&($jobData->paymentMethod==1||$jobData->paymentMethod==3)){
                                            #refund payment    

                                            $transactionDetails=$this->apimodelcustomer->getTransactionDetailsByJobID($jobData->jobID);
                    
                                            if($transactionDetails){
                                                $refunPaymentData=$this->refundTransaction($transactionDetails);

                                                if($refunPaymentData&&!empty($refunPaymentData['STATUS'])&&!empty($refunPaymentData['RESPCODE'])&&!empty($refunPaymentData['RESPMSG'])&&!empty($refunPaymentData['ORDERID'])){

                                                    $refundTransactionDetails=$this->apimodelcustomer->getRefundTransactionDetails($transactionDetails->refTransactionID);
                                                    if($refundTransactionDetails){
                                                            $updateData=array(
                                                                     "transactionID"=>$refunPaymentData['TXNID'],
                                                                      "merchantID"=>$refunPaymentData['MID'],
                                                                      "transactionAmount"=>(isset($refunPaymentData['TXNAMOUNT']))?$refunPaymentData['TXNAMOUNT']:"",
                                                                      "refundAmount"=>$refunPaymentData['REFUNDAMOUNT'],
                                                                      "bankTransactionID"=>(isset($refunPaymentData['BANKTXNID']))?$refunPaymentData['BANKTXNID']:"",
                                                                      "status"=>$refunPaymentData['STATUS'],
                                                                      "respCode"=>$refunPaymentData['RESPCODE'],
                                                                      "respMsg"=>$refunPaymentData['RESPMSG'],
                                                                      "transactionDateTime"=>(isset($refunPaymentData['TXNDATE']))?date('Y-m-d H:i:s',strtotime($refunPaymentData['TXNDATE'])):"",
                                                                      "gatewayName"=>(isset($refunPaymentData['GATEWAY']))?$refunPaymentData['GATEWAY']:"",
                                                                      "cardIssuer"=>(isset($refunPaymentData['CARD_ISSUER']))?$refunPaymentData['CARD_ISSUER']:"",
                                                                      "paymentMode"=>(isset($refunPaymentData['PAYMENTMODE']))?$refunPaymentData['PAYMENTMODE']:"",
                                                                      "totalRefundAmnt"=>(isset($refunPaymentData['TOTALREFUNDAMT']))?$refunPaymentData['TOTALREFUNDAMT']:"",
                                                                       "refundDateTime"=>(isset($refunPaymentData['REFUNDDATE']))?date('Y-m-d H:i:s',strtotime($refunPaymentData['REFUNDDATE'])):"",
                                                                        "refundType"=>(isset($refunPaymentData['REFUNDTYPE']))?$refunPaymentData['REFUNDTYPE']:"",
                                                                         "refID"=>(isset($refunPaymentData['REFID']))?$refunPaymentData['REFID']:"",
                                                                         "refundID"=>(isset($refunPaymentData['REFUNDID']))?$refunPaymentData['REFUNDID']:"",
                                                                      );
                                                          $updateTxn=$this->apimodelcustomer->updateRefundTransaction(array('refundPayID' =>$refundTransactionDetails->refundPayID),$updateData);
                                                    }else{

                                                     $insertData=array("refTransactionID"=>$refunPaymentData['ORDERID'],
                                                                      "transactionID"=>$refunPaymentData['TXNID'],
                                                                      "merchantID"=>$refunPaymentData['MID'],
                                                                      "transactionAmount"=>(isset($refunPaymentData['TXNAMOUNT']))?$refunPaymentData['TXNAMOUNT']:"",
                                                                      "refundAmount"=>$refunPaymentData['REFUNDAMOUNT'],
                                                                      "bankTransactionID"=>(isset($refunPaymentData['BANKTXNID']))?$refunPaymentData['BANKTXNID']:"",
                                                                      "status"=>$refunPaymentData['STATUS'],
                                                                      "respCode"=>$refunPaymentData['RESPCODE'],
                                                                      "respMsg"=>$refunPaymentData['RESPMSG'],
                                                                      "transactionDateTime"=>(isset($refunPaymentData['TXNDATE']))?date('Y-m-d H:i:s',strtotime($refunPaymentData['TXNDATE'])):"",
                                                                      "gatewayName"=>(isset($refunPaymentData['GATEWAY']))?$refunPaymentData['GATEWAY']:"",
                                                                      "cardIssuer"=>(isset($refunPaymentData['CARD_ISSUER']))?$refunPaymentData['CARD_ISSUER']:"",
                                                                      "paymentMode"=>(isset($refunPaymentData['PAYMENTMODE']))?$refunPaymentData['PAYMENTMODE']:"",
                                                                      "totalRefundAmnt"=>(isset($refunPaymentData['TOTALREFUNDAMT']))?$refunPaymentData['TOTALREFUNDAMT']:"",
                                                                       "refundDateTime"=>(isset($refunPaymentData['REFUNDDATE']))?date('Y-m-d H:i:s',strtotime($refunPaymentData['REFUNDDATE'])):"",
                                                                        "refundType"=>(isset($refunPaymentData['REFUNDTYPE']))?$refunPaymentData['REFUNDTYPE']:"",
                                                                         "refID"=>(isset($refunPaymentData['REFID']))?$refunPaymentData['REFID']:"",
                                                                         "refundID"=>(isset($refunPaymentData['REFUNDID']))?$refunPaymentData['REFUNDID']:"",
                                                                         "dataID"=>$jobData->jobID,
                                                                         "created"=>date('Y-m-d H:i:s')
                                                                      );
                                                            
                                                           $insertRefundTransaction=$this->apimodelcustomer->insertRefundPaytmTxn($insertData);     
                                                  
                                                }
                                                    //Insert transaction reports
                                                   $transactionReporData=array("refTransactionID"=>$refunPaymentData['ORDERID'],
                                                                      "transactionID"=>$refunPaymentData['TXNID'],
                                                                      "merchantID"=>$refunPaymentData['MID'],
                                                                        "transactionAmount"=>(isset($refunPaymentData['TXNAMOUNT']))?$refunPaymentData['TXNAMOUNT']:"",
                                                                       "bankTransactionID"=>(isset($refunPaymentData['BANKTXNID']))?$refunPaymentData['BANKTXNID']:"",
                                                                      "status"=>$refunPaymentData['STATUS'],
                                                                      "respCode"=>$refunPaymentData['RESPCODE'],
                                                                      "respMsg"=>$refunPaymentData['RESPMSG'],
                                                                     "transactionDateTime"=>(isset($refunPaymentData['TXNDATE']))?date('Y-m-d H:i:s',strtotime($refunPaymentData['TXNDATE'])):"",
                                                                      "gatewayName"=>(isset($refunPaymentData['GATEWAY']))?$refunPaymentData['GATEWAY']:"",
                                                                               "bankName"=>(isset($refunPaymentData['CARD_ISSUER']))?$refunPaymentData['CARD_ISSUER']:"",
                                                                              "paymentMode"=>(isset($refunPaymentData['PAYMENTMODE']))?$refunPaymentData['PAYMENTMODE']:"",
                                                                      "totalRefundAmnt"=>(isset($refunPaymentData['TOTALREFUNDAMT']))?$refunPaymentData['TOTALREFUNDAMT']:"",
                                                                       "refundDateTime"=>(isset($refunPaymentData['REFUNDDATE']))?date('Y-m-d H:i:s',strtotime($refunPaymentData['REFUNDDATE'])):"",
                                                                        "refundType"=>(isset($refunPaymentData['REFUNDTYPE']))?$refunPaymentData['REFUNDTYPE']:"",
                                                                         "refID"=>(isset($refunPaymentData['REFID']))?$refunPaymentData['REFID']:"",
                                                                         "refundID"=>(isset($refunPaymentData['REFUNDID']))?$refunPaymentData['REFUNDID']:"",
                                                                               "created"=>date('Y-m-d H:i:s'));                            
                                                                
                                                    $insertTransactionReport=$this->apimodelcustomer->insertTransactionReport($transactionReporData);


                                                    if($refunPaymentData['STATUS']=="TXN_SUCCESS"){ 
                                                         $status=1;
                                                          $updateJob=$this->apimodelcustomer->updateJob(array("jobID"=>$jobID),array("refundAmount"=>$refunPaymentData['TOTALREFUNDAMT']));
                                                         $invData=$this->apimodelcustomer->getInvoiceByJobID($jobID);
                                                         if($invData){
                                                            $this->apimodelcustomer->updateInvoice(array("invoiceID"=>$invData->invoiceID),array("isCanceled"=>1));
                                                         }
                                                    }else{
                                                        $updateJob=$this->apimodelcustomer->updateJob(array("jobID"=>$jobID),array("jobStatus"=>$jobData->jobStatus,"cancelReason"=>""));
                                                        $status=0;
                                                    }

                                                }else{
                                                   $status=0; 
                         $updateJob=$this->apimodelcustomer->updateJob(array("jobID"=>$jobID),array("jobStatus"=>$jobData->jobStatus,"cancelReason"=>"")); 
                                                }
                                            }else{
                                                $status=1; 
                                            }

                                            }else{
                                                $status=1;
                                            }
                                            if($status==1){
                                            #send SMS  
                                            if(!empty($jobData->custMobNo)&&($jobData->isSMSSubscribed==1)){
                                            $pkgName=$jobData->pkgName;
                                            $refID=$jobData->jobIdentifier;
                                            $smsDate=date('d-M',strtotime($jobData->date));
                                            $smsTime=date('h:i A',strtotime($jobData->reportingTime));    

                                            $message = 'Your '.$pkgName.' ('.$refID.') on '.$smsDate.' at '.$smsTime.' is cancelled. To reschedule, please call us on 8080720000. Thanks, Refreshed Car Care';
                                            
                                            $sendMsg=$this->sendSms2($jobData->custMobNo,$message);
                        //$sendMsg=true;
                                             if($sendMsg){
                                                $dataSms=array('recordID' =>$jobID,
                                                                                      'custID'=>$checkCustomer->custID,
                                                                                      'mobileNo'=>$jobData->custMobNo,
                                                                                      'sms'=>$message,
                                                                                      'smsType'=>1, //For Job SMS
                                                                                      'smsSubType'=>4, //For cancel Job SMS
                                                                                      'isActive'=>1,
                                                                                      'created'=>date('Y-m-d H:i:s')
                                                                                      );
                                                $insertSMS=$this->apimodelcustomer->insertSms($dataSms);
                                                $updateJob =$this->apimodelcustomer->updateJob(array('jobID' =>$jobID),array("isSmsSentCancel"=>1));
                                            }
                                            }
                                            $jobData=$this->getJobByID($jobID);
                                            $arrResponse = array("status" => 200, "message" => "Service canceled successfully.","data"=>$jobData);
                                            $statusCode=200;
                                            }else{
                                             $arrResponse = array("status" => 500, "message" => "Something went wrong.");
                                            $statusCode=500;   
                                            }
                                        }else{
                                            $arrResponse = array("status" => 500, "message" => "Something went wrong.");
                                            $statusCode=500;
                                        }                                 
                                }else{
                                    $arrResponse = array("status" => 400, "message" => "You can not cancel this service !");
                                    $statusCode=400;    
                                }    
                            }else{
                                $arrResponse = array("status" => 400, "message" => "You can not cancel this service !");
                                $statusCode=400;    
                            }
                        }else{
                          $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                          $statusCode=403;  
                        }
                    }else{
                      $arrResponse = array("status" => 400, "message" => "Please enter valid data.");
                      $statusCode=400;  
                    }
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                 $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
             $statusCode=405;
        }        
       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }

    /************************************ getNotifications ************************************/
    public function getNotifications(){        

        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $dataArr=array();
            $getHeaders=$this->getHeaders();
            $data = json_decode(file_get_contents('php://input'), true); 
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
 
            if(!empty($getHeaders)){
                $dataArr=array();
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
            
                if($checkCustomer&&$custID==$checkCustomer->custID){
                $updateNoti=$this->apimodelcustomer->updateNotification(array("recieverID"=>$checkCustomer->custID),array("isRead"=>1));                   
                $notificationData=$this->apimodelcustomer->getNotifications($checkCustomer->custID);
                if($notificationData){

                    foreach ($notificationData as $readNotification) {
                        if($readNotification->type==1){
                            #fetching job details
                            $jobData=$this->getJobByID($readNotification->dataID);
                            if($jobData){
                                $readNotification->jobDetails=$jobData;
                            }else{
                                $readNotification->jobDetails=(object)array();
                            }
                        }else{
                            $readNotification->jobDetails=(object)array();
                        }

                        $notiArr[]=$readNotification;
                    }

                }else{
                    $notiArr=array();
                }                                    
 
                $arrResponse = array("status" => 200, "message" => "success", "notificationData" =>$notiArr); 
                $statusCode=200;
               
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                       $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                   $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
               $statusCode=405;
        }
        http_response_code($statusCode); 
      echo json_encode($arrResponse);   

    }

     /**********************************Get Job Details**************************************/

    public function getJobDetails(){

    if($_SERVER['REQUEST_METHOD'] == "POST"){
            
            $getHeaders=$this->getHeaders();
            $data = json_decode(file_get_contents('php://input'), true); 
            $jobID = isset($data['jobID']) ? mysqli_real_escape_string($this->dbcharesc,$data['jobID']) : '';
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            if(!empty($getHeaders)&&!empty($jobID)){
                #validating customer
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);

                if($checkCustomer&&$custID==$checkCustomer->custID){                     
                  
                   $ordersArr=array();
                   #fetching orders data
                   $jobData =$this->apimodelcustomer->getJobByID($jobID);

                    if($jobData){

                        if($jobData->customerID==$checkCustomer->custID){
                        $jobDetails=$this->getJobByID($jobData->jobID);     
                        $arrResponse = array("status" => 200, "message" => "success","jobDetails"=>$jobDetails);
                          $statusCode=200;    
                        }else{
                          $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                          $statusCode=403;  
                        }
                    }else{
                      $arrResponse = array("status" => 400, "message" => "Please enter valid data.");
                      $statusCode=400;  
                    }
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                 $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
             $statusCode=405;
        }        
       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }

    /********************************** Logout **************************************/

    public function logout(){

    if($_SERVER['REQUEST_METHOD'] == "POST"){
            
            $getHeaders=$this->getHeaders();
            $data = json_decode(file_get_contents('php://input'), true); 
            $androidKey = isset($data['androidKey']) ? mysqli_real_escape_string($this->dbcharesc,$data['androidKey']) : '';
            $iosKey = isset($data['iosKey']) ? mysqli_real_escape_string($this->dbcharesc,$data['iosKey']) : '';
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            if(!empty($getHeaders)){
                #validating customer
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);

                if($checkCustomer&&$custID==$checkCustomer->custID){   

                         if(!empty($androidKey)){

                            $updateDeviceToken=$this->apimodelcustomer->updateDeviceToken(array("deviceToken"=>$androidKey,"deviceType"=>0,"custID"=>$checkCustomer->custID),array("isActive"=>0));

                         }

                         if(!empty($iosKey)){

                            $updateDeviceToken=$this->apimodelcustomer->updateDeviceToken(array("deviceToken"=>$iosKey,"deviceType"=>1,"custID"=>$checkCustomer->custID),array("isActive"=>0));

                         }
                        $arrResponse = array("status" => 200, "message" => "You have successfully logged out!");
                        $statusCode=200;    
                       
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                 $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
             $statusCode=405;
        }        
       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }

    /************************************ verify change mob No OTP ************************************/
    #verifyChangeMobNo
    public function verifyChangeMobNo(){

        if($_SERVER['REQUEST_METHOD'] == "POST"){           
            $getHeaders=$this->getHeaders();
            $data = json_decode(file_get_contents('php://input'), true);  
            $mobileNo = isset($data['mobileNo']) ? mysqli_real_escape_string($this->dbcharesc,$data['mobileNo']) : '';
            $OTP = isset($data['OTP']) ? mysqli_real_escape_string($this->dbcharesc,$data['OTP']) : '';
          
            if(!empty($mobileNo)&&!empty($OTP)&&!empty($getHeaders)){   

                 #validating customer
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);

                if($checkCustomer&&$custID==$checkCustomer->custID){ 

                    if($this->validateMobile($mobileNo)){            

                                         
                        $verifyCode =$this->apimodelcustomer->verifyCode($mobileNo);
                      
                    
                        if($verifyCode->otp==$OTP){  
                                $custData =$this->apimodelcustomer->getCustomerByMobNo($mobileNo);
                                if($custData){
                                     $arrResponse = array("status" => 409, "message" => "Customer already exists with this mobile number.");
                                $statusCode=409;
                              /*  if(empty($custData->custToken)){
                                    $accesstoken=$this->getTokens();
                                    $updateCustomer=$this->apimodelcustomer->updateCustomer(array("custID"=>$custData->custID),array("custToken"=>$accesstoken));
                                }

                              

                                if(!empty($androidKey)){
                                    $tokenData =$this->apimodelcustomer->checkDeviceToken($custData->custID,$androidKey,0);

                                    if(!$tokenData){
                                        $data=array("custID"=>$custData->custID,
                                                    "deviceToken"=>$androidKey,
                                                    "deviceType"=>0,
                                                    "isActive"=>1,
                                                    "created"=>date('Y-m-d H:i:s'));
                                        $tokenData =$this->apimodelcustomer->insertDeviceToken($data);
                                    }else{
                                        $updateDeviceToken=$this->apimodelcustomer->updateDeviceToken(array("deviceToken"=>$androidKey,"deviceType"=>0,"custID"=>$checkCustomer->custID),array("isActive"=>1));
                                    }
                                    
                                }

                                if(!empty($iosKey)){
                                    $tokenData =$this->apimodelcustomer->checkDeviceToken($custData->custID,$iosKey,1);

                                    if(!$tokenData){
                                        $data=array("custID"=>$custData->custID,
                                                    "deviceToken"=>$iosKey,
                                                    "deviceType"=>1,
                                                    "isActive"=>1,
                                                    "created"=>date('Y-m-d H:i:s'));
                                        $tokenData =$this->apimodelcustomer->insertDeviceToken($data);
                                    }else{
                                        $updateDeviceToken=$this->apimodelcustomer->updateDeviceToken(array("deviceToken"=>$iosKey,"deviceType"=>1,"custID"=>$checkCustomer->custID),array("isActive"=>1));
                                    }
                                    
                                }
                               

                                if(!empty($custData->custToken)){
                                header("accesstoken: $custData->custToken");
                                }
                                $isCustomerExist="1";
                                unset($custData->custToken);*/
                                }else{
                                    $updateCustomer=$this->apimodelcustomer->updateCustomer(array("custID"=>$checkCustomer->custID),array("custMobNo"=>$mobileNo));
                                    if($updateCustomer){
                                        $custData =$this->apimodelcustomer->getCustomerByMobNo($mobileNo);
                                        $arrResponse = array("status" => 200, "message" => "Mobile number updated successfully!","isCustomerExist"=>1,"data"=>$custData);
                                $statusCode=200;
                                    }else{
                                        $arrResponse = array("status" => 500, "message" => "Something went wrong!");
                                $statusCode=500;
                                    }
                                }   
                               
                               
                            }else{
                                $arrResponse = array("status" => 400, "message" => "Entered opt is wrong !");
                                $statusCode=400;
                            }                    
                    }else{
                         $arrResponse = array("status" => 400, "message" => "Enter valid mobile no !");
                         $statusCode=400;
                    }  
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }                        
                
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                $statusCode=400;
            }

        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
            $statusCode=405;
        }
        http_response_code($statusCode); 
      echo json_encode($arrResponse);
    
    }

     /*********************************Generate ref txn ID**************************************/

    public function generateRefTxnID(){

    if($_SERVER['REQUEST_METHOD'] == "POST"){
            
            $getHeaders=$this->getHeaders();
            $data = json_decode(file_get_contents('php://input'), true); 
            $jobID = isset($data['jobID']) ? mysqli_real_escape_string($this->dbcharesc,$data['jobID']) : '';
            $type = isset($data['type']) ? mysqli_real_escape_string($this->dbcharesc,$data['type']) : '';
            $platform = isset($data['platform']) ? mysqli_real_escape_string($this->dbcharesc,$data['platform']) : '';
            if(!empty($getHeaders)&&!empty($jobID)){
                #validating customer
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);

                if($checkCustomer&&$custID==$checkCustomer->custID){                     
                  
                   $ordersArr=array();

                   
                   #checking txn type
                    switch ($type) {
                        case '1':
                             #fetching orders data
                            $jobData =$this->apimodelcustomer->getJobByID($jobID);
                            break;
                        case '2':
                             #fetching orders data
                           // $jobData =$this->apimodelcustomer->getJobByID($jobID);
                            $jobData=false;
                            break;    
                        
                        default:
                            $jobData=false;
                            break;
                    }

                    if($jobData){

                        if($jobData->customerID==$checkCustomer->custID){                           

                                if($jobData->paymentStatus!=1){      

                                        do{
                                            $refTransactionID=$this->generateTransactionID();//1 for txn type job

                                            $txnData=array("refTransactionID"=>$refTransactionID,
                                                                                "dataID"=>$jobID,
                                                                                "refType"=>$type,
                                                                                "platform"=>$platform,
                                                                                "isActive"=>1,
                                                                                "created"=>date('Y-m-d H:i:s'));
                                            $insertTxn=$this->apimodelcustomer->insertPaytmTxn($txnData);


                                            }while($insertTxn<1);
                                        
                                        if($insertTxn){

                                         
                                            $arrResponse = array("status" => 200, "message" => "Success","refTransactionID"=>$refTransactionID);
                                            $statusCode=200;
                                        }else{
                                            $arrResponse = array("status" => 500, "message" => "Something went wrong.");
                                            $statusCode=500;
                                        }                                 
                                }else{
                                    $arrResponse = array("status" => 400, "message" => "Payment already done.");
                                    $statusCode=400;    
                                }    
                           
                        }else{
                          $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                          $statusCode=403;  
                        }
                    }else{
                      $arrResponse = array("status" => 400, "message" => "Please enter valid data.");
                      $statusCode=400;  
                    }
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                 $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
             $statusCode=405;
        }        
       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }

///////////////////////updateTransactionFrom Backend/////////////////////////////////////////////////////////////////////////////

    public function updateTransactionFromBackend(){

    if($_SERVER['REQUEST_METHOD'] == "POST"){

        $data = json_decode(file_get_contents('php://input'), true); 

        $this->apimodelcustomer->insertUpdateTransReq(array("data"=>json_encode($data),"created"=>date('Y-m-d H:i:s'))); 

        $MID = isset($data['MID']) ? mysqli_real_escape_string($this->dbcharesc,$data['MID']) : '';
        $ORDERID = isset($data['ORDERID']) ? mysqli_real_escape_string($this->dbcharesc,$data['ORDERID']) : '';
        $TXNAMOUNT = isset($data['TXNAMOUNT']) ? mysqli_real_escape_string($this->dbcharesc,$data['TXNAMOUNT']) : '';
        $CURRENCY = isset($data['CURRENCY']) ? mysqli_real_escape_string($this->dbcharesc,$data['CURRENCY']) : '';
        $TXNID = isset($data['TXNID']) ? mysqli_real_escape_string($this->dbcharesc,$data['TXNID']) : '';
        $TXNAMOUNT = isset($data['TXNAMOUNT']) ? mysqli_real_escape_string($this->dbcharesc,$data['TXNAMOUNT']) : '';
        $BANKTXNID = isset($data['BANKTXNID']) ? mysqli_real_escape_string($this->dbcharesc,$data['BANKTXNID']) : '';
        $STATUS = isset($data['STATUS']) ? mysqli_real_escape_string($this->dbcharesc,$data['STATUS']) : '';
        $RESPCODE = isset($data['RESPCODE']) ? mysqli_real_escape_string($this->dbcharesc,$data['RESPCODE']) : '';
        $RESPMSG = isset($data['RESPMSG']) ? mysqli_real_escape_string($this->dbcharesc,$data['RESPMSG']) : '';
        $TXNDATE = isset($data['TXNDATE']) ? mysqli_real_escape_string($this->dbcharesc,$data['TXNDATE']) : '';
        $GATEWAYNAME = isset($data['GATEWAYNAME']) ? mysqli_real_escape_string($this->dbcharesc,$data['GATEWAYNAME']) : '';
        $BANKNAME = isset($data['BANKNAME']) ? mysqli_real_escape_string($this->dbcharesc,$data['BANKNAME']) : '';
        $PAYMENTMODE = isset($data['PAYMENTMODE']) ? mysqli_real_escape_string($this->dbcharesc,$data['PAYMENTMODE']) : '';
        $CHECKSUMHASH = isset($data['CHECKSUMHASH']) ? mysqli_real_escape_string($this->dbcharesc,$data['CHECKSUMHASH']) : '';
        $paytmID = isset($data['paytmID']) ? mysqli_real_escape_string($this->dbcharesc,$data['paytmID']) : '';
           // $custID = isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
        if(!empty($ORDERID)&&!empty($STATUS)&&!empty($RESPCODE)&&!empty($RESPMSG)){
         
            $checkCustomer = array();

           if(true){                  
                
            $ordersArr=array(); //Initialise order array
                #validate transaction
                $txnData =$this->apimodelcustomer->getTransactionDetails($ORDERID);
                        
                if($txnData){
                    $updateData=array("merchantID"=>$MID,
                                      "transactionAmount"=>$TXNAMOUNT,
                                      "currency"=>$CURRENCY,
                                      "transactionID"=>$TXNID,
                                      "transactionAmount"=>$TXNAMOUNT,
                                      "bankTransactionID"=>$BANKTXNID,
                                      "status"=>$STATUS,
                                      "respCode"=>$RESPCODE,
                                      "respMsg"=>$RESPMSG,
                                      "transactionDateTime"=>$TXNDATE,
                                      "gatewayName"=>$GATEWAYNAME,
                                      "bankName"=>$BANKNAME,
                                      "paymentMode"=>$PAYMENTMODE,
                                      "checkSumHash"=>$CHECKSUMHASH);
                            
                                
                    $updateTxn=$this->apimodelcustomer->updateTransaction(array('paytmID' =>$txnData->paytmID),$updateData);
                      $checkCustomer =$this->apimodelcustomer->getCustByPaytmID($txnData->paytmID);
             
                   $transactionReporData=array("refTransactionID"=>$txnData->refTransactionID,
                                               "merchantID"=>$MID,
                                               "transactionAmount"=>$TXNAMOUNT,
                                               "currency"=>$CURRENCY,
                                               "transactionID"=>$TXNID,
                                               "transactionAmount"=>$TXNAMOUNT,
                                               "bankTransactionID"=>$BANKTXNID,
                                               "status"=>$STATUS,
                                               "respCode"=>$RESPCODE,
                                               "respMsg"=>$RESPMSG,
                                               "transactionDateTime"=>$TXNDATE,
                                               "gatewayName"=>$GATEWAYNAME,
                                               "bankName"=>$BANKNAME,
                                               "paymentMode"=>$PAYMENTMODE,
                                               "checkSumHash"=>$CHECKSUMHASH,
                                               "created"=>date('Y-m-d H:i:s'));                            
                                
                    $insertTransactionReport=$this->apimodelcustomer->insertTransactionReport($transactionReporData);
                               
                    if($updateTxn){

                        if($STATUS=="TXN_SUCCESS"){ 


                            if($txnData->refType==1){    //refType ->1 Job, 2->Membership 
                                #Activate Job
                                if($GATEWAYNAME=="WALLET"&&$PAYMENTMODE=="PPI"){
                                        $paymentMethod="1";
                                    }else{
                                        $paymentMethod="3";
                                    }

                                    $preUpdateJobDetails=$this->apimodelcustomer->getJobByID($txnData->dataID); 

                                    if(empty($preUpdateJobDetails->jobIdentifier)){
                                    #generate JobRef No
                                    $jobIdentifier="";

                                    do{
                                        $jobIdentifier=$this->generateRefId($preUpdateJobDetails->type);  
                                        #check id exists
                                        $checkJob=$this->apimodelcustomer->getJobByJobRefID($jobIdentifier);
                                        if($checkJob){  
                                        $jobIdentifier="";    
                                        }
                                    }while($jobIdentifier=="");
                                    }else{
                                       $jobIdentifier= $preUpdateJobDetails->jobIdentifier;
                                    }    
                                $this->apimodelcustomer->updateJob(array("jobID"=>$txnData->dataID),array("paymentMethod"=>$paymentMethod,
                                                                                                          "paymentStatus"=>1,
                                                                                                          "amountPaid"=>$TXNAMOUNT,
                                                                                                          "jobIdentifier"=>$jobIdentifier,
                                                                                                          "isActive"=>1));
                #generateInv
                          //      $generateInv=$this->generateInvoice($txnData->dataID);
    
                                #fetching job details   
                                $jobDetails=$this->getJobByID($txnData->dataID);   
                                    
                                $arrResponse = array("status" => 200, "message" => "Transaction completed successfully!","jobDetails"=>$jobDetails);
                                
                                $jobDetails=(object)$jobDetails;
                                #fetching pkg details
                                $pkgData=$this->apimodelcustomer->validatePkg($jobDetails->pkgID,$jobDetails->type);
                                $jobIdentifier=$jobDetails->jobName;
                              /*  $jobDate = $jobDetails->repotingTime;
                                $jobDate = $jobDetails->date;
*/
                                
                                    #send Sms 
                                    if(!empty($checkCustomer->custMobNo)&&($checkCustomer->isSMSSubscribed==1)){
                                       $smsDate=date('d-M',strtotime($jobDetails->serviceDate));
                                       $smsTime=date('h:i A',strtotime($jobDetails->serviceTime));

                                       // $message = 'Your '.$pkgData->pkgName.' ('.$jobIdentifier.') is confirmed for '.$smsDate.' at '.$smsTime.'. To cancel or reschedule, please call 8080720000. Thanks, Refreshed Car Care';
                                                          
                                        $message = 'Your '.$pkgData->pkgName.' ('.$jobIdentifier.') on '.$smsDate.' '.$smsTime.' is confirmed. Track your booking via app available on App Store & Google Play. For more, call 8080720000.';
                                        $sendMsg=$this->sendSms2($checkCustomer->custMobNo,$message);
                                        if($sendMsg){

                                            $dataSms=array('recordID' =>$jobDetails->jobID,
                                                           'custID'=>$checkCustomer->custID,
                                                           'mobileNo'=>$checkCustomer->custMobNo,
                                                           'sms'=>$message,
                                                            'smsType'=>1, //For Job SMS
                                                            'smsSubType'=>1, //For Confirm Job SMS
                                                            'isActive'=>1,
                                                            'created'=>date('Y-m-d H:i:s')
                                                             );
                                            $insertSMS=$this->apimodelcustomer->insertSms($dataSms);

                                            $updateJob =$this->apimodelcustomer->updateJob(array('jobID' =>$jobDetails->jobID),array("isSmsSentBooking"=>1));
                                        }

                                    }

                                    #send Pepo Campaign Email
                                    if(!empty($checkCustomer->custEmailID)){
                                    $this->bookingPepoCampaign($checkCustomer->custEmailID,$checkCustomer->custName,$jobIdentifier,$jobDetails->serviceDate,$jobDetails->serviceTime,$jobDetails->serviceAddress,$checkCustomer->custMobNo,$pkgData->pkgName,$jobDetails->serviceAmount,$jobDetails->jobID);
                                       
                                    }
                                 
                                                         # send Noti
                                                        if(!empty($jobDetails->isNotifyMe)){
                                                        
                                                        $notification="Your Refreshed service is confirmed: Booking ID ".$jobIdentifier;
                                                        
                                                        $notiData=array("recieverID"=>$checkCustomer->custID,
                                                                      "notification"=>$notification,
                                                                      "type"=>1,
                                                                      "subType"=>1,
                                                                      "dataID"=>$jobDetails->jobID,
                                                                      "isActive"=>1,
                                                                      "created"=>date('Y-m-d H:i:s'));
                                                        $insertNotiCust=$this->apimodelcustomer->insertNotiCust($notiData);
                                                        }

                                                        $deviceTokens=$this->apimodelcustomer->getDeviceTokens($checkCustomer->custID);
                                                        $badgeCount=$this->apimodelcustomer->getBadgeCount($checkCustomer->custID);
                                                        if($deviceTokens){
                                                          foreach ($deviceTokens as $readToken) {
                                                            if($readToken->deviceType==1){
                                                              //send ios noti
                                                              
                                                        $this->iOSNotification($readToken->deviceToken,$notification,$jobDetails->jobID,1,1,$badgeCount);
                                                            }else{
                                                              //send ios android noti
                                                        $this->androidNotification($readToken->deviceToken,$notification,$jobDetails->jobID,1,1);
                                                            }
                                                          }
                                                        }
                            }else{  


                                    #update membership request
                                    $updateMembershipRequest=$this->apimodelcustomer->updateMembershipRequest(array("membershipID"=>$txnData->dataID),array("isCompleted"=>1));
                                    #update membership
                                    $updateMembership=$this->apimodelcustomer->updateMembership(array("membershipID"=>$txnData->dataID),array("isActive"=>1)); 

                                    $membershipData=$this->apimodelcustomer->getCustomerMembershipByID($txnData->dataID);
                                    #update Cars for membership
                                    $updateMembership=$this->apimodelcustomer->updateCar(array("custID"=>$checkCustomer->custID),array("isInMembership"=>0));
                                    if(!empty($membershipData->custCars)){
                                    $carIDs=explode(',',$membershipData->custCars);
                                    foreach ($carIDs as $carID) {
                                     
                                      $updateCar=$this->apimodelcustomer->updateCar(array("carID"=>$carID),array("isInMembership"=>1));
                                    }
                                    }
                                if($GATEWAYNAME=="WALLET"&&$PAYMENTMODE=="PPI"){
                                        $paymentMethod="1";
                                    }else{
                                        $paymentMethod="3";
                                    }
                                    #generate Invoice
                                    $generateInv=$this->generateInvoiceMembership($checkCustomer,$txnData->dataID,$paymentMethod);
                                    /*#update invoice
                                    $updateInvoice=$this->apimodelcustomer->updateInvoice(array("jobID"=>$txnData->dataID,"type"=>2),array("paymentStatus"=>1,"amountPaid"=>"1200"));  */

                                    $arrResponse = array("status" => 200, "message" => "Transaction completed successfully!");    
                            }
                                    $statusCode=200;
                        }else{
                                        if($txnData->refType==1){  //refType ->1 Job, 2->Membership 
                                           // $updateJob=$this->apimodelcustomer->updateJob(array("jobID"=>$txnData->dataID),array("isActive"=>0));
                                            #update Booking Req
                                            $updateBookingReq=$this->apimodelcustomer->updateBookingReq(array("jobID"=>$txnData->dataID),array('isComplete'=>0));
                                        }else{
                                            $continue;
                                        }
                                        #update membership
                                       // $updateMembership=$this->apimodelcustomer->updateMembership(array("membershipID"=>$txnData->dataID),array("isActive"=>0));

                                        // print_r($updateMembership)

                                        $arrResponse = array("status" => 422, "message" => "Unable to procceed transation! Please try after sometime.");
                                         
                                        $statusCode=422;
                        }
                                    
                                    
                    }
                               
                    }else{
                        $arrResponse = array("status" => 500, "message" => "Something went wrong !");
                        $statusCode=500;
                    }   
                        
                   /* }else{
                      $arrResponse = array("status" => 400, "message" => "Please enter valid data.");
                      $statusCode=400;  
                    }*/
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                 $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
             $statusCode=405;
        }        
       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }


///////////////////////End updateTransactionFrom Backend/////////////////////////////////////////////////////////////////////////////
    /*********************************Update transaction **************************************/

    public function updateTransaction(){

    if($_SERVER['REQUEST_METHOD'] == "POST"){
            
        $getHeaders=$this->getHeaders();
        $data = json_decode(file_get_contents('php://input'), true); 
        $this->apimodelcustomer->insertUpdateTransReq(array("data"=>json_encode($data),"created"=>date('Y-m-d H:i:s')));       
        $MID = isset($data['MID']) ? mysqli_real_escape_string($this->dbcharesc,$data['MID']) : '';
        $ORDERID = isset($data['ORDERID']) ? mysqli_real_escape_string($this->dbcharesc,$data['ORDERID']) : '';
        $TXNAMOUNT = isset($data['TXNAMOUNT']) ? mysqli_real_escape_string($this->dbcharesc,$data['TXNAMOUNT']) : '';
        $CURRENCY = isset($data['CURRENCY']) ? mysqli_real_escape_string($this->dbcharesc,$data['CURRENCY']) : '';
        $TXNID = isset($data['TXNID']) ? mysqli_real_escape_string($this->dbcharesc,$data['TXNID']) : '';
        $TXNAMOUNT = isset($data['TXNAMOUNT']) ? mysqli_real_escape_string($this->dbcharesc,$data['TXNAMOUNT']) : '';
        $BANKTXNID = isset($data['BANKTXNID']) ? mysqli_real_escape_string($this->dbcharesc,$data['BANKTXNID']) : '';
        $STATUS = isset($data['STATUS']) ? mysqli_real_escape_string($this->dbcharesc,$data['STATUS']) : '';
        $RESPCODE = isset($data['RESPCODE']) ? mysqli_real_escape_string($this->dbcharesc,$data['RESPCODE']) : '';
        $RESPMSG = isset($data['RESPMSG']) ? mysqli_real_escape_string($this->dbcharesc,$data['RESPMSG']) : '';
        $TXNDATE = isset($data['TXNDATE']) ? mysqli_real_escape_string($this->dbcharesc,$data['TXNDATE']) : '';
        $GATEWAYNAME = isset($data['GATEWAYNAME']) ? mysqli_real_escape_string($this->dbcharesc,$data['GATEWAYNAME']) : '';
        $BANKNAME = isset($data['BANKNAME']) ? mysqli_real_escape_string($this->dbcharesc,$data['BANKNAME']) : '';
        $PAYMENTMODE = isset($data['PAYMENTMODE']) ? mysqli_real_escape_string($this->dbcharesc,$data['PAYMENTMODE']) : '';
        $CHECKSUMHASH = isset($data['CHECKSUMHASH']) ? mysqli_real_escape_string($this->dbcharesc,$data['CHECKSUMHASH']) : '';
           // $custID = isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
        if(!empty($getHeaders)&&!empty($ORDERID)&&!empty($STATUS)&&!empty($RESPCODE)&&!empty($RESPMSG)){
                
            #validating customer
            $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);

            if($checkCustomer){    //&&$custID==$checkCustomer->custID                 
                  
            $ordersArr=array(); //Initialise order array
                #validate transaction
                $txnData =$this->apimodelcustomer->getTransactionDetails($ORDERID);
                        
                if($txnData){
                    $updateData=array("merchantID"=>$MID,
                                      "transactionAmount"=>$TXNAMOUNT,
                                      "currency"=>$CURRENCY,
                                      "transactionID"=>$TXNID,
                                      "transactionAmount"=>$TXNAMOUNT,
                                      "bankTransactionID"=>$BANKTXNID,
                                      "status"=>$STATUS,
                                      "respCode"=>$RESPCODE,
                                      "respMsg"=>$RESPMSG,
                                      "transactionDateTime"=>$TXNDATE,
                                      "gatewayName"=>$GATEWAYNAME,
                                      "bankName"=>$BANKNAME,
                                      "paymentMode"=>$PAYMENTMODE,
                                      "checkSumHash"=>$CHECKSUMHASH);
                            
                                
                    $updateTxn=$this->apimodelcustomer->updateTransaction(array('paytmID' =>$txnData->paytmID),$updateData);

                    //Insert transaction reports
                   $transactionReporData=array("refTransactionID"=>$txnData->refTransactionID,
                                               "merchantID"=>$MID,
                                               "transactionAmount"=>$TXNAMOUNT,
                                               "currency"=>$CURRENCY,
                                               "transactionID"=>$TXNID,
                                               "transactionAmount"=>$TXNAMOUNT,
                                               "bankTransactionID"=>$BANKTXNID,
                                               "status"=>$STATUS,
                                               "respCode"=>$RESPCODE,
                                               "respMsg"=>$RESPMSG,
                                               "transactionDateTime"=>$TXNDATE,
                                               "gatewayName"=>$GATEWAYNAME,
                                               "bankName"=>$BANKNAME,
                                               "paymentMode"=>$PAYMENTMODE,
                                               "checkSumHash"=>$CHECKSUMHASH,
                                               "created"=>date('Y-m-d H:i:s'));                            
                                
                    $insertTransactionReport=$this->apimodelcustomer->insertTransactionReport($transactionReporData);
                               
                    if($updateTxn){

                        if($STATUS=="TXN_SUCCESS"){ 


                            if($txnData->refType==1){    //refType ->1 Job, 2->Membership 
                                #Activate Job
                                if($GATEWAYNAME=="WALLET"&&$PAYMENTMODE=="PPI"){
                                        $paymentMethod="1";
                                    }else{
                                        $paymentMethod="3";
                                    }

                                    $preUpdateJobDetails=$this->apimodelcustomer->getJobByID($txnData->dataID); 

                                    if(empty($preUpdateJobDetails->jobIdentifier)){
                                    #generate JobRef No
                                    $jobIdentifier="";

                                    do{
                                        $jobIdentifier=$this->generateRefId($preUpdateJobDetails->type);  
                                        #check id exists
                                        $checkJob=$this->apimodelcustomer->getJobByJobRefID($jobIdentifier);
                                        if($checkJob){  
                                        $jobIdentifier="";    
                                        }
                                    }while($jobIdentifier=="");
                                    }else{
                                       $jobIdentifier= $preUpdateJobDetails->jobIdentifier;
                                    }    
                                $this->apimodelcustomer->updateJob(array("jobID"=>$txnData->dataID),array("paymentMethod"=>$paymentMethod,
                                                                                                          "paymentStatus"=>1,
                                                                                                          "amountPaid"=>$TXNAMOUNT,
                                                                                                          "jobIdentifier"=>$jobIdentifier,
                                                                                                          "isActive"=>1));
			  #update Booking Req
                                            //                    $updateBookingReq=$this->apimodelcustomer->updateBookingReq(array("reqID"=>$preUpdateJobDetails->bookingReqID),array('jobID' =>$txnData->dataID,'isComplete'=>1));
                #generateInv
                          //      $generateInv=$this->generateInvoice($txnData->dataID);
    
                                #fetching job details   
                                $jobDetails=$this->getJobByID($txnData->dataID);   
                                    
                                $arrResponse = array("status" => 200, "message" => "Transaction completed successfully!","jobDetails"=>$jobDetails);
                                
                                $jobDetails=(object)$jobDetails;
                                #fetching pkg details
                                $pkgData=$this->apimodelcustomer->validatePkg($jobDetails->pkgID,$jobDetails->type);
                                $jobIdentifier=$jobDetails->jobName;
                                
                                    #send Sms 
                                    if(!empty($checkCustomer->custMobNo)&&($checkCustomer->isSMSSubscribed==1)){
                                       $smsDate=date('d-M',strtotime($jobDetails->serviceDate));
                                       $smsTime=date('h:i A',strtotime($jobDetails->serviceTime));
                                       $message = 'Your '.$pkgData->pkgName.' ('.$jobIdentifier.') is confirmed for '.$smsDate.' at '.$smsTime.'. To cancel or reschedule, please call 8080720000. Thanks, Refreshed Car Care';
                                                          
                                                          
                                        $sendMsg=$this->sendSms2($checkCustomer->custMobNo,$message);
                                        if($sendMsg){

                                            $dataSms=array('recordID' =>$jobDetails->jobID,
                                                           'custID'=>$checkCustomer->custID,
                                                           'mobileNo'=>$checkCustomer->custMobNo,
                                                           'sms'=>$message,
                                                            'smsType'=>1, //For Job SMS
                                                            'smsSubType'=>1, //For Confirm Job SMS
                                                            'isActive'=>1,
                                                            'created'=>date('Y-m-d H:i:s')
                                                             );
                                            $insertSMS=$this->apimodelcustomer->insertSms($dataSms);

                                            $updateJob =$this->apimodelcustomer->updateJob(array('jobID' =>$jobDetails->jobID),array("isSmsSentBooking"=>1));
                                        }

                                    }

                                    #send Pepo Campaign Email
                                    if(!empty($checkCustomer->custEmailID)){
                                    $this->bookingPepoCampaign($checkCustomer->custEmailID,$checkCustomer->custName,$jobIdentifier,$jobDetails->serviceDate,$jobDetails->serviceTime,$jobDetails->serviceAddress,$checkCustomer->custMobNo,$pkgData->pkgName,$jobDetails->serviceAmount,$jobDetails->jobID);
                                       
                                    }
                                    /*#send Pepo Campaign Email
                                    if(!empty($checkCustomer->custEmailID)){


                                              $email=$checkCustomer->custEmailID;
                                              $templateName="Booking Confirmation";
                            
                                                $data=array('name' => $checkCustomer->custName,
                                                          'fullName' => $checkCustomer->custName,
                                                          'refID' =>  $jobIdentifier, 
                                                          'date' => date('j / m / y',strtotime($jobDetails->serviceDate)),            
                                                          'time' => date('g:i A',strtotime($jobDetails->serviceTime)),
                                                          'serviceAddress'=>$jobDetails->serviceAddress,
                                                          'mobileNo'=>$checkCustomer->custMobNo,
                                                          'serviceName'=>$pkgData->pkgName,
                                                          'typeOfService'=>$pkgData->pkgName,
                                                          'price'=>"₹ ".$jobDetails->serviceAmount,
                                                          'subject'=>"Your Refreshed service is confirmed: Booking ID ".$jobIdentifier,

                                                            );
                                                            
                                                $emailData=$this->apimodelcustomer->getJobsEmailByJobID($jobDetails->jobID);
                                        if($emailData){
                                                if($emailData->isBookingConfirmSent!=1){
                                                    $response=$this->pepo_campaigns->send_transactional_email($email, $templateName, $data);
                                                    $data= json_decode($response);

                                                    if($data->error==null){
                                                    $data=array('isBookingConfirmSent' =>1);
                                                    $updateData=$this->apimodelcustomer->updateJobsEmail(array("id"=>$emailData->id),$data);
                                                    } 
                                                }

                                        }else{
                                                $response=$this->pepo_campaigns->send_transactional_email($email, $templateName, $data);
                                                    $data= json_decode($response);
                                                    if($data->error==null){

                                                    $data=array('jobID'=>$jobDetails->jobID,
                                                                              'isBookingConfirmSent' =>1,
                                                                              'isActive'=>1,
                                                                              'created'=>date('Y-m-d H:i:s'));
                                                    $insertData=$this->apimodelcustomer->insertJobsEmail($data);
                                                    } 
                                        }
                                                           
                                    }*/

                                                         # send Noti
                                                        if(!empty($jobDetails->isNotifyMe)){
                                                        
                                                        $notification="Your Refreshed service is confirmed: Booking ID ".$jobIdentifier;
                                                        
                                                        $notiData=array("recieverID"=>$checkCustomer->custID,
                                                                      "notification"=>$notification,
                                                                      "type"=>1,
                                                                      "subType"=>1,
                                                                      "dataID"=>$jobDetails->jobID,
                                                                      "isActive"=>1,
                                                                      "created"=>date('Y-m-d H:i:s'));
                                                        $insertNotiCust=$this->apimodelcustomer->insertNotiCust($notiData);
                                                        }

                                                        $deviceTokens=$this->apimodelcustomer->getDeviceTokens($checkCustomer->custID);
                                                        $badgeCount=$this->apimodelcustomer->getBadgeCount($checkCustomer->custID);
                                                        if($deviceTokens){
                                                          foreach ($deviceTokens as $readToken) {
                                                            if($readToken->deviceType==1){
                                                              //send ios noti
                                                              
                                                        $this->iOSNotification($readToken->deviceToken,$notification,$jobDetails->jobID,1,1,$badgeCount);
                                                            }else{
                                                              //send ios android noti
                                                        $this->androidNotification($readToken->deviceToken,$notification,$jobDetails->jobID,1,1);
                                                            }
                                                          }
                                                        }
                            }else{  


                                    #update membership request
                                    $updateMembershipRequest=$this->apimodelcustomer->updateMembershipRequest(array("membershipID"=>$txnData->dataID),array("isCompleted"=>1));
                                    #update membership
                                    $updateMembership=$this->apimodelcustomer->updateMembership(array("membershipID"=>$txnData->dataID),array("isActive"=>1)); 

                                    $membershipData=$this->apimodelcustomer->getCustomerMembershipByID($txnData->dataID);
                                    #update Cars for membership
                                    $updateMembership=$this->apimodelcustomer->updateCar(array("custID"=>$checkCustomer->custID),array("isInMembership"=>0));
                                    if(!empty($membershipData->custCars)){
                                    $carIDs=explode(',',$membershipData->custCars);
                                    foreach ($carIDs as $carID) {
                                     
                                      $updateCar=$this->apimodelcustomer->updateCar(array("carID"=>$carID),array("isInMembership"=>1));
                                    }
                                    }
                                if($GATEWAYNAME=="WALLET"&&$PAYMENTMODE=="PPI"){
                                        $paymentMethod="1";
                                    }else{
                                        $paymentMethod="3";
                                    }
                                    #generate Invoice
                                    $generateInv=$this->generateInvoiceMembership($checkCustomer,$txnData->dataID,$paymentMethod);
                                    /*#update invoice
                                    $updateInvoice=$this->apimodelcustomer->updateInvoice(array("jobID"=>$txnData->dataID,"type"=>2),array("paymentStatus"=>1,"amountPaid"=>"1200"));  */

                                    $arrResponse = array("status" => 200, "message" => "Transaction completed successfully!");    
                            }
                                    $statusCode=200;
                        }else{
                                        if($txnData->refType==1){  //refType ->1 Job, 2->Membership 
                                           // $updateJob=$this->apimodelcustomer->updateJob(array("jobID"=>$txnData->dataID),array("isActive"=>0));
                                            #update Booking Req
                                            $updateBookingReq=$this->apimodelcustomer->updateBookingReq(array("jobID"=>$txnData->dataID),array('isComplete'=>0));
                                        }else{
                                            $continue;
                                        }
                                        #update membership
                                       // $updateMembership=$this->apimodelcustomer->updateMembership(array("membershipID"=>$txnData->dataID),array("isActive"=>0));

                                        $arrResponse = array("status" => 422, "message" => "Unable to procceed transation! Please try after sometime.");
                                         
                $statusCode=422;
                        }
                                    
                                    
                    }
                               
                    }else{
                        $arrResponse = array("status" => 500, "message" => "Something went wrong !");
                        $statusCode=500;
                    }   
                        
                   /* }else{
                      $arrResponse = array("status" => 400, "message" => "Please enter valid data.");
                      $statusCode=400;  
                    }*/
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                    $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                 $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
             $statusCode=405;
        }        
       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }

    /************************************ getMembershipBenefits ************************************/
    public function getMembershipBenefits(){        

        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $dataArr=array();
            $getHeaders=$this->getHeaders();
          
            $data = json_decode(file_get_contents('php://input'), true);  
           
           
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            if(!empty($getHeaders)&&!empty($custID)){
                $dataArr=array();
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
            
                if($checkCustomer&&$custID==$checkCustomer->custID){
                $checkMembership=$this->apimodelcustomer->checkCustomerMembership($checkCustomer->custID);
        //print_r($checkMembership);
                $canRenew=0;
                
                if($checkMembership){
            $dataMem[]=array("heading"=>"Membership Details");
                    $startDate=$checkMembership->dateFrom;
                    $lastMembershipData=$this->apimodelcustomer->getCustomerMembershipLast($custID);
                    if($lastMembershipData){
                    $endDate=$lastMembershipData->dateTo;    
                    }else{
                    $endDate=$checkMembership->dateTo;
                    }
                    $dataMem[]=array("heading"=>"Membership Details",
                                                          "startDate"=>$startDate,
                                                          "endDate"=>$endDate);
                    $cars=$this->apimodelcustomer->getCustomerCarsMem($checkCustomer->custID);
                    if($cars){
                        foreach ($cars as $readCar) {
                          $dataMem[]=array("heading"=>$readCar->carMake." ".$readCar->carModel,
                                           "description"=>$readCar->carNoPlate);  
                        }
                    }

                    $renewData=$this->apimodelcustomer->getRenewMembershipData($checkCustomer->custID);
        //print_r($renewData);
                    if($renewData){
                      /*  $canRenew=1;
                        $message="Renew Membership at just ₹ 1,200";*/
             $nextDate=date( "Y-m-d", strtotime( " +8 days" ) );
                        if(strtotime($nextDate)>strtotime($renewData->dateTo)){
                        $canRenew=1;
                        $message="Renew Membership at just ₹ 1,200";
                        }else{
                         $message="Annual Membership at just ₹ 1,200";   
                        }
                    }else{
                       $message="Annual Membership at just ₹ 1,200";
                    }

                }else{
                    $canRenew=0;
                    $message="Annual Membership at just ₹ 1,200";
                    $startDate="";
                    $endDate="";
                    $dataMem=array();
                }               
             /*   array(array("heading"=>"Membership Details"),$dataMem,
                                                        array("heading"=>"Membership Details",
                                                          "startDate"=>$startDate,
                                                          "endDate"=>$endDate));  */
                $data=array($dataMem,array(array("heading"=>"Avail Your Membership",
                              "description"=>"",
                              "packages"=>array()),
                        array("heading"=>"Prefered Pricing",
                              "description"=>"Members enjoy lower rates as compared to Non-Member retail rates",
                              "packages"=>array(array("title1"=>"Express Wash",
                                                "price1"=>"₹ 499",
                                                "membershipPrice1"=>"₹ 399"),
                                          array("title1"=>"Deep Clean",
                                                "price1"=>"₹ 2499",
                                                "membershipPrice1"=>"₹ 1999"))
                                ),                        
                  array("heading"=>"Birthday GO",
                              "description"=>"Enjoy a complimentary Refreshed Express Wash during your birthday month",
                              "packages"=>array()),
                  array("heading"=>"Car health check",
                              "description"=>"Get your car battery and engine health report with our OBD-tech on a periodic basis",
                              "packages"=>array()),
                  array("heading"=>"Entourage",
                              "description"=>"Link upto Five cars which enjoy Refreshed’s services at member rates",
                              "packages"=>array())));
                  
                    $arrResponse = array("status" => 200, "message" => "success", "data" =>$data, "annualMembershipPrice"=>$message, "membershipPrice"=>"1200","canRenew"=>$canRenew); 
                       $statusCode=200;
              
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                       $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                   $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
               $statusCode=405;
        }
        http_response_code($statusCode); 
      echo json_encode($arrResponse);   

    }

     /*********************************Enroll membership **************************************/

    public function enrollMembership(){

    if($_SERVER['REQUEST_METHOD'] == "POST"){
            
            $getHeaders=$this->getHeaders();
            $data = json_decode(file_get_contents('php://input'), true); 
            $custID = isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            $custName = isset($data['custName']) ? mysqli_real_escape_string($this->dbcharesc,$data['custName']) : '';
            $custDOB = isset($data['custDOB']) ? mysqli_real_escape_string($this->dbcharesc,$data['custDOB']) : '';
            $custEmailID = isset($data['custEmailID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custEmailID']) : '';
            $contactNo = isset($data['contactNo']) ? mysqli_real_escape_string($this->dbcharesc,$data['contactNo']) : '';
            $addressID = isset($data['addressID']) ? mysqli_real_escape_string($this->dbcharesc,$data['addressID']) : '';
            $address = isset($data['address']) ? mysqli_real_escape_string($this->dbcharesc,$data['address']) : '';
            $cars = isset($data['cars']) ? mysqli_real_escape_string($this->dbcharesc,$data['cars']) : '';
            $paymentMethod = isset($data['paymentMethod']) ? mysqli_real_escape_string($this->dbcharesc,$data['paymentMethod']) : '';
            $amount = isset($data['amount']) ? mysqli_real_escape_string($this->dbcharesc,$data['amount']) : '';           
            $platform = isset($data['platform']) ? mysqli_real_escape_string($this->dbcharesc,$data['platform']) : '';
            if(!empty($custID)&&!empty($custName)&&!empty($custDOB)&&$custDOB!="0000-00-00"&&!empty($custEmailID)&&!empty($contactNo)&&!empty($addressID)&&!empty($getHeaders)){
                #validating customer
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
                
               
                #feching address 
                $addressData=$this->apimodelcustomer->getCustomerAddressByID($addressID);

                if($checkCustomer&&$addressData&&$custID==$checkCustomer->custID&&$custID==$addressData->custID){   
                   // print_r($addressData);
                    $membershipData=$this->apimodelcustomer->getCustomerMembership($custID); 

                    if(!$membershipData){ 
                        $dateFrom=date('Y-m-d');
                        $dateOneYear = date('Y-m-d', strtotime('+1 years'));
                        $dateTo=date('Y-m-d', strtotime('-1 Day',strtotime($dateOneYear)));
                    }else{
                        $lastMembershipData=$this->apimodelcustomer->getCustomerMembershipLast($custID);
                        if($lastMembershipData){
                        $dateFrom=date('Y-m-d', strtotime('+1 Day',strtotime($lastMembershipData->dateTo)));
                        $dateOneYear = date('Y-m-d', strtotime('+1 years',strtotime($dateFrom)));
                        $dateTo=date('Y-m-d', strtotime('-1 Day',strtotime($dateOneYear)));
                        }else{
                        $dateFrom=date('Y-m-d');
                        $dateOneYear = date('Y-m-d', strtotime('+1 years'));
                        $dateTo=date('Y-m-d', strtotime('-1 Day',strtotime($dateOneYear)));   
                        }
                    }

                        /** Start Transaction **/
                        $this->db->trans_begin();

                        #update Customer Details
                        $custData=array("custEmailID"=>$custEmailID,
                                        "custDOB"=>$custDOB);

                        $updateCustomer=$this->apimodelcustomer->updateCustomer(array("custID"=>$custID),$custData);

                        #inserData in membership        
                        $dataMembership=array("customerID"=>$custID,
                                              "dateFrom"=>$dateFrom,
                                              "dateTo"=>$dateTo,
                                              "custCars"=>$cars,
                                              "isActive"=>0,
                                              "created"=>date('Y-m-d H:i:s'));
                        $insertMembership=$this->apimodelcustomer->insertMembership($dataMembership);
                        $membershipID=$this->db->insert_id();
                        
                        
                        $address=$addressData->buildingDetails.','.$addressData->areaDetails.','.$addressData->landmark.',  '.$addressData->city.'  '.$addressData->state.', PIN : '.$addressData->pincode .',  GSTIN : '.$addressData->GSTNo;
                        $address=$this->arrangeAddress($address);

                       /* if($paymentMethod==2){
                                                  $paymentMethodDb=3;  
                                                }else{
                                                   $paymentMethodDb=1; 
                                                }*/
            if($paymentMethod==2){
                                                  $paymentMethodDb=3;  
                                                }else if($paymentMethod==1){
                                                   $paymentMethodDb=1; 
                                                }else{
                                                    $paymentMethodDb=0;
                }
                        #Insert membership request
                        $memRequestData=array("custName"=>$custName,
                                                  "dob"=>$custDOB,
                                                   "emailID"=>$custEmailID,
                                                  "contactNo"=>$contactNo,
                                                  "addressID"=>$addressID,
                                                  "address"=>$address,
                                                  "cars"=>$cars,
                                                  "paymentMethod"=>$paymentMethodDb,
                                                  "amount"=>$amount,
                                                  "membershipID"=>$membershipID,
                                                  "isCompleted"=>0,
                                                  "isActive"=>1,
                                                  "created"=>date('Y-m-d H:i:s'),
                                                  );
                                    
                        $insertMemRequest=$this->apimodelcustomer->insertMemRequest($memRequestData);

                        if($paymentMethod==1||$paymentMethod==2){      

                            do{
                                $refTransactionID=$this->generateTransactionID();//1 for txn type job

                                $txnData=array("refTransactionID"=>$refTransactionID,
                                                                    "dataID"=>$membershipID,
                                                                    "refType"=>2,
                                                                    "platform"=>$platform,
                                                                    "isActive"=>1,
                                                                    "created"=>date('Y-m-d H:i:s'));
                                $insertTxn=$this->apimodelcustomer->insertPaytmTxn($txnData);


                            }while($insertTxn<1);
                                        
                        }else{
                          $insertTxn=false;  
                        }

                        
                        
                        if ($this->db->trans_status() === FALSE || !$insertTxn)
                               {
                                $this->db->trans_rollback();
                                $statusCode=405;
                                $arrResponse = array("status" => 405, "message" => "Something went wrong !");
                        }else{
                                $this->db->trans_commit();
                                $arrResponse = array("status" => 200, "message" => "Membership enrolled successfully!","refTransactionID"=>$refTransactionID);
                                $statusCode=200;

                        }            
                       
                    /*}else{
                      $arrResponse = array("status" => 409, "message" => "Membership already enrolled.");
                      $statusCode=409;    
                    } */  
                        
                }else{
                      $arrResponse = array("status" => 400, "message" => "Please enter valid data.");
                      $statusCode=400;  
                }
                     
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                 $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
             $statusCode=405;
        }        
       http_response_code($statusCode); 
      echo json_encode($arrResponse);    

    }


    /************************************ getCancelReasons ************************************/
    public function getCancelReasons(){        

        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $dataArr=array();
            $getHeaders=$this->getHeaders();
            $data = json_decode(file_get_contents('php://input'), true); 
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
 
            if(!empty($getHeaders)){
                $dataArr=array();
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
            
                if($checkCustomer&&$custID==$checkCustomer->custID){
                              
                $reasons=$this->apimodelcustomer->getCancelReasons();
                                                
 
                $arrResponse = array("status" => 200, "message" => "success", "reasons" =>$reasons,"title"=>"Reason for cancellation ?"); 
                $statusCode=200;
               
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                       $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                   $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
               $statusCode=405;
        }
        http_response_code($statusCode); 
      echo json_encode($arrResponse);   

    }

 /************************************ getBadgeCount ************************************/
    public function getBadgeCount(){        

        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $dataArr=array();
            $getHeaders=$this->getHeaders();
            $data = json_decode(file_get_contents('php://input'), true); 
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
 
            if(!empty($getHeaders)){
                $dataArr=array();
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
            
                if($checkCustomer&&$custID==$checkCustomer->custID){
                              
                $badgeCount=$this->apimodelcustomer->getBadgeCount($custID);
                                                
 
                $arrResponse = array("status" => 200, "message" => "success", "badgeCount" =>$badgeCount); 
                $statusCode=200;
               
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                       $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                   $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
               $statusCode=405;
        }
        http_response_code($statusCode); 
      echo json_encode($arrResponse);   

    }
   
/************************************ getServiceOutlets ************************************/
    public function getServiceOutlets(){        

        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $dataArr=array();
            $getHeaders=$this->getHeaders();
            $data = json_decode(file_get_contents('php://input'), true); 
             $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
 
            if(!empty($getHeaders)){
                $dataArr=array();
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
            
                if($checkCustomer&&$custID==$checkCustomer->custID){
                              
                $serviceOutlets=$this->apimodelcustomer->getServiceOutlets();
                                                
 
                $arrResponse = array("status" => 200, "message" => "success", "serviceOutlets" =>$serviceOutlets,"heading"=>"Drive in to our service outlets for a wash\nNo advance booking required"); 
                $statusCode=200;
               
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                       $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                   $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
               $statusCode=405;
        }
        http_response_code($statusCode); 
      echo json_encode($arrResponse);   

    }
    /************************************ checkTransactionStatus ************************************/
    public function checkTransactionStatus(){        

        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $dataArr=array();
            $getHeaders=$this->getHeaders();
            $data = json_decode(file_get_contents('php://input'), true); 
            $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            $refTransactionID=isset($data['refTransactionID']) ? mysqli_real_escape_string($this->dbcharesc,$data['refTransactionID']) : '';
            if(!empty($getHeaders)&&!empty($refTransactionID)){
                $dataArr=array();
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
            
                if($checkCustomer&&$custID==$checkCustomer->custID){
                              
                $transactionDetails=$this->apimodelcustomer->getTransactionDetails($refTransactionID);
                
                if($transactionDetails){
            
           if($transactionDetails->status=="TXN_SUCCESS"){
                    if($transactionDetails->refType==1){
                    $jobData=$this->apimodelcustomer->getJobByID($transactionDetails->dataID);
                    if($jobData){
                         $jobName=$jobData->jobIdentifier; 
                    }else{
                       $jobName=""; 
                    }
            $message="Order confirmed successfully.";
                    }else{
            $message="Membership enrolled successfully.";
                        $jobName=""; 
                    }
                    $arrResponse = array("status" => 200, "message" => $message,"jobName"=>$jobName);  
                    $statusCode=200;
        }elseif($transactionDetails->respMsg!=""){
          $arrResponse = array("status" => 500, "message" =>$transactionDetails->respMsg); 
                    $statusCode=500;
        }else{
                    $arrResponse = array("status" => 500, "message" => "We could not complete your payment due to a technical error. You can try again now or please contact us if you are facing issues."); 
                    $statusCode=500;
                } 

                }else{
                    $arrResponse = array("status" => 500, "message" => "We could not complete your payment due to a technical error. You can try again now or please contact us if you are facing issues."); 
                    $statusCode=500;
                }                                
 
             
               
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                       $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                   $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
               $statusCode=405;
        }
        http_response_code($statusCode); 
      echo json_encode($arrResponse);   

    }


      /************************************ validatePromoCode ************************************/
    public function validatePromoCode(){        

        if($_SERVER['REQUEST_METHOD'] == "POST") {
            $dataArr=array();
            $getHeaders=$this->getHeaders();
            $data = json_decode(file_get_contents('php://input'), true); 
            $custID=isset($data['custID']) ? mysqli_real_escape_string($this->dbcharesc,$data['custID']) : '';
            $promoCode=isset($data['promoCode']) ? mysqli_real_escape_string($this->dbcharesc,$data['promoCode']) : '';
            $reqID=isset($data['reqID']) ? mysqli_real_escape_string($this->dbcharesc,$data['reqID']) : '';

            if(!empty($getHeaders)&&!empty($promoCode)&&!empty($reqID)){
                $dataArr=array();
                $checkCustomer =$this->apimodelcustomer->getCustByToken($getHeaders);
                $validateBookingRequest=$this->apimodelcustomer->validateBookingRequest($reqID);
            
                if($checkCustomer&&$custID==$checkCustomer->custID&&$validateBookingRequest&&$validateBookingRequest->custID==$checkCustomer->custID){
                              
                $promoCodeDetails=$this->apimodelcustomer->getPromoCodeDetails($promoCode);
                
                if($promoCodeDetails){   

                    if($promoCodeDetails->multiTimes==0){
                        $promoCodeUseDetails=$this->apimodelcustomer->checkForIsUsed($promoCode,0);
                        if($promoCodeUseDetails){
                            $message="This promocode has been already used .";
                            $status=0;
                        }else{
                            $status=1;
                        }
                    }elseif($promoCodeDetails->multiTimes==1){
                        $promoCodeUseDetails=$this->apimodelcustomer->checkForIsUsed($promoCode,$checkCustomer->custID);
                          if($promoCodeUseDetails){
                            $status=0;
                            $message="You have already used this promocode.";
                        }else{
                            $status=1;
                        }
                    }else{
                                 $status=0;
                                  $message="Please enter valid promocode.";
                            }

                    if($status==1){

                    $pkgData=$this->apimodelcustomer->getPackageDetails($validateBookingRequest->pkgID);
                     $carData=$this->apimodelcustomer->getCustomerCarsByID($validateBookingRequest->carID);
                    if($pkgData&&$carData){
                     $membership=$this->apimodelcustomer->getCustomerMembershipByDate($checkCustomer->custID,date('Y-m-d',strtotime($validateBookingRequest->date)));
                        if($membership&&!empty($carData->isInMembership)){
                            $pkgAmount=$pkgData->membershipPrice;

                        }else{
                            $pkgAmount=$pkgData->price;
                        }

			if($promoCodeDetails->unit=="%"){
                                                        $discountAmount=round($promoCodeDetails->value*$pkgAmount/100);
                                                    }else{
                                                        $discountAmount=$promoCodeDetails->value;
                                                    }
                        $pkgAmount=$pkgAmount-$discountAmount;
                      //  $pkgAmount=$pkgAmount-$promoCodeDetails->value;
                             $arrResponse = array("status" => 200, "message" => "success","promoCodeDetails"=>$promoCodeDetails,"pkgAmount"=> $pkgAmount);  
                        $statusCode=200;    
                   
                    }else{        
               
                      $arrResponse = array("status" => 500, "message" => "Something went wrong !");
                      $statusCode=500;
                    }  

                     }else{
                    $arrResponse = array("status" => 500, "message" => $message); 
                    $statusCode=500;
                }  

                }else{
                    $arrResponse = array("status" => 500, "message" => "Please enter valid promo code."); 
                    $statusCode=500;
                }                    
               
                }else{
                    $arrResponse = array("status" => 403, "message" => "Oops, please try again !");
                       $statusCode=403;
                }       
            }else{
                $arrResponse = array("status" => 400, "message" => "Required fields not found !");
                   $statusCode=400;
            }
        }else{
            $arrResponse = array("status" => 405, "message" => "Request method not accepted !");
               $statusCode=405;
        }
        http_response_code($statusCode); 
      echo json_encode($arrResponse);   

    }

    /*public  function test(){

        $custData=$this->apimodelcustomer->getCustomers();

        foreach ($custData as $readCust) {

            

            if(empty($readCust->custToken)){
               $accesstoken=$this->getTokens(); 

               if(!empty($accesstoken)){
                $updateCust=$this->apimodelcustomer->updateCustomer(array('custID' =>$readCust->custID),array("custToken"=>$accesstoken));
               }
            }
            # code...
        }
    }*/

    public function sendAndroidNotiTest(){
                          //  $androidKey=$_POST['androidKey'];   
            $androidKey="fVClFI1mTqs:APA91bHKRpe7X2H997LmYOKt_2if8kdeps7PQuDnX9LAH7swBJZsLYOQ86U9Y07lvxqvkh9JpalxqMUfrh1D9yKS-ayjK9YYDaulskivrorO1tSLPNUr47q3Vwi_mJTiGqOBv7K1YJVl";
                            $dataID="415";
                            $title="Refreshed Car Care";
                            $body="Testing Noti";
                            $category="1";
                            $subCategory="3";
                            $json_data = array(
                                "to" =>$androidKey,
  /*     "notification" =>array("body" => $body,
          "title" => $title,
          "category"=>$category,
          "icon" => "ic_launcher"
          ),*/
          "data" => array("dataID" => $dataID,
              "body" =>$body,
              "title" =>$title,
              "dataID"=>$dataID,
              "category"=>$category,
              "subCategory"=>$subCategory,
              "icon" => "ic_launcher"),
          );


                            $data = json_encode($json_data);
//FCM API end-point
                            $url = 'https://fcm.googleapis.com/fcm/send';
//api_key in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key

                            $server_key ='AIzaSyC2oEZ0l4ddRlLzDG-0BZuKFe1cERkpujY';
//header with content_type api key
                            $headers = array(
                                'Content-Type:application/json',
                                'Authorization:key='.$server_key
                                );
//CURL request to route notification to FCM connection server (provided by Google)
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_POST, true);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                            $result = curl_exec($ch);
                            if ($result === FALSE) {
                                die('Oops! FCM Send Error: ' . curl_error($ch));
                            }else{
                                print_r($result);

                            }
                            curl_close($ch);
    }

public function test(){
   // echo $this->generateTransactionID();
    $dateOneYear = date('Y-m-d', strtotime('+1 years'));
    $dateTo=date('Y-m-d', strtotime('-1 Day',strtotime($dateOneYear)));
}
   // private function iOSNotification($deviceToken,$notification,$dataID,$category){
  public function iosTest(){
    $deviceToken=$_POST['iosKey'];   
                      $dataID=$_POST['dataID'];
                            $title="Refreshed Car Care";
                            $notification="Your Refreshed service completed: Booking ID RPN613";
                            $category="1";
                            $subCategory="3";
                          //  $data=$this->getJobByID(409);
                           // print_r($data);
                            $data="adsadasdasdsa";
                               // Put your device token here (without spaces):
$deviceToken = $deviceToken;
$deviceToken = preg_replace("/[^a-zA-Z0-9]+/", "", $deviceToken);
// Put your private key's passphrase here:
$passphrase = 'Coders1234';

// Put your alert message here:
$message = $notification;

////////////////////////////////////////////////////////////////////////////////

$ctx = stream_context_create();
stream_context_set_option($ctx, 'ssl', 'local_cert', 'pushCertificateRefreshed.pem');
stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
$fp = stream_socket_client(
  'ssl://gateway.sandbox.push.apple.com:2195', $err,
  $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

if (!$fp)
  exit("Failed to connect: $err $errstr" . PHP_EOL);

//echo 'Connected to APNS' . PHP_EOL;
//$tBadge = $badgeCount;
$tBadge =0;
// Create the payload body
$body['aps'] = array(
  'alert' => $message,
  'badge' => $tBadge,
  'dataID'=>$dataID,
  'category'=>$category,
  'subCategory'=>$subCategory, 
  'sound' => 'default'
  );

// Encode the payload as JSON
$payload = json_encode($body);

// Build the binary notification
$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
$result = fwrite($fp, $msg, strlen($msg));

if (!$result)
  echo 'Message not delivered' . PHP_EOL;
else
  echo 'Message successfully delivered' . PHP_EOL;

// Close the connection to the server
fclose($fp);
}


public function generateInvoiceMembership($custData,$membershipID,$paymentMethod){
              $lastInvoiceData=$this->apimodelcustomer->getLastInvoice(2);
              $taxInvoiceNo=$lastInvoiceData->taxInvoiceNo;
              do{ 
                           
                            $invoiceData=$this->apimodelcustomer->getTaxInvoiceByID($taxInvoiceNo);  

                            if($invoiceData){

                            $data=explode('/', $taxInvoiceNo);
                        
                            $taxInvoiceNo="RMEM/1819/".str_pad(($data[2]+1),4,"0",STR_PAD_LEFT);
                            
                            $flag=0;
                            
                            }else{                  

                            $flag=1; 
                            }

                            
                            
                }while ($flag <= 0); 

                if($flag==1&&!empty($taxInvoiceNo)){
                     if($taxInvoiceNo){
                                
                                //$custData=$this->customermodel->getCustomerByID($custID);         
                                $billingName=$custData->custName;
                               
                              
                                $addressData=$this->apimodelcustomer->getCustomerAddressLatest($custData->custID);

                                if($addressData){
                                    $billingAddress=$this->arrangeAddress($addressData->buildingDetails.','.$addressData->areaDetails.','.$addressData->landmark.','.$addressData->city.','.$addressData->state.','.$addressData->pincode);
                                     $addressID=$addressData->addressID;
                                }else{
                                    $billingAddress='';
                                    $addressID="0";
                                }
                               

                                $invoiceDate=date('Y-m-d');

                                $carData=$this->apimodelcustomer->getCustomerCarsMem($custData->custID);
                                $carStr=' <br> ';
                                $carStrPepo=' ';
                                if($carData){
                                    $i=1;
                                    foreach ($carData as $readCar){

                                        $carStr .= $i.' . '.$readCar->carMake.' '.$readCar->carModel.' '.$readCar->carNoPlate.'<br> ';
                                        if($i==1){
                                         $carStrPepo .= $readCar->carMake.' '.$readCar->carModel.' '.$readCar->carNoPlate;
                                        }else{
                                          $carStrPepo .= ' \n '.$readCar->carMake.' '.$readCar->carModel.' '.$readCar->carNoPlate;   
                                        }
                                        $i++;
                                    }
                                }
                                $membershipData=$this->apimodelcustomer->getCustomerMembershipByID($membershipID);
                                $dateFrom=$membershipData->dateFrom;
                                $dateTo=$membershipData->dateTo;
                                $basicRate=round((1200/1.18),2);
                               

                                $taxData=$this->apimodelcustomer->getTaxDetails(); 
                                $HSN=$taxData->hsnSac;
                                $CGSTAmnt=round($taxData->CGST*$basicRate/100,2);
                                $SGSTAmnt=round($taxData->SGST*$basicRate/100,2);
                                
                                $totalAmt=round($basicRate+$CGSTAmnt+$SGSTAmnt);
                                $totalAmt=sprintf("%0.2f",$totalAmt);

                                //$invoiceNo="12345678899";
                                $dataINV['jobID']=$membershipID;
                                $dataINV['srNo']="1";
                                $dataINV['invoiceDate']=$invoiceDate;
                                $dataINV['taxInvoiceNo']=$taxInvoiceNo;
                                $dataINV['billTo']=$billingName;
                                $dataINV['address']=$billingAddress;
                                $dataINV['description']='Membership '.$carStr;
                                $dataINV['jobRefNo']=date('d M Y',strtotime($dateFrom)).' To <br/> '.date('d M Y',strtotime($dateTo));
                                $dataINV['serviceDate']=date('Y-m-d',strtotime($dateFrom));
                                $dataINV['rate']=$basicRate;
                                $dataINV['amount']=$basicRate;          
                                $dataINV['CGST']=$CGSTAmnt;
                                $dataINV['SGST']=$SGSTAmnt;
                                $dataINV['taxCGST']=$taxData->CGST;
                                $dataINV['taxSGST']=$taxData->SGST;         
                                $dataINV['totalAmount']=$totalAmt;
                                $dataINV['addressID']=$addressID;
                                $dataINV['HSN']=$HSN;
                                $dataINV['invoiceDate']=$invoiceDate;
                                $dataINV['isActive']=1;
                                $dataINV['paymentStatus']=1;
                                $dataINV['amountPaid']=1200;
                $dataINV['paymentMethod']=$paymentMethod;
                                $dataINV['type']='2';
                                $dataINV['created']=date('Y-m-d H:i:s');    
                                $insertData=$this->apimodelcustomer->insertInvoice($dataINV);
                                $invoiceID=$this->db->insert_id();
                                if($insertData){
                                     $encodedStr= $this->base64url_encode($invoiceID);

                                     $urlShort='http://refreshednow.com/cms/invoice/'.$encodedStr;
                                     require_once APPPATH.'third_party/GoogleUrlApi.php';
                                    $googer = new GoogleUrlApi();
                                    // Test: Shorten a URL
                                    $shortenUrl = $googer->shorten($urlShort);     
                                     if(!empty($custData->custEmailID)){    
                                        $email=$custData->custEmailID;
                    
                                         $checkForRenewal=$this->apimodelcustomer->getCustomerMembershipCount($custData->custID);
                                        if($checkForRenewal>1){
                                        $templateName="Membership Plan Renew";
                                        $data=array(
                                            'name' => $custData->custName,
                                            'startDate'=>date('d / m / Y',strtotime($dateFrom)),
                                            'expairyDate'=>date('d / m / Y',strtotime($dateTo)),
                                            'price'=>'1,200',
                                            'mobileNo'=>$custData->custMobNo,
                                            'cars'=>$carStrPepo,
                                            'typeOfService'=>'Refreshed Membership',
                                            'invoiceLink'=>$shortenUrl,
                                            'subject'=>'Your Refreshed Membership has been renewed',           
                                            );   
                                        }else{
                                        $templateName="Membership Purchased";
                                        $data=array(
                                            'name' => $custData->custName,
                                            'startDate'=>date('d / m / Y',strtotime($dateFrom)),
                                            'expairyDate'=>date('d / m / Y',strtotime($dateTo)),
                                            'price'=>'1,200',
                                            'mobileNo'=>$custData->custMobNo,
                                            'cars'=>$carStrPepo,
                                            'typeOfService'=>'Refreshed Membership',
                                            'invoiceLink'=>$shortenUrl,
                                            'subject'=>'Refreshed Member, Welcome to the club!',          
                                          );
                                        }
                                        $response=$this->pepo_campaigns->send_transactional_email($email, $templateName, $data);

                                       
                                    }

                                   return 1;
                                }else{
                                    
                                  return 0;        
                                }
                            }
                }
                 return 0; 
}

/************************Generate Invoice ********************************/
public function generateInvoice($jobID){

    
    if(!empty($jobID)){

        #Get Invoice By Job ID
        $invoiceData=$this->apimodelcustomer->getInvoiceByJobID($jobID);

        if($invoiceData){

           return false;
        }else{

            $jobData=$this->apimodelcustomer->getJobByID($jobID); 
           // print_r($jobData);
            if($jobData){

                if(!empty($jobData->billingName)){
                    $billingName=$jobData->billingName;
                }else{          
                    $billingName=$jobData->custName;
                }

                if(!empty($jobData->type)){
                    $hubData=$this->apimodelcustomer->getHubByID($jobData->hubDestID);
                    $billingAddress=$this->arrangeAddress($hubData->destAddress);
                }else{

                    $addressData=$this->apimodelcustomer->getCustomerAddressByID($jobData->billingAddressID);
                    if($addressData){
                        if(empty($addressData->GSTNo)){
                            $gstNo="NA";
                        }else{
                           $gstNo=$addressData->GSTNo;
                       }
                       $address=$addressData->buildingDetails.','.$addressData->areaDetails.','.$addressData->landmark.',  <br/> '.$addressData->city.' <br/>  '.$addressData->state.', State Code : 27 <br/>  PIN : '.$addressData->pincode .' <br/>  '.$addressData->state.' - 27 <br/>  GSTIN : '.$gstNo;
                       $billingAddress=$this->arrangeAddress($address);

                   }else{
                    
                    if (strpos($jobData->serviceAddress, 'GSTIN') !== false) {
                                                            
                         $addrArr=explode('GSTIN :', $jobData->serviceAddress, 2);
                         if(isset($addrArr[0])){
                          $billingAddress=$addrArr[0];  
                         }else{
                            $billingAddress=$jobData->serviceAddress;  
                         }
                         if(isset($addrArr[1])){
                          //  echo $addrArr[1];
                          $gstNo=(strpos($addrArr[1], 'NA') !== false)?"":$addrArr[1];  
                         }else{
                           $gstNo=""; 
                         }

                         if(!empty($gstNo)){
                            $billingAddress=$billingAddress.'<br/> GSTIN :'.$gstNo;
                         }

                    }else{
                      $billingAddress=$jobData->serviceAddress;  
                    }

                    $billingAddress=$this->arrangeAddress($billingAddress);
                }
            }

            $pkgData=$this->apimodelcustomer->getPackageDetails($jobData->pkgID); 


            if($jobData->pkgAmount==$pkgData->price){

                if($pkgData){
                    $basicRate=$pkgData->basicRate;
                }else{
                    $basicRate=0;
                }
            }else{
                $basicRate=round(($jobData->pkgAmount/1.18),2);
            }

            $taxData=$this->apimodelcustomer->getTaxDetails(); 
            $HSN=$taxData->hsnSac;
            $CGSTAmnt=round($taxData->CGST*$basicRate/100,2);
            $SGSTAmnt=round($taxData->SGST*$basicRate/100,2);
            
            $totalAmt=round($basicRate+$CGSTAmnt+$SGSTAmnt);
            $totalAmt=sprintf("%0.2f",$totalAmt);

            $wordAmount=$this->digitToWord($totalAmt);
            $invoiceDate=date('Y-m-d',strtotime($jobData->date));
	    #discount desc
            $bookingTypeData=$this->apimodelcustomer->getBookingTypeByID($jobData->bookingType);
            if($bookingTypeData&&$jobData->bookingType!=3){
                $discountDesc=$bookingTypeData->bookingType;
            }else{

                if(!empty($jobData->discountAmount)){
                   #checkForPromo
                 $promoCodeData=$this->apimodelcustomer->checkForPromo($jobID);
                 if($promoCodeData){
                    $discountDesc="Coupon Discount (".$promoCodeData->promoCode.")";
                 }else{
                    $discountDesc="";
                 }
                 
                }else{
                    $discountDesc="";  
                }

            }

         //   $invoiceNo="12345678899";
            $data['jobID']=$jobID;
            $data['srNo']="1";
            $data['invoiceDate']=$invoiceDate;
         //   $data['taxInvoiceNo']=$invoiceNo;
            $data['billTo']=$billingName;
            $data['address']=$billingAddress;
            $data['description']=$jobData->pkgName;
            $data['jobRefNo']=$jobData->jobIdentifier;
            $data['serviceDate']=date('Y-m-d',strtotime($jobData->date));
            $data['rate']=$basicRate;
            $data['amount']=$basicRate;         
            $data['CGST']=$CGSTAmnt;
            $data['SGST']=$SGSTAmnt;
            $data['taxCGST']=$taxData->CGST;
            $data['taxSGST']=$taxData->SGST;            
            $data['totalAmount']=sprintf("%0.2f",($totalAmt-$jobData->discountAmount));
            $data['addressID']=$jobData->billingAddressID;
            $data['HSN']=$HSN;
            $data['invoiceDate']=$invoiceDate;
            $data['isActive']=1;
            $data['discount']=$jobData->discountAmount;
            $data['discountDesc']=$discountDesc;
            $data['pureAmount']=$totalAmt;
            $data['carDetails']=$jobData->vehicleMake.' '.$jobData->vehicleModel.'<br>'.$jobData->vehicleNoPlate;
            $custData=$this->apimodelcustomer->getCustByID($jobData->customerID);

            if(!empty($custData->custEmailID)){
              $custEmail=', '.$custData->custEmailID;
            }else{
              $custEmail='';   
            }

       $data['contactDetails']=$custData->custMobNo.$custEmail;
       $data['created']=date('Y-m-d H:i:s'); 
           // print_r($data);
       $invData=$this->apimodelcustomer->getInvoiceByJobID($jobID); 

       if(!$invData){  

        $hubData=$this->apimodelcustomer->getHubByID($jobData->hubDestID);

        if($hubData->specialHub==1||$hubData->type==1){
            $data['type']=1;
            $lastData=$this->apimodelcustomer->getLastInvoice('1'); 
            if($lastData){

                $taxInvoiceNo=$lastData->taxInvoiceNo;
                if(empty($taxInvoiceNo)){
                    $taxInvoiceNo="RPP/1819/00001";  
                }

                $taxInvoiceNo=$this->generateInvoiceNo($taxInvoiceNo,1);               

            }else{
                $taxInvoiceNo="RPP/1819/00001"; 
                $taxInvoiceNo=$this->generateInvoiceNo($taxInvoiceNo,1);  
            }
        }else{
            $data['type']=$jobData->type;
            $lastData=$this->apimodelcustomer->getLastInvoice('0'); 
            if($lastData){

                $taxInvoiceNo=$lastData->taxInvoiceNo;
                if(empty($taxInvoiceNo)){
                    $taxInvoiceNo="RLPL/1819/00001";  
                }

                $taxInvoiceNo=$this->generateInvoiceNo($taxInvoiceNo,0);               

            }else{
                $taxInvoiceNo="RLPL/1819/00001"; 
                $taxInvoiceNo=$this->generateInvoiceNo($taxInvoiceNo,0);  
            }
               
        }
            if(!empty($taxInvoiceNo)){
                $data['taxInvoiceNo']=$taxInvoiceNo;       
                $insertData=$this->apimodelcustomer->insertInvoice($data);
                $invoiceID=$this->db->insert_id();
                if($insertData){
                  /*$encodedStr= $this->base64url_encode($invoiceID);

                 $urlShort='http://refreshednow.com/cmsTest/invoice/'.$encodedStr;
                 require_once APPPATH.'third_party/GoogleUrlApi.php';
                 $googer = new GoogleUrlApi();
                                   // Test: Shorten a URL
                 $shortenUrl = $googer->shorten($urlShort);   */
                return true;   
                }
            }   
                            // $arrResp=array('status'=>1);
    }

}      
}
}
return false; 

}

private function generateInvoiceNo($taxInvoiceNo,$type){
 
    if($type==0){
        do{ 

           $invoiceData=$this->apimodelcustomer->getTaxInvoiceByID($taxInvoiceNo); 

           if($invoiceData){

            $data=explode('/', $taxInvoiceNo);

            $taxInvoiceNo="RLPL/1819/".str_pad(($data[2]+1),5,"0",STR_PAD_LEFT);
            $flag=0;
        }else{
            $flag=1;
        }
        }while ($flag <= 0);
    }else if($type==1){
        do{ 

           $invoiceData=$this->apimodelcustomer->getTaxInvoiceByID($taxInvoiceNo); 

           if($invoiceData){

            $data=explode('/', $taxInvoiceNo);

            $taxInvoiceNo="RPP/1819/".str_pad(($data[2]+1),5,"0",STR_PAD_LEFT);
            $flag=0;
        }else{
            $flag=1;
        }
        }while ($flag <= 0);

    }else{
        $taxInvoiceNo="";
    }

return $taxInvoiceNo;                

}

private function iOSNotification($deviceToken,$notification,$dataID,$category,$subCategory,$badgeCount){
 /* public function iosTest(){
    $deviceToken=$_POST['iosKey'];   
                            $dataID="0";
                            $title="Refreshed Car Care";
                            $notification="Testing Noti";
                            $category="0";
                            $subCategory="0";
                          //  $data=$this->getJobByID(409);
                           // print_r($data);
                            $data="adsadasdasdsa";*/
                               // Put your device token here (without spaces):
$deviceToken = $deviceToken;
$deviceToken = preg_replace("/[^a-zA-Z0-9]+/", "", $deviceToken);
// Put your private key's passphrase here:
$passphrase = 'Coders1234';

// Put your alert message here:
$message = $notification;

////////////////////////////////////////////////////////////////////////////////

$ctx = stream_context_create();
stream_context_set_option($ctx, 'ssl', 'local_cert', 'pushCertificateRefreshed.pem');
stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
$fp = stream_socket_client(
  'ssl://gateway.push.apple.com:2195', $err,
  $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

if (!$fp)
  exit("Failed to connect: $err $errstr" . PHP_EOL);

//echo 'Connected to APNS' . PHP_EOL;
//$tBadge = $badgeCount;
$tBadge =$badgeCount;
// Create the payload body
$body['aps'] = array(
  'alert' => $message,
  'badge' => $tBadge,
  'dataID'=>$dataID,
  'category'=>$category,
  'subCategory'=>$subCategory, 
  'sound' => 'default'
  );

// Encode the payload as JSON
$payload = json_encode($body);

// Build the binary notification
$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
$result = fwrite($fp, $msg, strlen($msg));

/*if (!$result)
  echo 'Message not delivered' . PHP_EOL;
else
  echo 'Message successfully delivered' . PHP_EOL;*/

// Close the connection to the server
fclose($fp);
}

private function androidNotification($deviceToken,$notification,$dataID,$category,$subCategory){
                           
                            $title="Refreshed Car Care";
                            $body="Testing Noti";
                          
                            $json_data = array(
                                "to" =>$deviceToken,
      /* "notification" =>array("body" => $notification,
          "title" => $title,
          "category"=>$category,
          "subCategory"=>$subCategory,
          "icon" => "ic_launcher"
          ),*/
          "data" => array("dataID" => $dataID,
              "body" =>$notification,
              "title" =>$title,
              "category"=>$category,
              "subCategory"=>$subCategory,
              "icon" => "ic_launcher"),
          );


                            $data = json_encode($json_data);
//FCM API end-point
                            $url = 'https://fcm.googleapis.com/fcm/send';
//api_key in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key

                            $server_key ='AIzaSyC2oEZ0l4ddRlLzDG-0BZuKFe1cERkpujY';
//header with content_type api key
                            $headers = array(
                                'Content-Type:application/json',
                                'Authorization:key='.$server_key
                                );
//CURL request to route notification to FCM connection server (provided by Google)
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_POST, true);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                            $result = curl_exec($ch);
                            /*if ($result === FALSE) {
                                die('Oops! FCM Send Error: ' . curl_error($ch));
                            }else{
                                print_r($result);

                            }*/
                            curl_close($ch);
    }


public function refundTransaction($transactionDetails)
{
/*$transactionDetails=(object)array();
$transactionDetails->transactionID="20180516111212800110168459825544739";
$transactionDetails->refTransactionID="78594395669121192923";
$transactionDetails->transactionAmount="1.00";*/
if(!empty($transactionDetails->transactionID)&&!empty($transactionDetails->refTransactionID)&&!empty($transactionDetails->transactionAmount)){    
header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Expires: 0");

// following files need to be included
require_once(APPPATH . "/third_party/paytm/lib/config_paytm.php");
require_once(APPPATH . "/third_party/paytm/lib/encdec_paytm_refund.php");

$REFID = $this->generateRefundID();
//REFUND API
$data = array(
"MID"=>PAYTM_MERCHANT_MID,
"TXNID"=>$transactionDetails->transactionID,
"ORDERID"=>$transactionDetails->refTransactionID,
"REFUNDAMOUNT"=>$transactionDetails->transactionAmount,
"TXNTYPE"=>"REFUND",
"REFID"=>$REFID
);
//print_r($data);
$key = PAYTM_MERCHANT_KEY;
$hash = getChecksumFromArray($data, $key);
//echo $hash;
//die;
//UDEwsD8YosY3SEavdZBpnv4NjaEY3uvEfYBoXcn35i2x8FAT5PnXt4e0RXAuAR4ev3nB6zLDi2VD5jF6CaBBr+gY7wMU33XWvVK2TbzZTlI=
$apiURL = PAYTM_REFUND_URL;
$data['CHECKSUM'] = $hash;
$result = callAPI($apiURL, $data);
/*echo'<br>';
print "";
print_r($result);
print "";*/
//die;
return $result;
}else{
return false;
}
}
}
